var fn_calculate = function (search , value){
    if (typeof ($("div.portlet-heading:contains('"+search+"')").html()) !== "undefined") {
//            var regExp = /\{{([^)]+)\}}/;
//            var txt = $("div.portlet-heading:contains('"+search+"')").text();
//            var matches = regExp.exec(txt);
            var mmm = value;
            var values = search.split(/<=|>=|==|<>|!=|<|>/);
            var operator = search.match(/<=|>=|==|<>|!=|<|>/);
       
            if (    (operator == "<=" && mmm <= values[1]) ||
                    (operator == ">=" && mmm >= values[1]) ||
                    (operator == "==" && mmm == values[1]) || 
                    ((operator == "!=" || operator == "<>") && mmm != values[1] ) || 
                    (operator == "<" && mmm < values[1]) ||
                    (operator == ">" && mmm == values[1]) 
                )
            {
                $("div.portlet-heading:contains('" + search + "')").html($("div.portlet-heading:contains('" + search + "')").text().replace( search , ''));
            } else {
                $("div.portlet-heading:contains('" + search + "'):first").closest('.question_area').remove();
            }
    }
}

var fn_replace = function(search , value){
    if (typeof ($("div.portlet-heading:contains('"+search+"')").html()) !== "undefined") {
        $("div.portlet-heading:contains('"+search+"')").each(function () {
            $("div.portlet-heading:contains('"+search+"'):first").html($("div.portlet-heading:contains('"+search+"')").html().replace(search, ' <b><u> ' + value + '</u></b> '));
        });
    }
}

var click_event = function () {
    var groupID = $('#groupID').val();
    var cus_id = $('#cus_id').val();
    var str = '';

    if (groupID == 1) { //groupID 1 = admin
        $('#btn_back, #btn_finish').addClass('hide');
        $('#btn_next span').text("save");
        $('i.fa-arrow-circle-right').addClass("fa-floppy-o");
        $('i.fa-arrow-circle-right').removeClass("fa-arrow-circle-right");
    } else {
        $('.question_area, .panel').addClass('hide');
        $('.question_area:first').removeClass('hide');
        $('.hr-totop').addClass('hide');
    }

    var dealer_name = attr($('input[type=hidden][name=dealer_name]').val(), "");
    var dealer_address = attr($('input[type=hidden][name=dealer_address]').val(), "");
    var salesperson = attr($('input[type=hidden][name=salesperson]').val(), "");
    var model_name = attr($('input[type=hidden][name=model_name]').val(), "");
    var delivery_date = attr($('input[type=hidden][name=delivery_date]').val(), "");
    var service_date = attr($('input[type=hidden][name=service_date]').val(), "");
    var mileage = attr($('input[type=hidden][name=mileage]').val(), "");
    
    fn_calculate('{{mileage<40000}}' , mileage);
    fn_calculate('{{mileage>=40000}}' , mileage);
    
    fn_replace('[[dealer_name]]' , dealer_name);
    fn_replace('[[dealer_address]]' , dealer_address);
    fn_replace('[[salesperson]]' , salesperson);
    fn_replace('[[model_name]]' , model_name);
    fn_replace('[[delivery_date]]' , delivery_date);
    fn_replace('[[service_date]]' , service_date);

    $('#question_answer_area').find('input[type=radio]').on('click', function (e) {
        var jump_next = attr($(this).attr('jump'), 0);
        var jump_prev = $(this).closest('.question_area').attr('id');
        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);// .attr('jump_prev', jump_prev);
        //
        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
        //

        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
        $('input[type="hidden"][name=' + jump_prev + ']').remove();
        var h = '<input type="hidden" name="' + jump_prev + '" value="' + $(this).val() + '">';
        $(this).after(h);
    }).on('dblclick', function (e) {
        $(this).closest('.question_area').find('button.next').trigger('click');
    }).attr( 'checked', function (e) {
    	$('#question_answer_area').find('input[type=radio]:checked').trigger('click');
    });

    $('#question_answer_area').find('div.br-widget').on('click', function (e) {
        var jump_prev = $(this).closest('.question_area').attr('id');
        var jump_next = attr($(this).closest('.br-wrapper').find('select.rating').attr('jump'), '0');
        
        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);

        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
        $('input[type="hidden"][name=' + jump_prev + ']').remove();
        var h = '<input type="hidden" name="' + jump_prev + '" value="' + $(this).find('div.br-current-rating').text() + '">';
        $(this).after(h);
    }).on('dblclick', function (e) {
        $(this).closest('.question_area').find('button.next').trigger('click');
    })

    $('#question_answer_area').find('input[type=checkbox]').on('click', function (e) {
        var jump_prev = $(this).closest('.question_area').attr('id');
        var id = $(this).attr('id');
        var controls = $('input[type=checkbox][name=' + id + ']');
        var jump_next = '';
        $.each(controls, function (k, v) {
            if ($(this).prop('checked') == true) {
                jump_next += ',' + $(this).attr('jump');
            }
        });
        jump_next = jump_next.substr(1);
        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);
        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
        $('input[type="hidden"][name=' + jump_prev + ']').remove();
        var h = '<input type="hidden" name="' + jump_prev + '" value="' + jump_next + '">';
        $(this).after(h);
    });
    
    $('.question_area').find('input[type=checkbox]:checked').each(function(e) {
    	
    	var jump_prev = $(this).closest('.question_area').attr('id');
        var id = $(this).attr('id');
        var controls = $('input[type=checkbox][name=' + id + ']');
        var jump_next = '';
        $.each(controls, function (k, v) {
            if ($(this).prop('checked') == true) {
                jump_next += ',' + $(this).attr('jump');
            }
        });
        jump_next = jump_next.substr(1);
        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);
        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
        $('input[type="hidden"][name=' + jump_prev + ']').remove();
        var h = '<input type="hidden" name="' + jump_prev + '" value="' + jump_next + '">';
        $(this).after(h);
        
    });

    $('#question_answer_area').find('textarea').on('keypress', function (e) {
        var jump_next = attr($(this).attr('jump'), 0);
        var jump_prev = $(this).closest('.question_area').attr('id');
        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);// .attr('jump_prev', jump_prev);
        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
    }).on('click', function (e) {
    	str = '';
    	str = $('.question_area').find('textarea').val();
        if( str.length > 0 ){
        	var jump_next = attr($(this).attr('jump'), 0);
            var jump_prev = $(this).closest('.question_area').attr('id');
            $(this).closest('.question_area').find('button.next').attr('jump', jump_next);// .attr('jump_prev', jump_prev);
            $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
            $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
        }
    });
    
    $('#question_answer_area').find('input[type=text]').on('keyup', function (e) {
        var jump_next = attr($(this).attr('jump'), 0);
        var jump_prev = $(this).closest('.question_area').attr('id');
        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);// .attr('jump_prev', jump_prev);
        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
    }).on('keypress', function (e) {
        var jump_next = attr($(this).attr('jump'), 0);
        var jump_prev = $(this).closest('.question_area').attr('id');
        var code = e.keyCode || e.which;
        if (code == 13) { // Enter keycode
            // Do something
            if ($(this).val().length > 0) {
                var jump_next = attr($(this).attr('jump'), 0);
                var jump_prev = $(this).closest('.question_area').attr('id');
                $(this).closest('.question_area').find('button.next').attr('jump', jump_next);// .attr('jump_prev',// jump_prev);
                $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
                $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
            }
            $(this).closest('.question_area').find('button.next').trigger('click');
        }
    }).on('click', function (e) {
    	str = '';
    	str = $('.question_area').find('input:text').val();
        if( str.length > 0 ){
	        var jump_next = attr($(this).attr('jump'), 0);
	        var jump_prev = $(this).closest('.question_area').attr('id');
	        $(this).closest('.question_area').find('button.next').attr('jump', jump_next);// .attr('jump_prev', jump_prev);
	        $('#question_answer_area').find('#' + jump_next).find('button.back').attr('jump', jump_prev);
	        $('#question_answer_area').find('#99999').find('button.back').attr('jump', jump_prev);
        }
    });

    $('.finish').on('click', function (e) {
        e.preventDefault();
        handleSelectBox('#question_answer_area');
        var container = $(this).closest('.question_area');
        var data = get_data_serialize(container);
        var id = $(container).find('input[type=hidden][name=question_id]').val();
        if (pass_validation(container)) {
            var jump_next = '99999';
            data += '&jump=' + jump_next;
            data += '&project_id=' + $('input[type=hidden][name=project_id]').val();
            data += '&series_id=' + $('input[type=hidden][name=series_id]').val();
            data += '&question_type_id=' + $('input[type=hidden][name=question_type_id]').val();
            data += '&phase_id=' + $('input[type=hidden][name=phase_id]').val();
            data += '&question_id=' + $(container).find('input[type=hidden][name=question_id]').val();
            data += '&mode=insert';
            data += '&cus_id=' + cus_id;
            data += '&value_net=' + $('input[type=hidden][name=' + id + ']').val();
            $.ajax({
                type: "POST",
                url: get_base_url() + get_uri() + '/save_data',
                cache: false,
                async: false,
                data: data,
                beforeSend: function () {},
                success: function (html) {
                    $(container).addClass('hide');
                    $('#' + jump_next).removeClass('hide');
                    $('#' + jump_next).find('input[type=textbox]:first').focus();
                }
            });
        }
    })

    $('.back').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('.question_area');
        var data = get_data_serialize(container);
        var id = $(container).find('input[type=hidden][name=question_id]').val();
        if (pass_validation(container)) {
            var jump = attr($(this).attr('jump'), '0');
            if (jump == '0') {
            } else {
                var jump_next = jump;
                if (jump.indexOf(',') !== false) {
                    jump = jump.split(',');
                    $.each(jump, function (k, v) {
                        if (v !== '99999' && $('#' + v).length > 0) {
                            jump_next = v;
                        }
                    })
                }
                data += '&jump=' + jump_next;
                data += '&project_id=' + $('input[type=hidden][name=project_id]').val();
                data += '&series_id=' + $('input[type=hidden][name=series_id]').val();
                data += '&question_type_id=' + $('input[type=hidden][name=question_type_id]').val();
                data += '&phase_id=' + $('input[type=hidden][name=phase_id]').val();
                data += '&question_id=' + $(container).find('input[type=hidden][name=question_id]').val();
                data += '&mode=delete';
                data += '&cus_id=' + cus_id;
                data += '&value_net=' + $('input[type=hidden][name=' + id + ']').val();
                $(this).attr('jump', jump_next);
                $.ajax({
                    type: "POST",
                    url: get_base_url() + get_uri() + '/save_data',
                    cache: false,
                    async: false,
                    data: data,
                    beforeSend: function () {},
                    success: function (html) {
                        $(container).addClass('hide');
                        $('#' + jump_next).fadeIn('slow').removeClass('hide');
                        $('#' + jump_next).find('input[type=textbox]:first').focus();
                    }
                });
            }
        }
    })

    $('.next').on('click', function (e) {
        e.preventDefault();
        var container = $(this).closest('.question_area');
        var data = get_data_serialize(container);
        var id = $(container).find('input[type=hidden][name=question_id]').val();
        if (pass_validation(container)) {
            var jump = attr($(this).attr('jump'), '0');
            if (jump == '0') {
                var error = '<span class=invalid>' + locale['please_select_values'] + '</span>';
                $(this).before(error);
            } else {
                var jump_next = jump;
                if (jump.indexOf(',') > 0) {
                    jump = jump.split(',');
                    $.each(jump, function (k, v) {
                        if (v !== '99999' && $('#' + v).length > 0)
                            jump_next = v;
                    });
                }
                $(this).attr('jump', jump_next);
                data += '&jump=' + jump_next;
                data += '&project_id=' + $('input[type=hidden][name=project_id]').val();
                data += '&series_id=' + $('input[type=hidden][name=series_id]').val();
                data += '&question_type_id=' + $('input[type=hidden][name=question_type_id]').val();
                data += '&phase_id=' + $('input[type=hidden][name=phase_id]').val();
                data += '&question_id=' + $(container).find('input[type=hidden][name=question_id]').val();
                data += '&mode=insert';
                data += '&cus_id=' + cus_id;
                data += '&value_net=' + $('input[type=hidden][name=' + id + ']').val();
                $.ajax({
                    type: "POST",
                    url: get_base_url() + get_uri() + '/save_data',
                    cache: false,
                    async: false,
                    data: data,
                    beforeSend: function () {},
                    success: function (html) {
                        if (groupID == 1) { //groupID 1 = admin
                            end_saving();
                        } else {
                            $(container).addClass('hide');
                            $('#' + jump_next).fadeIn('slow').removeClass('hide');
                            $('#' + jump_next).find('input[type=textbox]:first').focus();
                        }
                    }
                });
            }
        }
    });

    $('button[id=btn_save_ending]').on('click', function (e) {
        e.preventDefault();
        var status_id = attr($('input[type=hidden][name=status_id]').val(), '');
        var sub_status_id = attr($('input[type=hidden][name=sub_status_id]').val(), '');
        var question_type_id = attr($('input[type=hidden][name=question_type_id]').val(), '');
        var start_call_time = attr($('input[type=hidden][name=start_call_time]').val(), '');

        if (status_id.length > 0 && sub_status_id.length > 0 && question_type_id.length > 0) {
            var data = 'cusid=' + cus_id
                    + '&status_id=' + status_id
                    + '&sub_status_id=' + sub_status_id
                    + '&question_type_id=' + question_type_id
                    + '&start_call_time=' + start_call_time;
            $.ajax({
                type: "POST",
                url: get_base_url() + get_uri() + '/save_ending',
                cache: false,
                async: false,
                data: data,
                beforeSend: function () {},
                success: function (html) {
                    end_saving();
                }
            });
        }
    });

    $('select[name=status_id]').on('change', function (e) {
        var status_id = $(this).val();
        var data = 'status_id=' + status_id;
        $.ajax({
            type: "POST",
            url: get_base_url() + get_uri() + '/get_sub_status',
            cache: false,
            async: false,
            data: data,
            beforeSend: function () {},
            success: function (data) {
                data = $.parseJSON(data);
                var selected;
                var sub_status_id = $('select[name=sub_status_id]').val();
                $('select[name=sub_status_id]').empty();
                $.each(data, function (k, v) {
                    if (v['id'] == sub_status_id) {
                        selected = 'selected';
                    } else {
                        selected = '';
                    }
                    $('select[name=sub_status_id]').append('<option value=' + v['id'] + ' ' + selected + '>' + v['name'] + '</option>');
                });
            }
        });
    });
}