<style>
    </style>
<div class='mainInfo'>
    

        <div class="pageTitle">Login</div>
         
        <div class="pageTitleBorder"></div>
        <p>Please login with your email/username and password below.</p>

        <div id="infoMessage"><? isset($message) ? print $message : ''; ?></div>

        <?php echo form_open("auth/login"); ?>

        <p>
                <label for="identity" id="label_identity">Email/Username:</label>
				<br>
                <?php echo form_input($identity); ?>
        </p>

        <p>
                <label for="password" id="label_Password">Password:</label>
				<br>
                <?php echo form_input($password); ?>
        </p>

        <p style="float: left;">
                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
				<label for="remember">Remember Me:</label>
                
        </p>
        <div class="col-xs-12">

        <p style="float:right;"><?php echo form_submit('submit', 'Login'); ?></p>
        </div>

        <?php echo form_close(); ?>

        <p style=" width: 100%; margin-top: 65px;"><a href="forgot_password" style="color: #014c8c;">Forgot your password?</a></p>

</div>
