<?

$tab_1 = array();
array_push($tab_1, $this->mcl->msb('dataTypeID', $t, array('class' => 'quarter-width required no-cookie', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->dtp('dateStart', $t, array('class' => 'quarter-width required', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->bt('view_report', 'submit', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->div('output', '', 'full-width'));
array_push($tab_1, $this->mcl->hd('hid_rep_template', $rep_template));
$o = $this->mcl->input_page(array($tab_1), $t);
echo $o;
?>
<script language="javascript">
$('#btn_view_report').off('click').on('click', function (e) {
    e.preventDefault();
    var template = attr($(cc).find('input[type=hidden][name=hid_rep_template]').val());

    if($('input[name=dateStart]').val()=='')
    {
        $('input[name=dateStart]').focus();
        return false;
    }

    var area = $('.search-box');
    handleValidationRules(area);
    if(pass_validation(area))
    {
        setTimeout(function () {
            var dateStart = attr($('input[name=dateStart]').val(), '');
            // var dateEnd = attr($('input[name=dateEnd]').val(), '');
            var dataTypeID = attr($('input[name=dataTypeID]').val(), '');

            var h = '<form name="frm_export" id="frm_export" action="' + get_base_url() + '<?=$this->router->class;?>' + '/genInternalReport" autocomplete="off" method="post" target="_blank">';
            h += '<input type="hidden" name="rep_name" id="rep_name" value="report_internal">';
            // h += '<input type="hidden" name="rep_template" id="rep_template" value="report_internal.xlsx">';
            h += '<input type="hidden" name="rep_template" id="rep_template" value="'+template+'">';
            h += '<input type=hidden name=dateStart id=dateStart value="' + dateStart + '">';
            // h += '<input type=hidden name=dateEnd id=dateEnd value="' + dateEnd + '">';
            h += '<input type=hidden name=dataTypeID id=dataTypeID value="' + dataTypeID + '">';
            h += '</form>';

            $(cc).append(h);
            $('form[id=frm_export]').submit();
            $('form[id=frm_export]').remove();
        }, 1000);
    }
});
</script>
