<?
$tab_1 = array();
//array_push($tab_1, $this->mcl->sb('sourceID', $t, array('class' => 'no-cookie half-width','rules' => 'trim|required')));
array_push($tab_1, $this->mcl->dtp('dateStart', $t, array('class' => 'quarter-width', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->dtp('dateEnd', $t, array('class' => 'quarter-width', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->bt('export_data_avg', 'avg_all', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->bt('export_avg_region', 'avg_region', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->bt('export_avg_agency', 'avg_agency', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->bt('export_avg_agent', 'avg_agent', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->div('output', '', 'full-width'));
$o = $this->mcl->input_page(array($tab_1), $t);
echo $o;
?>

<script language="javascript">
$('#btn_export_data_avg').on('click', function (e) {
    e.preventDefault();
    setTimeout(function () {
            var dateStart = attr($('input[name=dateStart]').val(), '');
            var dateEnd = attr($('input[name=dateEnd]').val(), '');
	        var h = '<form name="frm_export" id="frm_export" action="' + get_base_url() + '.ReportAVG/exportAvgScoreAll" autocomplete="off" method="post" target="_blank">';
            h += '<input type="hidden" name="rep_name" id="rep_name" value="reportrawdata">';
            h += '<input type="hidden" name="rep_template" id="rep_template" value="reportrawdata.xlsx">';
            h += '<input type=hidden name=dateStart id=dateStart value="' + dateStart + '">';
            h += '<input type=hidden name=dateEnd id=dateEnd value="' + dateEnd + '">';
            h += '</form>';

            $(cc).append(h);
            $('form[id=frm_export]').submit();
            $('form[id=frm_export]').remove();
    }, 1000);
});
$('#btn_export_avg_region').on('click', function (e) {
    e.preventDefault();
    setTimeout(function () {
            var dateStart = attr($('input[name=dateStart]').val(), '');
            var dateEnd = attr($('input[name=dateEnd]').val(), '');
            var report = 'region'
	        var h = '<form name="frm_export" id="frm_export" action="' + get_base_url() + '.ReportAVG/exportAvgScoreBy" autocomplete="off" method="post" target="_blank">';
            h += '<input type="hidden" name="rep_name" id="rep_name" value="reportrawdata">';
            h += '<input type="hidden" name="rep_template" id="rep_template" value="reportrawdata.xlsx">';
            h += '<input type=hidden name=dateStart id=dateStart value="' + dateStart + '">';
            h += '<input type=hidden name=dateEnd id=dateEnd value="' + dateEnd + '">';
            h += '<input type=hidden name=report id=report value="' + report + '">';
            h += '</form>';

            $(cc).append(h);
            $('form[id=frm_export]').submit();
            $('form[id=frm_export]').remove();
    }, 1000);
});
$('#btn_export_avg_agency').on('click', function (e) {
    e.preventDefault();
    setTimeout(function () {
            var dateStart = attr($('input[name=dateStart]').val(), '');
            var dateEnd = attr($('input[name=dateEnd]').val(), '');
            var report = 'agency'
	        var h = '<form name="frm_export" id="frm_export" action="' + get_base_url() + '.ReportAVG/exportAvgScoreBy" autocomplete="off" method="post" target="_blank">';
            h += '<input type="hidden" name="rep_name" id="rep_name" value="reportrawdata">';
            h += '<input type="hidden" name="rep_template" id="rep_template" value="reportrawdata.xlsx">';
            h += '<input type=hidden name=dateStart id=dateStart value="' + dateStart + '">';
            h += '<input type=hidden name=dateEnd id=dateEnd value="' + dateEnd + '">';
            h += '<input type=hidden name=report id=report value="' + report + '">';
            h += '</form>';

            $(cc).append(h);
            $('form[id=frm_export]').submit();
            $('form[id=frm_export]').remove();
    }, 1000);
});
$('#btn_export_avg_agent').on('click', function (e) {
    e.preventDefault();
    setTimeout(function () {
            var dateStart = attr($('input[name=dateStart]').val(), '');
            var dateEnd = attr($('input[name=dateEnd]').val(), '');
            var report = 'agent'
	        var h = '<form name="frm_export" id="frm_export" action="' + get_base_url() + '.ReportAVG/exportAvgScoreBy" autocomplete="off" method="post" target="_blank">';
            h += '<input type="hidden" name="rep_name" id="rep_name" value="reportrawdata">';
            h += '<input type="hidden" name="rep_template" id="rep_template" value="reportrawdata.xlsx">';
            h += '<input type=hidden name=dateStart id=dateStart value="' + dateStart + '">';
            h += '<input type=hidden name=dateEnd id=dateEnd value="' + dateEnd + '">';
            h += '<input type=hidden name=report id=report value="' + report + '">';
            h += '</form>';

            $(cc).append(h);
            $('form[id=frm_export]').submit();
            $('form[id=frm_export]').remove();
    }, 1000);
});
</script>