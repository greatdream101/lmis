<?
$chart = '
<div class="row">

    <div class="col-lg-6">
        <div class="card-box">
            <h4 class="text-dark  header-title m-t-0 m-b-30">Answer Report</h4>

            <div class="widget-chart text-center">
                <div id="sparkline2">
                    <canvas width="205" height="165" style="display: inline-block; width: 205px; height: 165px; vertical-align: top;"></canvas>
                </div>
                <ul class="list-inline m-t-15">
                    <li>
                        <h5 class="text-muted m-t-20">5 point</h5>
                        <h4 class="m-b-0">50</h4>
                    </li>
                    <li>
                        <h5 class="text-muted m-t-20">4 point</h5>
                        <h4 class="m-b-0">70</h4>
                    </li>
                    <li>
                        <h5 class="text-muted m-t-20">3 point</h5>
                        <h4 class="m-b-0">20</h4>
                    </li>
                    <li>
                        <h5 class="text-muted m-t-20">2 point</h5>
                        <h4 class="m-b-0">30</h4>
                    </li>
                    <li>
                        <h5 class="text-muted m-t-20">1 point</h5>
                        <h4 class="m-b-0">10</h4>
                    </li>
                </ul>
            </div>
        </div>

    </div>

    <div class="col-lg-6">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0 m-b-30">Overview Survey Status Report</h4>

            <div class="widget-chart text-center">
                <div id="sparkline3">
                    <canvas width="165" height="165" style="display: inline-block; width: 165px; height: 165px; vertical-align: top;"></canvas>
                </div>
                <ul class="list-inline m-t-15">
                    <li>
                        <h5 class="text-muted m-t-20">Over All</h5>
                        <h4 class="m-b-0">1000</h4>
                    </li>
                    <li>
                        <h5 class="text-muted m-t-20">Success</h5>
                        <h4 class="m-b-0">700</h4>
                    </li>
                    <li>
                        <h5 class="text-muted m-t-20">Waiting</h5>
                        <h4 class="m-b-0">300</h4>
                    </li>
                </ul>
            </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <h4 class="text-dark  header-title m-t-0">Customer Survey List</h4>
            <p class="text-muted m-b-25 font-13">
                Customer data survey list.
            </p>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Data Source</th>
                            <th>Send SMS Date</th>
                            <th>Survey Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>คุณ เอ บี</td>
                            <td>A</td>
                            <td>01/01/2016</td>
                            <td>26/04/2016</td>
                            <td><span class="label label-success">Success</span></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>คุณ ซี ดี</td>
                            <td>A</td>
                            <td>01/01/2016</td>
                            <td>26/04/2016</td>
                            <td><span class="label label-success">Success</span></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>คุณ อี เอฟ</td>
                            <td>B</td>
                            <td>01/05/2016</td>
                            <td>10/05/2016</td>
                            <td><span class="label label-success">Success</span></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>คุณ จี เอช</td>
                            <td>C</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-pink">waitng</span></td>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>คุณ ไอ เจ</td>
                            <td>C</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-pink">waitng</span></td>
                        </tr>

                        <tr>
                            <td>6</td>
                            <td>คุณ เค เอล</td>
                            <td>C</td>
                            <td>01/01/2016</td>
                            <td>31/05/2016</td>
                            <td><span class="label label-pink">waitng</span></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
';
$tab_1 = array();
array_push($tab_1, $this->mcl->div('chart',$chart,'hide'));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->dtp('dateStart', $t, array('class' => 'quarter-width', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->dtp('dateEnd', $t, array('class' => 'quarter-width', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->bt('view_sms', 'submit', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->div('output', '', 'full-width'));

$o = $this->mcl->input_page(array($tab_1), $t);
echo $o;
?>


<script src="../assets/plugins/jquery-circliful/js/jquery.circliful.min.js"></script>
<script src="../assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
<script language="javascript">
$(document).ready(function () { 
    $("#btn_view_sms").off('click').on('click', function (e) {
        e.preventDefault();
        $('div#chart').removeClass('hide');
        DrawSparkline();
    });
var DrawSparkline = function() {

    $('#sparkline2').sparkline([50,70,20,30,10], {
        type: 'bar',
        height: '165',
        barWidth: '50',
        barSpacing: '3',
        barColor: '#3bafda'
    });

    $('#sparkline3').sparkline([30, 70], {
        type: 'pie',
        width: '165',
        height: '165',
        sliceColors: ['#333333', '#3bafda']
    });


};

var resizeChart;
$(window).resize(function(e) {
    clearTimeout(resizeChart);
    resizeChart = setTimeout(function() {
        DrawSparkline();
    }, 300);
    });
});

</script>
