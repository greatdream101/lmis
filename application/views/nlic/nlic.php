<meta name="viewport" content="width=device-width, initial-scale=1">

<? $dirpart = dirname($_SERVER['SCRIPT_NAME']);?>
<link rel="stylesheet" href="../assets/css/nlic.css" type="text/css"  />
<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css"  />
<link rel="stylesheet" href="../assets/css/jquery-ui.css" type="text/css"  />
<link rel="stylesheet" href="../assets/css/responsive.css" type="text/css"  />

<script src="./../assets/js/jquery-3.2.1.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script src="../assets/js/nlic.js"></script>

<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<body>
        <div class="bg1600"></div>
        <div class="head-image-cover-left"> </div>
        <div class="head-image-cover-right"></div>
        <!-- <div class="bg-cover"></div> -->

        <div class="section topbar">
            <div class="row topbar-left">
                <div class="header-nlic col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img style="width: 120px;" src="<?= base_url() ?>assets/img/icon/mol_logo.png" >
                    <span id="nlic_mol_name"><?=$this->mcl->gl("nlic_mol_name")?></span>
                    <span id="button_mol"><a href="http://localhost/nlic/auth"><i class="fa fa-bars"></i></a></span>
                </div>
            </div>
        </div>

        <div class="section menu">
            <div class="row">
                <div class="menu-nlic">
                    <div class="col-lg-12">
                        <?php
                            // vd::d($menu);
                                $i = 1;
                                foreach($menu as $key=>$value){

                                    if($i == 6){
                                        print "<div class='menu-item col-lg-2 col-md-2 col-sm-12 col-xs-12' style='background: transparent;'>";
                                            print "<div class='menu-border' style='background: transparent; border: none;'>";
                                            // print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                            // print "<div>".$value['name']."</div>";
                                            print "</div>";
                                        print "</div>";
                                    }

                                    print "<div class='menu-item col-lg-2 col-md-2 col-sm-12 col-xs-12'>";
                                        print "<a href=".$value['link']." target='_blank'>";
                                        print "<div class='menu-border'>";
                                        print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                        print "<div>".$value['name']."</div>";
                                        print "</div>";
                                        print "</a>";
                                    print "</div>";

                                    $i++;

                                }
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="section chartJS">
            <div class="row">
                <div class="chartJS-nlic">
                    <div class="col-lg-12">

                        <div class='chartJS-div col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                            <a href="javascript:void(0)">
                                <div class='chartJS-border'>
                                    <div id="chart-header"><h2><u>ข้อมูลสถิติแรงงาน</u></h2></div>
                                    <div id="chart-subheader">จำนวนผู้ประกันตนในระบบประกันสังคม จำแนกตามมาตรา</div>
                                    <div><iframe style="border-radius: 10px;" height="500px" width="100%" src="http://20.64.8.197/chart?layout=none"></iframe></div>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="section eservice">
            <div class="row">
                <div class="eservice-nlic">
                    <div class="col-lg-12">
                        <div class='eservice-div col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                <div class='eservice-border'>
                                    <div id="eservice-header"><h2><u>ระบบสารสนเทศบริการประชาชน (e-Services)</u></h2></div>
                                        <?php
                                            // vd::d($eservice);
                                            foreach($eservice as $key=>$value){
                                                print "<div class='eservice-item col-lg-4 col-md-4 col-sm-12 col-xs-12'>";
                                                    print "<a href=".$value['link']." target='_blank'>";
                                                    print "<div class='service-border'>";
                                                    // print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                                    print "<div>".$value['name']."</div>";
                                                    print "</div>";
                                                    print "</a>";
                                                print "</div>";
                                            }
                                        ?>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="section document">
            <div class="row">
                <div class="document-nlic">
                    <div class="col-lg-12">
                        <div class='document-div col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                <div class='document-border'>
                                    <div id="document-header"><h2><u>เอกสารเผยแพร่</u></h2></div>
                                        <?
                                            foreach($document as $key=>$value){
                                                print "<div class='document-item col-lg-4 col-md-4 col-sm-12 col-xs-12'>";
                                                    print "<a href=".$value['link']." target='_blank'>";
                                                    print "<div class='document-border'>";
                                                    // print "<img style='width: 60px; margin-bottom: 10px;' src=../assets/img/icon/".$value['iconpart']." >";
                                                    print "<div>".$value['name']."</div>";
                                                    print "</div>";
                                                    print "</a>";
                                                print "</div>";
                                            }
                                        ?>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?
            // $this->db->SELECT("COUNT(*) AS total_present")->FROM("t_present_counter");
            // $total_present = $this->db->get()->result_array();

            // $this->db->SELECT("COUNT(*) AS total_mount")->FROM("t_summary_counter");
            // $this->db->WHERE("DATE_FORMAT(DATE, %Y-%m) = ".date('Y-m')." )");
            // $total_mount = $this->db->get()->result_array();

            // $this->db->SELECT("COUNT(*) AS total_mount")->FROM("t_summary_counter");
            // $this->db->WHERE("DATE_FORMAT(DATE, %Y) = ".date('Y')." )");
            // $total_year = $this->db->get()->result_array();

            // vd::d($this->db->last_query());
        ?>

        <div class="section footer">
            <div class="row">
                <div class="footer-nlic">

                    <div class="col-lg-12 footer-img">
                        <!-- ../img/cover/bg-cover.png -->
                        <img style='width: 50%; margin-bottom: 10px;' src=../assets/img/cover/bg-cover.png >
                    </div>

                    <div class="col-lg-12 footer-bg">

                        <div class='footer-div col-lg-10 col-md-10 col-sm-12 col-xs-12'>
                                <div class='footer-item-border col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <img style='width: 60px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_01.png' >
                                    <div>กรมสวัสดิการและคุ้มครองแรงงาน<br>Department of employment</div>
                                </div>
                                <div class='footer-item-border col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <img style='width: 60px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_02.png' >
                                    <div>สำนักงานประกันสังคม<br>Social security office</div>
                                </div>
                                <div class='footer-item-border col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <img style='width: 60px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_03.png' >
                                    <div>กรมการจัดหางาน<br>Department of employment</div>
                                </div>
                                <div class='footer-item-border col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <img style='width: 60px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_06.png' >
                                    <div>กรมสำนักงานพัฒนาฝีมือแรงงาน<br>Department of skill developememt</div>
                                </div>
                                <div class='footer-item-border col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <img style='width: 60px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_05.png' >
                                    <div>กระทรวงแรงงานร่วมเป็นส่วนหนึ่ง<br>ASEAN Thailand 2019</div>
                                </div>
                                <div class='footer-item-border col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <img style='width: 60px; margin-bottom: 10px;' src='../assets/img/icon/footer_logo_04.png' >
                                    <div>สถาบันส่งเสริมความปลอดภัย อาชีวอนามัย และสภาพแวดล้อมในการทำงาน<br>Thailand Institute of Occupational Safety and Health</div>
                                </div>
                        </div>
                        <div class='footer-div stat col-lg-2 col-md-2 col-sm-12 col-xs-12'>
                                <div class="stat-id"><b>สถิติ</b></div>
                                <div class="stat-id"><u>สถิติผู้เข้าชมวันนี้</u>    <span>0</span></div>
                                <div class="stat-id"><u>สถิติผู้เข้าชมเดือนนี้</u>   <span>0</span></div>
                                <div class="stat-id"><u>สถิติผู้เข้าชมทั้งหมด</u>  <span>0</span></div>
                        </div>

                        <div class="footer-copyrigth col-lg-12">
                                &copy; 2019 ศูนย์ข้อมูลแรงงานแห่งชาติ กระทรวงแรงงาน ถนนมิตรไมตรี แขวงดินแดง กทม. 10400 โทร.02-232-1436
                        </div>

                    </div>
                </div>
            </div>
        </div>

</body>