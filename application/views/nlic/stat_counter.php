<?php

//*** Select วันที่ในตาราง Counter ว่าปัจจุบันเก็บของวันที่เท่าไหร่  ***//
          $this->db->SELECT('DATE')->FROM('t_present_counter');
$result = $this->db->limit(1)->get()->result_array();

$checkDate = isset($result['DATE'])?$result['DATE']:"";

    if($checkDate != date("Y-m-d") && !empty($checkDate)){

        //*** ถ้าเป็นของเมื่อวานให้ทำการ Update Counter ไปยังตาราง daily และลบข้อมูล เพื่อเก็บของวันปัจจุบัน ***//
        $this->db->SET('DATE', date('Y-m-d', strtotime('-1 day')));
        $this->db->SET('NUM',"(SELECT COUNT(*) AS intYesterday FROM t_present_counter WHERE 1 AND DATE = '".date('Y-m-d')."') ",FALSE);
        $result = $this->db->INSERT("t_summary_counter");

        // *** ลบข้อมูลของเมื่อวานในตาราง t_present_counter ***//
        $this->db->WHERE("DATE !=", date("Y-m-d"));
        $this->db->DELETE('t_present_counter');
    }

        // *** Insert Counter ปัจจุบัน ***//
        $this->db->SET('DATE', date("Y-m-d"));
        $this->db->SET('IP'  , $_SERVER["REMOTE_ADDR"]);
        $this->db->INSERT("t_present_counter");
?>