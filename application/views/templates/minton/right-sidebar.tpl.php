<!-- Right Sidebar -->
<div class="side-bar right-bar">
        <div class="nicescroll">
                <ul class="nav nav-tabs tabs">
                        <li class="active tab">
                                <a href="#user-info" data-toggle="tab" aria-expanded="false">
                                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                                        <span class="hidden-xs">User Info</span>
                                </a>
                        </li>
                        <li class="tab">
                                <a href="#settings" data-toggle="tab" aria-expanded="false">
                                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                                        <span class="hidden-xs">Settings</span>
                                </a>
                        </li>
                        
                </ul>
                <div class="tab-content">
                        <div class="tab-pane fade in active" id="user-info">
                                
                        </div>
                        <div class="tab-pane fade" id="settings">

                                <div class="row m-t-20">
                                        <div class="col-xs-8">
                                                <h5 class="m-0">Notifications</h5>
                                                <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                                <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                                        </div>
                                </div>

                                <div class="row m-t-20">
                                        <div class="col-xs-8">
                                                <h5 class="m-0">API Access</h5>
                                                <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                                <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                                        </div>
                                </div>

                                <div class="row m-t-20">
                                        <div class="col-xs-8">
                                                <h5 class="m-0">Auto Updates</h5>
                                                <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                                <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                                        </div>
                                </div>

                                <div class="row m-t-20">
                                        <div class="col-xs-8">
                                                <h5 class="m-0">Online Status</h5>
                                                <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                                <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                                        </div>
                                </div>

                        </div>
                </div>
        </div>
</div>
<!-- /Right-bar -->