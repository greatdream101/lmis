<div class="topbar">
	<!-- LOGO -->
        <div class="topbar-left">
                <div class="text-center">
                    <img style="width: 50px; margin-top: 20px;margin-left: 5px;" src="<?= base_url() ?>assets/img/icon/mol_logo.png">
                </div>
        </div>
		<!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
                <div class="container">
                        <div class="">
                                <div class="pull-left">
                                        <button class="button-menu-mobile open-left waves-effect">
                                                <i class="md md-menu"></i>
                                        </button>
                                        <span class="clearfix"></span>
                                </div>
                                <ul class="nav navbar-nav navbar-right pull-right">
										<li class="hidden-xs">
                                                <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="md md-settings"></i></a>
                                        </li>
										<li>
                                                <a href="<?= base_url() ?>/auth/logout"><i class="fa fa-unlock-alt"></i></a>
                                        </li>
                                </ul>
                        </div>
                        <!--/.nav-collapse -->
                </div>
        </div>
</div>