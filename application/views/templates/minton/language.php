<?
if (!isset($_SESSION['site_lang'])) {
        $lang = 'thai';
} else {
        $lang = $_SESSION['site_lang'];
}

print form_hidden('site_lang', $lang);
$langs = $this->mdb->get_lang();

$default_langID = $langs['default_langID'];

?>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<li class="dropdown">
        <? if ($lang == 'thai') { ?>
                <a id="lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="lang-selected">
                                <img class="lang-flag" src="<?= base_url() ?>assets/img/flags/th.png" alt="Thai">
                        </span>
                </a>
        <? }else if ($lang == 'vietnam') { ?>
                <a id="lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="lang-selected">
                                <img class="lang-flag" src="<?= base_url() ?>assets/img/flags/vn.png" alt="Vietnamese">
                        </span>
                </a>
        <? } else { ?>
                <a id="lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="lang-selected">
                                <img class="lang-flag" src="<?= base_url() ?>assets/img/flags/en.png" alt="English">
                        </span>
                </a>
        <? } ?>


        <!--Language selector menu-->
        <ul class="head-list dropdown-menu lang-switch">
                <?
//                vd::d($lang);
                foreach ($langs['lang'] as $k => $v) {
                        ?>
                        <li>
                                <!--English-->
                                <a href="langswitch/switchLanguage/<?=$v['full_name']?>" id="<?=$v['langID']?>">
                                        <img class="lang-flag" src="<?= base_url() ?>assets/img/flags/<?=$v['code']?>.png" alt="<?=$v['full_name']?>">
                                        <span class="lang-id"><?=strtoupper($v['code'])?></span>
                                        <span class="lang-name"><?=$v['name']?></span>
                                </a>
                        </li>
                        <?
                }
                ?>
                
        </ul>
</li>