<link href="../assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
<link href="../assets/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/core.css" rel="stylesheet" type="text/css">
<link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="../assets/css/components.css" rel="stylesheet" type="text/css">
<link href="../assets/css/pages.css" rel="stylesheet" type="text/css">
<link href="../assets/css/menu.css" rel="stylesheet" type="text/css">
<link href="../assets/css/responsive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../assets/plugins/toastr/toastr.css" type="text/css"  />
<link rel="stylesheet" href="../assets/plugins/bootstrap-datatables/datatables.css"/>
<link href="../assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<link href="../assets/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css"  rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets/plugins/summernote/dist/summernote.css" type="text/css"  />
<link rel="stylesheet" href="../assets/plugins/select2/select2.css" type="text/css"  />
<link rel="stylesheet" href="../assets/plugins/popupwindow/popupwindow.css" type="text/css"  />

<link rel="stylesheet" href="../assets/plugins/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css" type="text/css"  />
<link rel="stylesheet" href="../assets/plugins/timepicker/bootstrap-timepicker.min.css" type="text/css"  />


<!-- Themes -->
<!--<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/bars-1to10.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/bars-movie.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/bars-square.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/bars-pill.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/bars-reversed.css">
<link rel="stylesheet" href=".../assets/plugins/jquery-bar-rating/dist/themes/bars-horizontal.css">-->

<!--<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/css-stars.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/bootstrap-stars.css">
<link rel="stylesheet" href="../assets/plugins/jquery-bar-rating/dist/themes/fontawesome-stars-o.css">-->
<link rel="stylesheet" type="text/css" href="../assets/css/jarvisWidget.css">
<link rel="stylesheet" href="../assets/plugins/smart-wizard/styles/smart_wizard.css" type="text/css"  />
<link rel="stylesheet" type="text/css" href="../assets/plugins/layout-grid/dist/css/layout-grid.min.css">
<link rel="stylesheet" type="text/css" href="../assets/plugins/pivottable-master/dist/pivot.css">
<link rel="stylesheet" type="text/css" href="../assets/plugins/c3.css">
<link rel="stylesheet" type="text/css" href="../assets/css/jasny-bootstrap.css">
<link rel="stylesheet" href="../assets/css/app.css" type="text/css"  />