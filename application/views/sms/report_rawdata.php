<?

$tab_1 = array();
array_push($tab_1, $this->mcl->dtp('dateStart', $t, array('class' => 'quarter-width', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->dtp('dateEnd', $t, array('class' => 'quarter-width', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->bt('view_sms', 'submit', array('class' => 'search pull-right')));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->div('output', '', 'full-width'));
$o = $this->mcl->input_page(array($tab_1), $t);
echo $o;
?>
<script language="javascript">
        $(document).ready(function () {

                $("#btn_view_sms").off('click').on('click', function (e) {
                        e.preventDefault();
                        var area = $('.search-box');
                        if (pass_validation(area)) {
                                var dateStart = attr($('input[name=dateStart]').val(),0);
                                var dateEnd = attr($('input[name=dateEnd]').val(),0);
                                var data = 'dateStart=' + dateStart + '&dateEnd=' + dateEnd;
                                var url = get_base_url() + get_uri() + '/get_SearchSMS';
                                // alert(data);return false;
                                $.ajax({
                                        type: 'POST',
                                        cache: false,
                                        async: false,
                                        url: url,
                                        data: data,
                                        beforeSend: function () {
                                        },
                                        success: function (html) {
                                                $('#output').html(html);
                                                handleDataTablesLocal('#output');
                                        }
                                });
                                return false;
                        } else {
                                return false;
                        }
                });
        });
</script>
