<!--
 * ************
 * Dashboard
 * ************
-->
<style>
.radio+.radio {
     margin-top: 10px;
}
.widget-simple-chart.text-right.card-box.count{
    text-align: left;
    padding: unset;
    margin: unset;
}
</style>

   <div class="col-sm-12 col-lg-12" id="searchData">
    	<div class="col-sm-6 col-lg-6">
    	<? echo $this->mcl->sb('sourceID', $t, array('class' => 'half-width','rules' => 'trim|required')); ?>
        </div>
        <div class="col-sm-6 col-lg-6">
        	<?= $this->mcl->bt('searchData', 'searchData', array('class' => 'search pull-left')); ?>
        </div>
    </div> 

    <div class="col-sm-12 col-lg-12" id="dashboard">
    
        <?php 
        $i = 0;
        $t['addonData']['dataType']['data']=array(array('name'=>'High risk','dataType'=>1),array('name'=>'Normal','dataType'=>2));
        foreach ( $flag as $k => $v) {
        ?>
            <div class="col-sm-6 col-lg-6">
            <h3 class="text-success counter" style="text-align: center;"> <?= $this->mcl->gl($k) ?> Total = <?= $this->mcl->gl($dataType[$i++]['totle']) ?> </h3>
            <?php foreach ( $v as $kk => $vv) { ?>
                <div class="col-sm-4 col-lg-4">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-success counter"> <?= $vv['flagSum'] ?> </h3>
                        <p class="text-muted text-nowrap"> <?= $vv['nameEN'] ?> </p>
                    </div>
                </div>
            <?php } ?>
            </div>
        <?php } ?>
        <?php if ($data == 1){?>
            <?=$this->mcl->hr('full-width');?>
            <div class="col-sm-6 col-lg-6">
                <?= $this->mcl->rb('dataType', $t, array('class' => 'no-cookie no-description form-check-inline col-sm-6 col-lg-6')) ?>
            </div>
            <div class="col-sm-6 col-lg-6">
                <?= $this->mcl->bt('search', 'search', array('class' => 'search pull-left')); ?>
            </div>

        <?php }?>

    </div>

<div class="col-sm-12 col-lg-12 approveSMS">
    <form id="approveSMS">
    <?=$this->mcl->hr('full-width');?>
    <div class="col-sm-12 col-lg-12"><h2 class="m-t-0 header-title"><?=$this->mcl->gl('option_for_send_sms')?></h2></div>
    <div class="col-sm-12 col-lg-3"><?=$this->mcl->sb('seriesID', $t, array('class' => 'no-cookie no-description'));?></div>
    <div class="col-sm-12 col-lg-3"><?=$this->mcl->sb('productID', $t, array('class' => 'no-cookie no-description'));?></div>
    <div class="col-sm-12 col-lg-3"><?=$this->mcl->sb('champaignID', $t, array('class' => 'no-cookie no-description'));?></div>
    <div class="col-sm-12 col-lg-3"><?=$this->mcl->bt('approve', 'approve', array('class' => 'half-width pull-left'));?></div>
    <?=$this->mcl->hr('full-width');?>
    <div class="col-sm-12 col-lg-12 count_sms" id="count_sms"></div>
    <div class="col-sm-12 col-lg-12 output" id="output"></div>
    <div class="col-sm-12 col-lg-12 countCusID" id="countCusID"></div>
    </form>
</div>

<script language="javascript">
	$(document).ready(function () {
		setTimeout(function() {
			$("div.approveSMS").css("display", "none");
			$("#btn_approve").css("display", "none");
			$("ul.nav.nav-tabs.pull-left.in").css("display", "none");
			$("div.jarviswidget-ctrls").css("display", "none");
			$("footer.footer").css("display", "none");
  		}, 200);

		handleRadioBox('.search-box');
		handleCheckBox();

		$("#btn_searchData").off('click').on('click', function (e) {
            e.preventDefault();
            var data = get_data_serialize('#ajax_content');
            var url = get_base_url() + '/sms_confirm';
            $.ajax({
                type: "POST",
                url: url,
                cache: false,
                async: false,
                data: data,
                beforeSend: function () {
                    start_loading();
                },
                success: function (html) {
                	//$( "#dashboard" ).html(html);
                	//$( "div#dashboard div#searchData" ).css("display", "none");
                    end_loading();
                }
            });
    	});
		

		$("#btn_approve").off('click').on('click', function (e) {
            e.preventDefault();

            var data = 'countCus='+$('input.checkbox:checked').length;

            var url = get_base_url() + 'sms_confirm/countCusID';
            
            $.ajax({
                type: "POST",
                url: url,
                cache: false,
                async: false,
                data: data,
                beforeSend: function () {
                    start_loading();
                },
                success: function (html) {
                    end_loading();
                    $('#countCusID').html(html);
                	//location.reload();
                }
            });
    	});
  		
		$('input[type=radio]').each(function (k, v) {
            if ($(this).prop('checked') == true) {
                    var name = $(this).attr('name');
                    var h = '<input type=hidden name=' + name + ' id=' + name + ' value=' + $(this).val() + '>';
                    $(this).closest('td').append(h);
            }
        });
        
		$("#btn_search").off('click').on('click', function (e) {
            e.preventDefault();

            var data = get_data_serialize('#ajax_content');
            var url = get_base_url() + '/sms_confirm/search';
            var datatype = attr($('input[type=hidden][name=dataType]').val(),'');

            if(datatype.length > 0){
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    async: false,
                    data: data,
                    beforeSend: function () {
                        start_loading();
                    },
                    success: function (html) {
                        end_loading();
                        $("div.approveSMS").css("display", "unset");
                        $('#output').html(html);
                        $("#btn_approve").css("display", "unset");
                        $("tr.second").css("display", "none");

                        var o = "<div class='widget-simple-chart text-right card-box count'><div class='circliful-chart' data-dimension='90' data-text='35%' data-width='5' data-fontsize='14' data-percent='35' data-fgcolor='#5fbeaa' data-bgcolor='#ebeff2'></div><h3 class='text-success counter'> <?=$this->mcl->gl('count_sms')?> "+$('input.checkbox').length+"</h3></div>";

                        $('#count_sms').html(o);

                        $('input#checkAll').click(function () {
                            if( $('input#checkAll:checked').val() == "on"){
                                //$('input.checkbox').trigger( "click" );
                                $('input.checkbox').prop("checked", true);
                            }else{
                                $('input.checkbox').prop("checked", false);
                                $('input:hidden[name=chk]').val('');
                            }
                        });
                    }
                });
            }
    	});
    	
	});
</script>