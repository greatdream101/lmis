<h1>Check duplicate Data</h1>

        <input type="file" id="files" class="form-control">
        <button id="btupload" class="btn btn-success text-uppercase">upload</button>

<script language="javascript">
    var name;
    var result;
    var head;
    function handleFile(e) {
        //Get the files from Upload control
        var files = e.target.files;
        var i, f;
        //Loop through files
        for (i = 0, f = files[i]; i != files.length; ++i) {
            var reader = new FileReader();
            name = f.name;
            //document.getElementById('txt').innerHTML = 'File name : '+name;
            reader.onload = function (e) {
                //debugger;
                var data = e.target.result;


                var workbook = XLSX.read(data, {type: 'binary'});

                var sheet_name_list = workbook.SheetNames;
                sheet_name_list.forEach(function (y) { /* iterate through sheets */
                    //Convert the cell value to Json
                    var roa = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                    head = get_header_row(workbook.Sheets[y]);
                    if (roa.length > 0) {
                        result = roa;
                    }
                });
                //Get the first column first cell value
                //alert(''+head);
            };
            reader.readAsArrayBuffer(f);
        }
    }
    
    function get_header_row(sheet) {
        var headers = [];
        var range = XLSX.utils.decode_range(sheet['!ref']);
        var C, R = range.s.r; /* start in the first row */
        /* walk every column in the range */
        for(C = range.s.c; C <= range.e.c; ++C) {
            var cell = sheet[XLSX.utils.encode_cell({c:C, r:R})]; /* find the cell in the first row */

            var hdr = "UNKNOWN " + C; // <-- replace with your desired default 
            if(cell && cell.t) hdr = XLSX.utils.format_cell(cell);

            headers.push(hdr);
        }
        return headers;
    }

    //Change event to dropdownlist
    $(document).ready(function () {
        $('#files').change(handleFile);
        $("#btupload").click(function () {
            //debugger;
            //alert($('input[name=chktype]:checked').val());
            // Create A File Reader HTML5

            if (pass_validation('#content')) {
                var imgVal = $('#files').val();
                if (imgVal != '')
                {
                    var data = "data=" + encodeURIComponent(JSON.stringify(result)) +
                            '&filename=' + name +
                            '&header='+JSON.stringify(head);
                    $.ajax({
                        method: "POST",
                        //             dataType: "json",
                        //                cache: false,
                        url: get_base_url()+"CheckDuplicate/readData",
                        data: data,
                        beforeSend: function () {
                            //start_spinloading();
                        }, success: function (result) {
                            //end_spinloading();
                            if(result > 0){
                                //end_saving();
                            }
                            //export_datatable();
                            return;
                        }
                    });


                } else {
                    alert("Please choose file.");
                }
            }
        });


    });
</script>