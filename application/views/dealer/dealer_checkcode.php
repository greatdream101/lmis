<style>
    #btn_new{
        display: none;
    }
    html{
        /* background: #211f23; */
        background-image: url("../assets/img/bg-mazda-2.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    body{
        background: none;
        /* background-image: url("../assets/img/bg-mazda-2.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover; */
    }
    .footer{
        display: none;
    }
    .content-page{
        /* background-image: url("../assets/img/bg-mazda-2.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover; */
    }
    #content_checkcode{
        /* background-color: #101010; */
        /* background-image: linear-gradient(to top, black , white, black 50%); */
        margin: 0px 20px 20px 0px;
        padding: 2%;
        width: 100%;
    }
    .header-content{
        color: white;
    }
    P{
        color: white;
    }
    #vin1{
        color: white;
    }
    #vin1 font{
        color: black;
    }
    #mazda_icon{
        text-align: center;
        /* margin: 10px 10px 25px 10px; */
        /* padding: 0 0px 0 18px; */
    }
    #mazda_icon img{
        width: 100px;
    }
    #form_checkcode{
        text-align: center;
        /* background-color: #c9c9ca; */
        width: 90%;
        padding: 20px 13px 0 0;
        margin: 0 0 0 5%;
        border-radius: 10px;
    }
    .tb_style {
        margin: 5px;
        text-align: center;
        border-radius: 30px;
        height: 50px;
        width: 100%;
        font-size: small;
    }
    #icon{
        font-size: 100px;
        width: 100%;
        /* margin-left: 8px; */
        margin: 10px 0 0 0;
    }
    .hidden-content{
        display: none;
        font-weight: bold;
        font-size: 20px;
        padding: 5% 0 5% 0;
        border-radius: 10px;
    }
    .disabled{
        pointer-events: none;
        background-color: #3e3e3e66;
        display: none;
    }
    .btn-style{
        border-radius: 7px;
        padding: 10px 30px 10px 30px;
        /* margin: 30px 10px 0 -5px; */
        margin: 30px 0px 0 0px;
        font-size: 15px;
        background: #23af58;
    }
    .input-group{
        width: 70%;
        margin: 0 0% 3% 15%;
    }
    .body-checkcode{
        width: 70%;
        margin: 0 0% 3% 15%;
    }
    #vin1{
        margin: 10px 0 10px 0;
    }
    h5 p{
        margin: 15px 0px 0 0;
    }
    font{
        background: white;
        border-radius: 10px;
        padding: 15px;
        font-size: small;
    }
    #div_back_form{
        position: relative;
        /* margin-top: 20px; */
        margin-bottom: 20px;
        width: 100%;
        float: left;
    }
</style>

<?php

$render = "";
$output = "";

$render .= $this->mcl->div("mazda_icon", img(array('src'=>base_url()."assets/img/mazda-icon.png")));

$render .= "<div class='header-content'>";
$render .= "<h2>Dealer info</h2>";
$render .= "</div>";

$render .= "<div class='input-group'>";
    $render .= "<h5><p align='left'>Dealer Code : </p></h5>";
    $render .= "<input type='text' name='DealerCode' id='DealerCode' placeholder='Please Enter Dealer Code.' class='tb_style' value=".$_SESSION['identity']." disabled='disabled'/> ";
    $render .= "<h5><p align='left'>Code : </p></h5>";
    $render .= "<input type='text' name='Code' id='Code' placeholder='Please Enter Code.' class='tb_style' /> ";
    $render .= "<button id='btn_check' class='btn-style btn-default' type='submit'>Submit</button>";
$render .= "</div>";

$render .= "<div class='body-content hidden-content'>";
    $render .= $this->mcl->div("icon", "");
    $render .= $this->mcl->div("vin1", "No data");
$render .= "</div>";

$render .= "<div class='body-checkcode hidden-content'>";
    $render .= "<h5><p align='left'>Dealer info.</p></h5>";
    $render .= "<input type='text' name='DealerInfo' id='DealerInfo' placeholder='Dealer Code.' class='tb_style' value=".$_SESSION['identity']." disabled='disabled' /> ";

    $render .= "<h5><p align='left'>Customer info.</p></h5>";
    $render .= "<input type='text' name='fname' id='fname' placeholder='insert your first name.'    class='tb_style' disabled='disabled' /> ";
    $render .= "<input type='text' name='lname' id='lname' placeholder='insert your last name.'     class='tb_style' disabled='disabled' /> ";
    $render .= "<input type='text' name='CustomerTel'  id='CustomerTel'  placeholder='Tel.'         class='tb_style' disabled='disabled' /> ";
    $render .= "<input type='text' name='CustomerVIN'  id='CustomerVIN'  placeholder='VIN number.'  class='tb_style' disabled='disabled' /> ";
    $render .= "<input type='hidden' name='submit_date' id='submit_date' value=".date("Y-m-d")." /> ";

    $render .= "<button id='btn_submit_VIN1'    class='btn-style btn-default disabled' type='submit'>Submit</button>";
    $render .= "<button id='btn_submit_VIN2'    class='btn-style btn-default disabled' type='submit'>Submit</button>";
    $render .= "<button id='btn_edit'           class='btn-style btn-default disabled' type='submit'>Edit</button>";
$render .= "</div>";

$render .= "<div id='div_back_form'><button id='btn_back_form' class='btn-style btn-danger disabled' type='submit'>Back</button></div>";

$output .= $this->mcl->div("form_checkcode", $render);



print "<div id='body_checkcode'>".$this->mcl->div("content_checkcode", $output, '')."</div>";
?>

<script>
    $(document).ready(function(){
        $("#btn_check").on("click", function(){
            $("#vin2").val("");
            var url  = get_base_url() + 'Dealer_checkcode/checkcode';
            var dealer_code = $("#DealerCode").val();
            var code        = $("#Code").val();

            if(DealerCode=="" || Code==""){
                showSystemMessage(2,"Please fill data before submit.");
                return false;
            }else{
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        cache: false,
                        data: {"dealer_code" : dealer_code, "code" : code},

                    success: function(result){
                        // console.log(result);
                        if(result == 0){ // No data
                            $("#vin1").hide("slow");
                            $("#btn_submit").addClass("disabled");
                            $("#btn_edit").addClass("disabled");
                            showSystemMessage(2, "No data.");

                        }else if(result[0].QA1 > 0 && result[0].QA2 > 0){ // No answer question

                            if(result[0].flagUsed == 1){ // update code already
                                    $(".header-content").html("<h2>Check Code</h2>");
                                    $(".input-group").hide();
                                    $(".body-content").show("slow");
                                    $("#icon").removeClass("glyphicon glyphicon-ok-circle");
                                    $("#icon").addClass("glyphicon glyphicon-remove-circle");
                                    $("#icon").css("color", "red");
                                    $("#vin1").html("ขออภัยรหัสนี้ถูกลงทะเบียนแล้ว</br>กรุณาตรวจสอบอีกครั้ง</br></br><font>VIN : "+result[0].VIN1+"</font>");
                                    $("#btn_submit").addClass("disabled");
                                    $("#btn_edit").addClass("disabled");
                                    $("#btn_back_form").show();
                                    $("#btn_back_form").removeClass("disabled");
                                    showSystemMessage(1, "This code is actived already.");
                            }else{
                                    $(".header-content").html("<h2>Check Code</h2>");
                                    $(".input-group").hide();
                                    $(".body-checkcode").show();
                                    $("#fname").val(result[0].fname);
                                    $("#lname").val(result[0].lname);
                                    $("#CustomerTel").val(result[0].mobilePhone);
                                    $("#CustomerVIN").val(result[0].VIN1);
                                    $("#btn_submit_VIN1").removeClass("disabled");
                                    $("#btn_edit").removeClass("disabled");
                                    showSystemMessage(1, "Checked.");
                            }

                        }else{
                            showSystemMessage(2, "Don't completed the question yet.");
                        }
                    }
                })
            }
        });

        $("#btn_submit_VIN1").on("click", function(){ // Update VIN1

            var url  = get_base_url() + 'Dealer_checkcode/save_data';
            var CustomerVIN  = $("#CustomerVIN").val();
            var dealer_code  = $("#DealerInfo").val();
            var Code         = $("#Code").val();
            var submitDate   = $("#submit_date").val();
            
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {"code" : Code, "dealer_code" : dealer_code, 'submit_date' : submitDate},
                    success: function(result){
                        $(".body-checkcode").hide();
                        $("#btn_back_form").show();
                        $("#btn_back_form").removeClass('disabled');
                        $(".body-content").show("slow");
                        $("#vin1").html("ดำเนินการลงทะเบียนเรียบร้อย. </br></br><font>VIN : "+CustomerVIN+"</font>");
                        $("#icon").removeClass("glyphicon glyphicon-remove-circle");
                        $("#icon").addClass("glyphicon glyphicon-ok-circle");
                        $("#icon").css("color", "#23af58");
                        showSystemMessage(1, "Actived code already.");
                    }
                })
        });

        $("#btn_submit_VIN2").on("click", function(){ // Update VIN2

            var url  = get_base_url() + 'Dealer_checkcode/update_data';
            var fname        = $("#fname").val();
            var lname        = $("#lname").val();
            var CustomerTel  = $("#CustomerTel").val();
            var CustomerVIN  = $("#CustomerVIN").val();
            var dealer_code  = $("#DealerInfo").val();
            var Code         = $("#Code").val();
            var submitDate   = $("#submit_date").val();

                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {"fname2" : fname, "lname2" : lname, "mobile2" : CustomerTel, "VIN2" : CustomerVIN, "code" : Code, "dealer_code" : dealer_code, "flagUsed" : "1", 'submit_date' : submitDate},
                    success: function(result){
                        $(".body-checkcode").hide();
                        $(".body-content").show("slow");
                        $("#vin1").html("ดำเนินการลงทะเบียนเรียบร้อย. </br></br><font>VIN : "+CustomerVIN+"</font>");
                        $("#icon").removeClass("glyphicon glyphicon-remove-circle");
                        $("#icon").addClass("glyphicon glyphicon-ok-circle");
                        $("#icon").css("color", "#23af58");
                        showSystemMessage(1, "Actived code already.");
                    }
                })
        });

        $("#btn_edit").on("click", function(){
            $("#fname").removeAttr("disabled");
            $("#lname").removeAttr("disabled");
            $("#CustomerTel").removeAttr("disabled");
            $("#CustomerVIN").removeAttr("disabled");
            $("#btn_submit_VIN2").removeClass("disabled");
            $("#btn_submit_VIN2").show();//submit for edit & flage data
            $("#btn_submit_VIN1").hide();//submit for flage data
            $("#btn_edit").hide();
            $("#btn_back_form").show();
            $("#btn_back_form").removeClass("disabled");
            showSystemMessage(1, "Open form edit data.");
        });

        $("#btn_back_form").on("click", function(){
            $(".header-content").show();
            $(".input-group").show();
            $(".body-content").hide();
            $(".body-checkcode").hide();
            $("#fname").attr("disabled", "disabled");
            $("#lname").attr("disabled", "disabled");
            $("#CustomerTel").attr("disabled", "disabled");
            $("#CustomerVIN").attr("disabled", "disabled");
            $("#btn_submit_VIN2").addClass("disabled");
            $("#btn_submit_VIN2").hide();
            $("#btn_submit_VIN1").show();
            $("#btn_edit").show();
            $("#btn_back_form").hide();
        });

    });
</script>