<?

$tab_1 = array();
array_push($tab_1, $this->mcl->hdr('raw_data', 'full-width'));
array_push($tab_1, $this->mcl->dtp('dateStart', $t, array('class' => 'quarter-width')));
array_push($tab_1, $this->mcl->dtp('dateEnd', $t, array('class' => 'quarter-width')));
array_push($tab_1, $this->mcl->bt('view_questions', 'submit', array('class' => 'full-width search')));
/* array_push($tab_1, $this->mcl->bt('export_rawdata', 'export', array('class' => 'search pull-right'))); */
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->pg('chart', 'Score Data', 'branch', 'branch', 'column', 'data'));
array_push($tab_1, $this->mcl->div('output', '', 'full-width'));
$o = $this->mcl->input_page(array($tab_1), $t);
echo $o;
?>

<script language="javascript">
        $(document).ready(function () {
        	$('#btn_export_rawdata').on('click', function (e) {
        	    e.preventDefault();
        	    setTimeout(function () {
        	            var dateStart = attr($('input[name=dateStart]').val(), '');
        	            var dateEnd = attr($('input[name=dateEnd]').val(), '');
        	            var seriesID = attr($(cc).find('input[type=hidden][name=seriesID]').val());
                        var clientsID = attr($(cc).find('input[type=hidden][name=clientsID]').val());
                        var flag = 'export';
                        
        		        var h = '<form name="frm_export" id="frm_export" action="' + get_base_url() + '.Dealer_rawdata/get_data" autocomplete="off" method="post" target="_blank">';
        	            h += '<input type="hidden" name="rep_name" id="rep_name" value="reportrawdata">';
        	            h += '<input type="hidden" name="rep_template" id="rep_template" value="reportrawdata.xlsx">';        	           
        	            h += '<input type=hidden name=dateStart id=dateStart value="' + dateStart + '">';
        	            h += '<input type=hidden name=dateEnd id=dateEnd value="' + dateEnd + '">';
        	            h += '<input type=hidden name=flag id=flag value="' + flag + '">';
        	            h += '</form>';

        	            $(cc).append(h);
        	            $('form[id=frm_export]').submit();
        	            $('form[id=frm_export]').remove();
        	    }, 1000);
        	});
                $("#btn_view_questions").off('click').on('click', function (e) {
                        e.preventDefault();
                        var area = $('.search-box');
                        if (pass_validation(area)) { 
                                var dateStart = attr($(cc).find('input[name=dateStart]').val());
                                var dateEnd = attr($(cc).find('input[name=dateEnd]').val());
                                var flag = 'view';
                                var data = 'dateStart=' + dateStart + '&dateEnd=' + dateEnd + '&flag=' + flag;
                                var url = get_base_url() + get_uri() + '/get_data';
                                $.ajax({
                                        type: 'POST',
                                        cache: false,
                                        async: false,
                                        url: url,
                                        data: data,
                                        beforeSend: function () {
                                        },
                                        success: function (html) {
                                                $('#output').html(html);
                                                handleDataTablesLocal('#output');
                                                setTimeout(function () {
                                                        $('button[id=btn_restore_config]').trigger('click');
                                                }, 1000);
                                        }
                                });
                                return false;
                        } else {
                                return false;
                        }
                });
        });
</script>