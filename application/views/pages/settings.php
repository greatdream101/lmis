<div class="container">

        <!-- Page-Title -->
        <div class="row">
                <div class="col-sm-12">
                        <div class="page-title-box">
                                <ol class="breadcrumb pull-right">
                                        <li><a href="#">Minton</a></li>
                                        <li><a href="#">Extras</a></li>
                                        <li class="active">Profile</li>
                                </ol>
                                <h4 class="page-title">Profile</h4>
                        </div>
                </div>
        </div>

        <div class="row">
                <div class="col-lg-3 col-md-4">
                        <div class="text-center card-box">
                                <div class="member-card">
                                        <div class="thumb-xl member-thumb m-b-10 center-block">
                                                <img src="assets/images/users/avatar-1.jpg" class="img-circle img-thumbnail" alt="profile-image">
                                        </div>

                                        <div class="">
                                                <h4 class="m-b-5">Mark A. McKnight</h4>
                                                <p class="text-muted">@webdesigner</p>
                                        </div>

                                        <button type="button" class="btn btn-success btn-sm w-sm waves-effect m-t-10 waves-light">Follow</button>
                                        <button type="button" class="btn btn-danger btn-sm w-sm waves-effect m-t-10 waves-light">Message</button>


                                        <div class="text-left m-t-40">
                                                <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">Johnathan Deo</span></p>

                                                <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">(123) 123 1234</span></p>

                                                <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">coderthemes@gmail.com</span></p>

                                                <p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15">USA</span></p>
                                        </div>

                                        <ul class="social-links list-inline m-t-30">
                                                <li>
                                                        <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                        <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                        <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                                </li>
                                        </ul>

                                </div>

                        </div> <!-- end card-box -->



                </div> <!-- end col -->


                <div class="col-md-8 col-lg-9">
                        <div class="">
                                <div class="">
                                        <ul class="nav nav-tabs navtab-custom">

                                                <li class="active">
                                                        <a href="#setting" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                                                <span class="hidden-xs">SETTINGS</span>
                                                        </a>
                                                </li>

                                        </ul>
                                        <div class="tab-content">

                                                <div class="tab-pane active" id="setting">

                                                        <div class="form-group">
                                                                <label for="FullName">Full Name</label>
                                                                <input type="text" value="John Doe" id="FullName" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="Email">Email</label>
                                                                <input type="email" value="first.last@example.com" id="Email" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="Username">Username</label>
                                                                <input type="text" value="john" id="Username" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="Password">Password</label>
                                                                <input type="password" placeholder="6 - 15 Characters" id="Password" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="RePassword">Re-Password</label>
                                                                <input type="password" placeholder="6 - 15 Characters" id="RePassword" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="AboutMe">About Me</label>
                                                                <textarea style="height: 125px" id="AboutMe" class="form-control">Loren gypsum dolor sit mate, consecrate disciplining lit, tied diam nonunion nib modernism tincidunt it Loretta dolor manga Amalia erst volute. Ur wise denim ad minim venial, quid nostrum exercise ration perambulator suspicious cortisol nil it applique ex ea commodore consequent.</textarea>
                                                        </div>
                                                        <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Save</button>

                                                </div>
                                        </div>
                                </div>
                        </div>

                </div> <!-- end col -->
        </div>
        <!-- end row -->



</div>