<?
$tab_1 = array();
array_push($tab_1, $this->mcl->hdr('assignment', 'full-width'));
array_push($tab_1, $this->mcl->hd('clientsID',$t['clientsID']));
array_push($tab_1, $this->mcl->hd('seriesID',$t['seriesID']));
array_push($tab_1, $this->mcl->sb('branchID', $t, array('class' => 'half-width no-cookie', 'rules' => 'trim|required')));
array_push($tab_1, $this->mcl->bt('assign_survey', 'save',array('class' => 'hide')));
array_push($tab_1, $this->mcl->div('surveyor_area', '', 'full-width'));
array_push($tab_1, $this->mcl->bt('cancel', 'previous',array('class' => 'footer')));
$o = $this->mcl->input_page(array($tab_1), $t);
print $o;
?>
<script language="javascript">
     $(document).ready(function ()
     {
          setTimeout(function ()
          {
               get_sb_branch();
               $('#btn_cancel_custom').removeClass('footer');
               $('#btn_save_custom').removeClass('footer');
          }, 10);

          $('#btn_assign_survey').on('click', function (e) {
                  e.preventDefault();
                  assign_survey();
          })
     });

     var handleCheckBoxCustom = function (container)
     {
          container = which_container(container);
          $(container).find('input[type=checkbox]').on('click', function (e)
          {
               if ($(this).hasClass('checkbox-master'))
               {
                    var table = attr($('#' + $(this).attr('name')), '');

                    if ($(this).prop('checked') == true)
                    {
                         $(table).find('input[type=checkbox]').prop('checked', true);
                    }
                    else
                    {
                         $(table).find('input[type=checkbox]').prop('checked', false);
                    }
               }
          })
     }

     $('select[id=branchID]').on('click', function (e) {
          // alert(attr($(cc).find('input[type=hidden][name=branchID]').val()));
          var data = 'seriesID=' + attr($(cc).find('input[type=hidden][name=seriesID]').val());
          data += '&branchID=' + attr($(cc).find('input[type=hidden][name=branchID]').val());
          var url = get_base_url() + get_uri() + '/get_surveyor_list';
          $.ajax({
                  type: 'POST',
                  cache: false,
                  async: false,
                  url: url,
                  data: data,
                  beforeSend: function () {
                  },
                  success: function (html) {
                       // console.log(html);
                      $('#surveyor_area').html(html);
                      setTimeout(function ()
                      {
                           handleTables('#surveyor_area');
                           handleCheckBoxCustom('#surveyor_area');
                           $('#btn_assign_survey').removeClass('hide');
                           // $('#btn_save_refresh').removeClass('hide');
                           // $('#btn_cancel').removeClass('hide');
                      }, 10);
                  }
          });
     })

     function get_sb_branch()
     {
          var data = 'seriesID=' + attr($(cc).find('input[type=hidden][name=seriesID]').val());
          var url = get_base_url() + get_uri() + '/get_sb_branch';
          $.ajax({
                  type: 'POST',
                  cache: false,
                  async: false,
                  url: url,
                  data: data,
                  beforeSend: function () {
                  },
                  success: function (html) {
                       // console.log(html);
                       var data = $.parseJSON(html);
                       $('select[name=branchID]').empty();
                       $('select[name=branchID]').append('<option value="0">please_select_option</option>');
                       $.each(data['data'], function (k, v) {
                              $('select[name=branchID]').append('<option value="' + v['id'] + '">' + v['name'] + '</option>');
                       })
                       $("#select2-chosen-2").text("please_select_option");
                  }
          });
     }

     function assign_survey()
     {
          var atLeastOneIsChecked = $('input[name="select_id[]"]:checked').length > 0;
     	if(!atLeastOneIsChecked)
     	{
     		alert('<?=$this->mcl->gl('select_more_option');?>');
     		return false;
     	}

     	if (!confirm("ยืนยันการบันทึกข้อมูล"))
     	{
     		return false;
     	}
          var data = 'seriesID=' + attr($(cc).find('input[type=hidden][name=seriesID]').val());
          data += '&branchID=' + attr($(cc).find('input[type=hidden][name=branchID]').val());
          data += '&clientsID=' + attr($(cc).find('input[type=hidden][name=clientsID]').val());
          data += "&" + $('#frm_assignment').serialize();
          // alert(data);return false;

          var url = get_base_url() + get_uri() + '/assign_survey';
          $.ajax({
                  type: 'POST',
                  cache: false,
                  async: false,
                  url: url,
                  data: data,
                  beforeSend: function () {
                  },
                  success: function (html) {
                       // console.log(html);
                       if(html>0)
                       {
                            alert('<?=$this->mcl->gl('save_data_complete');?>');
                            $('select[id=branchID]').click();
                       }
                  }
          });
     }
</script>
