<?php
//vd::d($t['edit_data']);

if (isset($t['edit_data']['value_id']) && $t['edit_data']['value_id'] == '99') {
      $class = 'always_disabled';
} else {
      $class = '';
}
                
$o = '';

$o .= '<div class=row><div class=col-md-12>';
$o .= $this->mcl->rb('controlID', $t, array('class' => 'required no-cookie'));
$o .= '</div></div>';

$o .= '<div class=row><div class=col-md-12>';
$o .= $this->mcl->tb('code', $t, array('class' => 'required ' . $class));
$o .= '</div></div>';

$o .= '<div class=row><div class=col-md-12>';
$o .= $this->mcl->tb('name', $t, array('class' => 'required ' . $class));
$o .= '</div></div>';

//$o .= '<div class=row><div class=col-md-12>';
//$o .= $this->mcl->tb('name_en', $t, 'required ' . $class);
//$o .= '</div></div>';

$o .= '<div class=row><div class=col-md-12>';
$o .= $this->mcl->tb('value', $t, array('class' => $class));
$o .= '</div></div>';
$o .= '<div class=row><div class=col-md-12>';
$t['edit_data']['exCalID'] = isset($t['edit_data']['exCalID'])?$t['edit_data']['exCalID']:1;
$o .= $this->mcl->rb('exCalID', $t, array('class' => 'form-check-inline is-ch-en no-cookie no-description required'));
$o .= '</div></div>';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->tb('option', $t);
$o .= '</div></div>';
$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->sb('jumpFrom', $t, array('class' => 'no-cookie'));
$o .= '</div></div>';
$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->sb('jumpTo', $t, array('class' => 'no-cookie'));
$o .= '</div></div>';
$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->ta('detail', $t, array('class' => 'no-cookie'));
$o .= '</div></div>';

//                }
$o .= '<div class=row><div class=col-md-12>';
if (isset($_POST['questionID']) && $_POST['questionID'] > 0) {
      $o .= $this->mcl->bt('add_answer', 'Update Answer', array('class' => 'btn btn-primary'));
} else {
      $o .= $this->mcl->bt('add_answer', 'Add Answer', array('class' => 'btn btn-primary'));
}
$o .= form_hidden('questionID', isset($_POST['questionID']) ? $_POST['questionID'] : 0);
$o .= '</div></div>';

print $o;
?>
