<?= $this->load->view('survey/loadScript.php', array(), true) ?>
<?= $this->load->view('survey/loadStyle.php', array(), true) ?>
<?
$o = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
$o.='<div class="topbar">
	<!-- LOGO -->
        <div class="topbar-left">
                <div class="text-center ">			
            </div>
        </div>
		<!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
                <div class="container">
                        <div class="">
                                <div class="pull-left">
                                <img src='.base_url().'assets/img/aia1.png class="logo aia-logo"> 
                                        <span class="clearfix"></span>
                                </div>
                                <ul class="nav navbar-nav navbar-right pull-right">
										
                                </ul>
                        </div>
                        <!--/.nav-collapse -->
                </div>
        </div>
</div>';
$o.=$this->mcl->hdr('questionnaire', 'full-width');
$o.=$this->mcl->hd('cusID', $t['cusID']);
$o.=$this->mcl->hd('surveyStatus', $t['surveyStatus']);
$o.=$this->mcl->div('scriptsGreeting', $data['scriptsGreeting'], 'full-width');
$o.=$this->mcl->div('question_answer_area', $data['question_info'], 'full-width');
$o.=$this->mcl->div('question_area', '', 'full-width');
$o.=$this->mcl->div('scriptsEnding', $data['scriptsEnding'], 'full-width hide');
print $o;
?>

<script language="javascript">
    $(document).ready(function () {

        setTimeout(function () {
            handleShowScripts();
            handleSaveInput();
        }, 50);

        $('.btn-begin-survey').off('click').on('click', function (e) {
            $('#question_answer_area').removeClass('hide');
            $('#scriptsGreeting').addClass('hide');
        })

        $('.btn-end-survey').off('click').on('click', function (e) {
            location.reload();
        })

        var handleSaveInput = function ()
        {
            $('.save-input').each(function (k,v)
            {
                $(this).on('change',function (e){
                    var questionid = $(v).closest('div.question-area').attr('questionid');
                    var inputtype = $(v).attr('type');
                    var inputname = $(v).attr('name');
                    var jumpTo = $(v).attr('jumpTo');

                    if (typeof inputtype === 'undefined')
                    {
                        inputtype = $(v).attr('type');
                        inputname = $(v).attr('name');
                    }

                    if (inputtype == 'radio')
                    {
                        var valueNet = $('input[name=' + inputname + ']:checked').val();
                    }
                    else if (inputtype == 'textarea')
                    {
                        var valueNet = $('textarea[name=' + inputname + ']').val();
                    }

                    // alert('inputtype : '+inputtype);
                    // alert('inputname : '+inputname);
                    // alert('jumpTo : '+jumpTo);
                    // alert('valueNet : '+valueNet);

                    data = 'cusID=' + $(cc).find('input[type=hidden][name=cusID]').val();
                    data += '&surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
                    data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                    data += '&questionID=' + questionid;
                    data += '&valueNet=' + valueNet;

                    var url = get_base_url() + '<?=$this->router->class;?>' + '/save_answer';
                    // alert(data);return false;

                    $.ajax({
                        type: "POST",
                        url: url,
                        cache: false,
                        async: false,
                        data: data,
                        beforeSend: function () {

                        },
                        success: function (result) {
                            // alert(result);return false;
                            //debugger;
                            var jsonResult = eval("(" + result + ")");
                            if (jsonResult.surveyID > 0)
                            {
                                $('input:hidden[name=surveyID]').val(jsonResult.surveyID);
                            }

                            if (jsonResult.valueID > 0)
                            {
                                $('input:hidden[name=valueid]').val(jsonResult.valueID);
                            }
                        }
                    });
                })
            })
        }

        $('#btn_last').on('click', function (e) {
            confirm_save();
        })
    });

    function handleShowScripts()
    {
        if ($('#surveyStatus').val() != 'completed')
        {
            $('#scriptsGreeting').addClass('hide');

            if ($('#scriptsEnding').is(':empty'))
            {
                alert('<?= $this->mcl->gl('end_survey'); ?>');
                // location.reload();
            } else
            {
                $('#question_answer_area').addClass('hide');
                $('#scriptsEnding').removeClass('hide');
            }

            return false;
        }

        if ($('#scriptsGreeting').hasClass('hide'))
        {
            if ($('#scriptsEnding').is(':empty'))
            {
                alert('<?= $this->mcl->gl('end_survey'); ?>');
                // location.reload();
            } else
            {
                $('#question_answer_area').addClass('hide');
                $('#scriptsEnding').removeClass('hide');
            }
        } else
        {
            if ($('#scriptsGreeting').is(':empty'))
            {
                $('#question_answer_area').removeClass('hide');
                // handleShowQuestion();
            } else
            {
                $('#question_answer_area').addClass('hide');
            }
        }
    }

    function confirm_save()
    {
        if(!confirm('<?=$this->mcl->gl('confirm_save');?>'))
        {
             return false;
        }

        data = 'surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
        data += '&qcStatusID=2';
        var url = get_base_url() + '<?=$this->router->class;?>' + '/update_qc_status';

        $.ajax({
            type: 'POST',
            cache: false,
            async: false,
            url: url,
            data: data,
            beforeSend: function () {
            },
            success: function (result)
            {
                if (result > 0)
                {
                    alert('<?= $this->mcl->gl('save_data_complete'); ?>');
                    handleShowScripts();
                }
            }
        });
    }
</script>
