<script language="javascript">
     $(document).ready(function ()
     {
          setTimeout(function ()
          {
               $('button[id=btn_new]').addClass('hide');
               handleDataTablesLocal('#ajax_content');
               $('a.buttons-excel').hide();
               $('.buttons-select-all').hide();
               $('.buttons-select-none').hide();
          }, 10);

          $(".btn-pop-modal").off('click').on('click', function (e) {
                  var targetID = $(this).closest('tr').find("input:hidden:first").val();
                  getData(targetID);
                  //$('#modal-detail').modal('toggle');
          })
     });

     function getData(targetID)
     {
             // var url = get_base_url() + 'ticketClients/getCaseDetail';
             var url = get_base_url() + get_uri() + 'Survey_scripts/getData';
             var data = "scriptsID=" + targetID;

             $('#scriptsText').val('');
             $('#scriptsTextEn').val('');
             
             $.ajax({
                    type: 'POST',
                    cache: false,
                    async: false,
                    url: url,
                    data: data,
                    beforeSend: function () {
                    },
                    success: function (result) {
                        //console.log(result); return false;

                        var data = $.parseJSON(result);
                        $('#scriptsType').select2();
                        $('#scriptsText').val(data['scriptsText']);
                        $('#scriptsTextEn').val(data['scriptsTextEn']);
                    }
             });
             
             $('#modal-detail').modal('toggle');
     }
</script>
<div class="row">
     <div class="col-md-12">
          <?php
               $o = $table;
               print $this->mcl->widget($this->mcl->gl($t['widget_header']), $o);
          ?>
     </div>
</div>
<div aria-hidden="true" aria-labelledby="mySmallModalLabel" class="modal fade bd-example-modal-lg" role="dialog"  tabindex="-1" id="modal-detail">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?= $this->mcl->gl('scripts_detail') ?></h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                         <span aria-hidden="true"> &times;</span>
                    </button>
               </div>
               <div class="modal-body">
                    <form>
                    <div class="form-group">
                         <input type="hidden" class="form-control" id="hidden_scriptsID" name="hidden_scriptsID">
                    </div>
                    <div class="form-group">
                         <label for="scriptsType"><?= $this->mcl->gl('scriptsType'); ?></label>
                         <select name="scriptsType" id="scriptsType" class="select2 col-sm-12">
                                   <option value="1">Greeting</option>
                                   <option value="2">Ending</option>
                                   <option value="3">Terminate</option>
                         </select>
                    </div>
                    <div class="form-group">
                         <label for=""><?= $this->mcl->gl('scriptsText') ?></label>
                         <textarea class="form-control" rows="3" id="scriptsText" name="scriptsText" ></textarea>
                    </div>
                    <div class="form-group">
                         <label for=""><?= $this->mcl->gl('scriptsTextEn') ?></label>
                         <textarea class="form-control" rows="3" id="scriptsTextEn" name="scriptsTextEn" ></textarea>
                    </div>
                    </form>
               </div>
               <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal"
                    type="button"><?= $this->mcl->gl('close') ?></button>
                    <button class="btn btn-primary btn-save-answer" type="button"><?= $this->mcl->gl('submit') ?></button>
               </div>
          </div>
     </div>
</div>
