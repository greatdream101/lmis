<?
$oo = '';
if (isset($t_value[0]))
{
        $hid_surveyID = $t_value[0]['surveyID'];
}
else
{
        $hid_surveyID = 0;
}
$oo .= '<input type="hidden" name="surveyID" id="surveyID" value="' . $hid_surveyID . '" />';

foreach ($a as $k => $v) {
        $oo .= '<div id="' . $k . '" class="hide section-header">' . $v['name'] . '</div>';
        $o = '';

        $o .= '<div class="section-area" section_id="' . $k . '">';
        $val_key = 0;
        foreach ($v['questions'] as $kk => $vv) {
                //jumpFrom
                $jump_keys1 = array_keys(array_column($t_value, 'jt_1'), $vv['questionID']);
                if ((isset($jump_keys1)) && (!empty($jump_keys1))) {
                        $jumpFrom_1 = $t_value[$jump_keys1[0]]['questionID'];
                }

                $jump_keys2 = array_keys(array_column($t_value, 'jt_2'), $vv['questionID']);
                if ((isset($jump_keys2)) && (!empty($jump_keys2))) {
                        $jumpFrom_2 = $t_value[$jump_keys2[0]]['questionID'];
                }

                if (!empty($jumpFrom_1)) {
                        $hidJumpFrom = $jumpFrom_1;
                } else if (!empty($jumpFrom_2)) {
                        $hidJumpFrom = $jumpFrom_2;
                } else {
                        $hidJumpFrom = "";
                }
                // vd::d($hidJumpFrom);// exit;
                //Get jumpFrom

                // $o .= '<div class="card-box question-area" questionID="' . $vv['questionID'] . '" hidJumpFrom="' . $hidJumpFrom . '">';
                $o .= '<div class="card-box question-area" questionID="' . $vv['questionID'] . '" requiredAnswer = "' .$vv['requiredAnswer']. '" hidJumpFrom="' . $hidJumpFrom . '">';
                $sortOrder = is_null($vv['sortOrder']) ? $vv['questionID'] : $vv['sortOrder'];
                $sortOrder = str_pad($sortOrder, 5, '0', STR_PAD_LEFT);
                $o .= '<div class="question-header">';
                if ($vv['parentID'] == 0) {
                        $o .= "<p>" . $vv['name'] . "</p>";
                } else {
                        $o .= "<p>" . $vv['name'] . "</p>";
                }
                $o .= '</div>';
                $o .= '<div class="answer-area" answerID="' . $vv['questionID'] . '">';

                $keys = array_keys(array_column($t_value, 'questionID'), $vv['questionID']);
                if ((isset($keys)) && (!empty($keys))) {
                        $valueNet = $t_value[$keys[0]]['valueNet'];
                        // vd::d($valueNet);
                }
                else
                {
                        $valueNet = "";
                }

                foreach ($vv['children'] as $kkk => $vvv) {
                        $sortOrder = is_null($vvv['sortOrder']) ? $vv['questionID'] : $vvv['sortOrder'];
                        $sortOrder = str_pad($sortOrder, 5, '0', STR_PAD_LEFT);
//vd::d($vvv);
                        $questionID = $vv['questionID'];

                        switch ($vvv['controlID']) {
                                case 1:

                                        $o .= $this->mcl->tan($vvv['code'], '', array('class' => 'no-description no-label', 'type' => 'textarea'
                                            , 'jumpTo' => $vvv['jumpTo'], 'jumpFrom' => $vvv['jumpFrom'], 'value' => $valueNet));
                                        // var_dump($t['edit_data']);
                                        // $o .= $this->mcl->tan($vvv['code'], $t, array('class' => 'no-description no-label disabled'));
                                        break;
                                case 2:

                                        $name = $vvv['code'];
                                        $val = $vvv['value'];
                                        $label = $vvv['name'];

                                        $o .= '<div class="checkbox">
                                        <input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="checkbox"
                                        jumpTo="' . $vvv['jumpTo'] . '" jumpFrom="' . $vvv['jumpFrom'] . '"
                                        showRemark="' . $vvv['showRemark'] . '" showSelect="' . $vvv['showSelect'] . '">
                                        <label for="' . $name . '"> ' . $label . ' </label>
                                        </div>';
                                        break;
                                case 3:
                                        // $name = $vvv['code'];
                                        $name = $vv['code'];
                                        // $radioID = $vvv['code'];   // Edit by Ja - Bug - Code Duplicate from admin
                                        $radioID = $name . "_" . $vvv['questionID'];
                                        $val = $vvv['value'];
                                        $label = $vvv['name'];
                                        // vd::d($val);exit;
                                        // Comment by Ja - 2018-05-28
                                        // $o .= '<div class="radio">
                                        // <input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="radio" jumpTo="' . $vvv['jumpTo'] . '" jumpFrom="' . $vvv['jumpFrom'] . '" showRemark="' . $vvv['showRemark'] . '" showSelect="' . $vvv['showSelect'] . '">
                                        // <label for="' . $name . '"> ' . $label . ' </label>
                                        // </div>';
                                        // $o .= '<div class="radio">
                                        //                <input id="' . $radioID . '" name="' . $name . '" value="' . $val . '" type="radio" jumpTo="' . $vvv['jumpTo'] . '" jumpFrom="' . $vvv['jumpFrom'] . '" showRemark="' . $vvv['showRemark'] . '" showSelect="' . $vvv['showSelect'] . '">
                                        //                <label for="' . $radioID . '"> ' . $label . ' </label>
                                        //           </div>';

                                        $o .= '<div class="radio">
                                                       <input id="' . $radioID . '" name="' . $name . '" value="' . $val . '" type="radio" jumpTo="' . $vvv['jumpTo'] . '" jumpFrom="' . $vvv['jumpFrom'] . '" showRemark="' . $vvv['showRemark'] . '" showSelect="' . $vvv['showSelect'] . '" ';

                                        if ((isset($keys)) && (!empty($keys))) {
                                                if ($val == $valueNet) {
                                                        $o .= ' checked ';
                                                }
                                        }
                                        $o .= '             >
                                                       <label for="' . $radioID . '"> ' . $label . ' </label>
                                                  </div>';
                                        break;
                                case 4:
                                        $val = explode('|', $vvv['value']);
                                        $label = explode('|', $vvv['option']);
                                        if (count($val) == count($label)) {
                                                $o .= '<select name="select1" id="select1" class="select2">';
                                                foreach ($label as $k1 => $v1) {
                                                        $o .= '<option value=' . $val[$k1] . '>' . $label[$k1] . '</option>';
                                                }
                                                $o .= '</select>';
                                        }
                                        break;
                                case 5:
                                        $o .= $this->mcl->tb($vvv['name'], '', array('class' => 'no-description no-label disabled'));
                                        break;
                                case 6://ten star
                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                        for ($i = 1; $i <= 10; $i++) {
                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                        }
                                        $o .= '</select>';
                                        break;
                                case 7://eleven star
                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                        for ($i = 0; $i <= 10; $i++) {
                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                        }
                                        $o .= '</select>';
                                        break;
//                                case 9://upload image
//                                        $o .= $this->mcl->fi('avatar', "", array('class' => 'no-cookie image no-label', 'path' => 'avatarPath'));
//                                        $o .= $this->mcl->hd('valueid', "");
                                //$o .= '<div hidden>';
                                //$o .= '<input id=btn_submit_'.$kkk.' class="btn_submit" type="submit" name="submit" value="submit">';
                                //$o .= '</div>';
//                                        break;
                        }
                }
                $o .= '</div>';
                $o .= $this->mcl->hr('full-width');
//vd::d($vv);
                if ((int) $vv['uploadMax'] > 0 && (int) $vv['uploadMin'] < (int) $vv['uploadMax']) {
                        $i = 1;
                        $width = floor(12 / $vv['uploadMax']);

                        if ($width < 2)
                                $width = 2;
                        $o .= $this->mcl->hdr('upload_picture');
                        $o .= '<div class="uploadMinAmount">' . $this->mcl->gl('upload_min_amount') . ': ' . $vv['uploadMin'] . '</div>';

                        $o .= '<div class="uploadMaxAmount">' . $this->mcl->gl('upload_max_amount') . ': ' . $vv['uploadMax'] . '</div>';

                        $o .= '<div class="row">';
                        for ($i = 1; $i <= $vv['uploadMax']; $i++) {
                                $o .= '<div class="col-lg-' . $width . ' col-xs-' . $width . '">';
                                $name = 'name_' . $vv['questionID'] . '_' . $i;

                                $keys = array_keys(array_column($t_value, 'controlName'), $name);
                                if ((isset($keys)) && (!empty($keys))) {
                                        $img = $this->question_mod->get_image($t_value[$keys[0]]['valueID'], $name);
                                        $img = json_decode($img, true);
                                        //vd::d($img[0]['filePath']);
                                        $path = base_url().$img[0]['filePath'].'/'.$img[0]['code'].'.'.$img[0]['extensionFile'];
                                        $o .= $this->mcl->fi($name, array(), array('class' => 'image no-label','img'=>$path));
                                        //$o .= $this->mcl->hdr($t_value[$keys[0]]['valueID']);
                                }else{
                                        $o .= $this->mcl->fi($name, array(), array('class' => 'image no-label'));
                                }


                                $o .= '</div>';
                        }

                        $o .= '</div>';
                        $o .= $this->mcl->hd('uploadMinAmount', $vv['uploadMin']);
                        $o .= $this->mcl->hd('uploadMaxAmount', $vv['uploadMax']);
                        $o .= $this->mcl->hr('full-width');
                }

                $o .= $this->mcl->bt('back', '', array('class' => 'btn_jump_back'));
                $o .= $this->mcl->bt('next', '', array('class' => 'btn_jump_to'));

                $o .= '</div>';
        }
        $o .= '</div>';

        $oo .= $o;
}
$oo .= $this->mcl->div('question_area', '', 'full-width');
$oo .= $this->mcl->hd('seriesID',$seriesID);

print $oo;
?>