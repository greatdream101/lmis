<?
$tab_1 = array();
array_push($tab_1, $this->mcl->hdr('questionnaire', 'full-width'));
array_push($tab_1, $this->mcl->div('scriptsGreeting', $data['scriptsGreeting'], 'full-width'));
array_push($tab_1, $this->mcl->div('question_answer_area', $data['question_info'], 'full-width'));
array_push($tab_1, $this->mcl->div('question_area', '', 'full-width'));
array_push($tab_1, $this->mcl->div('scriptsEnding', $data['scriptsEnding'], 'full-width hide'));
array_push($tab_1, $this->mcl->bt('save', 'save', array('class' => 'footer search', 'tab' => 'tab_1')));
$o = $this->mcl->input_page(array($tab_1), $t);
print $o;
?>

<script language="javascript">
     $(document).ready(function () {
          setTimeout(function ()
          {
               $('button[id=btn_new]').addClass('hide');
               //handleDataTablesLocal('#ajax_content');
               $('a.buttons-excel').hide();
               $('.buttons-select-all').hide();
               $('.buttons-select-none').hide();
               $('#btn_save').hide();
          }, 10);

          setTimeout(function () {
               // handleShowQuestion();
               handleShowScripts();
               handleNextButton();
               handleBackButton();
          }, 50);

          $('.btn-begin-survey').off('click').on('click', function (e) {
               $('#question_answer_area').removeClass('hide');
               $('#scriptsGreeting').addClass('hide');
               handleShowQuestion();
          })

          $('.btn-end-survey').off('click').on('click', function (e) {
               location.reload();
          })

          var handleShowScripts = function () {
               if($('#scriptsGreeting').hasClass('hide'))
               {
                    if($('#scriptsEnding').is(':empty'))
                    {
                         alert('<?=$this->mcl->gl('end_survey');?>');
                         location.reload();
                    }
                    else
                    {
                         $('#question_answer_area').addClass('hide');
                         $('#scriptsEnding').removeClass('hide');
                    }
               }
               else
               {
                    if($('#scriptsGreeting').is(':empty'))
                    {
                         $('#question_answer_area').removeClass('hide');
                         handleShowQuestion();
                    }
                    else
                    {
                         $('#question_answer_area').addClass('hide');
                    }
               }
          }

          var handleShowQuestion = function ()
          {
               $('.question-area:visible').find('.btn_jump_back').first().hide();
               var pageURI = attr($(cc).find('input[type=hidden][name=uri]').val());
               {
                    if (pageURI != 'survey_conduct')
                    {
                         return false;
                    }
               }

               var branchID = attr($(cc).find('input[type=hidden][name=branchID]').val());
               var seriesID = attr($(cc).find('input[type=hidden][name=seriesID]').val());

               var data = 'branchID=' + branchID;
               data += '&seriesID=' + seriesID;
               var url = get_base_url() + get_uri() + '/get_current_question';
               $.ajax({
                    type: 'POST',
                    cache: false,
                    async: false,
                    url: url,
                    data: data,
                    beforeSend: function () {
                    },
                    success: function (result) {
                         // console.log(questionid);
                         // console.log(result);
                         setTimeout(function () {
                              $('.question-area').hide();
                              var jsonResult = eval("(" + result + ")");

                              if (jsonResult.currentQuestion == 0)
                              {
                                   $('#question_answer_area').find('.question-area').first().show();
                                   $('.question-area:visible').find('.btn_jump_back').hide();
                                   var section_id = $('div.section-header:first').attr('id');
                                   handleShowSection(section_id);
                              }
                              else
                              {
                                   $('.question-area:visible').attr('hidJumpFrom', jsonResult.hidJumpFrom);
                                   var section_id = parseInt($('div[questionid=' + jsonResult.currentQuestion + ']').closest('.section-area').attr('section_id'), 0);

                                   handleShowSection(section_id);
                                   $('[questionid=' + jsonResult.currentQuestion + ']').fadeIn('slow').show();
                              }
                         }, 0);
                    }
               });
          }

          var handleShowSection = function (section_id)
          {
               $('.section-header').addClass('hide');
               $('.section-header').each(function () {
                    var s_id = parseInt($(this).attr('id'), 0);

                    if (s_id == section_id) {
                         $(this).fadeIn('slow').removeClass('hide');
                         return;
                    }
               });
          }

           var isPassValidateUploadFileAmount = function (area) {
                   var uploadMinAmount = attr($(area).find('input[type=hidden][name=uploadMinAmount]').val(), 0);
                   var uploadMaxAmount = attr($(area).find('input[type=hidden][name=uploadMaxAmount]').val(), 0);
                   var a = 0;
                   $('a.fileinput-exists').each(function () {
                           if ($(this).is(':visible'))
                                   a++;
                   });

                   if (a >= uploadMinAmount && a <= uploadMaxAmount) {
                           return true;
                   } else {
                           return false;
                   }
          }

          var handleNextButton = function () {
                   $('.btn_jump_to').on('click', function (e) {
                           var area = $(this).closest('.question-area');
                           if (isPassValidateUploadFileAmount(area)) {
                                   var questionid = $('.question-area:visible').attr('questionid');
                                   var inputtype = $('.question-area:visible').find('input:visible').attr('type');
                                   var inputname = $('.question-area:visible').find('input:visible').attr('name');

                                   if (typeof inputtype === 'undefined')
                                   {
                                           inputtype = $('.question-area:visible').find('textarea:visible').attr('type');
                                           inputname = $('.question-area:visible').find('textarea:visible').attr('name');
                                   }

                                   if (inputtype == 'radio')
                                   {
                                           if (($('.question-area:visible').find('input:visible:checked').length) < 1)
                                           {
                                                   alert('<?= $this->mcl->gl('please_select_choice') ?>');
                                                   return false;
                                           } else
                                           {
                                                   var jumpTo = $('.question-area:visible').find('input:visible:checked').attr('jumpTo');
                                                   var valueNet = $('input[name=' + inputname + ']:checked').val();
                                           }
                                   } else if (inputtype == 'textarea')
                                   {
                                           if ( ($('.question-area:visible').find('textarea:visible').val().trim().length<1) )
                                           {
                                                   alert('<?= $this->mcl->gl('please_investigate_the_error') ?>');
                                                   return false;
                                           } else
                                           {
                                                   var jumpTo = $('.question-area:visible').find('textarea:visible').attr('jumpTo');
                                                   var valueNet = $('textarea[name=' + inputname + ']').val();
                                           }
                                   } else
                                   {
                                           var jumpTo = $('.question-area:visible').find('input:visible').attr('jumpTo');
                                   }
/*
                                   var data = get_data_serialize('.popupwindow_container');
                                   data += '&uri=' + $(cc).find('input[type=hidden][name=uri]').val();
                                   data += '&surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
                                   data += '&branchID=' + $(cc).find('input[type=hidden][name=branchID]').val();
                                   data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                                   data += '&questionID=' + questionid;
                                   data += '&valueNet=' + valueNet;

                                   $.ajax({
                                           type: "POST",
                                           url: get_base_url() + get_uri() + '/save_answer',
                                           cache: false,
                                           async: false,
                                           data: data,
                                           beforeSend: function () {

                                           },
                                           success: function (result) {
                                                   // alert(result);
                                                   //debugger;
                                                   var jsonResult = eval("(" + result + ")");
                                                   if (jsonResult.surveyID > 0)
                                                   {
                                                           $('input:hidden[name=surveyID]').val(jsonResult.surveyID);
                                                   }

                                                   if (jsonResult.valueID > 0)
                                                   {
                                                           $('input:hidden[name=valueid]').val(jsonResult.valueID);
                                                           //debugger;
                                                           if (window.File && window.FileReader && window.FileList && window.Blob)
                                                           {
                                                                   //document.getElementById('avatar').onchange = function(){
                                                                   var xhr = new XMLHttpRequest();
                                                                   xhr.onreadystatechange = function (ev) {
                                                                           document.getElementById('filesInfo').innerHTML = 'Done!';
                                                                   };

                                                                   $('a.fileinput-exists').each(function () {
                                                                           //debugger;
                                                                           xhr.open('POST', get_base_url() + get_uri() + '/upload_attachments', true);
                                                                           var form = new FormData();
                                                                           form.append('valueid', jsonResult.valueID);
                                                                           var id = $(this).closest('.form-group').find('input[type=file]').attr('id');
                                                                           form.append('controlName', id);
                                                                           var files = document.getElementById(id).files;
                                                                           for (var i = 0; i < files.length; i++)
                                                                                   form.append('file' + i, files[i]);
                                                                           xhr.send(form);
                                                                   });
                                                           }
                                                           var section_id = parseInt($('div[questionid=' + jumpTo + ']').closest('.section-area').attr('section_id'), 0);

                                                           handleShowSection(section_id);
                                                   }
                                           }
                                   });
*/
                                   // return false;
                                   // $('.question-area:visible').hide();
                                   // $('[questionid=' + jumpTo + ']').show();
                                   // $('.question-area:visible').attr('hidJumpFrom', questionid);

                                   // temp
                                   var section_id = parseInt($('div[questionid=' + jumpTo + ']').closest('.section-area').attr('section_id'), 0);
                                   handleShowSection(section_id);
                                   // temp

                                   // Check Is Last Question ?
                                   if(jumpTo<0)
                                   {
                                        handleShowScripts();
                                   }
                                   else
                                   {
                                        $('.question-area:visible').hide();
                                        $('[questionid=' + jumpTo + ']').show();
                                        $('.question-area:visible').attr('hidJumpFrom', questionid);
                                   }
                           }
                           else
                           {
                                   var uploadMinAmount = attr($(area).find('input[type=hidden][name=uploadMinAmount]').val(), 0);
                                   var uploadMaxAmount = attr($(area).find('input[type=hidden][name=uploadMaxAmount]').val(), 0);
                                   alert(locale['you_have_to_upload_at_least'] + ': ' + uploadMinAmount);
                           }
                   })
          }

          var handleBackButton = function () {
               $('.btn_jump_back').on('click', function (e) {
                    $('.btn_jump_to').show();
                    var jumpFrom = $('.question-area:visible').attr('hidJumpFrom');
                    if (jumpFrom > 0)
                    {
                         $('.question-area:visible').hide();
                         $('[questionid=' + jumpFrom + ']').show();
                    }
                    else
                    {
                         var jumpFrom = $('.question-area:visible').find('input:visible').attr('jumpFrom');
                         $('.question-area:visible').hide();
                         $('[questionid=' + jumpFrom + ']').show();
                    }

                    var section_id = parseInt($('div[questionid=' + jumpFrom + ']').closest('.section-area').attr('section_id'), 0);

                    handleShowSection(section_id);
               })
           }
   });

   function previewAnswer()
   {
        data = 'surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
        var url = get_base_url() + get_uri() + '/preview_answer';
        $.ajax({
               type: 'POST',
               cache: false,
               async: false,
               url: url,
               data: data,
               beforeSend: function () {
               },
               success: function (result)
               {
                    $('#preview_area').html(result);
               }
        });
   }

   function confirm_save()
   {
        if(!confirm('<?=$this->mcl->gl('confirm_save');?>'))
        {
             return false;
        }
        $('#con-close-modal').modal('toggle');

        data = 'surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
        data += '&qcStatusID=1';
        var url = get_base_url() + get_uri() + '/update_qc_status';
        $.ajax({
               type: 'POST',
               cache: false,
               async: false,
               url: url,
               data: data,
               beforeSend: function () {
               },
               success: function (result)
               {
                    if(result>0)
                    {
                         alert('<?=$this->mcl->gl('save_data_complete');?>');
                         window.location.reload(get_base_url() +'index#/'+ get_uri());
                    }
               }
        });
   }
</script>
