<script src="<?php echo(base_url());?>assets/js/jquery-1.12.4.min.js"></script>
<script src="<?php echo(base_url());?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo(base_url());?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo(base_url());?>assets/js/detect.js"></script>
<script src="<?php echo(base_url());?>assets/js/fastclick.js"></script>
<script src="<?php echo(base_url());?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo(base_url());?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo(base_url());?>assets/js/waves.js"></script>
<script src="<?php echo(base_url());?>assets/js/wow.min.js"></script>
<script src="<?php echo(base_url());?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo(base_url());?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/switchery/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/layout-grid/dist/js/layout-grid.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/printThis/printThis.js"></script>

<script src="<?php echo(base_url());?>assets/plugins/toastr/toastr.min.js"></script>

<script src="<?php echo(base_url());?>assets/plugins/bootstrap-datatables/datatables.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/bootstrap-datatables/reloadAjax.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/bootstrap-datatables/fakeRowSpan.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/jquery-cookie/jquery.cookie.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/summernote/dist/summernote.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/html-clean/jquery.htmlClean.js"></script>

<script src="<?php echo(base_url());?>assets/plugins/jquery-sortable/jquery.tablesorter.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/jquery-sortable/jquery.tablesorter.widgets.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/jquery-sortable/jquery.tablesorter.pager.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/jquery-sortable/jquery.sortable.js"></script>

<script src="<?php echo(base_url());?>assets/plugins/select2/select2.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/popupwindow/popupwindow.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/jquery-countable/jquery.simplyCountable.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/xlsx.core.min.js" type="text/javascript"></script>
<script src="<?php echo(base_url());?>assets/plugins/moment-develop/min/moment.min.js" type="text/javascript"></script>
<script src="<?php echo(base_url());?>assets/plugins/moment-develop/min/locales.min.js" type="text/javascript"></script>

<script src="<?php echo(base_url());?>assets/plugins/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="<?php echo(base_url());?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script src="<?php echo(base_url());?>assets/plugins/jquery.timeelapse.min.js"></script>

<script src="<?php echo(base_url());?>assets/plugins/smartwidgets/jarvis.widget.min.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/smart-wizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/highcharts/js/highcharts.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/highcharts/js/modules/exporting.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/highcharts/js/export-csv.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/jquery.highchartTable-min.js"></script>
<!-- <script src="<?php echo(base_url());?>assets/js/jquery.app.js"></script> -->
<script type="text/javascript" src="<?php echo(base_url());?>assets/plugins/d3.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>assets/plugins/c3.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/pivottable-master/dist/pivot.js"></script>
<script src="<?php echo(base_url());?>assets/plugins/pivottable-master/dist/c3_renderers.js"></script>
<script src="<?php echo(base_url());?>assets/js/app.js"></script>
