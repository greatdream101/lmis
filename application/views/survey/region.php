<?php

$tab_1 = array();
array_push($tab_1, $this->mcl->tb('code', $t));
array_push($tab_1, $this->mcl->tb('name', $t, array('rules' => 'trim|required')));
array_push($tab_1, $this->mcl->tb('nameEN', $t));

$tab_2 = array();
array_push($tab_2, $this->mcl->tc('branchID', $t, 't_regionBranch'));
echo $this->mcl->input_page(array($tab_1, $tab_2), $t);
?>