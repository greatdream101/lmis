<?= $this->load->view('survey/loadScript.php', array(), true) ?>
<?= $this->load->view('survey/loadStyle.php', array(), true) ?>

<?

$a = '';
$a = '
<div class="section-area" section_id="1" style="display: block;">
    <div id="1" class="section-header" next_section="2" prev_section="0">ข้อมูลผู้ทำแบบสอบถาม</div>
    <div class="card-box " questionid="1" requiredanswer="1" hidjumpfrom="">
        <div class="" answerid="1">
            <div class="row">
                <div class="form-group clearfix">
                    <label class="col-md-3 control-label " for="fname">'.$this->mcl->gl('name').'</label>
                    <div class="col-md-9">
                        <input class="form-control required" id="fname" name="fname" type="text">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-3 control-label " for="lname">'.$this->mcl->gl('surname').'</label>
                    <div class="col-md-9">
                        <input id="lname" name="lname" type="text" class="required form-control">
            
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-3 control-label " for="email">'.$this->mcl->gl('email').'</label>
                    <div class="col-md-9">
                        <input id="email" name="email" type="text" class="required form-control">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-3 control-label " for="phone">'.$this->mcl->gl('phone').'</label>
                    <div class="col-md-9">
                        <input id="phone" name="phone" type="text" class="required form-control">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-3 control-label " for="age">'.$this->mcl->gl('age').'</label>
                    <div class="col-md-9">
                        <input id="age" name="age" type="text" class="required form-control">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-3 control-label " for="confirm">'.$this->mcl->gl('product').'</label>
                    <div class="col-md-9">
                        <select class="form-control" name="product">
                            <option>กรุณาเลือกสินค้า</option>
                            <option>Product 1</option>
                            <option>Product 2</option>
                            <option>Product 3</option>
                            <option>Product 4</option>
                            <option>Product 5</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
';



$o = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
$o.='<div class="topbar">
	<!-- LOGO -->
        <div class="topbar-left">
                <div class="text-center ">			
            </div>
        </div>
		<!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
                <div class="container">
                        <div class="">
                                <div class="pull-left">
                                <img src='.base_url().'assets/img/aia1.png class="logo aia-logo"> 
                                        <span class="clearfix"></span>
                                </div>
                                <ul class="nav navbar-nav navbar-right pull-right">
										
                                </ul>
                        </div>
                        <!--/.nav-collapse -->
                </div>
        </div>
</div>';
$o.=$this->mcl->hdr('questionnaire', 'full-width');
$o.=$this->mcl->hd('cusID', $t['cusID']);
$o.=$this->mcl->hd('surveyStatus', $t['surveyStatus']);
$o.=$this->mcl->div('customerInfo', $a, 'full-width');
$o.=$this->mcl->div('scriptsGreeting', $data['scriptsGreeting'], 'full-width');
$o.=$this->mcl->div('question_answer_area', $data['question_info'], 'full-width');
$o.=$this->mcl->div('question_area', '', 'full-width');
$o.=$this->mcl->div('scriptsEnding', $data['scriptsEnding'], 'full-width hide');
print $o;
?>

<script language="javascript">
    $(document).ready(function () {

        setTimeout(function () {
            handleShowScripts();
            handleNextButton();
            handleBackButton();
            handleSaveInput();
        }, 50);

        $('.btn-begin-survey').off('click').on('click', function (e) {
            $('#question_answer_area').removeClass('hide');
            $('#scriptsGreeting').addClass('hide');
            $('#customerInfo').addClass('hide');
            handleShowQuestion();
        })

        $('.btn-end-survey').off('click').on('click', function (e) {
            location.reload();
        })

        var handleShowQuestion = function ()
        {
            $('.section-area:visible').find('.btn_jump_to').last().attr('id','btn_last').text('<?=$this->mcl->gl('survey_save');?>');
            $('.section-area:visible').find('.btn_jump_back').first().hide();
            // $('.question-area').hide();
            $('#question_answer_area').find('.question-area').first().show();
            var section_id = $('div.section-header:first').attr('id');
            handleShowSection(section_id);
        }

        var handleShowSection = function (section_id)
        {
            $('.section-area').addClass('hide');
            $('.section-area').each(function () {
                var s_id = parseInt($(this).attr('section_id'), 0);

                if (s_id == section_id) {
                    $(this).fadeIn('slow').removeClass('hide');
                    $(this).removeClass('hide');
                }
            });
        }

        var handleNextButton = function () {
            $('.btn_jump_to').on('click', function (e)
            {
                var inputtype = $('.question-area:visible').find('input:visible').attr('type');
                var countQuestion = $('.question-area:visible').length;
                var countAnswer = $('.question-area:visible').find('input:visible:checked').length;

                if (typeof inputtype === 'undefined')
                {
                    inputtype = $('.question-area:visible').find('textarea:visible').attr('type');
                    inputname = $('.question-area:visible').find('textarea:visible').attr('name');
                }

                if (inputtype == 'radio')
                {
                    if(countAnswer < countQuestion)
                    {
                        alert('<?= $this->mcl->gl('please_answer_all_question') ?>');
                        return false;
                    }
                }

                var next_section = $('.section-header:visible').attr('next_section');
                if (next_section < 0)
                {
                    confirm_save();
                }
                else
                {
                    handleShowSection(next_section);
                }
            })
        }

        var handleBackButton = function () {
            $('.btn_jump_back').on('click', function (e)
            {
                var prev_section = $('.section-header:visible').attr('prev_section');
                handleShowSection(prev_section);
                // }
            })
        }

        var handleSaveInput = function ()
        {
            $('.save-input').each(function (k,v)
            {
                $(this).on('change',function (e){
                    var questionid = $(v).closest('div.question-area').attr('questionid');
                    var inputtype = $(v).attr('type');
                    var inputname = $(v).attr('name');
                    var jumpTo = $(v).attr('jumpTo');

                    if (typeof inputtype === 'undefined')
                    {
                        inputtype = $(v).attr('type');
                        inputname = $(v).attr('name');
                    }

                    if (inputtype == 'radio')
                    {
                        var valueNet = $('input[name=' + inputname + ']:checked').val();
                    }
                    else if (inputtype == 'textarea')
                    {
                        var valueNet = $('textarea[name=' + inputname + ']').val();
                    }

                    // alert('inputtype : '+inputtype);
                    // alert('inputname : '+inputname);
                    // alert('jumpTo : '+jumpTo);
                    // alert('valueNet : '+valueNet);

                    data = 'cusID=' + $(cc).find('input[type=hidden][name=cusID]').val();
                    data += '&surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
                    data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                    data += '&questionID=' + questionid;
                    data += '&valueNet=' + valueNet;

                    var url = get_base_url() + '<?=$this->router->class;?>' + '/save_answer';
                    // alert(data);return false;

                    $.ajax({
                        type: "POST",
                        url: url,
                        cache: false,
                        async: false,
                        data: data,
                        beforeSend: function () {

                        },
                        success: function (result) {
                            // alert(result);return false;
                            //debugger;
                            var jsonResult = eval("(" + result + ")");
                            if (jsonResult.surveyID > 0)
                            {
                                $('input:hidden[name=surveyID]').val(jsonResult.surveyID);
                            }

                            if (jsonResult.valueID > 0)
                            {
                                $('input:hidden[name=valueid]').val(jsonResult.valueID);
                            }
                        }
                    });
                })
            })
        }

        $('#btn_last').on('click', function (e) {
            confirm_save();
        })
    });

    function handleShowScripts()
    {
        if ($('#surveyStatus').val() != 'completed')
        {
            $('#scriptsGreeting').addClass('hide');
            $('#customerInfo').addClass('hide');

            if ($('#scriptsEnding').is(':empty'))
            {
                alert('<?= $this->mcl->gl('end_survey'); ?>');
                // location.reload();
            } else
            {
                $('#question_answer_area').addClass('hide');
                $('#scriptsEnding').removeClass('hide');
            }

            return false;
        }

        if ($('#scriptsGreeting').hasClass('hide'))
        {
            if ($('#scriptsEnding').is(':empty'))
            {
                alert('<?= $this->mcl->gl('end_survey'); ?>');
                // location.reload();
            } else
            {
                $('#question_answer_area').addClass('hide');
                $('#scriptsEnding').removeClass('hide');
            }
        } else
        {
            if ($('#scriptsGreeting').is(':empty'))
            {
                $('#question_answer_area').removeClass('hide');
                handleShowQuestion();
            } else
            {
                $('#question_answer_area').addClass('hide');
            }
        }
    }

    function confirm_save()
    {
        if(!confirm('<?=$this->mcl->gl('confirm_save');?>'))
        {
             return false;
        }

        data = 'surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
        data += '&qcStatusID=2';
        var url = get_base_url() + '<?=$this->router->class;?>' + '/update_qc_status';

        $.ajax({
            type: 'POST',
            cache: false,
            async: false,
            url: url,
            data: data,
            beforeSend: function () {
            },
            success: function (result)
            {
                if (result > 0)
                {
                    alert('<?= $this->mcl->gl('save_data_complete'); ?>');
                    handleShowScripts();
                }
            }
        });
    }
</script>
