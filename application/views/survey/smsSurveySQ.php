<?
$oo = '';
if (isset($t_value[0]))
{
        $hid_surveyID = $t_value[0]['surveyID'];
}
else
{
        $hid_surveyID = 0;
}
$oo .= '<input type="hidden" name="surveyID" id="surveyID" value="' . $hid_surveyID . '" />';
// vd::d($a);exit();
$current_loop = 1;

foreach ($a as $k => $v) {

        // $oo .= '<div id="' . $k . '" class="section-header">' . $v['name'] . '</div>';
        $o = '';

        if(array_key_exists($current_loop-1,$a)) {
            $prev_section = $a[$current_loop-1]['sectionID'];
        } else {
            $prev_section = '0';
        }

        if(array_key_exists($current_loop+1,$a)) {
            $next_section = $a[$current_loop+1]['sectionID'];
        } else {
            $next_section = '-1';
        }

        $current_loop++;

        $o .= '<div class="section-area" section_id="' . $k . '">';
        $o .= '<div id="' . $k . '" class="section-header" next_section="' . $next_section . '" prev_section="' . $prev_section . '">' . $v['name'] . '</div>';

        $val_key = 0;
        foreach ($v['questions'] as $kk => $vv) {
                //jumpFrom
                $jump_keys1 = array_keys(array_column($t_value, 'jt_1'), $vv['questionID']);
                if ((isset($jump_keys1)) && (!empty($jump_keys1))) {
                        $jumpFrom_1 = $t_value[$jump_keys1[0]]['questionID'];
                }

                $jump_keys2 = array_keys(array_column($t_value, 'jt_2'), $vv['questionID']);
                if ((isset($jump_keys2)) && (!empty($jump_keys2))) {
                        $jumpFrom_2 = $t_value[$jump_keys2[0]]['questionID'];
                }

                if (!empty($jumpFrom_1)) {
                        $hidJumpFrom = $jumpFrom_1;
                } else if (!empty($jumpFrom_2)) {
                        $hidJumpFrom = $jumpFrom_2;
                } else {
                        $hidJumpFrom = "";
                }

                $o .= '<div class="card-box question-area" questionID="' . $vv['questionID'] . '" requiredAnswer = "' .$vv['requiredAnswer']. '" hidJumpFrom="' . $hidJumpFrom . '">';
                $sortOrder = is_null($vv['sortOrder']) ? $vv['questionID'] : $vv['sortOrder'];
                $sortOrder = str_pad($sortOrder, 5, '0', STR_PAD_LEFT);
                $o .= '<div class="question-header">';
                if ($vv['parentID'] == 0) {
                        $o .= "<p>" . $vv['name'] . "</p>";
                } else {
                        $o .= "<p>" . $vv['name'] . "</p>";
                }
                $o .= '</div>';
                $o .= '<div class="answer-area" answerID="' . $vv['questionID'] . '">';

                $keys = array_keys(array_column($t_value, 'questionID'), $vv['questionID']);
                if ((isset($keys)) && (!empty($keys))) {
                        $valueNet = $t_value[$keys[0]]['valueNet'];
                        // vd::d($valueNet);
                }
                else
                {
                        $valueNet = "";
                }

                foreach ($vv['children'] as $kkk => $vvv) {
                        $sortOrder = is_null($vvv['sortOrder']) ? $vv['questionID'] : $vvv['sortOrder'];
                        $sortOrder = str_pad($sortOrder, 5, '0', STR_PAD_LEFT);
//vd::d($vvv);
                        $questionID = $vv['questionID'];

                        switch ($vvv['controlID']) {
                                case 1:

                                        $o .= $this->mcl->tan($vvv['code'], '', array('class' => 'no-description no-label save-input', 'type' => 'textarea'
                                            , 'jumpTo' => $vvv['jumpTo'], 'jumpFrom' => $vvv['jumpFrom'], 'value' => $valueNet));
                                        break;
                                case 2:

                                        $name = $vvv['code'];
                                        $val = $vvv['value'];
                                        $label = $vvv['name'];

                                        $o .= '<div class="checkbox">
                                        <input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="checkbox"
                                        jumpTo="' . $vvv['jumpTo'] . '" jumpFrom="' . $vvv['jumpFrom'] . '"
                                        showRemark="' . $vvv['showRemark'] . '" showSelect="' . $vvv['showSelect'] . '">
                                        <label for="' . $name . '"> ' . $label . ' </label>
                                        </div>';
                                        break;
                                case 3:
                                        $name = $vv['code'];
                                        $radioID = $name . "_" . $vvv['questionID'];
                                        $val = $vvv['value'];
                                        $label = $vvv['name'];

                                        $o .= '<div class="radio">
                                                       <input class="save-input" id="' . $radioID . '" name="' . $name . '" value="' . $val . '" type="radio" jumpTo="' . $vvv['jumpTo'] . '" jumpFrom="' . $vvv['jumpFrom'] . '" showRemark="' . $vvv['showRemark'] . '" showSelect="' . $vvv['showSelect'] . '" ';

                                        if ((isset($keys)) && (!empty($keys))) {
                                                if ($val == $valueNet) {
                                                        $o .= ' checked ';
                                                }
                                        }
                                        $o .= '             >
                                                       <label for="' . $radioID . '"> ' . $label . ' </label>
                                                  </div>';
                                        break;
                                case 4:
                                        $val = explode('|', $vvv['value']);
                                        $label = explode('|', $vvv['option']);
                                        if (count($val) == count($label)) {
                                                $o .= '<select name="select1" id="select1" class="select2">';
                                                foreach ($label as $k1 => $v1) {
                                                        $o .= '<option value=' . $val[$k1] . '>' . $label[$k1] . '</option>';
                                                }
                                                $o .= '</select>';
                                        }
                                        break;
                                case 5:
                                        $o .= $this->mcl->tb($vvv['name'], '', array('class' => 'no-description no-label disabled'));
                                        break;
                                case 6://ten star
                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                        for ($i = 1; $i <= 10; $i++) {
                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                        }
                                        $o .= '</select>';
                                        break;
                                case 7://eleven star
                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                        for ($i = 0; $i <= 10; $i++) {
                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                        }
                                        $o .= '</select>';
                                        break;
                        }
                }
                $o .= '</div>';
                // $o .= $this->mcl->hr('full-width');

                $o .= '</div>';
        }
        $o .= '<div class="card-box">';
            $o .= $this->mcl->bt('back', '', array('class' => 'btn_jump_back'));
            $o .= $this->mcl->bt('next', '', array('class' => 'btn_jump_to'));
        $o .= '</div>';

        $o .= '</div>';

        $oo .= $o;
}
// $oo .= 
// '<div class="card-box text-center">
//         <button id="btn_last" class="btn btn-default" type="submit">'.$this->mcl->gl('survey_save').'</button>
// </div>';
$oo .= $this->mcl->div('question_area', '', 'full-width');
$oo .= $this->mcl->hd('seriesID',$seriesID);

print $oo;
?>