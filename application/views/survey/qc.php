<?
$tab_1 = array();
array_push($tab_1, $this->mcl->hdr('questionnaire_qc', 'full-width'));
array_push($tab_1, $this->mcl->hd('surveyID',$t['surveyID']));
array_push($tab_1, $this->mcl->div('question_answer_area', '', 'full-width'));
array_push($tab_1, $this->mcl->div('preview_area', '', 'full-width'));
array_push($tab_1, $this->mcl->bt('preview', 'preview', array('class' => 'footer')));

$o = $this->mcl->input_page(array($tab_1), $t);
print $o;
?>
<script language="javascript">
     $(document).ready(function ()
     {
          setTimeout(function ()
          {
               get_questions();
          }, 10);

          $('#btn_preview').off('click').on('click', function (e) {
               previewAnswer();
          })
     });

     function get_questions()
     {
          var data = 'surveyID=' + attr($(cc).find('input[type=hidden][name=surveyID]').val());

          var url = get_base_url() + get_uri() + '/get_questions';
          $.ajax({
                  type: 'POST',
                  cache: false,
                  async: false,
                  url: url,
                  data: data,
                  beforeSend: function () {
                  },
                  success: function (html) {
                         $('#question_answer_area').empty().html(html);
                  }
          });
     }

     function previewAnswer()
     {
          data = 'surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
          var url = get_base_url() + 'Survey_conduct' + '/preview_answer';
          $.ajax({
                  type: 'POST',
                  cache: false,
                  async: false,
                  url: url,
                  data: data,
                  beforeSend: function () {
                  },
                  success: function (result)
                  {
                       $('#preview_area').html(result);
                       $('.preview_confirm').hide();
                  }
          });
     }
</script>
