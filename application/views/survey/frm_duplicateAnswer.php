<?php    

$t['schema_data']['schema']['numberOfDup']['max_length'] = 2;
$t['schema_data']['schema']['numberOfDup']['is_null'] = 'no';

$o = '';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->msb('duplicateTo', $t, array('class' => 'no-cookie required'));
$o .= '</div></div>';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->tb('numberOfDup', $t, array('class'=>'required is-number'));
$o .= '</div></div>';

$o .= '<div class=row><div class=col-md-12>';
$o .= $this->mcl->bt('duplicate_answer', 'Duplicate Answer', array('class' => 'btn btn-primary'));
$o .= form_hidden('questionID', isset($_POST['questionID']) ? $_POST['questionID'] : 0);
$o .= '</div></div>';

print $o;

							




