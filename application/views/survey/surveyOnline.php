<?= $this->load->view('survey/loadScript.php', array(), true) ?>
<?= $this->load->view('survey/loadStyle.php', array(), true) ?>
<?
//$tab_1 = array();
//array_push($tab_1, $this->mcl->hdr('questionnaire', 'full-width'));
//array_push($tab_1, $this->mcl->hd('cusID', $t['cusID']));
//array_push($tab_1, $this->mcl->hd('surveyStatus', $t['surveyStatus']));
//array_push($tab_1, $this->mcl->div('scriptsGreeting', $data['scriptsGreeting'], 'full-width'));
//array_push($tab_1, $this->mcl->div('question_answer_area', $data['question_info'], 'full-width'));
//array_push($tab_1, $this->mcl->div('question_area', '', 'full-width'));
//array_push($tab_1, $this->mcl->div('scriptsEnding', $data['scriptsEnding'], 'full-width hide'));
//array_push($tab_1, $this->mcl->bt('save', 'save', array('class' => 'footer search', 'tab' => 'tab_1')));
//$o = $this->mcl->input_page(array($tab_1), $t);
$o = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
$o.='<div class="topbar">
	<!-- LOGO -->
        <div class="topbar-left">
                <div class="text-center ">
							
                                                                </div>
        </div>
		<!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
                <div class="container">
                        <div class="">
                                <div class="pull-left">
                                <img src='.base_url().'assets/img/aia1.png class="logo aia-logo"> 
                                        <span class="clearfix"></span>
                                </div>
                                <ul class="nav navbar-nav navbar-right pull-right">
										
                                </ul>
                        </div>
                        <!--/.nav-collapse -->
                </div>
        </div>
</div>';
$o.=$this->mcl->hdr('questionnaire', 'full-width');
$o.=$this->mcl->hd('cusID', $t['cusID']);
$o.=$this->mcl->hd('surveyStatus', $t['surveyStatus']);
$o.=$this->mcl->div('scriptsGreeting', $data['scriptsGreeting'], 'full-width');
$o.=$this->mcl->div('question_answer_area', $data['question_info'], 'full-width');
$o.=$this->mcl->div('question_area', '', 'full-width');
$o.=$this->mcl->div('scriptsEnding', $data['scriptsEnding'], 'full-width hide');
$o.=$this->mcl->bt('save', 'save', array('class' => 'footer search'));
print $o;
?>

<script language="javascript">
    $(document).ready(function () {
        setTimeout(function ()
        {
            $('button[id=btn_new]').addClass('hide');
            $('a.buttons-excel').hide();
            $('.buttons-select-all').hide();
            $('.buttons-select-none').hide();
            $('#btn_save').hide();
        }, 10);

        setTimeout(function () {
            // handleShowQuestion();
            handleShowScripts();
            handleNextButton();
            handleBackButton();
        }, 50);

        $('.btn-begin-survey').off('click').on('click', function (e) {
            $('#question_answer_area').removeClass('hide');
            $('#scriptsGreeting').addClass('hide');
            handleShowQuestion();
        })

        $('.btn-end-survey').off('click').on('click', function (e) {
            location.reload();
        })

        var handleShowQuestion = function ()
        {
            $('.question-area:visible').find('.btn_jump_to').last().attr('id','btn_last').text('<?=$this->mcl->gl('survey_save');?>');
            
            $('.question-area:visible').find('.btn_jump_back').first().hide();
            $('.question-area').hide();
            $('#question_answer_area').find('.question-area').first().show();
            $('.question-area:visible').find('.btn_jump_back').hide();
            var section_id = $('div.section-header:first').attr('id');
            handleShowSection(section_id);
        }

        var handleShowSection = function (section_id)
        {
            $('.section-header').addClass('hide');
            $('.section-header').each(function () {
                var s_id = parseInt($(this).attr('id'), 0);

                if (s_id == section_id) {
                    // $(this).fadeIn('slow').removeClass('hide');
                    $(this).removeClass('hide');
                    return;
                }
            });
        }

        var handleNextButton = function () {
            $('.btn_jump_to').on('click', function (e)
            {
                var area = $(this).closest('.question-area');

                var questionid = $('.question-area:visible').attr('questionid');
                var requiredAnswer = $('.question-area:visible').attr('requiredanswer');
                var inputtype = $('.question-area:visible').find('input:visible').attr('type');
                var inputname = $('.question-area:visible').find('input:visible').attr('name');

                if (typeof inputtype === 'undefined')
                {
                    inputtype = $('.question-area:visible').find('textarea:visible').attr('type');
                    inputname = $('.question-area:visible').find('textarea:visible').attr('name');
                }

                if (inputtype == 'radio')
                {
                    if (($('.question-area:visible').find('input:visible:checked').length) < 1 && (requiredAnswer > 0))
                    {
                        alert('<?= $this->mcl->gl('please_select_choice') ?>');
                        return false;
                    } else
                    {
                        var jumpTo = $('.question-area:visible').find('input:visible:checked').attr('jumpTo');
                        var valueNet = $('input[name=' + inputname + ']:checked').val();
                    }
                } else if (inputtype == 'textarea')
                {
                    if (($('.question-area:visible').find('textarea:visible').val().trim().length < 1) && (requiredAnswer > 0))
                    {
                        alert('<?= $this->mcl->gl('please_investigate_the_error') ?>');
                        return false;
                    } else
                    {
                        var jumpTo = $('.question-area:visible').find('textarea:visible').attr('jumpTo');
                        var valueNet = $('textarea[name=' + inputname + ']').val();
                    }
                } else
                {
                    var jumpTo = $('.question-area:visible').find('input:visible').attr('jumpTo');
                }

                var data = get_data_serialize('.popupwindow_container');
                // data += '&uri=' + $(cc).find('input[type=hidden][name=uri]').val();
                data += '&cusID=' + $(cc).find('input[type=hidden][name=cusID]').val();
                data += '&surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
                data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                data += '&questionID=' + questionid;
                data += '&valueNet=' + valueNet;
                var url = get_base_url() + '<?=$this->router->class;?>' + '/save_answer';

                $.ajax({
                    type: "POST",
                    // url: get_base_url() + get_uri() + '/save_answer',
                    // url: get_base_url() + 'SurveyOnline' + '/save_answer',
                    url: url,
                    cache: false,
                    async: false,
                    data: data,
                    beforeSend: function () {

                    },
                    success: function (result) {
                        // alert(result);return false;
                        //debugger;
                        var jsonResult = eval("(" + result + ")");
                        if (jsonResult.surveyID > 0)
                        {
                            $('input:hidden[name=surveyID]').val(jsonResult.surveyID);
                        }

                        if (jsonResult.valueID > 0)
                        {
                            $('input:hidden[name=valueid]').val(jsonResult.valueID);
                        }
                    }
                });

                var section_id = parseInt($('div[questionid=' + jumpTo + ']').closest('.section-area').attr('section_id'), 0);
                handleShowSection(section_id);

                // Check Is Last Question ?
                if (jumpTo < 0)
                {
                    // handleShowScripts();
                    confirm_save();
                } else
                {
                    $('.question-area:visible').hide();
                    $('[questionid=' + jumpTo + ']').fadeIn('slow').show();
                    $('.question-area:visible').attr('hidJumpFrom', questionid);
                }
            })
        }

        var handleBackButton = function () {
            $('.btn_jump_back').on('click', function (e) {
                $('.btn_jump_to').show();
                var jumpFrom = $('.question-area:visible').attr('hidJumpFrom');
                if (jumpFrom > 0)
                {
                    $('.question-area:visible').hide();
                    $('[questionid=' + jumpFrom + ']').fadeIn('slow').show();
                } else
                {
                    var jumpFrom = $('.question-area:visible').find('input:visible').attr('jumpFrom');
                    $('.question-area:visible').hide();
                    $('[questionid=' + jumpFrom + ']').fadeIn('slow').show();
                }

                var section_id = parseInt($('div[questionid=' + jumpFrom + ']').closest('.section-area').attr('section_id'), 0);

                handleShowSection(section_id);
            })
        }

        // $('.btn-end-survey').on('click', function (e) {
        //     confirm_save();
        // })
    });

    function handleShowScripts()
    {
        if ($('#surveyStatus').val() != 'completed')
        {
            $('#scriptsGreeting').addClass('hide');

            if ($('#scriptsEnding').is(':empty'))
            {
                alert('<?= $this->mcl->gl('end_survey'); ?>');
                // location.reload();
            } else
            {
                $('#question_answer_area').addClass('hide');
                $('#scriptsEnding').removeClass('hide');
            }

            return false;
        }

        if ($('#scriptsGreeting').hasClass('hide'))
        {
            if ($('#scriptsEnding').is(':empty'))
            {
                alert('<?= $this->mcl->gl('end_survey'); ?>');
                // location.reload();
            } else
            {
                $('#question_answer_area').addClass('hide');
                $('#scriptsEnding').removeClass('hide');
            }
        } else
        {
            if ($('#scriptsGreeting').is(':empty'))
            {
                $('#question_answer_area').removeClass('hide');
                handleShowQuestion();
            } else
            {
                $('#question_answer_area').addClass('hide');
            }
        }
    }

    function confirm_save()
    {
        // if(!confirm('<?//=$this->mcl->gl('confirm_save');?>'))
        // {
        //      return false;
        // }

        data = 'surveyID=' + $(cc).find('input[type=hidden][name=surveyID]').val();
        data += '&qcStatusID=2';
        // var url = get_base_url() + get_uri() + '/update_qc_status';
        // var url = get_base_url() + 'SurveyOnline' + '/update_qc_status';
        var url = get_base_url() + '<?=$this->router->class;?>' + '/update_qc_status';

        $.ajax({
            type: 'POST',
            cache: false,
            async: false,
            url: url,
            data: data,
            beforeSend: function () {
            },
            success: function (result)
            {
                if (result > 0)
                {
                    alert('<?= $this->mcl->gl('save_data_complete'); ?>');
                    //  window.location.reload(get_base_url() +'index#/'+ get_uri());
                    // location.reload();
                    handleShowScripts();
                }
            }
        });
    }
</script>
