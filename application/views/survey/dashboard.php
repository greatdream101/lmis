<?php
$totalView = '

<div class="row">
    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2"></div>
            <h3 class="text-success counter">'.$ctStatus['all'].'</h3>
            <p class="text-muted text-nowrap">Daily Survey</p>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart" data-dimension="90" data-text="75%" data-width="5" data-fontsize="14" data-percent="75" data-fgcolor="#3bafda" data-bgcolor="#ebeff2"></div>
            <h3 class="text-primary counter">'.$ctStatus['survey'].'</h3>
            <p class="text-muted text-nowrap">Pending Survey</p>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart" data-dimension="90" data-text="49%" data-width="5" data-fontsize="14" data-percent="49" data-fgcolor="#98a6ad" data-bgcolor="#ebeff2"></div>
            <h3 class="text-inverse counter">'.$ctStatus['qc'].'</h3>
            <p class="text-muted text-nowrap">Pending QC</p>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart" data-dimension="90" data-text="58%" data-width="5" data-fontsize="14" data-percent="58" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
            <h3 class="text-pink">'.$ctStatus['approved'].'</h3>
            <p class="text-muted text-nowrap">Approved</p>
        </div>
    </div>
</div> ';

    $tab_1 = array();
    array_push($tab_1, $this->mcl->div('totalView', $totalView, 'full-width'));
    array_push($tab_1, $this->mcl->msb('regionID',$t,array('class'=>'quarter-width required')));
    array_push($tab_1, $this->mcl->msb('branchID',$t,array('class'=>'quarter-width required')));
    array_push($tab_1, $this->mcl->sb('seriesID',$t,array('class'=>'quarter-width')));
    array_push($tab_1, $this->mcl->bt('view_dashboard', 'view_dashboard'));
    array_push($tab_1, $this->mcl->hr('full-width'));
    array_push($tab_1, $this->mcl->pg('chartColumn', 'Overview Surveyor by Regions', 'week_no', 'week_no', 'column', 'data'));
    array_push($tab_1, $this->mcl->div('output', '', 'full-width'));
    $o = $this->mcl->input_page(array($tab_1), $t);
    
    print $o;    

?>

<script language="javascript">
        $(document).ready(function () {
            $('#btn_new').hide();

            handleValidationRules();

            $('#btn_view_dashboard').off('click').on('click', function (e) {
                    var regionID = attr($('input[type=hidden][name=regionID]').val() , 0);
                    var branchID = attr($('input[type=hidden][name=branchID]').val() , 0);
                    var seriesID = attr($('input[type=hidden][name=seriesID]').val() , 0);
                    var data = get_data_serialize('#input_page');
                        data += 'regionID='+regionID+'&branchID='+branchID+'&seriesID='+seriesID;
                    if (pass_validation('#input_page')) {
                        $.ajax({
                            type: "POST",
                            url: get_base_url() + get_uri() + '/get_surveyData',
                            cache: false,
                            async: false,
                            data: data,
                            beforeSend: function () {

                            },
                            success: function (html) {
                                output = $.parseJSON(html);
                                handleHighChart('chartColumn', output['data']);
                                $('#output').html(output['table']);
                            }
                        });
                    }
            });  
        })
</script>
