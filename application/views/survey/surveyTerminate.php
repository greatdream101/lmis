<?= $this->load->view('survey/loadScript.php', array(), true) ?>
<?= $this->load->view('survey/loadStyle.php', array(), true) ?>

<style>
        @font-face {
                font-family: 'MazdaType';
                src: url('/mzgift/assets/fonts/MazdaType-Regular.otf2') format('otf2'),
                        url('/mzgift/assets/fonts/MazdaTypeTH-Regular.otf') format('otf');
                font-weight: normal;
                font-style: normal;
        }
        body{
                /* background-image: linear-gradient(to bottom, white -70%, black); */
                background-image: url("../../assets/img/bg-mazda-2.jpg");
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
        }
        #logo{
                margin: 2% 0 0 0;
                /* padding: 2%; */
        }
        .img-responsive{
                margin: 0 auto;
                width: 150px;
        }
        #content{
                text-align: center;
                /* border: 1px solid white; */
                border-radius: 30px;
                /* background: white; */
                margin: 0 5% 0 5%;
                padding: 2%;
                width: 90%;
                color: white;
        }
        #code{
                border: 1px solid white;
                border-radius: 5px;
                width: 90%;
                background: #cecece;
                margin: 4% 0 0 5%;
                padding: 2%;
                color: black;
        }
        #label-head{
                font-style: normal;
                font-weight: bold;
                /* font : message-box; */
                /* color: #434444; */
        }
        #label-content{
                font-size: 16px;
                margin: 20px 0 20px 0;
        }
        #survey-content{
                font-size: 16px;
                margin: 20px 0 0 0;
        }
        .hidden_content{
                display: none;
        }
        #buttonContent{
                text-align: center;
        }
        .form-check-inline {
                text-align: left;
                display: inline-block;
                margin:0 auto;
                padding: 15px 0 0 25%;
        }
        .submit{
                background: #24b059;
                padding: 5px;
                width: 100px;
                border-radius: 5px;
                border: 1px solid white;
                margin-top: 10px;
                margin-bottom: 10px;
        }
        .submit:hover{
                background: green;
        }
        #note{
                margin: 20px 7px 0 7px;
                padding: 4% 0% 5% 0%;
                color: white;
                font-size: 11px;
        }
        #QRcode{
                width: 100px;
                float: right;
                /* margin: 0 0 0 5px; */
                padding: 0 0 10px 0px;
        }
        @media screen and (min-width: 800px) {
                .form-check-inline {
                        padding: 15px 0 0 40%;
                }
                #note{
                        padding: 4% 12% 5% 10%;
                }
        }

</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php

                print $this->mcl->div('logo', img(array("src"=>base_url().'/assets/img/mazda-icon4.png', "class"=>"img-responsive center-block")), "text-center");

                $str_node = "**<b><u>เงื่อนไขการใช้บริการ</u></b>**
                                </br>(1).ขอสงวนสิทธิ์พิจารณาการรับรางวัล 1 คน ต่อ 1 รหัสการใช้งานเท่านั้น
                                </br>(2).รางวัลที่ได้รับไม่สามารถเปลี่ยนเป็นเงินสดหรือของรางวัลอื่นได้
                                ".$this->mcl->div('QRcode', img(array("src"=>base_url().'/assets/img/mazda-qr-code.png', "class"=>"img-QRcode img-responsive")))."
                                </br>(3).รหัสของท่านจะถูกเก็บไว้เมื่อทำรายการเสร็จสมบูรณ์ ท่านสามารถบันทึกภาพหน้าจอ หรือกดลิงค์นี้อีกครั้งเพื่อแสดงรหัสเมื่อต้องการรับส่วนลดที่ศูนย์บริการ
                                </br>(4).ท่านสามารถใช้สิทธิ์ได้ตั้งแต่ วันที่ 1 พ.ย. 2562 - 31 มี.ค. 2563
                        ";


                if(empty($cusInfo)){
                        echo "<div id='content' class='output-area col-lg-12'>Sorry, Page not found.</div>";
                        die;
                }elseif($cusInfo['QA2'] != "" && $cusInfo['QA2'] != "0"){

                        if($cusInfo['flagUsed'] == 1){

                                echo "<div id='content' class='output-area col-lg-12'>";

                                echo "<h2>Sorry!</h2> </br> ขออภัยรหัสที่ส่งเข้ามา <b><u>ถูกใช้งานแล้ว</u></b>  </br> กรุณาตรวจสอบรหัสอีกครั้ง </br> (1 รหัสสามารถใช้งานได้เพียง 1 ครั้ง) </br>";

                                echo "<div id='code'><h2><b>".$cusInfo['code']."</b></h2></div>";

                                echo "</div>";

                        }else{

                                echo "<div id='content' class='output-area col-lg-12'>";

                                echo "<div id='label-head'><h3><b><u>ท่านทำรายการเสร็จสมบูรณ์</u></b></h3></div>";

                                echo "กรุณาแสดงรหัสนี้เพื่อรับส่วนสดค่าแรง ".number_format($cusInfo['discount'], 0, "", ',')." บาท</br> ณ ศูนย์บริการมาสด้าในวันเข้ารับบริการ";

                                // echo $this->mcl->div('label-code', "<h3><font id='label-head'>รหัสของคุณคือ</font></h3>");

                                echo "<div id='code'><h2><b>".$cusInfo['code']."</b></h2></div>";

                                echo "</div>";

                                echo $this->mcl->div('note', $str_node);

                                // $footer_content .= $this->mcl->div('QRcode', img(array("src"=>base_url().'/assets/img/mazda-qr-code.png', "class"=>"img-QRcode img-responsive")));

                                // echo $this->mcl->div('footer_content', $footer_content);
                        }

                        die;
                }

                $o         = '';

                $o.=$this->mcl->hd('cusID', $cusInfo['cusID']);

                $o.=$this->mcl->hd('hiddenCode', $cusInfo['code']);

                $o.=$this->mcl->div('label-head', "<h2><font id='label-head'>กรุณาตอบคำถาม</font></h2>");

                $o.=$this->mcl->div('label-content', "<font id='label-content'>เพื่อรับสิทธิพิเศษ คุณสามารถนำรหัสนี้</br>ไปใช้ได้ ณ จุดบริการ</font>");

                foreach($question as $value){

                        if(!empty($value['code'])){
                                $o.=$this->mcl->hd('sectionID', $value['code']);
                                $o.=$this->mcl->div('survey-content', "<b>คำถาม</b> : ".$value['name']);
                        }else{
                                $survey_data['addonData']['survey_data']['data'][$value['value']] = array('name'=>'('.$value['value'].')'.$value['name'] , 'data'=>$value['value']);
                        }

                }

                $o.=$this->mcl->rb('survey_data', $survey_data, array('class'=>'required no-cookie no-description form-check-inline no-label col-xs-12 col-md-8'));

                // $o.=$this->mcl->div('label-code', "<h3><font id='label-head'>รหัสของคุณคือ</font></h3>", "hidden_content");

                $o.=$this->mcl->div('code', "<h2><font id='label-head'><u>XXXXXXXX</u></font></h2>", "hidden_content");

                $page=$this->mcl->div('content', $o);

                echo $page;

                echo "<div id='buttonContent'><button class='submit btn-default'>ยืนยัน</button></div>";

                echo $this->mcl->div('note', $str_node, "hidden_content");

                // echo $this->mcl->div('QRcode', img(array("src"=>base_url().'/assets/img/mazda-qr-code.png', "class"=>"img-QRcode img-responsive")), "hidden_content");

        ?>

<script>
        $(document).ready(function(){
                $(".submit").on("click", function(){

                        var url         = get_base_url()+"SurveyOnline/save_answer";
                        var cusID       = $("#cusID").val();
                        var hiddenCode  = $("#hiddenCode").val();
                        var sectionID   = $("#sectionID").val();
                        var answerID    = $("#survey_data:checked").val();
                        var data        = {"cusID":cusID, "sectionID":sectionID, "answerID":answerID};

                                if(typeof(answerID) == "undefined"){
                                        showSystemMessage(2, 'กรุณาตอบแบบสอบถามด้วยค่ะ');
                                }else{
                                        $.ajax({
                                        type: "POST",
                                        url:  url,
                                        data: data,
                                        success: function(result){

                                                window.reload();

                                                // if(sectionID == "QA1"){
                                                //         window.reload();
                                                // }else{
                                                //         $("#survey-content").hide();
                                                //         $(".form-group").hide();
                                                //         $("#buttonContent").hide();

                                                //         $(".hidden_content").show('slow');
                                                //         $("#note").show('slow');
                                                //         $("#QRcode").show('slow');
                                                //         $("#label-head").html("<h3><b><u>ท่านทำรายการเสร็จสมบูรณ์</u></b></h3>");
                                                //         // $("#label-content").html("กรุณาแสดงรหัสนี้เพื่อรับส่วนสดค่าแรง {discount} บาท</br>ณ ศูนย์บริการมาสด้าในวันเข้ารับบริการ");
                                                //         $("#label-content").html("กรุณาแสดงรหัสนี้เพื่อรับส่วนสดค่าแรง</br>ณ ศูนย์บริการมาสด้าในวันเข้ารับบริการ");
                                                //         $("#code").html("<h2><b><u>"+hiddenCode+"</u></b></h2>");
                                                //         showSystemMessage(1, 'Save data success.');
                                                // }
                                        }
                                })
                        }
                });
        })

</script>
