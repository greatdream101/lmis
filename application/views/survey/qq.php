<?php
$oo = '';

//vd::d($a);
foreach ($a as $k => $v) {

        $oo .= '<table class="table table-bordered table-striped table-condensed" id="qqq">
                <thead>
                    <tr style="background: #0880a9;">';
        $oo .= '<th colspan=2><h4 style="color: #fff;">' . $v['name'] . '</h4></th>';
        $oo .= '<th style="width: 115px;">
								<a sectionID="' . $v['sectionID'] . '" class="ques-button btn btn-primary" href="#" title="add new question">
								<i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a></th>';
        $oo .= '</tr>
							</thead>
							<tbody>';

        $o = '';
        $o .= '<tr><td colspan=3 id="qq">';
        foreach ($v['questions'] as $kk => $vv) {
                $sortOrder = is_null($vv['sortOrder']) ? $vv['questionID'] : $vv['sortOrder'];
                $sortOrder = str_pad($sortOrder, 5, '0', STR_PAD_LEFT);
                $o .= '<table id=' . $vv['questionID'] . ' class="table" order=' . $sortOrder . '>';
                $o .= '<tr id=' . $vv['questionID'] . ' parentID="' . $vv['parentID'] . '" style="font-size: 14px;background: #565555;color: #fff;">';

                $o .= '<td style="background: #ccc;" class="col-lg-1">
                            <a class="up"><i class="fa fa-arrow-circle-o-up fa-2x"></i></a>
                            <a class="down"><i class="fa fa-arrow-circle-o-down fa-2x"></i></a>
                        </td>';


                $o .= '<td style="text-align: left;" class="col-lg-7">';
                if ($vv['parentID'] == 0) {
                        $o .= "<p>" . $vv['name'] . "</p>";
                } else {
                        $o .= "<p>" . $vv['name'] . "</p>";
                }
                $o .= '</td>';
                //vd::d($vv);
                $o .= '<td style="text-align: right;" class="col-lg-4">';

                if ($vv['parentID'] == 0) {

                        $o .= '<a sectionID="' . $v['sectionID'] . '" parentID="' . $vv['questionID'] . '" class="ans-dup-button btn btn-info" href="#" title="duplicate answer"><i class="fa fa-exchange fa-2x" aria-hidden="true"></i></a>';
                        $o .= '<a sectionID="' . $v['sectionID'] . '" id="' . $vv['questionID'] . '" class="ques-edit-button btn btn-warning" href="#" title="edit question"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>';
                        $o .= '<a  id="' . $vv['questionID'] . '" parentID=' . $vv['questionID'] . ' sectionID="' . $v['sectionID'] . '" class="ques-del-button btn btn-danger" href="#" title="delete question"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>';
                        $o .= '<a parentID=' . $vv['questionID'] . ' sectionID="' . $v['sectionID'] . '" class="ans-button btn btn-primary" href="#" title="add answer"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a>';

                } else {
                        $o .= '<a id="' . $vv['questionID'] . '" sectionID="' . $v['sectionID'] . '" parentID="' . $vv['parentID'] . '" class="ans-edit-button btn btn-warning" href="#" title="edit answer"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>';
                        $o .= '<a id="' . $vv['questionID'] . '" sectionID="' . $v['sectionID'] . '" parentID="' . $vv['parentID'] . '" class="ans-del-button btn btn-dangter" href="#" title="delete answer"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>';
                }

                $o .= '</td></tr>';
                
                foreach ($vv['children'] as $kkk => $vvv) {
                        $sortOrder = is_null($vvv['sortOrder']) ? $vvv['questionID'] : $vvv['sortOrder'];
                        $sortOrder = str_pad($sortOrder, 5, '0', STR_PAD_LEFT);
                        $o .= '<tr id=' . $vvv['questionID'] . ' parentID="' . $vvv['parentID'] . '" order=' . $sortOrder . '>';
                        $o .= '<td colspan=2 class="col-lg-10">';
                        switch ($vvv['controlID']) {
                                case 1:
                                        $o .= $this->mcl->tan($vvv['name'], '', array('class'=>'no-description no-label disabled'));
                                        break;
                                case 2:

                                        $name = $vvv['code'];
                                        $val = $vvv['value'];
                                        $label = $vvv['name'];

                                        $o .= '<div class="checkbox">
<input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="checkbox">
<label for="' . $name . '"> ' . $label . ' </label>
</div>';
                                        break;
                                case 3:
                                        $name = $vvv['code'];
                                        $val = $vvv['value'];
                                        $label = $vvv['name'];

                                        $o .= '<div class="radio">
<input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="radio" jumpFrom="'.$vvv['jumpFrom'].'" jumpTo="'.$vvv['jumpTo'].'">
<label for="' . $name . '"> ' . $label . ' </label>
</div>';
                                        break;
                                case 4:
                                        $val = explode('|', $vvv['value']);
                                        $label = explode('|', $vvv['option']);
                                        if (count($val) == count($label)) {
                                                $o .= '<select name="select1" id="select1" class="select2">';
                                                foreach ($label as $k1 => $v1) {
                                                        $o .= '<option value=' . $val[$k1] . '>' . $label[$k1] . '</option>';
                                                }
                                                $o .= '</select>';
                                        }
                                        break;
                                case 5:
                                        $o .= $this->mcl->tb($vvv['name'], '', array('class'=>'no-description no-label disabled'));
                                        break;
                                case 6://ten star
                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                        for ($i = 1; $i <= 10; $i++) {
                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                        }
                                        $o .= '</select>';
                                        break;
                                case 7://eleven star
                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                        for ($i = 0; $i <= 10; $i++) {
                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                        }
                                        $o .= '</select>';
                                        break;
                        }

                        $o .= '</td>';

                        $o .= '<td class="col-lg-2"style="text-align: right;" >';

                        if ($vvv['parentID'] == 0) {
                                $o .= '<a sectionID="' . $v['sectionID'] . '" id="' . $vvv['questionID'] . '" class="ques-edit-button btn btn-primary" href="#" title="edit question"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>';
                                $o .= '<a parentID=' . $vvv['questionID'] . ' sectionID="' . $v['sectionID'] . '" class="ans-button btn btn-primary" href="#" title="add answer"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a>';
                                $o .= '<a  id="' . $vv['questionID'] . '" parentID=' . $vvv['questionID'] . ' sectionID="' . $v['sectionID'] . '" class="ques-del-button btn btn-danger" href="#" title="delete question"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>';
                        } else {
                                $o .= '<a href="#"class="up-answer"><i class="fa fa-arrow-circle-o-up fa-2x"></i></a><a href="#" class="down-answer"><i class="fa fa-arrow-circle-o-down fa-2x"></i></a>';
                                $o .= '<a id="' . $vvv['questionID'] . '" sectionID="' . $v['sectionID'] . '" parentID="' . $vvv['parentID'] . '" class="ans-edit-button btn btn-warning" href="#" title="edit answer"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>';
                                $o .= '<a id="' . $vvv['questionID'] . '" sectionID="' . $v['sectionID'] . '" parentID="' . $vvv['parentID'] . '" class="ans-del-button btn btn-danger" href="#" title="delete answer"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>';
                        }

                        $o .= '</td>
					</tr>';
                }
                $o .= '</table>';
        }
        $oo .= $o;
        $o .= '</tr>';
        $oo .= '</tbody>
        </table>';
}
print $oo;
?>
<script language="javascript">
        $(document).ready(function () {
                handleSelectBox('#question_answer_area');
        })
</script>