<script language="javascript">
     $(document).ready(function ()
     {
          $('#btn_preview').remove();

          setTimeout(function ()
          {
               $('button[id=btn_new]').addClass('hide');
               handleDataTablesLocal('#ajax_content');
               $('a.buttons-excel').hide();
               $('.buttons-select-all').hide();
               $('.buttons-select-none').hide();
          }, 10);
     });
</script>
<div class="row">
     <div class="col-md-12">
          <?php
               $o = $table;
               print $this->mcl->widget($this->mcl->gl('Survey_list'), $o);
          ?>
     </div>
</div>
