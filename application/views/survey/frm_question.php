<?php

$o = '<div id "question_body" class=row><div class=col-md-12>';
$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->tb('code', $t, array('class'=>''));
$o .= '</div></div>';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->ta('name', $t, array('class'=>'required'));
$o .= '</div></div>';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->ta('nameEN', $t, array('class'=>'required'));
$o .= '</div></div>';

$o .= '<hr>';
$t['schema_data']['schema']['uploadMin']['max_length'] = 1;
$t['schema_data']['schema']['uploadMin']['is_null'] = 'no';
$t['schema_data']['schema']['uploadMax']['max_length'] = 1;
$t['schema_data']['schema']['uploadMax']['is_null'] = 'no';
$t['edit_data']['uploadMin'] = isset($t['edit_data']['uploadMin'])?$t['edit_data']['uploadMin']:0;
$t['edit_data']['uploadMax'] = isset($t['edit_data']['uploadMax'])?$t['edit_data']['uploadMax']:0;
$o .= '<div class="row">';
$o .= '<div class=col-md-4>';
$o .= $this->mcl->tb('uploadMin', $t, array('class'=>'required is-number'));
$o .= '</div>';
$o .= '<div class=col-md-4>';
$o .= $this->mcl->tb('uploadMax', $t, array('class'=>'required is-number'));
$o .= '</div></div>';
$o .= '<hr>';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->sb('graphTypeID', $t, array('class'=>'required no-cookie'));
$o .= '</div></div>';

$o .= '<div class="row"><div class=col-md-12>';
$o .= $this->mcl->sb('sectionID', $t, array('class'=>'required no-cookie'));
$o .= '</div></div>';

//$o.='<div class="row"><div class=col-md-12>';
//$o .= $this->mcl->tb('name_en', $t, 'required');
//$o.='</div></div>';
//$o.='<div class="row"><div class=col-md-12>';
//$o .= $this->mcl->tb('name_rep', $t, 'required');
//$o.='</div></div>';

$o .= '<div class="row"><div class=col-md-12>';
if (isset($_POST['questionID']) && $_POST['questionID'] > 0) {
        $o .= $this->mcl->bt('add_question', 'Update Question', array('class'=>'btn btn-primary'));
} else {
        $o .= $this->mcl->bt('add_question', 'Add Question', array('class'=>'btn btn-primary'));
}
$o .= form_hidden('questionID', isset($_POST['questionID']) ? $_POST['questionID'] : 0);
$o .= form_hidden('sectionID', isset($t['edit_data']['sectionID']) ? $t['edit_data']['sectionID'] : 0);
$o .= '</div></div>';

$o .= '</div></div>';

print $o;
?>							



