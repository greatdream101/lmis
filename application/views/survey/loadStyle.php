<link href="<?php echo(base_url());?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
<link href="<?php echo(base_url());?>assets/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
<link href="<?php echo(base_url());?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo(base_url());?>assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?php echo(base_url());?>assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="<?php echo(base_url());?>assets/css/components.css" rel="stylesheet" type="text/css">
<link href="<?php echo(base_url());?>assets/css/pages.css" rel="stylesheet" type="text/css">
<link href="<?php echo(base_url());?>assets/css/menu.css" rel="stylesheet" type="text/css">
<link href="<?php echo(base_url());?>assets/css/responsive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/toastr/toastr.css" type="text/css"  />
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/bootstrap-datatables/datatables.css"/>
<link href="<?php echo(base_url());?>assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo(base_url());?>assets/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css"  rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/summernote/dist/summernote.css" type="text/css"  />
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/select2/select2.css" type="text/css"  />
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/popupwindow/popupwindow.css" type="text/css"  />

<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css" type="text/css"  />
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/timepicker/bootstrap-timepicker.min.css" type="text/css"  />

<link rel="stylesheet" type="text/css" href="<?php echo(base_url());?>assets/css/jarvisWidget.css">
<link rel="stylesheet" href="<?php echo(base_url());?>assets/plugins/smart-wizard/styles/smart_wizard.css" type="text/css"  />
<link rel="stylesheet" type="text/css" href="<?php echo(base_url());?>assets/plugins/layout-grid/dist/css/layout-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo(base_url());?>assets/plugins/pivottable-master/dist/pivot.css">
<link rel="stylesheet" type="text/css" href="<?php echo(base_url());?>assets/plugins/c3.css">
<link rel="stylesheet" type="text/css" href="<?php echo(base_url());?>assets/css/jasny-bootstrap.css">
<link rel="stylesheet" href="<?php echo(base_url());?>assets/css/app.css" type="text/css"  />
