<?
$tab_1 = array();
array_push($tab_1, $this->mcl->hdr('questionnaire', 'full-width'));
array_push($tab_1, $this->mcl->sb('seriesID', $t, array('rules' => 'trim|required')));
array_push($tab_1, $this->mcl->bt('view_questions', 'view_questions'));
array_push($tab_1, $this->mcl->hr('full-width'));
array_push($tab_1, $this->mcl->div('question_answer_area', '', 'full-width'));
array_push($tab_1, $this->mcl->div('question_area', '', 'full-width'));
//array_push($tab_1, $this->mcl->bt('save', 'save',array('class'=>'footer', 'tab'=>'tab_1')));
$o = $this->mcl->input_page(array($tab_1), $t);

print $o;
?>

<script language="javascript">
        $(document).ready(function () {
                $('#btn_new').hide();

                var handleAuthData = function () {
                        var is_newable = parseInt($('input[type=hidden][name=auth_data\\[new\\]]').val(), 10) == 1 ? true : false;
                        if (is_newable == true)
                                $('a.ques-button, a.ans-button').removeAttr('disabled');
                        else {
                                $('a.ques-button, a.ans-button').attr('disabled', 'disabled')
                        }

                        var is_edittable = parseInt($('input[type=hidden][name=auth_data\\[edit\\]]').val(), 10) == 1 ? true : false;
                        if (is_edittable == true)
                                $('a.ques-edit-button, a.ans-edit-button').removeAttr('disabled');
                        else {
                                $('a.ques-edit-button, a.ans-edit-button').attr('disabled', 'disabled')
                        }

                        var is_deletable = parseInt($('input[type=hidden][name=auth_data\\[delete\\]]').val(), 10) == 1 ? true : false;
                        if (is_deletable == true)
                                $('a.ques-del-button, a.ans-del-button').removeAttr('disabled');
                        else {
                                $('a.ques-del-button, a.ans-del-button').attr('disabled', 'disabled')
                        }

                        var is_savable = parseInt($('input[type=hidden][name=auth_data\\[save\\]]').val(), 10) == 1 ? true : false;
                        if (is_savable == true)
                                $('button[id=btn_add_question], button[id=btn_add_answer], button[id=btn_add_snippet]').removeAttr('disabled');
                        else {
                                $('button[id=btn_add_question], button[id=btn_add_answer], button[id=btn_add_snippet]').attr('disabled', 'disabled')
                        }
                }

                var save_position = function (data) {
                        $.ajax({
                                type: "POST",
                                url: get_base_url() + get_uri() + '/save_position',
                                cache: false,
                                async: false,
                                data: 'q=' + data,
                                beforeSend: function () {

                                },
                                success: function (html) {

                                }
                        });
                }

                var direction = function () {

                        $('.up-answer').on('click', function (e) {
                                e.preventDefault();
                                var a = $(this).closest('tr');
                                var a_id = $(a).attr('id');
                                var c = attr($(this).closest('tr').prev().find('.up-answer'), '');
                                if (c.length > 0) {
                                        var b = $(this).closest('tr').prev().fadeOut('fast');
                                        var b_id = $(b).attr('id');
                                        if (b.length > 0)
                                                a.insertBefore(b.fadeIn('slow'));
                                        var a_order = $(a).attr('order');
                                        var b_order = $(b).attr('order');
                                        var t = '';
                                        t = a_order;
                                        a_order = b_order;
                                        b_order = t;
                                        $(a).attr('order', a_order);
                                        $(b).attr('order', b_order);
                                        var data = a_id + '-' + a_order + ',' + b_id + '-' + b_order;
                                        save_position(data);
                                }
                        })

                        $('.down-answer').on('click', function (e) {
                                e.preventDefault();
                                var a = $(this).closest('tr');
                                var a_id = $(a).attr('id');
                                var c = attr($(this).closest('tr').next().find('.up-answer'), '');
                                if (c.length > 0) {
                                        var b = $(this).closest('tr').next().fadeOut('fast');
                                        var b_id = $(b).attr('id');
                                        if (b.length > 0)
                                                a.insertAfter(b.fadeIn('slow'))
                                        var a_order = $(a).attr('order');
                                        var b_order = $(b).attr('order');
                                        var t = '';
                                        t = a_order;
                                        a_order = b_order;
                                        b_order = t;
                                        $(a).attr('order', a_order);
                                        $(b).attr('order', b_order);
                                        var data = a_id + '-' + a_order + ',' + b_id + '-' + b_order;
                                        save_position(data);
                                }
                        })

                        $('.up').on('click', function (e) {
                                e.preventDefault();
                                var a = $(this).closest('table');
                                var a_id = $(a).attr('id');
                                var b = $(this).closest('table').prev().fadeOut('fast');
                                var b_id = $(b).attr('id');
                                if (b.length > 0)
                                        a.insertBefore(b.fadeIn('slow'));
                                var a_order = $(a).attr('order');
                                var b_order = $(b).attr('order');
                                var t = '';
                                t = a_order;
                                a_order = b_order;
                                b_order = t;
                                $(a).attr('order', a_order);
                                $(b).attr('order', b_order);
                                var data = a_id + '-' + a_order + ',' + b_id + '-' + b_order;
                                save_position(data);
                        })

                        $('.down').on('click', function (e) {
                                e.preventDefault();
                                var a = $(this).closest('table');
                                var a_id = $(a).attr('id');
                                var b = $(this).closest('table').next().fadeOut('fast');
                                var b_id = $(b).attr('id');
                                if (b.length > 0)
                                        a.insertAfter(b.fadeIn('slow'))
                                var a_order = $(a).attr('order');
                                var b_order = $(b).attr('order');
                                var t = '';
                                t = a_order;
                                a_order = b_order;
                                b_order = t;
                                $(a).attr('order', a_order);
                                $(b).attr('order', b_order);
                                var data = a_id + '-' + a_order + ',' + b_id + '-' + b_order;
                                save_position(data);
                        })
                }

                var click_question = function (area) {
                        area = which_container(area);

                        $(area).find('.ques-del-button').off('click').on('click', function (e) {
                                e.preventDefault();
                                var id = $(this).attr('id');
                                var undo = '<a class="btn-cancel-delete btn btn-success">' + locale['cancel'] + '</a>';
                                var row = $(this).closest('tr');
                                var row_child = $('tr[parentid=' + id + ']');
                                if ($(this).closest('td').find('.btn-cancel-delete').length == 0) {
                                        $(this).html(locale['sure']).after(undo);
                                } else {
                                        var data = 'questionID=' + id + '&action=delete&uri=questionnaire/question';
                                        $.ajax({
                                                type: "POST",
                                                url: get_base_url() + get_uri() + '/delete_question',
                                                cache: false,
                                                async: false,
                                                data: data,
                                                beforeSend: function () {

                                                },
                                                success: function (html) {
                                                        $(row).remove();
                                                        $(row_child).remove();
                                                }
                                        });
                                }
                                $('.btn-cancel-delete').off('click').on('click', function (e) {
                                        e.preventDefault();
                                        $(this).closest('td').find('.ques-del-button').html('<i class="fa fa-trash fa-2x"></i>');
                                        $(this).remove();
                                })
                        });

                        $(area).find('.ques-button, .ques-edit-button').off('click').on('click', function (e) {
                                e.preventDefault();

                                var sectionID = attr($(this).attr('sectionid'), 0);
                                var parentID = attr($(this).attr('parentid'), 0);
                                var seriesID = $('input[type=hidden][name=seriesID]').val();
                                var url = get_base_url() + get_uri() + '/frm_question';
                                var id = attr($(this).attr('id'), 0);
                                var mode = 'insert';
                                if ($(this).hasClass('ques-edit-button')) {
                                        mode = 'edit';
                                }
                                var data = 'questionID=' + id + '&sectionID=' + sectionID + '&seriesID=' + seriesID;
                                ;
                                $.ajax({
                                        url: url,
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {

                                                $('#question_area').empty().html(data).removeClass('hide').PopupWindow({
                                                        title: locale['add_edit_question'],
                                                        modal: true,
                                                        statusBar: false,
                                                        height: 500,
                                                        width: 800,
                                                        top: 'auto',
                                                        left: 'auto',
                                                        buttons: {
                                                                close: true,
                                                                maximize: false,
                                                                collapse: false,
                                                                minimize: false
                                                        }
                                                }).on('close.popupwindow', function () {
                                                        if ($("#question_area").PopupWindow("getState"))
                                                                $("#question_area").PopupWindow("destroy");
                                                        $('body').append($('#question_area'));
                                                        $('#question_area').addClass('hide');
                                                })

                                                handleValidationRules('.popupwindow_container');
                                                handleRadioBox('.popupwindow_container');
                                                handleTextBox('.popupwindow_container');
                                                handleSelectBox('.popupwindow_container');
                                                handleTextArea('.popupwindow_container');
                                                handleAuthData();
                                                $('button[id=btn_add_question]').off('click').on('click', function (e) {
                                                        e.preventDefault();
                                                        if (pass_validation('.popupwindow_container')) {
                                                                var data = get_data_serialize('.popupwindow_container');
                                                                data += '&uri=add_question';
                                                                data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();

                                                                var name = $('.popupwindow_container').find('input[type=text][name=name]').val();
                                                                $.ajax({
                                                                        type: "POST",
                                                                        url: get_base_url() + get_uri() + '/add_question',
                                                                        cache: false,
                                                                        async: false,
                                                                        data: data,
                                                                        beforeSend: function () {

                                                                        },
                                                                        success: function (html) {
                                                                                $("#question_area").PopupWindow("close");
                                                                                $('button[id=btn_view_questions]').trigger('click');
                                                                        }
                                                                });
                                                        }
                                                })
                                        }
                                });
                        });
                }

                var click_answer = function (area) {
                        area = which_container(area);

                        $(area).find('.ans-del-button').off('click').on('click', function (e) {
                                e.preventDefault();
                                var id = $(this).attr('id');
                                var undo = '<a class="btn-cancel-delete btn btn-success">' + locale['cancel'] + '</a>';
                                var row = $(this).closest('tr');
                                if ($(this).closest('td').find('.btn-cancel-delete').length == 0) {
                                        $(this).html(locale['sure']).after(undo);
                                } else {
                                        var data = 'questionID=' + id + '&action=delete&uri=questionnaire/question';

                                        $.ajax({
                                                type: "POST",
                                                url: get_base_url() + get_uri() + '/delete_answer',
                                                cache: false,
                                                async: false,
                                                data: data,
                                                beforeSend: function () {

                                                },
                                                success: function (html) {
                                                        $(row).remove();
                                                }
                                        });
                                }
                                $('.btn-cancel-delete').off('click').on('click', function (e) {
                                        e.preventDefault();
                                        $(this).closest('td').find('.ans-del-button').html('<i class="fa fa-trash fa-2x"></i>');
                                        $(this).remove();
                                })
                        });

                        $(area).find('.ans-button, .ans-edit-button').off('click').on('click', function (e) {
                                e.preventDefault();
                                var sectionID = attr($(this).attr('sectionid'), 0);
                                var parentID = attr($(this).attr('parentid'), 0);
                                var seriesID = attr($('input[type=hidden][name=seriesID]').val(), 0);
                                var url = get_base_url() + get_uri() + '/frm_answer';
                                var id = attr($(this).attr('id'), 0);
                                var data = 'questionID=' + id + '&parentID=' + parentID + '&sectionID=' + sectionID + '&seriesID=' + seriesID;
                                var mode = 'insert';
                                if ($(this).hasClass('ans-edit-button')) {
                                        mode = 'edit';
                                }
                                $.ajax({
                                        url: url,
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                                if ($("#question_area").PopupWindow("getState"))
                                                        //debugger;
                                                        $("#question_area").PopupWindow("destroy");
                                                $('#question_area').empty().html(data).removeClass('hide').PopupWindow({
                                                        title: locale['add_edit_answer'],
                                                        modal: true,
                                                        statusBar: false,
                                                        height: 400,
                                                        width: 700,
                                                        top: 'auto',
                                                        left: 'auto',
                                                        buttons: {
                                                                close: true,
                                                                maximize: false,
                                                                collapse: false,
                                                                minimize: false
                                                        }
                                                }).on('close.popupwindow', function () {
                                                        if ($("#question_area").PopupWindow("getState"))
                                                                $("#question_area").PopupWindow("destroy");
                                                        $('body').append($('#question_area'));
                                                        $('#question_area').addClass('hide');
                                                })

                                                handleValidationRules('.popupwindow_container');
                                                handleRadioBox('.popupwindow_container');
                                                handleTextBoxes('.popupwindow_container');
                                                handleSelectBoxes('.popupwindow_container');
                                                handleTextArea('.popupwindow_container');
                                                handleAuthData();
                                                //debugger;
                                                $('button[id=btn_add_answer]').off('click').on('click', function (e) {
                                                        e.preventDefault();
                                                        if (pass_validation('.popupwindow_container')) {
                                                                var data = get_data_serialize('.popupwindow_container');
                                                                data += '&uri=' + $(cc).find('input[type=hidden][name=uri]').val();
                                                                data += '&sectionID=' + sectionID;
                                                                data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                                                                data += '&flag_id=' + $(cc).find('input[type=hidden][name=flag_id]').val();
                                                                data += '&parentID=' + parentID;
                                                                $.ajax({
                                                                        type: "POST",
                                                                        url: get_base_url() + get_uri() + '/add_answer',
                                                                        cache: false,
                                                                        async: false,
                                                                        data: data,
                                                                        beforeSend: function () {

                                                                        },
                                                                        success: function (html) {
                                                                                $("#question_area").PopupWindow("destroy");
                                                                                $('#question_area').addClass('hide');
                                                                                $('button[id=btn_view_questions]').trigger('click');

                                                                                end_saving();
                                                                        }
                                                                });
                                                        }
                                                })
                                        }
                                });
                        });

                        $(area).find('.ans-dup-button').off('click').on('click', function (e) {
                                e.preventDefault();
                                var sectionID = attr($(this).attr('sectionid'), 0);
                                var parentID = attr($(this).attr('parentid'), 0);
                                var seriesID = attr($('input[type=hidden][name=seriesID]').val(), 0);
                                var url = get_base_url() + get_uri() + '/frm_duplicateAnswer';
                                var id = attr($(this).attr('id'), 0);
                                var data = 'questionID=' + id + '&parentID=' + parentID + '&sectionID=' + sectionID + '&seriesID=' + seriesID;
                                $.ajax({
                                        url: url,
                                        dataType: 'html',
                                        type: 'POST',
                                        data: data,
                                        success: function (data) {
                                                if ($("#question_area").PopupWindow("getState"))
                                                        //debugger;
                                                        $("#question_area").PopupWindow("destroy");
                                                $('#question_area').empty().html(data).removeClass('hide').PopupWindow({
                                                        title: locale['add_dup_answer'],
                                                        modal: true,
                                                        statusBar: false,
                                                        height: 400,
                                                        width: 700,
                                                        top: 'auto',
                                                        left: 'auto',
                                                        buttons: {
                                                                close: true,
                                                                maximize: false,
                                                                collapse: false,
                                                                minimize: false
                                                        }
                                                }).on('close.popupwindow', function () {
                                                        if ($("#question_area").PopupWindow("getState"))
                                                                $("#question_area").PopupWindow("destroy");
                                                        $('body').append($('#question_area'));
                                                        $('#question_area').addClass('hide');
                                                })

                                                handleValidationRules('.popupwindow_container');
                                                handleRadioBox('.popupwindow_container');
                                                handleTextBoxes('.popupwindow_container');
                                                handleSelectBoxes('.popupwindow_container');
                                                handleTextArea('.popupwindow_container');
                                                handleAuthData();
                                                //debugger;
                                                $('button[id=btn_duplicate_answer]').off('click').on('click', function (e) {
                                                        e.preventDefault();
                                                        if (pass_validation('.popupwindow_container')) {
                                                                var data = get_data_serialize('.popupwindow_container');
                                                                data += '&uri=' + $(cc).find('input[type=hidden][name=uri]').val();
                                                                data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                                                                data += '&parentID=' + parentID;
                                                                $.ajax({
                                                                        type: "POST",
                                                                        url: get_base_url() + get_uri() + '/duplicate_answer',
                                                                        cache: false,
                                                                        async: false,
                                                                        data: data,
                                                                        beforeSend: function () {
                                                                        },
                                                                        success: function (html) {
                                                                                $("#question_area").PopupWindow("destroy");
                                                                                $('#question_area').addClass('hide');
                                                                                $('button[id=btn_view_questions]').trigger('click');

                                                                                end_saving();
                                                                        }
                                                                });
                                                        }

                                                        // if (pass_validation('.popupwindow_container')) {
                                                        //         var data = get_data_serialize('.popupwindow_container');
                                                        //         data += '&uri=' + $(cc).find('input[type=hidden][name=uri]').val();
                                                        //         data += '&sectionID=' + sectionID;
                                                        //         data += '&seriesID=' + $(cc).find('input[type=hidden][name=seriesID]').val();
                                                        //         data += '&flag_id=' + $(cc).find('input[type=hidden][name=flag_id]').val();
                                                        //         data += '&parentID=' + parentID;
                                                        //         $.ajax({
                                                        //                 type: "POST",
                                                        //                 url: get_base_url() + get_uri() + '/duplicate_answer',
                                                        //                 cache: false,
                                                        //                 async: false,
                                                        //                 data: data,
                                                        //                 beforeSend: function () {

                                                        //                 },
                                                        //                 success: function (html) {
                                                        //                         $("#question_area").PopupWindow("destroy");
                                                        //                         $('#question_area').addClass('hide');
                                                        //                         $('button[id=btn_view_questions]').trigger('click');

                                                        //                         end_saving();
                                                        //                 }
                                                        //         });
                                                        // }
                                                })
                                        }
                                });
                        });
                }

                $('button[id=btn_view_questions]').on('click', function (e) {
                        e.preventDefault();
                        //debugger;
                        var area = $(this).closest('.tab-pane');
                        if (pass_validation(area)) {
                                var seriesID = attr($(cc).find('input[type=hidden][name=seriesID]').val());

                                var data = 'seriesID=' + seriesID;
                                var url = get_base_url() + get_uri() + '/get_questions';
                                $.ajax({
                                        type: 'POST',
                                        cache: false,
                                        async: false,
                                        url: url,
                                        data: data,
                                        beforeSend: function () {
                                        },
                                        success: function (html) {
                                                $('#question_answer_area').empty().html(html);
                                                setTimeout(function () {
                                                        handleAuthData();
                                                        click_question();
                                                        click_answer();
                                                        handleRating();
                                                        direction();
                                                }, 1000);
                                        }
                                });
                                data = null;
                                show_footer(1);
                        } else {
                                show_footer(0);
                        }

                        return false;
                });
        });
</script>
