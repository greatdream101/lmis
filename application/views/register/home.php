<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
        @font-face {
                font-family: 'MazdaType';
                src: url('assets/fonts/MazdaType-Regular.otf2') format('otf2'),
                        url('assets/fonts/MazdaTypeTH-Regular.otf') format('otf');
                font-weight: normal;
                font-style: normal;
        }
        body{
                /* background-image: linear-gradient(to bottom, white -70%, black); */
                background-image: url("../assets/img/bg-img/img-screen.jpg");
                background-position: center;
                background-repeat: no-repeat;
                background-size: fixed;
        }
        #logo{
                margin: 2% 0 0 0;
                /* padding: 2%; */
        }
        .img-responsive{
                margin: 0 auto;
                width: 150px;
        }
        @media screen and (min-width: 800px) {
                .form-check-inline {
                        padding: 15px 0 0 40%;
                }
                #note{
                        padding: 4% 12% 5% 10%;
                }
                #customer-info{
                        margin: 0 auto;
                        padding: 50px;
                }
        }

</style>