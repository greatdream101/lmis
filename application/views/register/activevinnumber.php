<style>
        @font-face {
                font-family: 'MazdaType';
                src: url('/mzgift/assets/fonts/MazdaType-Regular.otf2') format('otf2'),
                        url('/mzgift/assets/fonts/MazdaTypeTH-Regular.otf') format('otf');
                font-weight: normal;
                font-style: normal;
        }
        html{
            background: none;
        }
        body{
                /* background-image: linear-gradient(to bottom, white -70%, black); */
                background-image: url("../assets/img/bg-mazda-2.jpg");
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
        }
        #logo{
                margin: 2% 0 0 0;
                /* padding: 2%; */
        }
        .img-responsive{
                margin: 0 auto;
                width: 150px;
        }
        #container{
                padding: 0 5% 5% 5%;
                text-align: auto;
        }
        #customer-info{
                margin: 0 auto;
                padding: 50px;
                width: 80%;
                text-align: left;
                background-color: white;
                border-radius: 10px;
        }
        #title{
            text-align: center;
        }
        #button_group{
                text-align: center;
        }
        #btn_submit, #btn_active, #btn_back{
                margin: 0 auto;
                width: 100px;
                height: 35px;
                font-size: 16px !important;
        }
        #result{
            margin: 0 auto;
            text-align: center;
        }
        .glyphicon-remove-sign{
            margin: 0 auto;
            color: #f44336;
            font-size: 100px
        }
        .glyphicon-ok-sign{
            margin: 0 auto;
            color: #78a83f;
            font-size: 100px
        }
        .glyphicon-info-sign{
            margin: 0 auto;
            color: #3f51b5;
            font-size: 100px
        }

        #div_result_checkcode{
                text-align: left;
                /* background: white; */
                /* width: 80%; */
                padding: 50px;
                margin: 0 auto;
                border-radius: 10px;
                /* margin-left: 150px; */
        }

        #button_group{
                text-align: center;
                margin-bottom: 20px;

        }
        .group_result{
                margin: 0 auto;
                text-align: center;
                margin-bottom: 20px;
        }
        footer{
            display: none;
        }
        @media screen and (min-width: 800px) {
                .form-check-inline {
                        padding: 15px 0 0 40%;
                }
                #note{
                        padding: 4% 12% 5% 10%;
                }
        }

</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php

                $o = "";
                $o .= "<div id='logo'>";
                        $o .= img(array("src"=>base_url().'/assets/img/mazda-icon4.png', "class"=>"img-responsive center-block"));
                $o .= "</div>";
                $o .= "<form id='customer-info'>";
                $o .= "<div id='container'>";
                        $o .= $this->mcl->div('title', '<h2>'.$this->mcl->gl('title_check_vin').'</h2>');
                        $o .= $this->mcl->tb('VIN1', array(), array('class'=>'no-descriptions required'));
                        $o .= $this->mcl->div('button_group', $this->mcl->bt('submit', 'button_okay'));
                $o .= "</div>";
                $o .= "</form>";
                $o .= $this->mcl->div('result', '');

                print $o;

        ?>

<script>
        $(document).ready(function(){
                $("#btn_submit").on("click", function(){

                        var url         = get_base_url()+"Register/activatecode";
                        var vin        = $('#VIN1').val();
                        var data        = {'vin':vin};

                        if(vin == ''){
                                showSystemMessage(2, 'กรุณาระบุหมายเลขตัวถังรถ');
                        }else{
                                $.ajax({
                                        type: "POST",
                                        url:  url,
                                        data: data,
                                        success: function(result){

                                                $('#result').html(result);
                                                $('#customer-info').hide();

                                                $('#btn_active').on("click", function(){
                                                        var url         = get_base_url()+"Register/active";
                                                        var data        = $('#customer-info').serialize();
                                                        $.ajax({
                                                                type: "POST",
                                                                url:  url,
                                                                data: data,
                                                                success: function(result){
                                                                        $('#result').html('');
                                                                        $('#customer-info').show();
                                                                        showSystemMessage(1, 'Update already.');
                                                                }
                                                        });
                                                });

                                                $('#btn_back').on("click", function(){
                                                        $('#result').html('');
                                                        $('#customer-info').show();
                                                });

                                        }
                                });
                        }
                });

        })

</script>
