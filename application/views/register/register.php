<?= $this->load->view('survey/loadScript.php', array(), true) ?>
<?= $this->load->view('survey/loadStyle.php', array(), true) ?>

<style>
        @font-face {
                font-family: 'MazdaType';
                src: url('assets/fonts/MazdaType-Regular.otf2') format('otf2'),
                        url('assets/fonts/MazdaTypeTH-Regular.otf') format('otf');
                font-weight: normal;
                font-style: normal;
        }
        body{
                /* background-image: linear-gradient(to bottom, white -70%, black); */
                background-image: url("../assets/img/bg-mazda-2.jpg");
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
        }
        #logo{
                margin: 2% 0 0 0;
                /* padding: 2%; */
        }
        #title{
            text-align: center;
            font-size: 30px;
        }
        .img-responsive{
                margin: 0 auto;
                width: 150px;
        }
        #container{
                /* text-align: center; */
                /* border: 1px solid white; */
                /* border-radius: 30px; */
                /* background: white; */
                /* margin: 0 5% 5% 5%; */
                padding: 0 5% 5% 5%;
                text-align: center;
                /* width: 80%; */
                /* color: white; */
        }
        #div_agree{
                text-align: center;
        }
        #agree_permission{
                margin: 0 auto;
                width: 17px;
                height: 17px;
        }
        #customer-info{
                margin: 0 auto;
                padding: 20px;
                width: 80%;
                text-align: left;
                background-color: white;
                border-radius: 10px;
        }
        select#itemID{
                width: 100%;
                height: 35px;
                border-radius: 5px;
        }
        #button_group{
                text-align: center;
                margin-top: 10px;
        }
        #btn_submit{
                margin: 0 auto;
                /* width: 100px; */
                height: 35px;
                font-size: 16px !important;
        }
        @media screen and (min-width: 800px) {
                .form-check-inline {
                        padding: 15px 0 0 40%;
                }
                #note{
                        padding: 4% 12% 5% 10%;
                }
                #customer-info{
                        margin: 0 auto;
                        padding: 50px;
                }
        }

</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php

                $o = "";
                $o .= "<div id='logo'>";
                        $o .= img(array("src"=>base_url().'/assets/img/mazda-icon4.png', "class"=>"img-responsive center-block"));
                $o .= "</div>";
                $o .= "<div id='container'>";
                        $o .= "<form id='customer-info'>";
                        $o .= $this->mcl->div('title', $this->mcl->gl('hd_register'));
                                $o .= $this->mcl->tb('fname', array(), array('class'=>'no-descriptions required'));
                                $o .= $this->mcl->tb('lname', array(), array('class'=>'no-descriptions required'));
                                $o .= $this->mcl->tb('mobile', array(), array('class'=>'no-descriptions required', 'type'=>'number'));
                                $o .= $this->mcl->tb('VIN1', array(), array('class'=>'no-descriptions required'));
                                $o .= $this->mcl->emp_cb('itemID', $t, array('class'=>'no-descriptions custom-control custom-checkbox'));
                                // $o .= $this->mcl->tb('branch', array(), array('class'=>'no-descriptions required'));
                                $o .= "<div id='div_agree'>";
                                $o .= "<input type='checkbox' name='agree_permission' id='agree_permission' style='display: inline-block; vertical-align: middle;' value='1' required>";
                                $o .= "<b style='display: inline-block;'><u>ข้าพเจ้ายินดีรับข่าวสารเกี่ยวกับผลิตภัณฑ์ และส่วนลดทางการตลาดอื่นๆ จากบริษัทฯ ในครั้งต่อไป</u></b>";
                                $o .= "<div id='div_alert' style='display: none; color: red;'><span>หมายเหตุ : กรุณาเลือกยินยอมด้วยค่ะ</span></div>";
                                $o .= "</div>";
                                $o .= $this->mcl->div('button_group', $this->mcl->bt('submit', 'button_confirm'));
                        $o .= "</form>";
                $o .= "</div>";

                print $o;
        ?>

<script>
        $(document).ready(function(){

                $('#VIN1').attr('maxlength', '17');

                // $('#btn_submit').attr('disabled', 'disabled');

                // $('#agree_permission').on('click', function(){
                //         if($(this).is(':checked')){
                //                 $('#btn_submit').removeAttr('disabled');
                //         }else{
                //                 $('#btn_submit').attr('disabled', 'disabled');
                //         }
                // });

                $('#mobile').on('keypress', function(){
                        var length = $(this).val().length;
                        if(length > 9){
                        return false;
                        }
                });

                $("#btn_submit").on("click", function(){

                        var url         = get_base_url()+"Register/save_data";
                        var data        = $('#customer-info').serialize();
                        var vin         = $('#VIN1').val();
                        var required    = 0;
                        var checkbox    = 0;
                        var agree       = 0;

                        $('input.required').each(function(){
                                if($(this).val() == ''){
                                        required++;
                                }
                        });

                        $('input[type=checkbox]').each(function(){
                                $checked = $(this).is(':checked');
                                if($checked){
                                        checkbox++;
                                }
                        });

                        if(vin.length < 17){
                                showSystemMessage(2, 'VIN should has 17 digit');
                                $('#vin_note').html('');
                                $('#VIN1').after("<div id='vin_note' style='color: red;'>กรุณาใส่หมายเลขเครื่องให้ครบ 17 หลักด้วยค่ะ</div>");
                        }

                        if(required > 0 || checkbox == 0 || vin.length < 17){
                                showSystemMessage(2, 'Please complete data');
                        }else{

                                if($('#agree_permission').is(':checked')){

                                        $('#div_alert').hide();

                                        $.ajax({
                                                type: "POST",
                                                url:  url,
                                                data: data,
                                                success: function(result){
                                                        if(result != '0'){
                                                                showSystemMessage(1, 'Saving');
                                                                $('#customer-info').html(result);
                                                                // setTimeout(function(){ location.reload(); }, 2000);
                                                        }else{
                                                                showSystemMessage(2, 'หมายเลขเครื่องยนต์นี้ ถูกใช้ลงทะเบียนไปแล้ว!');
                                                        }
                                                }
                                        });
                                }else{
                                        $(this).focus();
                                        $('#div_alert').show();
                                        showSystemMessage(2, 'Please agree permission');
                                        return false;
                                }

                        }
                });
        })

</script>
