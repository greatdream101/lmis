<style>
    #btn_new{
        display: none;
    }

    tr.second{
        display: none;
    }

    a.buttons-excel{
        /* display: none; */
    }

    .inline{
        display: inline-block;
    }
    input.ronumber {
        width: 75%;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="modal_edit_ronumber" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit ROnumber</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <span>Ro number : </span>
            <input type='text' id='modal_ro_number' name='ro_number' value='' />
            <input type='hidden' id='modal_cusID' name='cusID' value='' />
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id='modal_save'>Save changes</button>
        </div>
    </div>
    </div>
</div>

    <?php

        $output = $this->mcl->widget('Customerlist', $table);

        print $output;

    ?>

<script type="text/javascript">
    $(document).ready(function(){

        // handleControls('#Customerlist');
        // handleRadioBox('.search-box');
        handleDataTablesLocal('#ajax_content');

        // var e = jQuery.Event("keydown");
        // e.which = 50; // # Some key code value
        // $("input").trigger(e);

        $('.ronumber').each(function(){

            $(this).on('keyup', function(){

                var url      = get_base_url()+'Customer/update_ro_number';
                var cusID    = $(this).attr('id');
                var ronumber = $(this).val();

                $.ajax({
                    type : "POST",
                    url  : url,
                    data : {'cusID':cusID, 'ro_number':ronumber},
                    success: function(){
                        //saved
                    }
                });
            });
        });

        // $('.btn-ronumber').each(function(){

        //     $(this).on('click', function(){

        //         var id = $(this).attr('id');

        //         $('input.ronumber[id='+id+']').removeAttr('disabled');

        //     });
        // });

        $('.edit_ronumber').each(function(){
            $(this).on('click', function(){
                var ro_number = $(this).text();
                var cusID     = $(this).attr('id');

                $('#modal_ro_number').val(ro_number);
                $('#modal_cusID').val(cusID);
                $('#modal_edit_ronumber').modal('show');

            });
        });

        $('#modal_save').on('click', function(){

            var url       = get_base_url()+'Customer/update_ro_number';
            var ro_number = $('#modal_ro_number').val();
            var cusID     = $('#modal_cusID').val();

            $.ajax({
                type : "POST",
                url  : url,
                data : {'cusID':cusID, 'ro_number':ro_number},
                success:function(){
                    showSystemMessage(1, 'Updated');
                    $('#modal_edit_ronumber').modal('hide');
                }
            });
        });

        $('#modal_edit_ronumber').on('hidden.bs.modal', function (){
            location.reload();
        });

    });
</script>