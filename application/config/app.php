<?
$config['ignore_columns'] = array('display','active', 'sortOrder', 'createdID', 'createdDate', 'updatedID', 'updatedDate', 'deletedID', 'deletedDate', 'computerName', 'post_parent');
$config['display'] = 'active'; //active
$config['web_table'] = 'web'; //web
$config['function_table'] = 'functions';
$config['privilege_table'] = 'privilege';
$config['function_privilege_table'] = 'functionPrivilege';
$config['function_addon_table'] = 'functionAddon';
$config['user_table'] = 'users';
$config['group_table'] = 'groups';
$config['user_group_table'] = 'usersGroups';
$config['translation_table'] = 'translation';
$config['language_table'] = 'language';
$config['module_table'] = 'module';
$config['create_id'] = 'createdID';
$config['create_date'] = 'createdDate';
$config['update_id'] = 'updatedID';
$config['update_date'] = 'updatedDate';
$config['delete_id'] = 'deletedID';
$config['delete_date'] = 'deletedDate';
$config['tab_class'] = 'nav nav-tabs navtab-custom';
$config['left_pane_width'] = 'col-md-8';
$config['right_pane_width'] = 'col-md-4';
$config['extension_office'] = 'doc, docx,  xls, xlsx, ppt, pptx, mdb, accdb, accdbx';
$config['extension_pdf'] = 'pdf';
$config['extension_zip'] = 'rar, zip, 7z';
$config['extension_image'] = 'gif, png, jpg, jpeg';

$config['upload_dir']['t_wall_user_uploads'] = '/upload/wall';
$config['upload_dir']['documents'] = '/upload/documents';
$config['upload_dir']['t_ma_icon'] = '/upload/doc_icon';
$config['upload_dir']['user_info'] = '/upload/avatar';
$config['upload_dir']['pk101'] = '/upload/action/pk101';
$config['upload_dir']['pk206'] = '/upload/action';
$config['upload_dir']['task_evaluation_result_c'] = '/upload/ass_doc';
$config['upload_dir']['ajax_evaluation'] = '/upload/ass_doc';
$config['upload_dir']['task_evaluation_result_m'] = '/upload/ass_doc';
$config['upload_dir']['behavior_evaluation_plan'] = '/upload/ass_doc';
$config['upload_dir']['behavior_evaluation_result'] = '/upload/ass_doc';
$config['upload_dir']['evaluation_assignment_input_self'] = '/upload/ass_doc';
$config['upload_dir']['evaluation_assignment_input_under'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_com_document_plan'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_com_document_result'] = '/upload/ass_doc';
$config['upload_dir']['t_doc_document_detail'] = '/upload/documents';
$config['upload_dir']['t_act_pk206_document_detail'] = '/upload/action';
$config['upload_dir']['t_act_pk101_document_detail'] = '/upload/action';
$config['upload_dir']['t_ma_user'] = '/upload/avatar';
$config['upload_dir']['t_ass_task_evaluation_result'] = '/upload/ass_doc';

$config['upload_dir']['task_evaluation_result_m'] = '/upload/ass_doc';
$config['upload_dir']['task_evaluation_result_c'] = '/upload/ass_doc';
$config['upload_dir']['task_evaluation_result_under'] = '/upload/ass_doc';

$config['upload_dir']['t_ass_task_evaluation_result_document'] = '/upload/ass_doc';

$config['upload_dir']['evaluation_assignment_input_self'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_evaluation_assignment_input_self_detail'] = '/upload/ass_doc';

$config['upload_dir']['evaluation_assignment_input_under'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_evaluation_assignment_input_under_detail'] = '/upload/ass_doc';

$config['upload_dir']['behavior_evaluation_plan'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_behavior_evaluation_detail'] = '/upload/ass_doc';

$config['upload_dir']['behavior_evaluation_result'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_behavior_evaluation_detail_approve']='/upload/ass_doc';

$config['upload_dir']['t_ass_com_document_self'] = '/upload/ass_doc';
$config['upload_dir']['t_ass_com_document_boss'] = '/upload/ass_doc';

$config['upload_dir']['t_act_pk101'] = '/upload/action/pk101';
$config['option_length'] = 170;

$config['extension']['t_ass_task_evaluation_result_document'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';

$config['extension']['task_evaluation_result_m'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
$config['extension']['task_evaluation_result_c'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
$config['extension']['task_evaluation_result_under'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
$config['extension']['behavior_evaluation_plan'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
$config['extension']['behavior_evaluation_result'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
$config['extension']['evaluation_assignment_input_self'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
$config['extension']['evaluation_assignment_input_under'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['extension']['pk206'] = 'doc,docx,xls,xlsx,pdf';
//$config['extension']['document_icon'] = 'gif,png,jpg,jpeg';
//$config['extension']['user_info'] = 'gif,png,jpg,jpeg';
//$config['extension']['user_info'] = 'gif,png,jpg,jpeg';
//$config['extension']['documents'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['extension']['ajax_evaluation'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['extension']['behavior_evaluation_plan'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['extension']['behavior_evaluation_result'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['extension']['evaluation_assignment_input_self'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['extension']['evaluation_assignment_input_under'] = 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,mdb,accdb,accdbx';
//$config['table_name']['Ajax_evaluation'] = 't_ass_task_evaluation_result_document';
//$config['table_name']['documents'] = 't_doc_document_detail';
//$config['table_name']['pk206'] = 't_act_pk206_document_detail';
?>