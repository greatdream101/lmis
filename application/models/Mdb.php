<?php

if (!defined('BASEPATH')) {
        exit('No direct script access allowed');
}

class Mdb extends CI_Model {

        public $level = 0;
        public $display;
        public $groupID = [];
        public $auth = [];

        public function __construct() {
                $this->display = $this->config->item('display');
                $this->load->model('schema_model');
        }

        public function query($sql) {
                $this->db->trans_begin();
                $query = $this->db->query($sql);
                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return false;
                } else {
                        $this->db->trans_commit();
                }
                $result = [];
                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                $result[] = $row;
                        }
                }
                $query->free_result();
                return $result;
        }

        function get_data($table='',$where=[],$strSelect='*'){
                $this->db->select($strSelect)->from($table);
                if(isset($where) && !empty($where)){
                        $this->db->where($where);
                }
                $this->db->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $result = $query->result_array();
            
            return $result;
        }

        public function r_delete($table, $where = []) {
                $this->db->trans_begin();
                if (count($where) == 0) {
                        $this->db->empty_table($table);
                } else {
                        $this->db->delete($table, $where);
                }
//                var_dump($this->db->last_query());
                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return false;
                } else {
                        $this->db->trans_commit();
                        return 1;
                }
        }

        public function delete($table, $data, $indexColumn, $id) {
                $this->db->trans_begin();
//                $data['display'] = 0;
                $data['computerName'] = $this->security_lib->get_computerName();
                $data['deletedDate'] = date('Y-m-d H:i:s');
                $data['deletedID'] = $this->ion_auth->get_userID();
                $this->db->where($indexColumn, $id);
                $this->db->update($table, $data);
                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return false;
                } else {
                        $this->db->trans_commit();
                }
        }

        function update_where($table, $data, $where) {
                $this->db->trans_begin();
                $t = $this->schema_model->get_schema($table);

                foreach ($where as $k => $v) {
                        $this->db->where($k, $v);
                }
                $d = array();
                foreach ($t['schema'] as $k => $v) {
                        if ($k !== $t['indexColumn'])
                                if (array_key_exists($k, $data))
                                        $d[$k] = $data[$k];
                }

                $d['computerName'] = $this->security_lib->get_computerName();
                $d[$this->config->item('update_date')] = date('Y-m-d H:i:s');
                $d[$this->config->item('update_id')] = $this->ion_auth->get_userID();
                $this->db->update($table, $d);
                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return false;
                } else {
                        $this->db->trans_commit();
                        return 1;
                }
        }

        public function update($table, $data, $indexColumn, $id) {
                $this->db->trans_begin();
                $data['computerName'] = $this->security_lib->get_computerName();
                $data[$this->config->item('update_date')] = date('Y-m-d H:i:s');
                $data[$this->config->item('update_id')] = $this->ion_auth->get_userID();
                $this->db->where($indexColumn, $id);
                $this->db->update($table, $data);
                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return false;
                } else {
                        $this->db->trans_commit();
                        return $id;
                }
        }

        public function insert($table, $data) {
                $this->db->trans_begin();
                $t = $this->schema_model->get_schema($table);

                $d = array();
                foreach ($t['schema'] as $k => $v) {
                        if ($k !== $t['indexColumn'])
                                if (array_key_exists($k, $data))
                                        $d[$k] = $data[$k];
                }

                $d['computerName'] = $this->security_lib->get_computerName();
                $d[$this->config->item('display')] = 1;
                $d[$this->config->item('create_date')] = date('Y-m-d H:i:s');
                $d[$this->config->item('create_id')] = $this->ion_auth->get_userID();

                $this->db->insert($table, $d);

                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return false;
                } else {
                        $id = $this->db->insert_id();
                        $this->db->trans_commit();

                        return $id;
                }
        }
        
        public function num_rows($table,$where){
            $this->db->select('*')->from($table)->where($where);
            $query = $this->db->get();
            $num_rows = $query->num_rows(); 
            return $num_rows;
        }

        public function mapping($table) {
                $s = $this->schema_model->get_schema($table);
                $s = $s['schema'];
                $a = [];
                foreach ($s as $k => $v) {
                        if (isset($_POST[$v])) {
                                $a[$v] = $_POST[$v];
                        } else {
                                $vv = explode('_', $v);
                                if ($vv[count($vv) - 1] == 'date') {
                                        $a[$v] = '0000-00-00';
                                } else {
                                        $a[$v] = '';
                                }
                        }
                }
                return $a;
        }

        public function get_idle_timeout() {
                $this->db->select('idle_timeout')->from($this->config->item('web_table'))->where($this->config->item('display'), 1);
                $query = $this->db->get();
                if ($query->num_rows() == 1) {
                        $row = $query->row();
                        $idle_timeout = $row->idle_timeout;
                } else {
                        $idle_timeout = 10;
                }
                return $idle_timeout;
        }

        public function get_website_data() {
                $this->db->select('title,projectName,projectShortName,idleTimeout,description,author,smtpHost,smtpPort,smtpUser,smtpName,smtpPassword')->from($this->config->item('web_table'))->where($this->config->item('display'), 1);
                $query = $this->db->get();
                if ($query->num_rows() == 1) {
                        $row = $query->result_array();
                        $web_main = $row[0];
                        if ($this->ion_auth->is_logged_in()) {
                                $web_main['function_top'] = $this->get_function_top();
                                $web_main['function_tree'] = $this->get_treeviewData();
                        } else {
                                $web_main['function_top'] = [];
                                $web_main['function_tree'] = $this->get_treeviewData();
                        }

                        $web_main['userID'] = $this->ion_auth->get_userID();
                        if (!is_null($web_main['userID'])) {
                                $web_main['username'] = $this->ion_auth->get_user_name();
                                $web_main['groupID'] = $this->ion_auth->get_groupID();

                                $this->db->select('avatar')->from($this->config->item('user_table'))->where('userID', $this->ion_auth->get_userID())->where($this->config->item('display'), 1);
                                $query = $this->db->get();
                                if ($query->num_rows() > 0) {
                                        $row = $query->row();
                                        if (strlen($row->avatar) > 0) {
                                                $avatar = '/upload/avatar/' . $row->avatar;
                                        } else {
                                                $avatar = '/upload/avatar/default.jpg';
                                        }
                                } else {
                                        $avatar = 'default.jpg';
                                }
                                $web_main['avatar'] = $avatar;
                        } else {
                                $web_main['username'] = 'anonymous';

                                $web_main['username'] = '';
                                $web_main['groupID'] = 0;
                                $web_main['avatar'] = 'default.jpg';
                        }

                        return $web_main;
                } else {
                        $this->r_delete('t_web_main');
                        return [];
                }
        }

        public function get_treeviewData() {
                $userID = $this->ion_auth->get_userID();
                $module_id = 1;

                if ((int) $userID > 0) {
                        if ($this->ion_auth->is_admin($userID)) {
                                $page_nav = $this->tree_menu->get_tree(2);
                        } else {

                                $users_groups = $this->ion_auth_model->get_users_groups($userID)->result();

                                if (!is_null($users_groups) && isset($users_groups[0])) {
                                        $groupID = $users_groups[0]->groupID;
                                        $page_nav = $this->tree_menu->get_group_tree(2, $groupID);
                                } else {
//                        $page_nav = get_default_pageNav();
                                        $page_nav = $this->tree_menu->get_tree(3);
                                }
                        }
                } else {
//        $page_nav = get_default_pageNav();
                        $page_nav = $this->tree_menu->get_tree(3);
                }
                return $page_nav;
        }

        public function save_sub_data($schema_data, $tableData, $indexColumn, $id) {

                $user = $userID = $this->ion_auth->get_userID();
                $computerName = $this->security_lib->get_computerName();
                if (strpos($tableData['sub_table_name'], ',') == false) {
                        $subtable = [trim($tableData['sub_table_name'])];
                } else {
                        $subtable = explode(',', $tableData['sub_table_name']);
                }
                if (isset($subtable) && is_array($subtable)) {
//array
                        $subTableAdd = [];
                        $subTableEdit = [];
                        $subTableDelete = [];
                        $subTableDeleteInsert = [];

                        foreach ($subtable as $va) {
                                $t = trim($va);
                                if (strlen($t) > 0) {
                                        $subdata = $this->schema_model->get_schema($t);

                                        $subIndexColumn[$t] = $subdata["indexColumn"];
                                        $subColumn[$t] = $subdata["columns"];

                                        foreach ($_POST as $ke2 => $va2) {
                                                if (strpos($ke2, $t) !== false) {
                                                        $ke2 = str_replace($t, "", $ke2);
                                                        $pos = strrpos($ke2, "_");
                                                        $curid = substr($ke2, $pos + 1);
                                                        $name = substr($ke2, 1, $pos - 1);
                                                        if (substr($curid, -1) == "d") {
                                                                $cur_id = (int) str_replace('d', '', $curid);
                                                                $subTableDelete[$t][] = $cur_id;
                                                        } else if (substr($curid, -1) == "i") {
                                                                if ($curid !== 'idi') {
                                                                        if (substr($name, 0, 1) == '_') {
                                                                                $name = substr($name, 1);
                                                                        }
                                                                        $subTableDeleteInsert[$t][$curid][$name] = $va2;
                                                                }
                                                        } else if (is_numeric($curid)) {
                                                                $subTableEdit[$t][$curid][$name] = $va2;
                                                        } else {
                                                                $subTableAdd[$t][$curid][$name] = $va2;
                                                        }
                                                }
                                        }
                                }
                        }
//                        var_dump($subTableDeleteInsert);
                        foreach ($subTableDeleteInsert as $ke => $va) {
                                $sql = "delete from " . $ke . " where " . $indexColumn . "='" . $id . "'";
                                $this->db->query($sql);

                                foreach ($va as $ke2 => $va2) {
                                        foreach ($va2 as $ke3 => $va3) {
                                                if (strlen($va3) > 0) {
                                                        $data = [$ke3 => $va3, $indexColumn => $id];
                                                        $this->insert($ke, $data);
                                                }
                                        }
                                }
                        }

                        foreach ($subTableDelete as $ke => $va) {
                                foreach ($va as $va2) {
                                        $va2 = str_replace('d', '', $va2);
//                                        $sql = "update " . $ke . " set computerName='" . $computerName . "', display='0', delete_id='$userID', delete_date=now() where " . $subIndexColumn[$ke] . " = '" . $va2 . "'";
                                        //                                        $query = $this->db->query($sql);
                                        $data = [$this->display => 0, 'delete_id' => $userID, 'delete_date' => date('Y-m-d H:i:s')];
                                        $this->update($ke, $data, $subIndexColumn[$ke], $va2);
                                        $id_delete = $va2;
//                                        var_dump($this->db->last_query());
                                }
                        }

                        foreach ($subTableEdit as $tab => $va3) {
                                foreach ($va3 as $subId => $subdat) {
                                        if ($id > 0) {
                                                $sql = $indexColumn . " = " . $id;
                                                foreach ($subColumn[$tab] as $va) {
                                                        if ($va == $subIndexColumn[$tab]) {
                                                                continue;
                                                        }

                                                        if ($va == $indexColumn) {
                                                                continue;
                                                        }

                                                        if (array_key_exists($va, $subdat)) {
                                                                if (strpos($subdat[$va], '/') > 0) {
                                                                        $e = explode('/', $subdat[$va]);
                                                                        if (count($e) == 3) {
                                                                                $y = $e[2] - 543;
                                                                                $m = $e[1];
                                                                                $d = $e[0];
                                                                                if ($y > 0) {
                                                                                        $sql .= ", " . $va . " = '" . $y . '-' . $m . '-' . $d . "'";
                                                                                } else {
                                                                                        $sql .= ", " . $va . " = '" . $subdat[$va] . "'";
                                                                                }
                                                                        } else {
                                                                                $sql .= ", " . $va . " = '" . $subdat[$va] . "'";
                                                                        }
                                                                } else {
                                                                        $sql .= ", " . $va . " = '" . $subdat[$va] . "'";
                                                                }
                                                        }
                                                }

                                                $sql = "update " . $tab . " set " . $sql;
                                                $sql .= ", " . $this->config->item('update_date') . "=now(), " . $this->config->item('update_id') . "='" . $userID . "', computerName='" . $computerName . "'";
                                                $sql .= " where " . $subIndexColumn[$tab] . " = " . $subId;

                                                $query = $this->db->query($sql);
                                                $id_update = $subId;
                                        }
                                }
                        }

                        foreach ($subTableAdd as $tab => $va3) {
                                foreach ($va3 as $subId => $subdat) {
                                        $sql1 = $indexColumn;
                                        $sql2 = $id;
                                        $pass_insert = false;
                                        foreach ($subColumn[$tab] as $va) {
                                                if ($va == $subIndexColumn[$tab]) {
                                                        continue;
                                                }

                                                if ($va == $indexColumn) {
                                                        continue;
                                                }

                                                if (array_key_exists($va, $subdat) && strlen($subdat[$va]) > 0) {
                                                        $sql1 .= "," . $va;
                                                        $sql2 .= ",'" . $subdat[$va] . "'";
                                                        $pass_insert = true;
                                                }
                                        }

                                        if (isset($subTableUpload[$tab][$subId])) {
                                                foreach ($subTableUpload[$tab][$subId] as $k => $v) {
                                                        $sql1 .= "," . $k;
                                                        $sql2 .= ",'" . $v . "'";
                                                        $pass_insert = true;
                                                }
                                        }

                                        $sql1 .= ',' . $this->config->item('create_id') . ',' . $this->config->item('create_date') . ',' . 'computerName';
                                        $sql2 .= ",'$userID',now(), '" . $computerName . "'";
                                        if ($pass_insert) {
                                                $sql = "insert into " . $tab . " (" . $sql1 . ") values (" . $sql2 . ");";
                                                $query = $this->db->query($sql);
                                                $id_insert = $this->db->insert_id();
//                                                        $id = $id_insert;
                                        }
                                }
                        }
                }
                return 1;
        }

        public function save_data($schema_data, $tableData) {
            
                $userID = $this->ion_auth->get_userID();
                $computerName = $this->security_lib->get_computerName();
                $continue = true;

                if (isset($_POST["sys_validation"])) {
                        foreach ($_POST["sys_validation"] as $key => $va) {
                                $this->form_validation->set_rules($key, $key, $va);
                        }

                        if ($this->form_validation->run() === false) {
                                $continue = false;
                                echo json_encode($this->form_validation->_error_array);
                                die();
                        }
                }
                
                $id = 0;
                $this->db->trans_begin();

                if ($continue) {
                        $id_update = 0;
                        $id_insert = 0;
                        $id_delete = 0;

//verify?
                        $table = $tableData['table_name'];
                        $table = str_replace('v_', 't_', $table);
                        if (isset($_POST['id'])) {
                                $id = $_POST['id'];
                        } else {
                                $id = 0;
                        }

                        $indexColumn = $tableData['indexColumn'];
                        $data = $schema_data;

                        $isKeepLog = $tableData['isKeepLog']; /// 1 = isKeepLog, 0 = normal insert or update ;; for master table

                        $Column = $data["columns"];

                        $sql = '';
                        if (isset($indexColumn) && $id > 0) {
                                $sql .= $this->config->item('update_date') . " = now()";
                                $sql .= ", " . $this->config->item('update_id') . " = '" . $userID . "', computerName='" . $computerName . "'";
                                foreach ($Column as $va) {
                                        if ($va == $indexColumn) {
                                                continue;
                                        }

                                        if (array_key_exists($va, $_POST)) {
                                                if (strpos($_POST[$va], '/') > 0) {
                                                        $e = explode('/', $_POST[$va]);
                                                        if (count($e) == 3) {
                                                                $y = $e[2] - 543;
                                                                $m = $e[1];
                                                                $d = $e[0];
                                                                if ($y > 0) {
                                                                        $sql .= ", " . $va . " = '" . $y . '-' . $m . '-' . $d . "'";
                                                                } else {
                                                                        $sql .= ", " . $va . " = '" . htmlspecialchars($_POST[$va], ENT_QUOTES) . "'";
                                                                }
                                                        } else {
                                                                $sql .= ", " . $va . " = '" . htmlspecialchars($_POST[$va], ENT_QUOTES) . "'";
                                                        }
                                                } else {
                                                        $sql .= ", " . $va . " = '" . htmlspecialchars($_POST[$va], ENT_QUOTES) . "'";
                                                }
                                                unset($_POST[$va]);
                                        }
                                }

                                $sql = "update " . $table . " set " . $sql;
                                $sql .= " where " . $indexColumn . "='" . $id . "'";

                                $query = $this->db->query($sql);
//                                var_dump($sql);
                        } else {
                                $sql1 = $this->config->item('create_date') . ',' . $this->config->item('create_id') . ',' . $this->config->item('display') . ',computerName';
                                $sql2 = "now()," . $userID . ",1,'" . $computerName . "'";

                                foreach ($Column as $va) {
                                        if ($va == $indexColumn) {
                                                continue;
                                        }

                                        if (array_key_exists($va, $_POST)) {
                                                $sql1 .= "," . $va;
                                                if (strpos($_POST[$va], '/') > 0) {
                                                        $e = explode('/', $_POST[$va]);
                                                        if (count($e) == 3) {
                                                                $y = $e[2] - 543;
                                                                $m = $e[1];
                                                                $d = $e[0];
                                                                if ($y > 0) {
                                                                        $sql2 .= ", " . $va . " = '" . $y . '-' . $m . '-' . $d . "'";
                                                                } else {
                                                                        $sql2 .= ",'" . htmlspecialchars($_POST[$va], ENT_QUOTES) . "'";
                                                                }
                                                        } else {
                                                                $sql2 .= ",'" . htmlspecialchars($_POST[$va], ENT_QUOTES) . "'";
                                                        }
                                                } else {
                                                        $sql2 .= ",'" . htmlspecialchars($_POST[$va], ENT_QUOTES) . "'";
                                                }
                                                unset($_POST[$va]);
                                        }
                                }

                                if (strlen($tableData['sub_table_name']) == 0) {
                                        $pass_insert = true;
                                } else {
                                        if ($tableData['sub_table_name'] == $table) {
                                                $pass_insert = false;
                                        } else {
                                                $d = explode(',', $tableData['sub_table_name']);
                                                if (!in_array($table, $d)) {
                                                        $pass_insert = true;
                                                } else {
                                                        $pass_insert = false;
                                                }
                                        }
                                }

                                if ($pass_insert) {
//table data is instring subTableData, so do not insert to table
                                        $sql = "insert into " . $table . " (" . $sql1 . ") values (" . $sql2 . ");";
                                        $query = $this->db->query($sql);
//var_dump($this->db->last_query());
                                        $query = $this->query("select LAST_INSERT_ID() as id;");
                                        $id = $query[0]["id"];
                                }
                        }
                        
//                         var_dump($schema_data);
//                         var_dump($tableData);
//                         var_dump($indexColumn);
//                         var_dump($id);
//                         exit();

                        $this->save_sub_data($schema_data, $tableData, $indexColumn, $id);
                        unset($_POST['btn_save']);
                }

                if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                        return 0;
                } else {
//return max($id, $id_insert, $id_update, $id_delete);
                        $this->db->trans_commit();
                        return $id;
                }
        }

        public function get_addonData($uri) {
                $table_wo_code_column = [$this->config->item('user_table')];
                $t = $this->get_table_name($uri);
                $addon = [];
                $table_name = $t['table_name'];
                $functionID = $t['functionID'];

                if ($functionID > 0) {
                        $this->db->select('functionID,addonData,columnData,whereData,indexColumn,orderBy,parentData,linkageData')->from($this->config->item('function_addon_table'))->where('functionID', $functionID);
                        $query = $this->db->get();
//vd::d($this->db->last_query());      
                        if ($query->num_rows() > 0) {
                                foreach ($query->result_array() as $row) {
                                        $ss = $this->schema_model->get_schema($row['addonData']);
                                        if (substr($row['addonData'], 0, 2) == 'v_') {
                                                if (!is_null($row['indexColumn'])) {
                                                        $indexColumn = $row['indexColumn'];
                                                } else {
                                                        $indexColumn = '';
                                                }
                                        } else {
                                                if (!is_null($ss['indexColumn'])) {
                                                        $indexColumn = $ss['indexColumn'];
                                                } else {
                                                        $indexColumn = '';
                                                }
                                        }

                                        if (strlen($row['columnData']) > 0) {
                                                $columns = explode(',', $row['columnData']);
                                                if (strlen($indexColumn) > 0 && !in_array($indexColumn, $columns)) {
                                                        array_push($columns, $indexColumn);
                                                }

                                                $columns = implode(',', $columns);
                                        } else {
                                                if (substr($row['addonData'], 0, 2) == 'v_') {
                                                        if (strlen($row['columnData']) > 0) {
                                                                $columns = $row['columnData'];
                                                                if (strlen($indexColumn) > 0 && !in_array($indexColumn, $columns)) {
                                                                        array_push($columns, $indexColumn);
                                                                }

                                                                $columns = implode(',', $columns);
                                                        } else {
                                                                $columns = '*';
                                                        }
                                                } else {
                                                        if (strlen($row['columnData']) > 0) {
                                                                $ss = $ss['schema'];
                                                                $ss = array_keys($ss);
                                                                $ss = array_diff($ss, $this->config->item('ignore_columns'));
                                                                $columns = implode(',', $ss);
                                                        } else {
                                                                $columns = '*';
                                                        }
                                                }
                                        }

                                        $where = $this->display . '=1';
                                        if (strlen($row['whereData']) > 0) {
                                                $where .= ' and ' . $row['whereData'];
                                        }

                                        $orderBy = 'order by';
                                        if (strlen($row['orderBy']) > 0) {
                                                $orderBy .= ' ' . $row['orderBy'];
                                        } else {
                                                if (strpos($row['columnData'], 'code') !== false) {
                                                        $orderBy .= ' code asc';
                                                } else {
                                                        if (strlen($indexColumn) > 0)
                                                                $orderBy .= ' ' . $indexColumn . ' asc';
                                                        else {
                                                                $orderBy = '';
                                                        }
                                                }
                                        }

                                        $linkageData = [];
                                        if (strlen($row['parentData']) > 0) {
                                                $linkageData = $this->get_linkageData($row['addonData'], $ss, $row['parentData'], $row['linkageData']);
                                        }
//var_dump($where);
                                        $addon[$indexColumn] = ['functionID'=>$row['functionID'], 'table_name' => $row['addonData'], 'data' => $this->get($row['addonData'], $where, $columns, $orderBy, $indexColumn), 'indexColumn' => $indexColumn, 'columnData' => $columns, 'linkageData' => $linkageData];
                                }
                        }
                }
                return $addon;
        }

        public function get_linkageData($table_name, $ss, $parentData, $linkageData) {
                $indexColumn_p = $ss['indexColumn']; //dealer_id, model_id

                $s = $this->schema_model->get_schema($parentData);
                $indexColumn_v = $s['indexColumn']; //dealer_groupID, model_groupID

                $this->db->select($indexColumn_v . ', code, name')->from($parentData)->where($this->display, 1);
                $query = $this->db->get();
                $data = [];
                foreach ($query->result_array() as $row) {
                        $a = [];
                        $a[$indexColumn_v] = $row[$indexColumn_v];
                        $a['code'] = $row['code'];
                        $a['name'] = $row['name'];
                        $this->db->select($indexColumn_p)->from($linkageData)->where($indexColumn_v, $row[$indexColumn_v])->where($this->display, 1);
                        $query1 = $this->db->get();

                        foreach ($query1->result_array() as $row1) {
                                $this->db->select('*')->from($table_name)->where($indexColumn_p, $row1[$indexColumn_p])->where($this->display, 1);
                                $query2 = $this->db->get();
                                foreach ($query2->result_array() as $row2) {
                                        $a['data'][$row2[$indexColumn_p]] = $row2;
                                }
                        }
                        $data[$row[$indexColumn_v]] = $a;
                }

                return $data;
        }

        public function get_auth_data($url) {
                $userID = $this->session->userdata('userID');

                $auth = [];
                $this->db->select('privilegeID, name')->from($this->config->item('privilege_table'))->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $privilege_data = [];
                $num_priv = 0;
                foreach ($query->result() as $row) {
                        $a = strtolower($row->name);
                        $privilege_data[$row->privilegeID] = $a;
                        $num_priv++;
                }
                if ($this->ion_auth->is_admin($userID)) {
                        foreach ($privilege_data as $k => $v) {
                                $p[strtolower($v)] = 1;
                        }
                        return ['auth_data' => $p, 'userID' => $userID];
                } else {

                        $users_groups = $this->ion_auth_model->get_users_groups($userID)->result();
                        if (count($users_groups) > 0) {
                                $groupID = $users_groups[0]->groupID;

                                if ((int) $groupID > 0) {
                                        $this->db->select('functionID')->from($this->config->item('function_table'))->where('functionUri', $url);
                                        $query = $this->db->get();
                                        $num_rows = $query->num_rows();
                                        if ($num_rows > 0) {
                                                $row = $query->row();
                                                $fid = $row->functionID;

                                                if ((int) $groupID > 0) {
                                                        $sql = "select aes_decrypt(authLevel, 'soso_8R7f') as authLevel from  " . $this->config->item('group_table') . " where groupID in (" . $groupID . ")";
//                                                        $sql = "select authLevel from ".$this->config->item('group_table')." where groupID in (" . $groupID . ")";
                                                        $query = $this->db->query($sql);
                                                        foreach ($query->result_array() as $row) {
                                                                if (strlen($row['authLevel']) > 0 && !is_null($row['authLevel'])) {
                                                                        $c = explode(',', $row['authLevel']);
                                                                        foreach ($c as $ck => $cv) {
                                                                                $a = explode('-', $cv);
                                                                                $a0 = $a[0];
                                                                                $b = explode('_', $a0);
                                                                                $functionID = $b[1];
                                                                                if ($functionID == $fid) {
                                                                                        $privileges = $a[1];
                                                                                        $auth[$groupID] = str_pad($privileges, $num_priv, '0', STR_PAD_RIGHT);
                                                                                }
                                                                        }
                                                                } else {
                                                                        return ['auth_data' => [], 'userID' => $userID];
                                                                }
                                                        }
                                                        if (isset($auth[$groupID])) {
                                                                foreach ($privilege_data as $k => $v) {
                                                                        $p[strtolower($v)] = substr($auth[$groupID], $k - 1, 1);
                                                                }
                                                        } else {
                                                                return ['auth_data' => [], 'userID' => $userID];
                                                        }

                                                        return ['auth_data' => $p];
                                                } else {
                                                        return ['auth_data' => [], 'userID' => $userID];
                                                }
                                        } else {
                                                return ['auth_data' => [], 'userID' => $userID];
                                        }
                                } else {
                                        return ['auth_data' => [], 'userID' => $userID];
                                }
                        } else {
                                return ['auth_data' => [], 'userID' => $userID];
                        }
                }
        }

        public function get_new($table, $columns = "*", $where = [], $order = "") {

                $totalRecord = $this->db->count_all($table);
                if (isset($_GET['size'])) {
                        $totalRecordPerPage = $_GET['size'];
                } else {
                        $totalRecordPerPage = 10;
                }

//get total pages
                $totalPage = floor($totalRecord / $totalRecordPerPage); //floor;
                $modResult = $totalRecord % $totalRecordPerPage;
                if ($modResult != 0) {
                        $totalPage++;
                }

                if (isset($_GET["page"])) {
                        $pageNo = $_GET["page"];
                }
                //FROM AJAX GET
                else {
                        $pageNo = 0;
                }

//set the SQL limit and offset
                $limit = $totalRecordPerPage;
                $offset = (($pageNo) * $totalRecordPerPage);

                $cc = explode(',', $columns);
                $cw = [];
                $co = '';
                for ($i = 0; $i < count($cc); $i++) {
                        if (isset($_GET['filter'][$i])) {
                                $cw[$cc[$i]] = $_GET['filter'][$i];
                        }

                        if (isset($_GET['column'][$i])) {
//                                $co[$cc[$i]] = $_GET['column'][$i];
                                if ($_GET['column'][$i] == 1) {
                                        $co .= $cc[$i] . ' asc,';
                                } else {
                                        $co .= $cc[$i] . ' desc,';
                                }
                        }
                }

                if (strlen($co) > 0) {
                        $co = substr($co, 0, strlen($co) - 1);
                        $this->db->select($columns)->from($table)->like($cw)->orderBy($co)->limit($limit, $offset);
                } else {
                        $this->db->select($columns)->from($table)->like($cw)->limit($limit, $offset);
                }

                $query = $this->db->get();

                $a = ['total_rows' => $totalRecord, 'rows' => $query->result_array(), 'headers' => explode(',', $columns)];
                print json_encode($a);
        }

        public function get($table, $where = "", $column = "*", $last = "", $indexColumn = '') {

                $tablename = $table;
                $result = [];

                $sql = "select " . $column . " from " . $tablename;

                if (is_numeric($where)) {
                        $sql .= " where id = '" . $where . "'";
                } else if (is_array($where)) {
                        $sql .= " where 1=1";
                        foreach ($where as $ke => $va) {
                                $sql .= " And " . $tablename . "." . $ke . " = '" . $va . "' ";
                        }
                } else {
                        $sql .= ' where ' . $where;
                }

                if (strpos($last, 'name') == false) {
                        $sql .= " " . $last;
                }

                $query = $this->db->query($sql);
                $result = [];
//vd::d($sql);
                if ($query->num_rows() > 0) {
                        if (strlen($indexColumn) > 0) {
                                foreach ($query->result_array() as $row) {
                                        $result[$row[$indexColumn]] = $row;
                                }
                        } else {
                                foreach ($query->result_array() as $row) {
                                        array_push($result, $row);
                                }
                        }
                } else {
                        $s = $this->schema_model->get_schema($tablename);
                        $s = $s['schema'];
                        foreach ($s as $k => $v) {
                                $col[$k] = '';
                        }
                        array_push($result, $col);
                }

                $query->free_result();

                return $result;
        }

//        function get_data($t, $s, $role = 'list') {
        //                $columns = $t['columnData'];
        //                $indexColumn = $s['indexColumn'];
        //                if (strlen($columns) == 0) {
        //                        $a_columns = array_keys($s['schema']);
        //                        if (!in_array($indexColumn, $a_columns))
        //                                array_push($a_columns, $indexColumn);
        //                        $s_columns = implode(',', $a_columns);
        //                }
        //
    //                $table = $t['table_name'];
        //                $view = $t['view_name'];
        //                if (strlen($t['columnData']) > 0) {
        //                        $column = $t['columnData'];
        //                } else {
        //                        $column = '';
        //                }
        //                $result = array();
        //
    //                if ($role == 'recycle') {
        //                        $active = 0;
        //                } else {
        //                        $active = 1;
        //                }
        //
    //                if (strlen($view) > 0) {
        //                        $where = strlen($t['whereData']) > 0 ? $t['whereData'] : "1=1";
        //                        if (strlen($column) > 0) {
        //                                $this->db->select($column)->from($view)->where($where)->where($this->display, $active);
        //                        } else {
        //                                $this->db->select('*')->from($view)->where($where)->where($this->display, $active);
        //                        }
        //                        $query = $this->db->get();
        //
    //                        $rows = array();
        //                        $a_columns = array();
        //                        if ($query->num_rows() > 0) {
        //                                $rows = $query->result_array();
        //                                $header = $rows[0];
        //                                foreach ($header as $k => $v) {
        //                                        if (!in_array($k, $this->config->item('ignore_columns')) && substr($k, strlen($k) - 3, 3) !== '_id')
        //                                                array_push($a_columns, $k);
        //                                }
        //                        }else {
        //                                $header = array_keys($s['schema']);
        //                                foreach ($header as $k => $v) {
        //                                        if (!in_array($v, $this->config->item('ignore_columns')) && substr($v, strlen($k) - 3, 3) !== '_id')
        //                                                array_push($a_columns, $v);
        //                                }
        //                        }
        //
    //                        $result = array('columns' => $a_columns, 'data' => $rows, 'role' => $role);
        //                } else {
        //                        if (strlen($table) > 0) {
        //                                $where = strlen($t['whereData']) > 0 ? $t['whereData'] : "1=1";
        //                                if (strlen($column) > 0) {
        //                                        $this->db->select($column)->from($table)->where($where)->where($this->display, $active);
        //                                } else {
        //                                        $this->db->select($s_columns)->from($table)->where($where)->where($this->display, $active);
        //                                }
        //                                $query = $this->db->get();
        //                                $rows = array();
        //
    //                                $a_columns = array();
        //                                if ($query->num_rows() > 0) {
        //                                        $rows = $query->result_array();
        //                                        $header = $rows[0];
        //                                        foreach ($header as $k => $v) {
        //                                                if (!in_array($k, $this->config->item('ignore_columns')) && substr($k, strlen($k) - 3, 3) !== '_id')
        //                                                        array_push($a_columns, $k);
        //                                        }
        //                                }else {
        //                                        $header = array_keys($s['schema']);
        //                                        foreach ($header as $k => $v) {
        //                                                if (!in_array($v, $this->config->item('ignore_columns')) && substr($v, strlen($k) - 3, 3) !== '_id')
        //                                                        array_push($a_columns, $v);
        //                                        }
        //                                }
        //
    //                                $result = array('columns' => $a_columns, 'data' => $rows, 'role' => $role);
        //                        } else {
        //                                $result = array('columns' => array(), 'data' => array());
        //                        }
        //                }
        //
    //                return $result;
        //        }

        public function get_data_table($sTable, $aColumns = '*', $sIndexColumn, $where_value = '', $action = 'list', $orderBy = '') {

                /*
                 * Paging
                 */

                $sLimit = "";

                if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
                        $sLimit = "LIMIT " . intval($_GET['iDisplayStart']) . ", " .
                                intval($_GET['iDisplayLength']);
                }

                /*
                 * Ordering
                 */

                $sOrder = "";
                if (isset($_GET['iSortCol_0'])) {
                        $sOrder = "ORDER BY  ";
                        for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                                        $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
                    " . ($_GET['sSortDir_' . $i] === 'asc' ? 'desc' : 'asc') . ", ";
                                }
                        }

                        $sOrder = substr_replace($sOrder, "", -2);
                        if ($sOrder == "ORDER BY") {
                                $sOrder = "";
                        }
                }

                if (strlen($orderBy) > 0)
                        $sOrder = 'ORDER BY ' . $orderBy;

//                if (strtolower($sOrder) == strtolower("ORDER BY  " . $sIndexColumn . " ASC")) {
                //                        $sOrder = "ORDER BY " . $sIndexColumn . " DESC";
                //                }

                /*
                 * Filtering
                 * NOTE this does not match the built-in DataTables filtering which does it
                 * word by word on any field. It's possible to do here, but concerned about efficiency
                 * on very large tables, and MySQL's regex functionality is very limited
                 */
                if ($action == 'list') {
                        $sWhere = 'where 1 and ' . $this->display . '=1';
                } else {
                        $sWhere = 'where 1 and ' . $this->display . '=0';
                }
                if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
                        $sWhere = "WHERE (";
                        for ($i = 0; $i < count($aColumns); $i++) {
                                if (substr($aColumns[$i], strlen($aColumns[$i]) - 5) == '_date') {
                                        $sWhere .= $aColumns[$i] . " LIKE '%000-00-00%' OR ";
                                } else {
                                        $sWhere .= $aColumns[$i] . " LIKE '%" . $this->escape_str(trim($_GET['sSearch'])) . "%' OR ";
                                }
                        }
                        $sWhere = substr_replace($sWhere, "", -3);
                        $sWhere .= ') ';
                }

                /* Individual column filtering */

                for ($i = 0; $i < count($aColumns); $i++) {
                        if (isset($_GET['sSearch_' . $i]) && strlen($_GET['sSearch_' . $i]) > 0) {
                                if ($sWhere == "") {
                                        $sWhere = "WHERE ";
                                } else {
                                        $sWhere .= " AND ";
                                }
                                $sWhere .= $aColumns[$i] . " LIKE '%" . $this->escape_str(trim($_GET['sSearch_' . $i])) . "%' ";
                        }
                }

                if (strlen($where_value) > 0) {
                        $sWhere .= ' and ' . $where_value;
                }

                /*
                 * SQL queries
                 * Get data to display
                 */
                $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
    FROM   $sTable
    $sWhere
    $sOrder
    $sLimit
  ";

                $rResult = $this->db->query($sQuery);

//var_dump($this->db->last_query());die();
                $sQuery = "
    SELECT FOUND_ROWS() as count
  ";

                $rResultFilterTotal = $this->db->query($sQuery);
                $iFilteredTotal = $rResultFilterTotal->row();
                $iFilteredTotal = $iFilteredTotal->count;

                $sQuery = "
    SELECT COUNT(*) as count
    FROM   $sTable
                                   WHERE " . $this->display . "=1
  ";
                if (strlen($where_value) > 0) {
                        $sQuery .= ' and ' . $where_value;
                }

                $rResultTotal = $this->db->query($sQuery);

                $iTotal = $rResultTotal->row();
                $iTotal = $iTotal->count;

                if (isset($_GET['sEcho'])) {
                        $sEcho = intval($_GET['sEcho']);
                } else {
                        $sEcho = 0;
                }

                $output = [
                    "sEcho" => $sEcho,
                    "iTotalRecords" => $iTotal,
                    "iTotalDisplayRecords" => $iFilteredTotal,
                    "aaData" => [],
                ];
                $schema = $this->schema_model->get_schema(str_replace('v_', 't_', $sTable));
                foreach ($rResult->result_array() as $aRow) {
                        $row = [];
                        for ($i = 0; $i < count($aColumns); $i++) {
                                $row[] = $aRow[$aColumns[$i]];
                        }
                        $output['aaData'][] = $row;
                }

                echo json_encode($output);
        }

        public function get_subTableData($tableData, $schema_data, $id) {

                $subTableData = $tableData['sub_table_name'];
                if (strlen($tableData['view_name']) > 0) {
                        $index_colum = $tableData['indexColumn'];
                } else {
                        $index_colum = $schema_data['indexColumn'];
                }
                if (strpos($subTableData, ',') !== false) {
                        $subTableData = explode(',', $subTableData);
                } else {
                        $subTableData = [$subTableData];
                }

                $a = [];

                foreach ($subTableData as $k => $v) {
                        if (strlen($v) > 0) {
                                $this->db->select('*')->from($v)->where($index_colum, $id)->where($this->config->item('display'), 1)->orderBy('sortOrder', 'asc');
                                $query = $this->db->get();

                                $data = $query->result_array();
                                $new_data = [];
                                foreach ($data as $kk => $vv) {
                                        $aa = array_diff(array_keys($vv), $this->config->item('ignore_columns'));

                                        $new_data[] = array_intersect_key($vv, array_flip($aa));
                                }

                                if ($query->num_rows() > 0) {
                                        $a[$v] = $new_data;
                                } else {
                                        $a[$v] = [];
                                }
                        }
                }

                return $a;
        }

        public function get_data_by_id($tableData, $schema_data, $id) {
                if ($id == 0) {
                        return array();
                } else {
                        if (strlen($tableData['view_name']) > 0) {
                                $this->db->select('*')->from($tableData['view_name'])->where($tableData['indexColumn'], $id);
                        } else {
                                $this->db->select('*')->from($tableData['table_name'])->where($schema_data['indexColumn'], $id);
                        }

                        $query = $this->db->get();
//var_dump($this->db->last_query());
                        if ($query->num_rows() > 0) {
                                $row = $query->row_array();
                                if (strlen($tableData['view_name']) > 0) {
                                        $row['id'] = $row[$tableData['indexColumn']];
                                } else {
                                        $row['id'] = $row[$schema_data['indexColumn']];
                                }
                                $row['subTableData'] = $this->get_subTableData($tableData, $schema_data, $id);
                        } else {
                                $row = [];

                                foreach ($schema_data['schema'] as $k => $v) {
                                        if ($v['column_name'] == $schema_data['indexColumn']) {
                                                $row[$v['column_name']] = 0;
                                        } else {
                                                $row[$v['column_name']] = '';
                                        }
                                }
                                $row['subTableData'] = [];
                        }

                        return $row;
                }
        }

        public function get_tableData($controller_name, $method = 'list', $id = 0) {
             
                if ($this->ion_auth->logged_in() && $controller_name !== 'auth') {
                        switch ($method) {
                                case 'list':
                                        $tableData = $this->get_table_name($controller_name);
                                        $schema_data = $this->schema_model->get_schema($tableData['table_name']);
                                        $auth_data = $this->get_auth_data($controller_name);

                                        $data_by_id = [];
                                        if (strlen($tableData['table_name']) == 0) {
                                                $addonData = $this->get_addonData($controller_name);
                                        } else {
                                                $addonData = [];
                                        }
                                        $data = [];
                                        return ['tableData' => $tableData, 'schema_data' => $schema_data, 'auth_data' => $auth_data, 'addonData' => $addonData, 'data' => $data, 'edit_data' => $data_by_id, 'role' => $method, 'id' => 0];
                                case 'add':
                                        //insert
                                        $tableData = $this->get_table_name($controller_name);
                                        $schema_data = $this->schema_model->get_schema($tableData['table_name']);
                                        $data_by_id = $this->get_data_by_id($tableData, $schema_data, 0);
                                        $addonData = $this->get_addonData($controller_name);
                                        $auth_data = $this->get_auth_data($controller_name);
                                        return ['tableData' => $tableData, 'schema_data' => $schema_data, 'auth_data' => $auth_data, 'addonData' => $addonData, 'data' => [], 'edit_data' => $data_by_id, 'role' => $method];
                                case 'save':
                                        //
                                        return ['tableData' => $tableData, 'schema_data' => $schema_data, 'auth_data' => $auth_data, 'addonData' => [], 'data' => [], 'edit_data' => [], 'role' => $method];
                                default: //edit or delete
                                        $tableData = $this->get_table_name($controller_name);
                                        $schema_data = $this->schema_model->get_schema($tableData['table_name']);
                                        $data_by_id = $this->get_data_by_id($tableData, $schema_data, $id);
                                        $addonData = $this->get_addonData($controller_name);
                                        $auth_data = $this->get_auth_data($controller_name);
                                        return ['tableData' => $tableData, 'schema_data' => $schema_data, 'auth_data' => $auth_data, 'addonData' => $addonData, 'data' => [], 'edit_data' => $data_by_id, 'method' => $method];
                        }
                } else {
                        return ['tableData' => [], 'schema_data' => [], 'auth_data' => [], 'addonData' => [], 'data' => [], 'edit_data' => [], 'role' => $method];
                }
        }

        public function get_table_name($uri) {
                if (strlen($uri) > 0) {
                        $u = $this->get($this->config->item('function_table'), array('functionUri' => $uri), 'functionID, tableData, viewData, columnData, whereData,isKeepLog, saveTypeID, subTableData,indexColumn, orderBy');
                        
                        if (count($u) > 0) {
                                $functionID = $u[0]['functionID'];
                                $table_name = $u[0]['tableData'];
                                if ($table_name == null) {
                                        $table_name = '';
                                }

                                $view_name = $u[0]['viewData'];
                                if ($view_name == null) {
                                        $view_name = '';
                                }

                                $sub_table_name = $u[0]['subTableData'];
                                if ($sub_table_name == null) {
                                        $sub_table_name = '';
                                }

                                if ($u[0]['subTableData'] == null) {
                                        $sub_table_name = '';
                                }

                                if ($u[0]['viewData'] == null) {
                                        $view_name = '';
                                }

                                if ($u[0]['columnData'] == null) {
                                        $columnData = '';
                                } else {
                                        $columnData = $u[0]['columnData'];
                                }

                                if ($u[0]['whereData'] == null) {
                                        $whereData = '';
                                } else {
                                        $whereData = $u[0]['whereData'];
                                }

                                if ($u[0]['orderBy'] == null) {
                                        $orderBy = '';
                                } else {
                                        $orderBy = $u[0]['orderBy'];
                                }

                                $isKeepLog = $u[0]['isKeepLog'];
                                if ($isKeepLog == null) {
                                        $isKeepLog = '';
                                }

                                $saveTypeID = $u[0]['saveTypeID'];
                                if ($saveTypeID == null) {
                                        $saveTypeID = '';
                                }

                                if (strlen($table_name) > 0) {
                                        if (strlen($view_name) > 0) {
                                                $indexColumn = $u[0]['indexColumn'];
                                        } else {
                                                $this->db->select('column_name')->from('information_schema.key_column_usage')->where('table_name', trim($table_name))->where('TABLE_SCHEMA', $this->db->database);
                                                $query = $this->db->get();
                                                $row = $query->row_array();
                                                $indexColumn = $row['column_name'];
                                        }
                                } else {
                                        $indexColumn = '';
                                }

                                return ['functionID' => $functionID, 'table_name' => $table_name, 'sub_table_name' => $sub_table_name, 'view_name' => $view_name, 'columnData' => $columnData, 'whereData' => $whereData, 'isKeepLog' => $isKeepLog, 'saveTypeID' => $saveTypeID, 'indexColumn' => $indexColumn, 'uri' => $uri, 'orderBy' => $orderBy];
                        } else {
                                return ['functionID' => '', 'table_name' => '', 'sub_table_name' => '', 'view_name' => '', 'columnData' => '', 'whereData' => '', 'isKeepLog' => '', 'saveTypeID' => '', 'uri' => $uri, 'indexColumn' => '', 'orderBy' => ''];
                        }
                } else {
                        return ['functionID' => '', 'table_name' => '', 'sub_table_name' => '', 'view_name' => '', 'columnData' => '', 'whereData' => '', 'isKeepLog' => '', 'saveTypeID' => '', 'uri' => $uri, 'indexColumn' => '', 'orderBy' => ''];
                }
        }

        public function get_function_top() {
                $this->db->select('name,functionUri,icon')->from($this->config->item('function_table'))->where($this->config->item('display'), '1')->where('position', '1');
                $query = $this->db->get();
                $o = [];
                foreach ($query->result() as $row) {
                        $e = ['name' => $row->name, 'uri' => $row->functionUri, 'icon' => $row->icon];
                        array_push($o, $e);
                }
                return $o;
        }

        public function get_functionID($fo, $fc) {

                $this->db->select('functionID')->from($this->config->item('function_table'))->where('display', '1')->where('functionUri', $fo . '/' . $fc)->limit(1);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                        $row = $query->row();
                        return $row->functionID;
                } else {
                        return 0;
                }
        }

        function save_group_privilege() {
                $this->load->library('auth_level');

                $result = $this->auth_level->in($_POST);

                $userID = $this->ion_auth->get_userID();
                foreach ($result as $k => $v) {

                        $data['authLevel'] = $this->encryption->encrypt($v);

                        $this->mdb->update_where($this->config->item('group_table'), $data, array('groupID' => $k));
                }
        //        var_dump($this->db->last_query());
                return 1;
        }

        function get_group_function($groupID) {

                if (strpos($groupID, ',') !== false) {
                        $gid = explode(',', $groupID);
                } else {
                        $gid = array($groupID);
                }

                $this->db->select('privilegeID, name')->from($this->config->item('privilege_table'))->where($this->display, 1);
                $query = $this->db->get();

                $num_priv = 0;
                foreach ($query->result() as $row) {
                        $num_priv++;
                }

                foreach ($gid as $g => $gv) {
                        if (strlen($gv) > 0) {
                                $sql = 'select groupID, code, name, authLevel from ' . $this->config->item('group_table') . ' where groupID in (' . $gv . ')';

                                $query = $this->db->query($sql);

                                foreach ($query->result_array() as $row) {

                                        array_push($this->groupID, $row);

                                        if (strlen($row['authLevel']) > 0) {
                                                $c = explode(',', $this->encryption->decrypt($row['authLevel']));

                                                foreach ($c as $ck => $cv) {
                                                        $a = explode('-', $cv);
                                                        if (count($a) == 2) {
                                                                $a0 = $a[0];
                                                                $b = explode('_', $a0);
                                                                $functionID = $b[1];
                                                                $privileges = $a[1];
                                                                $this->auth[$gv][$functionID] = str_pad($privileges, $num_priv, '0', STR_PAD_RIGHT);
                                                        }
                                                }
                                        }
                                }
                        }
                }

                $this->privileges = $this->mdb->get($this->config->item('privilege_table'), array($this->display => 1), 'privilegeID, code, name');

                $this->db->select('functionID')->from($this->config->item('function_table'))->where($this->display, 1);
                $query = $this->db->get();
                $fp = array();
                foreach ($query->result() as $row) {
                        $functionID = $row->functionID;
                        $this->db->select('privilegeID')->from($this->config->item('function_privilege_table'))->where('functionID', $functionID)->where($this->display, 1);
                        $query1 = $this->db->get();
                        $e = array();
                        foreach ($query1->result() as $row1) {
                                array_push($e, $row1->privilegeID);
                        }
                        $fp[$functionID] = $e;
                }

                $page_nav = $this->tree_menu->get_tree(2);


//                $this->o = $this->mcl->cb('Select All', 'group_function_area', '', '', '', 'checkbox-master group-function');
                //                ($label, $name, $max_length = 0, $mask = '', $search_container = '', $placeholder = '', $value = '', $validations = '', $class = '')
                //                $this->o.= $this->mcl->generate_textbox('', 'search_group_function', 0, '', 'group_function_area');
                $this->o = '<input type=checkbox id=group_function_area name=group_function_area class="checkbox-master group-function">' . ' ' . $this->mcl->gl('select_all');
                $this->o .= "<div id=group_function_area>";
                $this->o .= '<table class="table table-striped table-bordered do_not_clear my_table_input table-checkbox">';
                $this->o .= '<thead>';
                $this->o .= '<tr data-level="header" class="header">';
                $this->o .= '<th></th>';

                foreach ($this->groupID as $k => $v) {
                        $this->o .= '<th>';
                        $this->o .= $v['name'];
                        $this->o .= '</th>';
                }
                $this->o .= '</tr>';
                $this->o .= '</thead>';
                $this->o .= '<tbody>';
                $this->o .= $this->draw($page_nav, $fp, 'cb-privilege');
                $this->o .= '</tbody>';
                $this->o .= '</table>';
                $this->o .= '</div>';

                return $this->o;
        }

        public function draw($tree, $fp, $class) {
//                $custom_view_function = array(1, 5, 6, 8, 9);

                foreach ($tree as $k => $v) {
                        if (strtolower($v['name']) !== 'functions') {
                                $lv = $v['level'];
                                $functionID = $v['functionID'];
                                $fpp = $fp[$functionID];

//                                $custom_view = (int) $v['custom_view'];
                                $lv1 = (int) $lv + 1;
                                $this->o .= '<tr id="' . $functionID . '" data-level="' . $lv1 . '"><td><div class="indent-' . $lv . '">' . $v['code'] . ' ' . $v['name'] . '</div></td>';
                                if (!isset($v['sub'])) {
                                        foreach ($this->groupID as $kk => $vv) {
                                                $this->o .= '<td class="data">';
                                                $vvv = '';
                                                if (isset($this->auth[$vv['groupID']][$functionID])) {
//                                                        if ($custom_view == 0) {
                                                        foreach ($this->privileges as $p => $pv) {
                                                                $label = '<i class="fa fa-' . strtolower($pv['code']) . '"></i>';
                                                                $name = $vv['groupID'] . '_' . $functionID;
                                                                $value = $pv['privilegeID'];
//                                                                $icon = 'fa-' . strtolower($pv['code']);
                                                                $icon = '';
                                                                if (in_array($value, $fpp)) {
                                                                        if (substr($this->auth[$vv['groupID']][$functionID], $value - 1, 1) == '1') {
                                                                                $this->o .= '<input type=checkbox name=' . $name . ' id=' . $name . ' class="row0 do_not_clear cb-privilege" checked value=' . $value . '>' . $label;
                                                                        } else {
                                                                                $this->o .= '<input type=checkbox name=' . $name . ' id=' . $name . ' class="row0 do_not_clear cb-privilege" value=' . $value . '>' . $label;
                                                                        }
                                                                }
                                                        }
                                                        $vvv = $this->auth[$vv['groupID']][$functionID];
                                                        $this->o .= form_hidden($vv['groupID'] . '_' . $functionID, $vvv);
                                                } else {

                                                        $vvv = '';
                                                        foreach ($this->privileges as $p => $pv) {
                                                                $label = '<i class="fa fa-' . strtolower($pv['code']) . '"></i>';
                                                                $name = $vv['groupID'] . '_' . $functionID;
                                                                $value = $pv['privilegeID'];

                                                                $icon = '';
                                                                if (in_array($value, $fpp)) {
                                                                        $this->o .= '<input type=checkbox name=' . $name . ' id=' . $name . ' class="row0 do_not_clear cb-privilege" value=' . $value . '>' . $label;
                                                                        if (isset($this->auth[$vv['groupID']][$functionID])) {
                                                                                $vvv .= '1';
                                                                        } else {
                                                                                $vvv .= '0';
                                                                        }
                                                                } else {
                                                                        $vvv .= '0';
                                                                }
                                                        }
//
                                                        $this->o .= form_hidden($vv['groupID'] . '_' . $functionID, $vvv);
                                                }
                                                $this->o .= '</td>';
                                        }
                                } else {
                                        foreach ($this->groupID as $kk => $vv) {
                                                $this->o .= '<td class="data">';

                                                $label = '<i class="fa fa-eye"></i>';
                                                $name = $vv['groupID'] . '_' . $functionID;

                                                if (isset($this->auth[$vv['groupID']][$functionID])) {
                                                        $vvv = $this->auth[$vv['groupID']][$functionID];
                                                        $value = '1';
                                                        if (substr($vvv, 0, 1) == 1) {
                                                                $this->o .= '<input type=checkbox name=' . $name . ' id=' . $name . ' class="row0 do_not_clear cb-privilege" checked value=' . $value . '>' . $label;
                                                        } else {
                                                                $this->o .= '<input type=checkbox name=' . $name . ' id=' . $name . ' class="row0 do_not_clear cb-privilege" value=' . $value . '>' . $label; //$this->mcl->cb($name, array(). '', 'row0 do_not_clear');
                                                        }
                                                } else {
                                                        $value = '1';
                                                        $this->o .= '<input type=checkbox name=' . $name . ' id=' . $name . ' class="row0 do_not_clear cb-privilege" value=' . $value . '>' . $label;
                                                        $vvv = '';
                                                        foreach ($this->privileges as $p => $pv) {
                                                                $vvv .= '0';
                                                        }
                                                }
                                                $this->o .= form_hidden($vv['groupID'] . '_' . $functionID, $vvv);
                                                $this->o .= '</td>';
                                        }
                                }
                                $this->o .= '</tr>';
                                if (isset($v['sub']) && count($v['sub']) > 0) {
                                        $this->o .= $this->draw($v['sub'], $fp, $class);
                                }
                        }
                }
        }

        public function escape_str($str, $like = false) {
                if (is_array($str)) {
                        foreach ($str as $key => $val) {
                                $str[$key] = $this->escape_str($val, $like);
                        }

                        return $str;
                }

                $str = addslashes($str);

// escape LIKE condition wildcards
                if ($like === true) {
                        $str = str_replace(['%', '_'], ['\\%', '\\_'], $str);
                }

                return $str;
        }

        public function undelete_data() {

                if (isset($_POST['id'])) {
//                        $data['indexColumn'] = $_POST['indexColumn'];
                        $data[$this->config->item('display')] = 1;
                        $this->delete($_POST['table_name'], $data, $_POST['indexColumn'], $_POST['id']);
//                        $user = $userID = $this->ion_auth->get_userID();
                        //                        $computerName = $this->security_lib->get_computerName();
                        //                        $sql = "update " . $_POST['table_name'] . " set computerName='" . $computerName . "', display = 1 ,edit_date = now(),edit_id = " . $userID . " where " . $_POST['indexColumn'] . " = '" . $_POST['id'] . "'";
                        //
            //                        $query = $this->db->query($sql);
                        unset($_POST['btn_delete']);
                        print $_POST['id'];
                }
        }

        public function delete_data() {
                if (isset($_POST['id'])) {
//                        $data['indexColumn'] = $_POST['indexColumn'];
                        $data[$this->config->item('display')] = 0;
                        $this->delete($_POST['table_name'], $data, $_POST['indexColumn'], $_POST['id']);
                        unset($_POST['btn_delete']);
                        print $_POST['id'];
                }
        }

        public $subs = [];

        public function get_sub_tree($table_name, $column_name, $indexColumn, $where = []) {
                $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), 1)->where_in($indexColumn, $where);
                $query = $this->db->get();
                $row = $query->row_array();
                array_push($this->subs, $row);

                $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), 1)->where('parentID', $row['organization_id'])->orderBy('code', 'asc');
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                array_push($this->subs, $row);
                                $this->get_sub_trees($table_name, $column_name, $indexColumn, $row[$indexColumn]);
                        }
                }
        }

        public function get_sub_trees($table_name, $column_name, $indexColumn, $where_value) {

                $this->db->select($column_name)->from($table_name)->where($this->config->item('display'), 1)->where('parentID', $where_value)->orderBy('code', 'asc');
                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                array_push($this->subs, $row);
                                $this->get_sub_trees($table_name, $column_name, $indexColumn, $row[$indexColumn]);
                        }
                }
        }

        public function check_validation() {
                $continue = true;
                if (isset($_POST["sys_validation"])) {
                        foreach ($_POST["sys_validation"] as $key => $va) {
                                $this->form_validation->set_rules($key, $key, $va);
                        }

                        if ($this->form_validation->run() === false) {
                                $continue = false;
                                echo json_encode($this->form_validation->_error_array);
                                die();
                        }
                }
                return $continue;
        }

        public function get_font_face() {
                $this->db->select("name")->from('t_ma_font')->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $o = [];
                foreach ($query->result() as $row) {
                        $e = ['font_id' => $row->name, 'code' => '', 'name' => $row->name];
                        array_push($o, $e);
                }
                return $o;
        }

        public function get_organization_id_from_userID() {
                $userID = $this->ion_auth->get_userID();
                if (!is_null($userID)) {
                        $this->db->select('employee_id')->from($this->config->item('user_table'))->where('userID', $userID);
                        $query = $this->db->get();
                        $row = $query->row();
                        $employee_id = $row->employee_id;
                        $this->db->select('organization_id')->from('t_ma_employee')->where('employee_id', $employee_id);
                        $query = $this->db->get();
                        if ($query->num_rows() > 0) {
                                $row = $query->row();
                                $organization_id = $row->organization_id;
                                return $organization_id;
                        } else {
                                return 0;
                        }
                } else {
                        return 0;
                }
        }

        public function get_department_id_from_userID() {
                $userID = $this->ion_auth->get_userID();
                if (!is_null($userID)) {
                        $this->db->select('employee_id')->from($this->config->item('user_table'))->where('userID', $userID);
                        $query = $this->db->get();
                        $row = $query->row();
                        $employee_id = $row->employee_id;
                        $this->db->select('department_id')->from('t_ma_employee')->where('employee_id', $employee_id);
                        $query = $this->db->get();
                        if ($query->num_rows() > 0) {
                                $row = $query->row();
                                $department_id = $row->department_id;
                                return $department_id;
                        } else {
                                return 0;
                        }
                } else {
                        return 0;
                }
        }

        public function get_division_id_from_userID() {
                $userID = $this->ion_auth->get_userID();
                if (!is_null($userID)) {
                        $this->db->select('employee_id')->from($this->config->item('user_table'))->where('userID', $userID);
                        $query = $this->db->get();
                        $row = $query->row();
                        $employee_id = $row->employee_id;
                        $this->db->select('division_id')->from('t_ma_employee')->where('employee_id', $employee_id);
                        $query = $this->db->get();
                        if ($query->num_rows() > 0) {
                                $row = $query->row();
                                $division_id = $row->division_id;
                                return $division_id;
                        } else {
                                return 0;
                        }
                } else {
                        return 0;
                }
        }

        public function save_data_tc() {
                $tt = $this->mdb->get_tableData($_POST['uri']);
                $t = $tt['tableData'];
                $s = $tt['schema_data'];
                $indexColumn = $s['indexColumn'];

                $result = $this->save_data($s, $t);

                if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                } else {
                        $id = $result;
                }
//
                //                $dtn = explode(',', $t['sub_table_name']);
                //                foreach ($dtn as $k => $v) {
                //                        if ($v !== null) {
                //                                $vals = array();
                //                                $s0 = $this->schema->get_schema($v);
                //
        //                                $s = $s0['schema'];
                //                                $this->db->delete($v, array($indexColumn => $id));
                //
        //                                $vals['create_id'] = $this->ion_auth->get_userID();
                //                                $vals['create_date'] = date('Y-m-d H:i:s');
                //                                $vals['computerName'] = $this->security_lib->get_computerName();
                //                                $vals[$indexColumn] = $id;
                //                                foreach ($_POST as $kk => $vv) {
                //                                        if (strpos($kk, $v) !== false) {
                //
        //                                                $val = $_POST[$kk];
                //                                                if (strlen($val) > 0) {
                //                                                        $cols = explode('_', str_replace($v . '_', '', $kk));
                //                                                        $last = array_pop($cols);
                //                                                        $cols = implode('_', $cols);
                //
        //                                                        if (substr($last, strlen($last) - 1, 1) == 'i') {
                //                                                                $vals[$cols] = str_replace('i', '', $last);
                //
        //                                                                $this->db->insert($v, $vals);
                //                                                        }
                //                                                }
                //                                        }
                //                                }
                //                        }
                //                }
                return 1;
        }

        public function get_depth_level($table_name, $val, $parent_column) {
                $schema = $this->schema_model->get_schema($table_name);
                $indexColumn = $schema['indexColumn'];
                $this->get_parents($table_name, $indexColumn, $val, $parent_column);
                $level = $this->level;
                $level--;
                print($level);
        }

        public function get_parents($table_name, $indexColumn, $pid, $parent_column, $found = []) {
                array_push($found, $pid);
                $this->level++;
                $this->db->select('*')->from($table_name)->where($indexColumn, $pid);
                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                $found[] = $this->get_parents($table_name, $indexColumn, $row[$parent_column], $parent_column, $found);
                        }
                }
                return $found;
        }

        public function get_group() {
                $this->db->select('groupID,code,name')->from($this->config->item('group_table'))->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $o = [];
                if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                                $e = ['groupID' => $row->groupID, 'name' => $row->name, 'code' => $row->code];
                                $o[$row->groupID] = $e;
                        }
                }
                return ['data' => $o, 'indexColumn' => 'groupID'];
        }

        function get_user_data($id) {
                $this->db->select('*')->from('v_ma_user_main')->where('userID', $id);
                $query = $this->db->get();
                $row = $query->row_array();
                $aa = array();
                if ($query->num_rows() > 0) {
                        foreach ($row as $k => $v) {
                                $aa[$k] = htmlspecialchars($v, ENT_QUOTES, 'UTF-8');
                        }
                } else {
                        $aa = array();
                }
                $a['data'] = $aa;
                $a['subTableData'] = array();
                return $a;
        }

        function get_group_from_userID($userID) {
                $this->db->select('groupID')->from($this->config->item('user_group_table'))->where('userID', $userID)->orderBy('user_groupID')->limit(1);

                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                        $row = $query->row_array();
                        return $row['groupID'];
                } else {
                        return '';
                }
        }

        public function generate_password($old_password = 'midland') {
                $a = $this->ion_auth_model->hash_password($old_password, $this->ion_auth_model->salt());
                if ($a == false) {
                        $a = "";
                }

                return ['user_password' => $a, 'salt' => $this->ion_auth_model->salt(), 'remember_code' => sha1($old_password)];
        }

        public function save_agile($leave_id) {
                $data = $_POST;
                if (isset($data['id'])) {
                        $data['agile_type_id'] = $_POST['agile_type_id'];
                        $id = $_POST['id'];
                        unset($data['id']);
                        $this->update('t_wall_agile_event', $data, 'event_id', $id);
                } else {
                        $data['userID'] = $this->ion_auth->get_userID();
                        $data['title'] = substr($_POST['message'], 0, 255);
                        $data['description'] = $_POST['message'];
                        $data['color'] = 'blue';
                        $data['is_active'] = 1;
                        $data['start_date'] = $_POST['start_date'];
                        $data['end_date'] = $_POST['end_date'];
                        $data['message_id'] = $leave_id;
                        unset($data['message']);
                        if (isset($data['leave_type_id'])) {
                                unset($data['leave_type_id']);
                        }

                        if (isset($data['is_active'])) {
                                unset($data['is_active']);
                        }

                        $data['agile_type_id'] = isset($_POST['agile_type_id']) ? $_POST['agile_type_id'] : 2;
                        $id = $this->insert('t_wall_agile_event', $data);
                }
                return $id;
        }

        public function save_log_events() {
                $data = ['button' => $_POST['btn_name'], 'page' => $_POST['page']];
                return $this->insert('activityLog', $data);
        }

        function set_translation() {
                /*
                 * To change this license header, choose License Headers in Project Properties.
                 * To change this template file, choose Tools | Templates
                 * and open the template in the editor.
                 */
                $this->load->model('lang_mod');
                $alang = $this->lang_mod->get_active_lang();

                foreach ($alang as $k => $v) {
                        $name = $v['name'];
                        $code = $v['code'];
                        $lang = $v['lang'];
//                        print($name);
                        if (!is_dir(APPPATH . 'language/' . $lang))
                                mkdir(APPPATH . 'language/' . $lang, 0777, TRUE);
                        $file = fopen(APPPATH . 'language/' . $lang . '/application_lang.php', "w");
                        $file_xml = fopen('./assets/xml/' . $lang . '.xml', "w");
                        $a = "<?\n";
                        $b = '';

                        if (strlen($code) > 0) {
                                $this->db->select('code,name' . $code)->from($this->config->item('translation_table'))->where($this->config->item('display'), '1');
                        } else {
                                $this->db->select('code,name')->from($this->config->item('translation_table'))->where($this->config->item('display'), '1');
                        }
                        $query = $this->db->get();
                        foreach ($query->result_array() as $result) {
                                if (strlen($code) > 0) {
                                        $a .= '$lang[\'' . $result['code'] . '\'] =   "' . $result['name' . $code] . "\";\n";
                                        $b .= '<lang id="' . $result['code'] . '">' . $result['name' . $code] . "</lang>";
                                } else {
                                        $a .= '$lang[\'' . $result['code'] . '\'] =   "' . $result['name'] . "\";\n";
                                        $b .= '<lang id="' . $result['code'] . '">' . $result['name'] . "</lang>";
                                }
                        }
                        $a .= '?>';
                        fwrite($file, $a);
                        fwrite($file_xml, $b);
                        fclose($file);
                        fclose($file_xml);
                }
        }

        function get_lang() {
                $this->db->select('langID, code,name,lang')->from($this->config->item('language_table'))->where('is_active', 1);
                $query = $this->db->get();
                $a = array();
                foreach ($query->result_array() as $row) {

                        $e = array('langID' => $row['langID'], 'code' => $row['code'], 'name' => $row['name'], 'pic' => $row['code'], 'lang' => $row['lang'], 'full_name' => $row['lang']);
                        array_push($a, $e);
                }

                return $a;
        }
        
        public function get_group_users(){
            $userID = $this->session->userdata('userID');
            $this->db->select('groupID')->from('usersGroups')->where('userID',$userID);
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
                $res = $query->row_array();
                return $res['groupID'];
            }
        }

        function get_select_data(){
                $function_id = $_POST['function_id'];
                $table_name = $_POST['table_name'];
                $this->db->select('columnData,indexColumn')->from('functionAddon')->where('addonData', $table_name);
                $query = $this->db->get();
//                var_dump($this->db->last_query());
                $a = array();
                if($query->num_rows()>0){
                        $row = $query->row_array();
                        
                        $this->db->select($row['columnData'])->from($table_name)->where($this->config->item('display'),1);
                        $query2 = $this->db->get();
                        if($query2->num_rows()>0){
                                foreach($query2->result_array() as $row2){
                                        
                                        $a[$row2[$row['indexColumn']]] = $row2;
                                }
                        }else{
                                $a = array();
                        }
                }
                return $a;
        }
}