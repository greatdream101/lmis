<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Admin_mod extends CI_Model {

        function delete_data(){
                $userID = $_POST['id'];
                $this->mdb->update_where($this->config->item('user_table'), array($this->config->item('display')=>0), array('userID'=>$userID));
                $this->mdb->update_where($this->config->item('user_group_table'), array($this->config->item('display')=>0), array('userID'=>$userID));
                return 1;
        }
        
        function get_groupID($userID) {
                $this->db->select('groupID')->from($this->config->item('user_group_table'))->where('userID', $userID);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                        $row = $query->row_array();
                        return $row['groupID'];
                } else {
                        return 0;
                }
        }

        function save_user_password() {
                $password = $this->mdb->generate_password($_POST['password']);
                $userID = $this->ion_auth->get_userID();
                $data['password'] = $password['user_password'];
                $data['salt'] = $password['salt'];
                $data['remember_code'] = $password['remember_code'];
                if (isset($_POST['avatar']) && strlen($_POST['avatar']) > 0) {
                        $data['avatar'] = $_POST['avatar'];
                } else {
                        $data['avatar'] = '';
                }

                if (isset($_POST['id'])) {
                        $data['id'] = $_POST['id'];
                }

                $data['groupID'] = $_POST['groupID'];
                $data['username'] = $_POST['username'];
                $data['fname'] = $_POST['fname'];
                $data['lname'] = $_POST['lname'];
            
                $email = $_POST['email'];
                $data['email'] = $_POST['email'];
                $this->mdb->r_delete($this->config->item('user_table'), array('length(username)' => 0));
                $this->mdb->r_delete($this->config->item('user_table'), array('length(email)' => 0));
                if ((int) $data['id'] == 0) {
                        $this->db->select('userID')->from($this->config->item('user_table'))->where('username', $data['username'])->or_where('email', $data['email'])->where($this->config->item('display'), 1);
                        $query = $this->db->get();
                        if ($query->num_rows() > 0) {
                                $error['username'] = 'The email [' . $data['username'] . '] exists?';
                                print json_encode($error);
                        } else {
                                
                                $last_userID = $this->mdb->insert($this->config->item('user_table'), $data);
                                $this->mdb->insert($this->config->item('user_group_table'), array('userID'=>$last_userID,'groupID'=>$data['groupID']));
                                return 1;
                        }
                } else {
                        $this->mdb->r_delete($this->config->item('user_table'), array($this->config->item('display') => 0, 'email' => $data['email']));
                        $this->mdb->r_delete($this->config->item('user_table'), array($this->config->item('display') => 0, 'username' => $data['username']));


                        $this->mdb->update_where($this->config->item('user_table'), $data, array('userID' => $data['id'], $this->config->item('display') => 1));
                        $this->mdb->r_delete($this->config->item('user_group_table'), array('userID' => $data['id']));

                        $data['userID'] = $data['id'];
                        $this->mdb->insert($this->config->item('user_group_table'), $data);
                        return 1;
                }
        }

}
