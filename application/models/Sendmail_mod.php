<?php

class Sendmail_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        function get_customer($survey_id) {
                $this->db->select('cusID,seriesID')->from('t_survey')->where('surveyID', $survey_id);
                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                        $row = $query->row_array();
                        $cusID = $row['cusID'];
                        $seriesID = $row['seriesID'];
                } else {
                        $cusID = 0;
                        $seriesID = 0;
                }

                if ($cusID > 0 && $seriesID > 0) {
                        $this->db->select('valueNet')->from('t_value')->where('surveyID', $survey_id)->where('cusID', $cusID)->where('seriesID', $seriesID);
                        $query = $this->db->get();

                        $below = 0;
                        if ($query->num_rows() > 0) {
                                foreach ($query->result_array() as $row) {
                                        if (is_numeric($row['valueNet']) && (int) $row['valueNet'] <= 2) {
                                                $below = 1;
                                                break;
                                        }
                                }
                        }
                } else {
                        $below = 0;
                }
                
                if ($below) {
                        $this->db->select('insuredName as name,insuredDedupID,mobilePhone')->from('temp_cleanData')->where('id', $cusID);
                        $query = $this->db->get();
                        return $query->result_array();
                } else {
                        return array();
                }
        }

        function get_data() {

                $a = array(1, 2, 3);
                $chkSucecessMail = 0;
                $this->db->select('series_id')->from('t_series')->where('active_online', 1);
                $query = $this->db->get();
                $row = $query->row_array();
                $series_id = $row['series_id'];
                $this->db->select('fname, lname, titlename, cusid, created_date, cntsendmail,email,tel')->from('t_customer')->where('flag_csi', 1)->where('flag_cli', 1)->where('flag_input', 1)->where('cntsendmail>=', 0)->where('cntsendmail<', 4)->where('series_id', $series_id);
                $this->db->where('(survey_status_id = 1 OR survey_status_id = 2)');
                $this->db->where('(survey_status_cli_id = 1 OR survey_status_cli_id = 2)');
                $query = $this->db->get();

                $b = array();

                if ($query->num_rows() > 0) {

                        foreach ($query->result_array() as $row) {

                                $now_date = date('Y-m-d H:i:s');
                                $uid = uniqid();
                                $token = md5($uid . $row['email'] . $now_date);

                                if (strlen($row['email']) > 0 && strlen($row['fname']) > 0 && strlen($row['lname']) > 0) {
                                        //change dulation sendmail day
                                        $cntsendmail = (int) $row['cntsendmail'] * 3;
                                        $new_date = date('Y-m-d', strtotime($row['created_date'] . ' + ' . $cntsendmail . ' days'));
                                        $today = date('Y-m-d');

                                        if ($new_date == $today) {
                                                if ((int) $row['cntsendmail'] < 3) {
                                                        $b[$row['cusid']] = array('email' => $row['email'], 'cntsendmail' => $row['cntsendmail'], 'name' => $row['titlename'] . ' ' . $row['fname'] . ' ' . $row['lname'], 'tokenid' => $token);

                                                        $this->db->select('id')->from('t_url')->where('cusid', $row['cusid']);
                                                        $query = $this->db->get();

                                                        if ($query->num_rows() > 0) {

                                                                $this->db->update('t_url', array('tokenid' => $token, 'tel' => $row['tel']), array('cusid' => $row['cusid']));
                                                        } else {
                                                                $query = 'INSERT INTO t_url (cusid, tokenid, tel, flag_active) VALUES ("' . $row['cusid'] . '", "' . $token . '", "' . $row['tel'] . '", "1") ON DUPLICATE KEY UPDATE cusid="' . $row['cusid'] . '"';
                                                                $this->db->query($query);
                                                        }
                                                } elseif ((int) $row['cntsendmail'] == 3) {
                                                        $row['cntsendmail'] ++;
                                                        $this->db->select('id')->from('t_url')->where('cusid', $row['cusid']);
                                                        $query = $this->db->get();

                                                        if ($query->num_rows() > 0) {
                                                                $this->db->update('t_url', array('flag_active' => 0), array('cusid' => $row['cusid']));
                                                                $this->mdb->update_where('t_customer', array('cntsendmail' => $row['cntsendmail'], 'flag_input' => 2), array('cusid' => $row['cusid']));
                                                        }
                                                        $chkSucecessMail = 1;
                                                }
                                        }
                                }
                        }
                }
                //var_dump($b);exit();
                if ($chkSucecessMail == 1) {
                        $a[] = array('status' => '0', 'detail' => 'Sent To Tele and Set Tokenid is not active.', 'timestamp' => $today);
                } else {
                        $a = $this->send_mail($b);
                }

                return $a;
        }

        function get_mail_content() {
                $this->db->select('subject, content_email,sent_to')->from('t_contentemail')->where('active', 1);
                $query = $this->db->get();
                return $query->row_array();
        }

        function send_mail($cus) {
                $aut_key = "QUlBQ3VzdG9tZXI6MTUzMzU1MzU5MTY0NA==";
                $url = "http://172.22.1.54/io/email_sender_api/email_sender";
                
                $success = 0;
                $cont = $this->get_mail_content();

                $a = array();

                $subject = $cont['subject'];
                $content = $cont['content_email'];
                $send_to = $cont['sent_to'];
                
                foreach ($cus as $k => $v) {
                        $cusid = $k;
                        $name = $v['name'];
                        $mobile = $v['mobilePhone'];
                        $policy = $v['insuredDedupID'];

                        $content_mail = str_replace('{Toname}', $name, $content);
                        $content_mail = str_replace('{Mobile}', $mobile, $content_mail);
                        $content_mail = str_replace('{Policy}', $policy, $content_mail);

//                        $cntsendmail = $v['cntsendmail'];

                        $str = array('aut_key' => $aut_key, 'send_email' => $send_to, 'subject' => $subject, 'content' => $content_mail, 'function_uri' => 'Sendmail_mod/send_mail');

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, $url);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($str));
                        $response = curl_exec($curl);
                        $code = curl_getinfo($curl);
                        $err = curl_error($curl);
                        curl_close($curl);

                        $result = json_decode(stripslashes($response), true);

//                        $a[$cusid] = array('status' => $result['status'], 'detail' => $result['detail']);

                        $success = $result['status'];

                        if ($success) {
                                return 1;
//                                $this->update_cntsendmail($cusid, $cntsendmail);
                        } else {
                                return 0;
                        }
                }

//                if (empty($a)) {
//                        $today = date('Y-m-d H:i:s');
//                        $a[] = array('status' => '0', 'detail' => 'not customer', 'timestamp' => $today);
//                }
//
//                return $a;
        }

        function update_cntsendmail($cusid, $cntsendmail) {
                $cntsendmail++;
                $this->mdb->update_where('t_customer', array('cntsendmail' => $cntsendmail), array('cusid' => $cusid));

                $this->db->select('survey_status_id, survey_status_cli_id')->from('t_customer')->where('cusid', $cusid);
                $query = $this->db->get();

                $t_customer = $query->row_array();

                if ($t_customer['survey_status_id'] == 1 && $t_customer['survey_status_cli_id'] == 1) {
                        $this->mdb->update_where('t_customer', array('survey_status_id' => 2), array('cusid' => $cusid));
                        $this->mdb->update_where('t_customer', array('survey_status_cli_id' => 2), array('cusid' => $cusid));

                        $this->load->library('security_lib');
                        $computer_name = $this->security_lib->get_computer_name();
                        $dates = date('Y-m-d H:i:s');
                        $status_history_csi = array(
                            'cusid' => $cusid
                            , 'statusid' => 2
                            , 'flag_id' => 1
                            , 'create_id' => 1
                            , 'datestatus' => $dates
                            , 'computer_name' => $computer_name
                            , 'created_date' => $dates
                            , 'created_id' => 1
                            , 'active' => '1'
                        );
                        $this->mdb->insert('t_statushistory', $status_history_csi);

                        $status_history_cli = array(
                            'cusid' => $cusid
                            , 'statusid' => 2
                            , 'flag_id' => 2
                            , 'create_id' => 1
                            , 'datestatus' => $dates
                            , 'computer_name' => $computer_name
                            , 'created_date' => $dates
                            , 'created_id' => 1
                            , 'active' => '1'
                        );
                        $this->mdb->insert('t_statushistory', $status_history_cli);
                }
        }

}
