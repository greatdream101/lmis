
<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Pages_model extends CI_Model {

        var $error = array();
        var $data = array();

        public function __construct() {
                parent::__construct();
        }

        function get_department() {
                $this->db->select('department_id, name,detail')->from('t_ma_department')->where('display', 1);
                $query = $this->db->get();
                $row = $query->row();

                $this->db->select('code,name,position_name,admin_position_name,phone')->from('v_ma_employee')->where('division_id', 0)->where('department_id', $row->department_id)->where('display',1)->orderBy('admin_position_code','asc');
                $query = $this->db->get();
                $o = array();
                foreach ($query->result_array() as $row1) {
                        array_push($o, $row1);
                }

                $a = array('department_id' => $row->department_id, 'name' => $row->name, 'detail' => $row->detail, 'under' => $o);
                return $a;
        }

        function get_division($department_id) {
                $this->db->select('division_id, name,detail')->from('t_ma_division')->where('display', 1);
                $query = $this->db->get();
                $o = array();
                foreach ($query->result() as $row) {

                        $this->db->select('code,name,position_name,admin_position_name,phone')->from('v_ma_employee')->where('department_id',$department_id)->where('division_id', $row->division_id)->where('display',1)->orderBy('admin_position_code','asc');
                        $query1 = $this->db->get();
                        $u = array();
                        foreach ($query1->result_array() as $row1) {
                                array_push($u, $row1);
                        }
                        $e = array('division_id' => $row->division_id, 'name' => $row->name, 'detail' => $row->detail, 'under' => $u);
                        array_push($o, $e);
                }
                return $o;
        }

        function get_gantt_data($fc, $id) {

                $this->db->select('gantt_id,name,start_date,end_date,actual_start_date,actual_end_date,resource,complete,detail')->from('t_ass_gantt')->where('display', 1);
                $query = $this->db->get();
                $e = array();
//                $a = array('gtaskblue' => 0, 'gtaskred' => 1, 'gtaskyellow' => 2, 'gtaskgreen' => 3, 'gtaskpurple' => 4, 'gtaskpink' => 5);
                $a = array('gtaskpink' => 5);
                if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                                $color = array_rand($a, 1);
                                $id = $row->gantt_id;

                                if (strlen($row->name) > 50) {
                                        $pos = strpos($row->name, ' ');
                                        $name = substr($row->name, 0, $pos);
                                } else {
                                        $name = $row->name;
                                }

                                $start_date = $row->start_date;

                                $end_date = $row->end_date;

                                $actual_start_date = $row->actual_start_date;

                                $actual_end_date = $row->actual_end_date;

                                $style = $color;
                                $link = '';
                                $milestone = 0;
                                $resource = $row->resource;
                                if (is_null($row->actual_end_date)) {
                                        $caption = 'in_process';
                                        $complete = 0;
                                } else {
                                        if ($row->actual_end_date > $row->end_date) {
                                                $caption = 'late';
                                                $complete = 'N.A.';
                                        } else {
                                                $caption = 'finished';
                                                $complete = 100;
                                        }
                                }

                                $group = 0;
                                $parent = 0;
                                $open = 1;
                                $depend = '';

                                $notes = $row->detail;
                                $ee = array('id' => $id, 'name' => $name, 'start_date' => $start_date, 'end_date' => $end_date, 'actual_start_date' => $actual_start_date, 'actual_end_date' => $actual_end_date, 'style' => $style, 'link' => $link, 'milestone' => $milestone, 'resource' => $resource, 'complete' => $complete, 'group' => $group, 'parent' => $parent, 'open' => $open, 'depend' => $depend, 'caption' => $caption, 'notes' => $notes);

                                array_push($e, $ee);
                        }
                }

                return $e;
        }

}

?>
