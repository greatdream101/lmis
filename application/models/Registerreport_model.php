<?php

    class Registerreport_model extends CI_Model{

        public function __construct(){

            parent::__construct();

        }

        public function get_report($str='', $where=[], $table=''){

            $this->db->SELECT($str);

            $this->db->FROM($table);

                foreach ($where as $k => $v) {
                    switch ($k) {
                        // case 'cusID':
                        //     $a = explode(",", $v);
                        //         $this->db->where_in($k, $a);
                        //     break;
                        case 'staff_id':
                            if(strlen($v)>0)
                                $this->db->where('staff_id', $v);
                            break;

                        case 'startDate':
                            if(strlen($v)>0)
                                $this->db->where('DATE(usedDate)>=', $v);
                            break;

                        case 'endDate':
                            if(strlen($v)>0)
                                $this->db->where('DATE(usedDate)<=', $v);
                            break;

                        default:
                            #nothing
                            break;
                    }
                }

            $this->db->orderBy('cusID','asc');

            $result = $this->db->get()->result_array();

            return $result;
        }
    }