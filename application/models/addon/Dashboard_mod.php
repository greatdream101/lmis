<?php

class Dashboard_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
                $this->load->model('addon/question_mod');
                $this->load->model('addon/branch_mod');
        }

        function get_data($series_id) {
                $a = $this->question_mod->get_value($series_id);


                $users = array_key_exists('users', $a) ? $a['users'] : array();
                $questions = $a['questions'];
                $values = $a['values'];
                $remarks = $a['remarks'];
                $links = $a['links'];
                $branches = $a['branches'];

                $o = array();
                $o[0]['name'] = $this->mcl->gl('name');
                $o[0]['branch_name'] = $this->mcl->gl('branch_name');
                foreach ($questions as $kk => $vv) {
                        $question_id = $vv['questionID'];
                        $o[0]['q_' . $question_id] = 'q_' . $question_id;
                        $o[0]['r_' . $question_id] = 'r_' . $question_id;
                        $o[0]['l_' . $question_id] = 'l_' . $question_id;
                }

                foreach ($users as $k => $v) {
                        $user_id = $v['userID'];
                        $user_name = $v['fname'] . ' ' . $v['lname'];
                        $o[$user_id]['name'] = $user_name;
                        foreach ($branches as $kb => $vb) {
                                $branch_id = $kb;
                                $branch_name = $vb['name'];
                                $o[$user_id]['branch_name'] = $branch_name;

                                foreach ($questions as $kk => $vv) {
                                        $question_id = $vv['questionID'];
                                        $value = isset($values[$user_id . '_' . $branch_id . '_' . $question_id]) ? $values[$user_id . '_' . $branch_id . '_' . $question_id] : '';
                                        $remark = isset($remarks[$user_id . '_' . $branch_id . '_' . $question_id]) ? $remarks[$user_id . '_' . $branch_id . '_' . $question_id] : '';
                                        $link = isset($links[$user_id . '_' . $branch_id . '_' . $question_id]) ? $link[$user_id . '_' . $branch_id . '_' . $question_id] : '';

                                        $o[$user_id]['q_' . $question_id] = $value;
                                        $o[$user_id]['r_' . $question_id] = $remark;
                                        $o[$user_id]['l_' . $question_id] = $link;
                                }
                        }
                }

                return $o;
        }

        function get_data_old($series_id) {
                $a = $this->question_mod->get_value($series_id);
                $customers = $a['customers'];
                $questions = $a['questions'];
                $values = $a['values'];
                $remarks = $a['remarks'];
                $o = array();
                foreach ($customers as $k => $v) {
                        $customer_id = $v['cusID'];
                        $customer_name = $v['fname'] . ' ' . $v['lname'];

                        foreach ($questions as $kk => $vv) {
                                $question_id = $vv['questionID'];
                                $value = isset($values[$customer_id . '_' . $question_id]) ? $values[$customer_id . '_' . $question_id] : '';
                                $remark = isset($remarks[$customer_id . '_' . $question_id]) ? $remarks[$customer_id . '_' . $question_id] : '';

                                $e = array('customer_name' => $customer_name, 'question' => $vv['name'], 'value' => $value, 'remark' => $remark);
                                array_push($o, $e);
                        }
                }
                $data = json_encode($o);

                $file = fopen('./upload/json/mps.json', "w");
                fwrite($file, $data);
                fclose($file);
        }

        function get_dashboard_branch($clientsID){
                $this->db->select('clientsBranchID,clientsID,branchID')->from('t_clientsBranch')->where('clientsID',$clientsID);
                $query = $this->db->get();
                $result = $query->result_array();
                return $result;
        }

        public function get_survey_list($clientsID) {
                $regionID = isset($_POST['regionID'])?$_POST['regionID'] : '';
                $branchID = isset($_POST['branchID'])?$_POST['branchID'] : '';
                $seriesID = isset($_POST['seriesID'])?$_POST['seriesID'] : 0;

                $regionID = explode(',',$regionID);
                $branchID = explode(',',$branchID);
                $this->db->select('get_detail,fname,lname,branch,series,survey_status,updatedDate')
                        ->from('v_surveyMain');
                        $this->db->where('clientsID',$clientsID);
                if(count($branchID) > 0){
                        $this->db->where_in('branchID',$branchID);
                }
                if($seriesID > 0){
                        $this->db->where('seriesID',$seriesID);
                }
                if(count($regionID) > 0){
                        $this->db->where_in('regionID',$regionID);
                }

                $query = $this->db->get();
                $result = $query->result_array();

                return $result;
        }

        function count_status($clientsID){
                $query2 = $this->db->select('*')->from('qcStatus')->where('active',1)->get();
                $qcStatus = $query2->result_array();
                $today = date('Y-m-d');
                // All
                $query1 = $this->db->select('count(qcStatusID) as ct')
                                ->from('v_surveyMain')
                                ->where('clientsID',$clientsID)
                                ->where('updatedDate',$today)->get();    
                $row1 =  $query1->row_array(); 
                // 4 Pendding Survey
                $query2 = $this->db->select('count(qcStatusID) as ct')
                                ->from('v_surveyMain')
                                ->where('clientsID',$clientsID)
                                ->where('qcStatusID',4)
                                ->where('updatedDate',$today)->get();    
                $row2 =  $query2->row_array();  
                // 1 Pendding QC
                $query3 = $this->db->select('count(qcStatusID) as ct')
                                ->from('v_surveyMain')
                                ->where('clientsID',$clientsID)
                                ->where('qcStatusID',1)
                                ->where('updatedDate',$today)->get();    
                $row3 =  $query3->row_array();         
                // 2 Approved
                $query4 = $this->db->select('count(qcStatusID) as ct')
                                ->from('v_surveyMain')
                                ->where('clientsID',$clientsID)
                                ->where('qcStatusID',2)
                                ->where('updatedDate',$today)->get();    
                $row4 =  $query4->row_array();          
                
                return array('all'=>$row1['ct'],'survey'=>$row2['ct'],'qc'=>$row3['ct'],'approved'=>$row4['ct']);
        }

        function get_surveyData($clientsID){
                $regionID = isset($_POST['regionID'])?$_POST['regionID'] : '';
                $branchID = isset($_POST['branchID'])?$_POST['branchID'] : '';
                $seriesID = isset($_POST['seriesID'])?$_POST['seriesID'] : 0;

                $reg = explode(',',$regionID);
                $bra = explode(',',$branchID);

                $query1 = $this->db->select('*')->from('t_region')->where_in('regionID',$reg)->get();
                $region = $query1->result_array();

                $query2 = $this->db->select('*')->from('qcStatus')->where('active',1)->get();
                $qcStatus = $query2->result_array();

                $region_name = [];
                foreach($region as $vRegion){
                        array_push($region_name,$vRegion['name']);
                }
                        $countQC = [];
                        foreach($qcStatus as $vQC){
                                $countQC = [];
                                foreach($region as $vRegion){
                                        $this->db->select('count(qcStatusID) as ct')
                                        ->from('v_surveyMain')
                                        ->where('regionID',$vRegion['regionID'])
                                        ->where('qcStatusID',$vQC['qcStatusID']);
                                        $this->db->where('clientsID',$clientsID);
                                        if($seriesID > 0){
                                                $this->db->where('seriesID',$seriesID);
                                        }
                                        if(count($branchID) > 0){
                                                $this->db->where_in('branchID',$bra);
                                        }
                                        $query=$this->db->get();
                                        $ctQC = $query->result_array();
                                        array_push($countQC,(int) $ctQC[0]['ct']);
                                }
                                $data[] = array('name' => $vQC['name'], 'data' => $countQC);
                        }
                return array('data' => $data,
                'week_no' => $region_name,
                'yasix_name' => 'Surveyor (Person)',
                'yAxis' => 1);
        }

        function get_clientsByUser(){
                $userID = $this->session->userdata('userID');
                $query = $this->db->select('clientsID')->from('t_userClients')->where('userID',$userID)->get();
                $clientsID = $query->row_array();
                if($query->num_rows() > 0){
                        return $clientsID['clientsID'];
                }else{
                        return 0;
                }
                
        }

}
