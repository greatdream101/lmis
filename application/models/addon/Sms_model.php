<?php

class Sms_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*
     * ************
     * Get Data
     * ************
     */
    
    function get_dataSource($where=array(), $select = 'sourceID, sourceName, dataTypeID, createdDate', $limit = '2', $orderby = 'DESC') {
        $this->db->select($select);
        $this->db->from('t_dataSource');
        $this->db->where($this->config->item('display'), 1);
        if (!empty($where)){
            if (!empty($where['createdDate'])){
                $str = 'createdDate LIKE "%'.$where['createdDate'].'%"';
                $this->db->where($str);
            }else{
                $this->db->where($where);
            }
        }
        $this->db->orderby('sourceID', $orderby);
        $this->db->limit($limit);
        $query = $this->db->get();
        //vd::d($this->db->last_query());//die();
        return $query->result_array();
    }
    
    function get_officeHours() {
        $this->db->select('officeHoursID,code,startTime,endTime')->from('t_officeHours')->where($this->config->item('display'), 1);
        $query = $this->db->get();
        //vd::d($this->db->last_query());die();
        return $query->row_array();
    }
    
    function get_holiday($strDate='') {
        $this->db->select('holidayID,code,name,nameEN')->from('t_holiday')->where($this->config->item('display'), 1)->where('holidayDate' ,$strDate);
        $query = $this->db->get();
        //vd::d($this->db->last_query());die();
        return $query->row_array();
    }
    
    function get_tempSMS() {
        $this->db->select('cusID')->from('tempSMS')->where($this->config->item('display'), 1);
        //$this->db->limit('90');
        $query = $this->db->get();
        return $query->result_array();
        //return $query->row_array();
    }
    
    function get_surveyURL() {
        $this->db->select('surveyURL')->from('t_surveyURL')->where($this->config->item('display'), 1);
        $query = $this->db->get();
        //return $query->result_array();
        return $query->row_array();
    }
    
    /*
     * ************
     * Save Data
     * ************
     */
    
    function save_response($cusID, $response = array()) {
        
        if ($response['status'] == "0"){
            $status = 1;
        }else{
            $status = $response['status'];
        }
            $this->db->where_in('CusID', $cusID);
            $this->db->delete('tempSMS');
            
            $this->db->where('cusID', $cusID);
            $this->db->update('t_customer', array('sms_status'=>$status, 'sms_result'=>$response['response']));
        //}
        
        $this->db->insert('t_activity_log', $response);
        return $this->db->insert_id();
        
    }
}
