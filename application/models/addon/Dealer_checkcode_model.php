<?php

Class Dealer_checkcode_model extends CI_Model{
    public function __construct(){
        parent::__construct();

    }

    public function save_data($data=[]){

        $update =  array(
            "flagUsed"  => 1,
            "dealer_code" => $data['dealer_code'],
            "submit_date" => $data['submit_date']
        );

        $result = $this->db->where("code", $data['code'])->update('t_customer', $update);

        return $result;
    }

    public function update_data($data=[]){

        $result = $this->db->where("code", $data['code'])->update('t_customer', $data);

        return $result;

    }

    public function dealer_checkcode($strSelect=[],$where=[]){

		$this->db->select($strSelect)
		->from('v_customerSource')
        ->where($this->config->item('display'), 1);
		if(isset($where) && !empty($where)){
			$this->db->where($where);
		}
		$query  = $this->db->get();
		$result = $query->result_array();

		return $result;
    }

}
?>