<?php

class CheckDuplicate_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('enc_dec_lib');
    }
    
    function dataDeDup($s_high) {
        $this->db->select('id, insuredDedupID')->from('temp_cleanData')->where('flagID', 1)->where('sourceID', $s_high);
        $query = $this->db->get();
        $a = array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $id = $row['id'];
                if (!in_array($row['insuredDedupID'], $a))
                    array_push($a, $row['insuredDedupID']);
                    else {
                        $data['flagID'] = 2;
                        $this->db->where('id', $id);
                        $this->db->update('temp_cleanData', $data);
                    }
            }
        }
        
        $this->db->select('id, agentCD, agentName, mobilePhone, insuredDedupID, code, flagID, discount, contentSMSID')->from('temp_cleanData')->where('flagID', 1)->where('sourceID', $s_high);
        $query = $this->db->get();
        $high = array();
        foreach ($query->result_array() as $row) {
            $high[$row['insuredDedupID']] = array(
                    'id' => $row['id']
                    , 'fname' => $row['agentCD']
                     , 'lname' => $row['agentName']
                    , 'mobile' => $row['mobilePhone']
                    , 'flagID' => $row['flagID']
                    , 'sourceID' => $s_high
                    , 'code' => $row['code']
                    , 'VIN1' => $row['insuredDedupID']
                    , 'discount' => $row['discount']
                    , 'contentSMSID' => $row['contentSMSID']
            );
        }
        
        return array('high' => $high);
    }
    
    function dataDeDupBetween($data, $source_id) {
        
        foreach ($data as $k => $v) {
            $d = array();
            $d['flagID'] = 3;
            $this->db->where('insuredDedupID', $k);
            $this->db->where('sourceID', $source_id);
            $this->db->update('temp_cleanData', $d);
        }
        $this->db->select('id, insuredName,mobilePhone,insuredDedupID,flagID,effectiveMonth,effectiveDate')->from('temp_cleanData')->where('flagID', 1)->where('sourceID', $source_id);
        $query = $this->db->get();
        $normal = array();
        foreach ($query->result_array() as $row) {
            $normal[$row['insuredDedupID']] = array('id' => $row['id'], 'fname' => $row['insuredName'], 'mobile' => $row['mobilePhone'], 'flagID' => $row['flagID'], 'sourceID' => $source_id, 'effectiveMonth' => $row['effectiveMonth'], 'effectiveDate' => $row['effectiveDate']);
        }
        return $normal;
    }
    
    function insert($data, $s_high) {
        
        $this->db->select('cusID')->from('t_customer')->where('sourceID', $s_high);
        $query = $this->db->get();
        $row1 = $query->num_rows();
        
        if ($row1 == 0) {
            $user_id = $this->ion_auth->get_userID();
            $date = date('Y-m-d H:i:s');
            foreach ($data as $k => $v) {
                $v['cusID'] = $v['id'];
                unset($v['id']);
                $v['mobile'] = $this->enc_dec_lib->enc($v['mobile']);
                $v['created_id'] = $user_id;
                $v['created_date'] = $date;
                $this->db->insert('t_customer', $v);
            }
        }
        return 1;
    }
    
    function saveDataSource($data) {
        $this->db->select('sourceID')
        ->from('t_dataSource')
        ->where('sourceName', $data['sourceName'])
        ->orderby('sourceID', 'desc')->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->insert('t_dataSource', $data);
            $insertID = $this->db->insert_id();
        } else {
            $insertID = 0;
        }
        
        return $insertID;
    }
    
    function saveCleanData($data) {
        $this->db->trans_begin();
        $this->db->insert_batch('temp_cleanData', $data);
        
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return 1;
        }
    }
    
}
