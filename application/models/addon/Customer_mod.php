<?php

class Customer_mod extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getCustomerData($conditionArray = array()) {
        $this->db->select('*')
            ->from('v_customerSource')
            ->where('active', 1);
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }
        
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function get_CustomerSMS($select = array(), $conditionArray = array(), $str = '') {
        $this->db->select($select)->from('v_cusForSMS');
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }
        $this->db->group_by('mobilePhone');
        $query = $this->db->get();
        //vd::d($this->db->last_query());die();
        return $query->result_array();
    }
    
}
