<?php

class Flag_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*
     * ************
     * Get Data
     * ************
     */
    
    function get_dataType() {
        $this->db->select('dataTypeID,code,name,nameEN')->from('t_dataType')->where($this->config->item('display'), 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    function get_flagData() {
        $this->db->select('flagID,code,name,nameEN')->from('flag')->where($this->config->item('display'), 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_sumFlag($conditionArray = array()) {
        $this->db->select('count(flagID) AS flagSum')->from('v_cusForDashboardSMS');
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }
        $query = $this->db->get();
        //vd::d($this->db->last_query());//die();
        return $query->row_array();
    }
    
    function get_sumStatusSMS($select, $table, $conditionArray = array(), $str="") {
        $this->db->select($select)->from($table);
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }
        if (! empty($str)) {
            $this->db->where($str);
        }
        $query = $this->db->get();
        //vd::d($this->db->last_query());//die();
        return $query->row_array();
    }
    
    /*
     * ************
     * Save Data
     * ************
     */
}
