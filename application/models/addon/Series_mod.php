<?php

class Series_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        function get_data() {
                $this->db->select('seriesID,code,name,nameEN')->from('t_series')->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $o = array();
                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                $o[$row['seriesID']] = $row;
                        }
                }
                return $o;
        }

        function get_branch_series($branchID) {
                // $this->db->select('seriesID,code,name,nameEN')->from('t_branch')->where($this->config->item('display'), 1);
                $this->db->select('s.seriesID,s.name')
                              ->from('t_series s')
                              ->join('t_branchSeries bs','bs.seriesID = s.seriesID','INNER')
                              ->where('bs.branchID',$branchID);
                $query = $this->db->get();

                $a = array();
                foreach ($query->result_array() as $k => $v) {
                    $a[] = array('id' => $v['seriesID'], 'name' => $v['name']);
                }
                //var_dump($this->db->last_query());
                return array('data' => $a);
        }

        function get_series_list()
        {
                $this->db->select('seriesID,name,nameEN')->from('t_series')->where('active',1);

                $query = $this->db->get();
                $result = $query->result_array();
                return $result;
        }

}
