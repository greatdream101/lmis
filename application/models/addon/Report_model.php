<?php
class Report_model extends CI_Model {
	
	public function __construct() {
		// Call the Model constructor
		parent::__construct();
		$this->load->model('addon/Question_mod');
		$this->load->model('addon/branch_mod');
	}
	
	function get_data($series_id) {
		
		$a = $this->Question_mod->get_value($series_id);
		$users = $a['users'];
		$questions = $a['questions'];
		$values = $a['values'];
		$remarks = $a['remarks'];
		$links = $a['links'];
		$branches = $a['branches'];
		$o = array();
		$o[0]['name'] = $this->mcl->gl('name');
		$o[0]['branch_name'] = $this->mcl->gl('branch_name');
		foreach ($questions as $kk => $vv) {
			$question_id = $vv['questionID'];
			$o[0]['q_' . $question_id] = 'q_' . $question_id;
			$o[0]['r_' . $question_id] = 'r_' . $question_id;
			$o[0]['l_' . $question_id] = 'l_' . $question_id;
		}
		
		foreach ($users as $k => $v) {
			$user_id = $v['userID'];
			$user_name = $v['fname'] . ' ' . $v['lname'];
			$o[$user_id]['name'] = $user_name;
			foreach ($branches as $kb => $vb) {
				$branch_id = $kb;
				$branch_name = $vb['name'];
				$o[$user_id]['branch_name'] = $branch_name;
				
				foreach ($questions as $kk => $vv) {
					$question_id = $vv['questionID'];
					$value = isset($values[$user_id . '_' . $branch_id . '_' . $question_id]) ? $values[$user_id . '_' . $branch_id . '_' . $question_id] : '';
					$remark = isset($remarks[$user_id . '_' . $branch_id . '_' . $question_id]) ? $remarks[$user_id . '_' . $branch_id . '_' . $question_id] : '';
					$link = isset($links[$user_id . '_' . $branch_id . '_' . $question_id]) ? $link[$user_id . '_' . $branch_id . '_' . $question_id] : '';
					
					$o[$user_id]['q_' . $question_id] = $value;
					$o[$user_id]['r_' . $question_id] = $remark;
					$o[$user_id]['l_' . $question_id] = $link;
				}
			}
		}
		
		return $o;
	}
	
	public function get_mapClientsToSeries($strSelect=[],$where=[]){
		$this->db->select($strSelect)
		->from('v_mapClientsToSeries')
		->where($this->config->item('display'), 1);
		if(isset($where) && !empty($where)){
			$this->db->where($where);
		}		
		$query = $this->db->get();		
		$result = $query->result_array();
		
		return $result;
	}
	
	public function get_seriesName($seriesID){
		$this->db->select('seriesID, code, name, startDate, endDate')
		->from('t_series')
		->where($this->config->item('display'), 1);
		$this->db->where('seriesID', $seriesID);
		$query = $this->db->get();
		$result = $query->result_array();
		
		return $result;
	}
	
	public function get_SMSRawdata($dateStart, $dateEnd) {
		$this->db->select('*')->from('v_sms_rawdata')->where('active', 1);
		if($dateStart != "" && $dateEnd != "") $this->db->where("created_date BETWEEN '". $dateStart ." 00:00:00' AND '". $dateEnd ." 23:59:59' ","", FALSE);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function get_SMSPerformance($dateStart, $dateEnd, $sourceID) {
		$cntSource = sizeof($sourceID);
		$this->db->select('COUNT(1) as cnt, response, sourceID, "" as sourceName ')->from('t_activity_log')->where('active', 1);
		if($sourceID != "") $this->db->where_in("sourceID", $sourceID);
		if($dateStart != "" && $dateEnd != "") $this->db->where("created_date BETWEEN '". $dateStart ." 00:00:00' AND '". $dateEnd ." 23:59:59' ","", FALSE);
		if($cntSource == 1) $this->db->group_by('sourceID'); 
		$this->db->group_by('response'); 
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
    }
    
    public function getInternalReport($selectedDate='',$dataTypeID='') {
        /*
        $targetYear = date_format(date_create($selectedDate),"Y");
	    $this->db->select('*')->from('v_internalReport')->where('createdYear',$targetYear);
	    $query = $this->db->get()->result_array();
	    
        return $query;
        */
        
        $query = $this->db->query('call sp_internalReport("'.$selectedDate.'","'.$dataTypeID.'")');
        $res = $query->result_array();

        $query->free_result();
        $this->db->close();

        return $res;
    }
    
    public function getDivSummarySMS($selectedDate='',$dataTypeID='') {
        /*
        $targetYear = date_format(date_create($createdDate),"Y");
        $this->db->select('*')->from('v_divisionSummarySMS')->where('createdYear',$targetYear);
        $query = $this->db->get()->result_array();
	    
        return $query;
        */

        $query = $this->db->query('call sp_divisionSummarySMS("'.$selectedDate.'","'.$dataTypeID.'")');
        $res = $query->result_array();

        $query->free_result();
        $this->db->close();

        return $res;
    }
    
    public function getRegSummarySMS($selectedDate='',$dataTypeID='') {
        /*
        $targetMonth = date_format(date_create($createdDate),"Y-m");
	    $this->db->select('*')->from('v_regionSummarySMS')->where('createdMonth',$targetMonth);
        $query = $this->db->get()->result_array();

        return $query;
        */
        $query = $this->db->query('call sp_regionSummarySMS("'.$selectedDate.'","'.$dataTypeID.'")');
        $res = $query->result_array();

        $query->free_result();
        $this->db->close();
	    
	    return $res;
	}
    
    public function getAllDivision() {
	    $this->db->select('*')->from('v_divisionLists')->orderby('division', 'ASC');
	    $query = $this->db->get()->result_array();
	    
	    return $query;
    }
    
    public function getAllRegion() {
	    $this->db->select('region')->from('v_regionLists')->orderby('region', 'ASC');
	    $query = $this->db->get()->result_array();
	    
	    return $query;
    }
    
    public function getScoreByAL($createdDate='') {
        $targetMonth = date_format(date_create($createdDate),"Y-m");

        $this->db->select('division,
                            agencyCD,
                            agencyName,
                            IFNULL((SumQ1/CountQ1),0) AS AvgQ1,
                            IFNULL((SumQ2_1/CountQ2_1),0) AS AvgQ2_1,
                            IFNULL((SumQ2_2/CountQ2_2),0) AS AvgQ2_2,
                            IFNULL((SumQ2_3/CountQ2_3),0) AS AvgQ2_3,
                            IFNULL((SumQ2_4/CountQ2_4),0) AS AvgQ2_4,
                            IFNULL((SumQ2_5/CountQ2_5),0) AS AvgQ2_5,
                            IFNULL((SumQ2_6/CountQ2_6),0) AS AvgQ2_6')
            ->from('v_appendix')
            ->where('createdMonth',$targetMonth)
            ->group_by('createdMonth,agencyCD')
            ->orderby('createdMonth,division,agencyCD');

        $query = $this->db->get()->result_array();
	    
	    return $query;
    }
    
    public function get_msbDataType()
    {
        $arr = $this->db->select('dataTypeID,nameEN')->from('t_dataType')->get()->result_array();
        foreach ($arr as $lsts)
        {
            $arrDatatype[$lsts['dataTypeID']] = array(
                "dataTypeID" => $lsts['dataTypeID'],
                "name" => $lsts['nameEN']
            );
        }

        return $arrDatatype;
    }
}