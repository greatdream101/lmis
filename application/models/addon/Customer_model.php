<?php

class Customer_model extends CI_Model {

	public function __construct() {
		// Call the Model constructor
		parent::__construct();
	}
	
	function getReportAVG($select='*',$dateStart='', $dateEnd='') {
	    if ($dateEnd!='') {
	        $end = explode('-', $dateEnd);
	        $stringEnd = $end[0].'/'.$end[1];
	    }
	    if($dateStart!=''){
	       $start = explode('-', $dateStart);
	       $stringStart = $start[0].'/'.$start[1];
	    }	    

	    $this->db->select($select)->from('v_report_avg');
	    $this->db->where('flagID', 1);
	    //$this->db->where_in('region', ['R2']);
	    //$this->db->where_in('division', ['D06']);
	    //$this->db->where_in('agencyCD', ['0000011781','0000009601','0000004773','0000001156','0000000743']);
	    //$this->db->where('agencyCD','0000001156');
	    if($dateStart != "" && $dateEnd != "") {
	        $this->db->where("effectiveMonth BETWEEN '".$stringStart."' AND '".$stringEnd."'","", FALSE);
	        //$this->db->where("createdDate BETWEEN '". $dateStart ." 00:00:00' AND '". $dateEnd ." 23:59:59' ","", FALSE);
	    }else {
	        $this->db->where("effectiveMonth is NOT NULL",NULL, FALSE);
	    }
	    $this->db->orderby('id', 'ASC');
	    $query = $this->db->get()->result_array();
	    
	    return $query;
	}
	
	function getDealerReport($select='*',$dateStart='', $dateEnd='', $userGroupID='', $dealerCode='') {
	    if ($dateEnd!='') {
	        $end = explode('-', $dateEnd);
	        $stringEnd = $end[0].'/'.$end[1];
	    }
	    if($dateStart!=''){
	        $start = explode('-', $dateStart);
	        $stringStart = $start[0].'/'.$start[1];
	    }
	    
	    $this->db->select($select)->from('v_dealer_report');
		$this->db->where('flagID', 1);

		if($dateStart != "" && $dateEnd != "") {
			//$this->db->where("effectiveMonth BETWEEN '".$stringStart."' AND '".$stringEnd."'","", FALSE);
			$this->db->where("submit_date BETWEEN '". $dateStart ." 00:00:00' AND '". $dateEnd ." 23:59:59' ","", FALSE);
		}

		if($userGroupID == 2){ // if group id is dealer should where by dealer_code
			if(!isset($dealerCode) || !empty($dealerCode)){
				$this->db->where("dealer_code", $dealerCode);
			}
		}


		$this->db->orderby('id', 'ASC');
		$query = $this->db->get()->result_array();

		return $query;
	}
	
	function getTempCleanData() {
	    $this->db->select('id, insuredName, insuredDedupID, mobilePhone, agentName')->from('temp_cleanData')->where('active', 1);
	    $query = $this->db->get()->result_array();
	    
	    return $query;
	}
	
	function updateData($cusID, $data)
	{
		$this->db->where('cusID', $cusID);
		$this->db->update('t_customer', $data);
		//vd::d($this->db->last_query());die();
		return $this->db->affected_rows();
	}

	function get_DataSource() {
		$this->db->select('*')->from('t_dataSource')->where('active', 1);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function getCustomerData($conditionArray = array()) {
        $this->db->select('*')
            ->from('t_customer')
            ->where('active', 1);
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }
        
        $query = $this->db->get();
        return $query->row_array();
	}
	
	function getQuestionData($conditionArray = array()) {
        $this->db->select('*')
            ->from('t_question')
            ->where('active', 1);
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }

        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_CustomerSMS($select = array(), $conditionArray = array(), $str = '') {
        $this->db->select($select)->from('v_cusForSendSMS');
        if (! empty($conditionArray)) {
            $this->db->where($conditionArray);
        }
        if (! empty($str)) {
            $this->db->where($str);
        }
        $this->db->orderby('id', 'ASC');
        $query = $this->db->get();
        //vd::d($this->db->last_query());die();
        return $query->result_array();
    }
    
    function getRefNo($sourceID) {
        
        $this->db->select('*')->from('t_dataSource')->where('sourceID', $sourceID);
        $query = $this->db->get()->row_array();
        
        $arr = explode(" ", $query["createdDate"]);
        
        $str = 'createdDate LIKE "%'.$arr[0].'%"';
        $this->db->select('sourceID')->from('t_dataSource')->where($str);
        $query = $this->db->get()->result_array();
        
        
        $str = 'sourceID IN ('.$query[0]["sourceID"].', '.$query[1]["sourceID"].')';
        $this->db->select('cusID, sourceID, surveyToken')->from('t_customer')->where($str);
        $query = $this->db->get()->result_array();
        
        
        return $query;
    }
    
    /*
     * ************
     * Save Data
     * ************
     */
    
    function save_tempSMS($table_data) {
        
        $this->db->select('*')->from('tempSMS')->where('cusID', $table_data['cusID']);
        $query = $this->db->get()->row_array();
        
        if( empty($query) ){
            $this->db->insert('tempSMS', $table_data);
            return 1;
        }else{
            return 0;
        }
        
        
    }
}
