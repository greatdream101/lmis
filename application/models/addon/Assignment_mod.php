<?php

class Assignment_mod extends CI_Model {

     public function __construct() {
          // Call the Model constructor
          parent::__construct();
     }

     function get_clients_list() {
          $this->db->select('clientsID,code,name,nameEN')->from('t_clients')
                         ->where('active', 1);

          $query = $this->db->get();
          $result = $query->result_array();
          return $result;
     }

     function get_series_list($clientsID)
     {
          $this->db->select('s.seriesID,name')
                         ->from('t_series s')
                         ->join('t_branchSeries bs','bs.seriesID=s.seriesID','INNER')
                         ->join('t_clientsBranch cb','cb.branchID=bs.branchID','INNER')
                         ->where('s.active', 1)
                         ->where('cb.active', 1)
                         ->where('cb.clientsID', $clientsID)
                         ->group_by('s.seriesID');

          $query = $this->db->get();
          $result = $query->result_array();
          return $result;
     }

     function get_branch_series($seriesID)
     {
          $this->db->select('b.branchID,b.name')
                         ->from('t_branch b')
                         ->join('t_branchSeries bs','bs.branchID=b.branchID','INNER')
                         ->where('bs.seriesID', $seriesID);
          $query = $this->db->get();
          $a = array();
          foreach ($query->result_array() as $k => $v) {
               $a[] = array('id' => $v['branchID'], 'name' => $v['name']);
          }
          return array('data' => $a);
     }

     function get_surveyor_list($seriesID,$branchID)
     {
          $this->db->select('userID')
                         ->from('t_survey')
                         ->where('seriesID',$seriesID)
                         ->where('branchID',$branchID);
          $query = $this->db->get();

          if ($query->num_rows() > 0)
          {
               $assigned_surveyor = array();
               foreach ($query->result_array() as $k=>$v)
               {
                    $assigned_surveyor[] = $v['userID'];
               }

               $this->db->select('userID,fname,lname,email')
                              ->from('v_users')
                              ->where('groupID',21)
                              ->where_not_in('userID',$assigned_surveyor)
                              ->where('active',1)
                              ->orderby('userID','ASC');
               $query = $this->db->get();
               $surveyor_list = $query->result_array();
               return $surveyor_list;
          }
          else
          {
               $this->db->select('userID,fname,lname,email')
                              ->from('v_users')
                              ->where('groupID',21)
                              ->where('active',1)
                              ->orderby('userID','ASC');
               $query = $this->db->get();
               $surveyor_list = $query->result_array();
               return $surveyor_list;
          }
     }

     function assign_survey($data)
     {
          $this->db->insert_batch('t_survey', $data);
          $result = $this->db->affected_rows();
          return $result;
     }
}
