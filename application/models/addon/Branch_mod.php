<?php

class Branch_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        function get_data() {
                $this->db->select('branchID,code,name,nameEN')->from('t_branch')->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $o = array();
                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                $o[$row['branchID']] = $row;
                        }
                }
                return $o;
        }
        
        function get_data_branch() {
                $this->db->select('branchID,code,name,nameEN')->from('t_branch')->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $o = array();
                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                $o[$row['branchID']] = $row;
                        }
                }
                return $o;
        }
}
