<?php

class Question_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
                $this->load->model('users_model');
                $this->load->library('random_lib');
        }

        function get_all_questions($seriesID) {
                $this->db->select('seriesID, sectionID, questionID, code, name, nameEN, weight, graphTypeID')->from('t_question')->where('seriesID', $seriesID)->where('parentID', 0)->orderby('code asc');
                $query = $this->db->get();
                $rows = array();
                if ($query->num_rows() > 0) {
						foreach ($query->result_array() as $row) {
                                $rows[$row['questionID']] = $row;
                        }
                }

                return $rows;
        }

        function get_exCalID($seriesID) {
        	$this->db->select('questionID, parentID, value')
        	->from('t_question')
        	->where('seriesID', $seriesID)
        	->where('parentID <>', 0)
        	->where('exCalID', 2);
        	$query = $this->db->get()->result_array();
        	$rows = array();
        	$count = count($query);
        	if ($count> 0) {
        		foreach ($query as $row) {
        			$rows[$row['questionID']] = $row;
        		}
        	}

        	return $rows;
        }

        function add_answer() {
                $controlID = $_POST['controlID'];
                $code = str_replace('.', '_', trim($_POST['code']));
                $name = $_POST['name'];
                $parentID = $_POST['parentID'];
                $sectionID = $_POST['sectionID'];
                $seriesID = $_POST['seriesID'];
                $questionID = $_POST['questionID'];
                $exCalID = $_POST['exCalID'];
                $valueID = $_POST['value'];
                $option = $_POST['option'];
                $jumpTo = $_POST['jumpTo'];
                $jumpFrom = $_POST['jumpFrom'];
                $detail = $_POST['detail'];
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'sectionID' => $sectionID,
                    'seriesID' => $seriesID,
                    'parentID' => $parentID,
                    'controlID' => $controlID,
                	'exCalID' => $exCalID,
                    'value' => $valueID,
                    'option' => $option,
                    'jumpTo' => $jumpTo,
                    'jumpFrom' => $jumpFrom,
                    'detail' => $detail
                );
                
                if ($questionID > 0) {
                        $this->mdb->update_where('t_question', $data, array('questionID' => $questionID));
                } else {

                        $this->db->select('max(sortOrder) AS sortOrder')->from('t_question')->where('sortOrder IS NOT NULL');
                        $this->db->orderby('sortOrder', 'asc');
                        $query = $this->db->get();

                        foreach ($query->result_array() as $row) {
                                $sortOrder = $row['sortOrder'];
                        }

                        $insertID = $this->mdb->insert('t_question', $data);
                        $insertID = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
                        $this->mdb->update_where('t_question', array('sortOrder' => $insertID), array('questionID' => $questionID));
                }
                return 1;
        }

        function add_question() {

                $code = str_replace('.', '_', trim($_POST['code']));
                $name = $_POST['name'];
                $nameEN = isset($_POST['nameEN']) ? $_POST['nameEN'] : '';

                $sectionID = $_POST['sectionID'];
                $seriesID = $_POST['seriesID'];
                $questionID = $_POST['questionID'];
                $graphTypeID = $_POST['graphTypeID'];
                $uploadMin = $_POST['uploadMin'];
                $uploadMax = $_POST['uploadMax'];

                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'nameEN' => $nameEN,
                    'sectionID' => $sectionID,
                    'seriesID' => $seriesID,
                    'graphTypeID' => $graphTypeID,
                    'uploadMin'=>$uploadMin ,
                    'uploadMax'=>$uploadMax
                );

                if ($questionID > 0) {
                        $this->mdb->update_where('t_question', $data, array('questionID' => $questionID));
                        var_dump($this->db->last_query());
                        die();
                } else {
                        $this->db->select('max(sortOrder) AS sortOrder')->from('t_question')->where('sortOrder IS NOT NULL');
                        $this->db->orderby('sortOrder', 'asc');
                        $query = $this->db->get();

                        foreach ($query->result_array() as $row) {
                                $sortOrder = $row['sortOrder'];
                        }

                        $data['sortOrder'] = str_pad(($sortOrder + 1), 5, "0", STR_PAD_LEFT);

                        $insert_id = $this->mdb->insert('t_question', $data);
                        $insert_id = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
                        $this->mdb->update_where('t_question', array('sortOrder' => $insert_id), array('questionID' => $questionID));
                }
                return 1;
        }

        function delete_answer() {
                $questionID = $_POST['questionID'];
                $this->mdb->r_delete('t_question', array('questionID' => $questionID));
                return 1;
        }

        function delete_question() {
                $questionID = $_POST['questionID'];
                $this->mdb->r_delete('t_question', array('parentID' => $questionID));
                $this->mdb->r_delete('t_question', array('questionID' => $questionID));
                return 1;
        }

        public function get_question($questionID) {
                $this->db->select('*')->from('t_question')->where('questionID', $questionID);
                $query = $this->db->get();
                return $query->row_array();
        }

        public function get_value($seriesID) {
        	$values = [];
        	$this->db->select('seriesID, surveyID, cusID, questionID, userID, valueNet')->from('t_value')->where('seriesID', $seriesID);
        	$query = $this->db->get()->result_array();
        	foreach ($query as $row) {
	        	$key = $row['seriesID'].'_'.$row['surveyID'].'_'.$row['cusID'].'_'.$row['questionID'];
	        	$values[$key] = $row['valueNet'];
	        }
	        
        	return ['values' => $values];
        }

        public function get_data($seriesID) {
                $this->db->select('*')->from('v_seriesSection')->where('active', '1');

                if (strlen($seriesID) > 0)
                        $this->db->where('seriesID', $seriesID);

                $query = $this->db->get();

                $o = [];

                foreach ($query->result_array() as $row) {
                        $sectionID = $row['sectionID'];

                        $this->db->select('*')->from('t_question')->where('sectionID', $sectionID)->where('parentID', 0)->where('active', 1)->orderby('sortOrder', 'ASC');

                        if (strlen($seriesID) > 0) {
                                $this->db->where('seriesID', $seriesID);
                        }
                        $query1 = $this->db->get();
                        $a = ['sectionID' => $sectionID, 'seriesName' => $row['seriesName'], 'name' => $row['name'], 'nameEN' => $row['nameEN']];
                        $b = [];
                        foreach ($query1->result_array() as $row1) {
                                $this->db->select('*')->from('t_question')->where('parentID', $row1['questionID'])->where('active', '1')->orderby('sortOrder', 'ASC');
                                $query2 = $this->db->get();
                                $c = [];
                                if ($query2->num_rows() > 0) {
                                        foreach ($query2->result_array() as $row2) {
                                                $c[] = ['questionID' => $row2['questionID'], 'code' => $row2['code'], 'name' => $row2['name'], 'nameEN' => $row2['nameEN'], 'parentID' => $row2['parentID'], 'controlID' => $row2['controlID'], 'sortOrder' => $row2['sortOrder'], 'jumpTo' => $row2['jumpTo'], 'jumpFrom' => $row2['jumpFrom'], 'detail' => $row2['detail'], 'uploadMin' => $row2['uploadMin'],'uploadMax'=>$row2['uploadMax'], 'option' => $row2['option'], 'value' => $row2['value'], 'showRemark' => $row2['showRemark'], 'showSelect' => $row2['showSelect']];
                                        }
                                }
                                $b[] = ['questionID' => $row1['questionID'], 'requiredAnswer' => $row1['requiredAnswer'], 'code' => $row1['code'], 'name' => $row1['name'], 'nameEN' => $row1['nameEN'], 'parentID' => $row1['parentID'], 'children' => $c, 'sortOrder' => $row1['sortOrder'], 'detail' => $row1['detail'],  'uploadMin' => $row1['uploadMin'],'uploadMax'=>$row1['uploadMax']];
                        }
                        $a['questions'] = $b;
                        $o[$sectionID] = $a;
                }

                return $o;
        }

        function save_position() {
                $q = explode(',', $_POST['q']);
                foreach ($q as $k => $v) {
                        $e = explode('-', $v);
                        $q_id = $e[0];
                        $order = $e[1];
                        $order = str_pad($order, 3, '0', STR_PAD_LEFT);
                        $this->mdb->update_where('t_question', array('sortOrder' => $order), array('questionID' => $q_id));
                }
        }

        function get_section($seriesID) {
                $this->db->select('sectionID,name')->from('v_seriesSection')->where('seriesID', $seriesID);
                $query = $this->db->get()->result_array();
                $a = array();
                foreach ($query as $row) {
                        $a[$row['sectionID']] = $row;
                }
                return $a;
        }

        function save_ending() {
                $status_id = $_POST['statusID'];
                $sub_status_id = $_POST['subStatusID'];
                $cusid = $_POST['cusID'];

                $start_call_time = $_POST['start_call_time'];


                $this->db->select('cnt_call')->from('customer')->where('cusid', $cusid);
                $query = $this->db->get();
                $row = $query->row_array();
                $cnt_call = strtolower($row['cnt_call']);

                $latest_call_time = date("Y-m-d H:i:s");

                $user_id = $this->ion_auth->get_user_id();
                $data = array('status_id' => $status_id, 'sub_status_id' => $sub_status_id, 'latest_call_time' => $latest_call_time, 'cnt_call' => ++$cnt_call);
                $where = array('cusid' => $cusid);
                $this->mdb->update_where('customer_' . $suffix, $data, $where);

                $data['cusid'] = $cusid;
                $data['user_id'] = $user_id;
                $data['question_type_id'] = $question_type_id;
                $data['start_date'] = $start_call_time;
                $data['stop_date'] = $latest_call_time;

                $this->mdb->insert('call_history', $data);
                return 1;
        }

        function save_data() {

                $questionID = 0;
                $data = array();
                $data['cusid'] = $_POST['cus_id'];
                $str = NULL;
                foreach ($_POST as $k => $v) {
                        switch ($k) {
                                case 'questionID':
                                        $data['questionID'] = $v;
                                        $questionID = $v;
                                        break;
                                case 'project_id':
                                case 'question_type_id':
                                case 'seriesID':
                                case 'phase_id':
                                        $data[$k] = $v;
                                        break;
                                case 'sys_validation':
                                case 'mode':
                                        break;
                                case 'q2_1_name':
                                        //$data['dummy_1'] = $v;
                                        $str .= $v;
                                        break;
                                case 'q2_1b':
                                        //$data['dummy_2'] = $v;
                                        $str .= $v;
                                        break;
                                case 'q8':
                                        $str .= $v;
                                        break;
                                case 'q8_1':
                                        $str .= $v;
                                        break;
                                case 'valueNet':
                                        $data['valueNet'] = empty($str) ? $v : $str;
                                        break;
                        }
                }

                if ($questionID > 0 && count($data) > 0) {

                        $this->mdb->r_delete('t_value', array('questionID' => $data['questionID'], 'project_id' => $data['project_id'], 'seriesID' => $data['seriesID'], 'question_type_id' => $data['question_type_id'], 'phase_id' => $data['phase_id'], 'cusid' => $data['cusid']));

                        if ($_POST['mode'] == 'insert') {

                                $this->mdb->insert('t_value', $data);
                        }
                }
                return 1;
        }

        public function get_question_value($project_id, $seriesID, $question_type_id, $phase_id, $cus_id) {
                $this->db->select('questionID,valueNet')->from('t_value')
                        ->where('seriesID', $seriesID)
                        ->where('cusid', $cus_id);
                $query = $this->db->get();
                $cus_value = array();
                foreach ($query->result_array() as $val) {
                        $cus_value[$val['questionID']] = $val['valueNet'];
                }
                return $cus_value;
        }

        function get_control_question($questionID) {
                $this->db->select('controlID')->from('t_question')->where('questionID', $questionID);
                $qry = $this->db->get();
                if ($qry->num_rows() > 0) {
                        $res = $qry->row_array();
                        return $res['controlID'];
                } else {
                        return 0;
                }
        }

        // For New wizard question io (Daikin)
        public function get_question_val($project_id, $cus_id, $stepNumber = 0) {
                $this->db->select('valueID,questionID,valueNet,detail')->from('t_value')
                        ->where('project_id', $project_id)
                        ->where('cusid', $cus_id);
                $query = $this->db->get();
                $cus_value = array();
                $valueID = array();
                $dataNumber = 0;
                foreach ($query->result_array() as $val) {
                        $control = $this->get_control_question($val['questionID']);
                        $valueID[$dataNumber] = $val['valueID'];
                        if ($control == 1 || $control == 2 || $control == 4 || $control == 5) {
                                $cus_value[$val['questionID']] = $val['detail'];
                        } else {
                                $cus_value[$val['questionID']] = $val['valueNet'];
                        }
                        $dataNumber++;
                }
                for ($i = $dataNumber; $i < $stepNumber; $i++) {
                        $valueID[$i] = 0;
                }
                return array('question' => $cus_value, 'valueID' => $valueID);
        }

        public function get_last_answer($userID, $branchID, $seriesID) {
                $this->db->select('*')->from('t_value')
                        ->where('createdID', $userID)
                        ->where('branchID', $branchID)
                        ->where('seriesID', $seriesID)
                        ->orderby('updatedDate', 'DESC');
                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                        return $query->row_array();
                } else {
                        return 0;
                }
        }

        public function get_question_info($questionID) {
                $this->db->select('*')->from('t_question')
                        ->where('questionID', $questionID);

                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                        return $query->row_array();
                } else {
                        return 0;
                }
        }

        public function get_next_question($parentID, $value) {
                $this->db->select('*')->from('t_question')
                        ->where('parentID', $parentID)
                        ->where('value', $value);;

                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                        return $query->row_array();
                } else {
                        return 0;
                }
        }

        public function save_answer($userID, $surveyID, $branchID, $seriesID, $questionID, $data) {
             $valueID = 0;
               if($surveyID>0)
               {
                    $data_main = array(
                         'updatedID' => $userID,
                         'updatedDate' => date("Y-m-d H:i:s"),
                    );

                    $this->db->where('surveyID', $surveyID);
                    $this->db->update('t_survey', $data_main);
               }
               else
               {
                    $data_main = array(
                         'userID' => $userID,
                         'seriesID' => $seriesID,
                         'branchID' => $branchID,
                         'qcStatusID' => 4,
                         'createdID' =>$userID,
                         'createdDate' => date("Y-m-d H:i:s"),
                         'updatedID' =>$userID,
                         'updatedDate' => date("Y-m-d H:i:s"),
                    );

                    $this->db->insert('t_survey', $data_main);
                    $surveyID = $this->db->insert_id();
               }

                $this->db->select('*')->from('t_value')
                        ->where('createdID', $userID)
                        ->where('branchID', $branchID)
                        ->where('seriesID', $seriesID)
                        ->where('questionID', $questionID);

                $query = $this->db->get();

                if ($query->num_rows() > 0)
                {
                        // Update value
                        $data['surveyID'] = $surveyID;
                        $data['updatedID'] = $userID;
                        $data['updatedDate'] = date("Y-m-d H:i:s");

                        $this->db->where('createdID', $userID)
                                ->where('branchID', $branchID)
                                ->where('seriesID', $seriesID)
                                ->where('questionID', $questionID);
                        $this->db->update('t_value', $data);
$row = $query->row_array();
                        $valueID = $row['valueID'];//$this->db->affected_rows();
                        // return $updated_status;
                }
                else
                {
                        // Insert value
                        $createdDate = date("Y-m-d H:i:s");
                        $data['surveyID'] = $surveyID;
                        $data['createdID'] = $userID;
                        $data['createdDate'] = $createdDate;
                        $data['userID'] = $userID;
                        $data['updatedID'] = $userID;

                        $this->db->insert('t_value', $data);
                        $valueID = $this->db->insert_id();
                }

                $save_result['surveyID'] = $surveyID;
                $save_result['valueID'] = $valueID;

                return $save_result;
        }

        public function get_answer_value($userID, $branchID, $seriesID) {
                $this->db->select('*')
                        // ->select('channelID AS chanelID')
                        // ->select('channelRemark AS chanelRemark')
                        ->from('t_value')
                        ->where('createdID', $userID)
                        ->where('branchID', $branchID)
                        ->where('seriesID', $seriesID);

                $query = $this->db->get();
                $result = $query->result_array();

                return $result;
        }

        /*
        // OLD
        public function get_answer_value_jump($userID, $branchID, $seriesID) {
                $this->db->select('v.valueID,v.questionID, v.valueNet, q.jumpTo AS jt_1, qq.jumpTo AS jt_2')
                        ->select('sm.surveyID, sm.qcStatusID,a.controlName,a.filePath,a.code,a.extensionFile')
                        ->select('qcs.name AS qcStatus')
                        ->from('t_value v')
                        ->join('t_question q','q.questionID = v.questionID','LEFT')
                        ->join('t_question qq','qq.parentID = v.questionID AND qq.`value` = v.valueNet','LEFT')
                        ->join('t_survey sm','sm.surveyID = v.surveyID','LEFT')
                        ->join('t_attachments a','a.valueID = v.valueID','LEFT')
                        ->join('qcStatus qcs','qcs.qcStatusID = sm.qcStatusID','LEFT')
                        ->where('v.createdID', $userID)
                        ->where('v.branchID', $branchID)
                        ->where('v.seriesID', $seriesID);
                $query = $this->db->get();
                $result = $query->result_array();

                return $result;
        }
        */
        public function get_answer_value_jump($surveyID) {
                $this->db->select('v.valueID,v.questionID, v.valueNet, q.jumpTo AS jt_1, qq.jumpTo AS jt_2')
                        ->select('sm.surveyID, sm.qcStatusID,a.controlName,a.filePath,a.code,a.extensionFile')
                        ->select('qcs.name AS qcStatus')
                        ->from('t_value v')
                        ->join('t_question q','q.questionID = v.questionID','LEFT')
                        ->join('t_question qq','qq.parentID = v.questionID AND qq.`value` = v.valueNet','LEFT')
                        ->join('t_survey sm','sm.surveyID = v.surveyID','LEFT')
                        ->join('t_attachments a','a.valueID = v.valueID','LEFT')
                        ->join('qcStatus qcs','qcs.qcStatusID = sm.qcStatusID','LEFT')
                        ->where('sm.surveyID', $surveyID);
                $query = $this->db->get();
                $result = $query->result_array();

                return $result;
        }

        public function get_image($valueid, $controlname) {
                $this->db->select('controlName,filePath,code,extensionFile')
                ->from('t_attachments')
                ->where('valueID', $valueid)
                ->where('controlName', $controlname);

                $query = $this->db->get();
                $result = $query->result_array();
                $response = json_encode($result);
                return $response;
        }

        function generateRandomString($length = 1) {
                return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
        }

        function duplicate_answer(){
                $parentID = $_POST['parentID'];
                $duplicateTo = !empty($_POST['duplicateTo'])?$_POST['duplicateTo']:0;
                $numberOfDup = !empty($_POST['numberOfDup'])?$_POST['numberOfDup']:0;
                $dup_to_id = explode(',',$duplicateTo);
                foreach($dup_to_id as $val){
                        for($i=1;$i<=$numberOfDup;$i++){
                                $insert_id = 0;
                                $code = $this->random_lib->generateRandomString(4);
                                $strIns = 'insert into t_question (seriesID,sectionID,`code`,`name`,nameEN,parentID,`value`,`option`,controlID,graphTypeID)';
                                $strIns .= ' select seriesID,'.$val.',"q'.$code.'",`name`,nameEN,0,`value`,`option`,controlID,graphTypeID';
                                $strIns .= ' from t_question where questionID='.$parentID;
                                $this->db->query($strIns);
                                $insert_id = $this->db->insert_id();
                                if($insert_id > 0){
                                        $strIns = 'insert into t_question (seriesID,sectionID,`code`,`name`,nameEN,parentID,`value`,`option`,controlID,graphTypeID)';
                                        $strIns .= ' select seriesID,'.$val.',"ans'.$insert_id.'",`name`,nameEN,'.$insert_id.',`value`,`option`,controlID,graphTypeID';
                                        $strIns .= ' from t_question where parentID='.$parentID;
                                        $this->db->query($strIns);
                                }
                        }
                }
                return 1;
		}

		function save_attachments($table_data){
			$data = array();
			$this->db->insert('t_attachments', $table_data);
			return $this->db->insert_id();
		}

        function delete_attachments($valueid,$controlName){
            $this->db->delete('t_attachments', array('valueID' => $valueid, 'controlName' => $controlName));
        }

		function save_qc_status(){
		    $updatedDate = date("Y-m-d H:i:s");
		    $qcID = $this->ion_auth->get_userID();
		    $computerName = $this->security_lib->get_computerName();
		    $data['updatedID'] = $qcID;
		    $data['updatedDate'] = $updatedDate;
		    $data['computerName'] = $computerName;
		    $data['remark'] = isset($_POST['remark'])?$_POST['remark']:'';
		    $data['qcStatusID'] = 1; // wait
		    if($_POST['status'] == 'pass'){
		        $data['qcStatusID'] = 2;
		    }
		    if($_POST['status'] == 'reject'){
		        $data['qcStatusID'] = 3;
		    }

		    // Update last status
		    $this->db->where('seriesID',$_POST['series']);
		    $this->db->where('branchID',$_POST['branch']);
		    $this->db->where('questionID',$_POST['question']);
		    $this->db->where('userID',$_POST['user']);
		    $this->db->update('t_value',$data);
		    //vd::d($this->db->last_query());

		    // Insert History
		    $data['createdID'] = $qcID;
		    $data['createdDate'] = $updatedDate;
		    $data['seriesID'] = $_POST['series'];
		    $data['branchID'] = $_POST['branch'];
		    $data['questionID'] = $_POST['question'];
		    $data['userID'] = $_POST['user'];
		    $this->db->insert('t_qcHistory',$data);
		    //vd::d($this->db->last_query());

		    return $data['qcStatusID'];
		}

          function update_qc_status($surveyID, $update_data)
          {
               $this->db->where('surveyID', $surveyID);
               $this->db->update('t_survey', $update_data);

               return $this->db->affected_rows();
          }

          public function get_survey_list($userID=0) {
                 // $this->db->select('sm.surveyID AS get_detail, c.name AS clients, b.name AS branch, s.name AS series, u.fname, u.lname, sm.updatedDate, qcs.name AS survey_status')
                 $this->db->select('sm.surveyID AS get_detail, s.name AS series, u.fname, u.lname, sm.updatedDate, qcs.name AS survey_status')
                         ->from('t_survey sm')
                         ->join('users u','u.userID = sm.userID','INNER')
                         // ->join('t_clients c','c.clientsID = sm.clientsID','INNER')
                         // ->join('t_branch b','b.branchID = sm.branchID','INNER')
                         ->join('t_series s','s.seriesID = sm.seriesID','INNER')
                         ->join('qcStatus qcs','qcs.qcStatusID = sm.qcStatusID','INNER')
                         ->where('sm.active', 1);
                    if ($userID>0) {
                         $this->db->where('sm.userID',$userID);
                         // $this->db->where('sm.qcStatusID>2');
                    }

                 $query = $this->db->get();
                 $result = $query->result_array();
                 return $result;
         }

         /* //MSP
         public function get_survey_info($surveyID)
         {
                $this->db->select('sm.branchID, sm.seriesID, sm.userID, sm.surveyID,  u.fname, u.lname, b.name AS branch, s.name AS series, sm.updatedDate, qcs.name AS survey_status')
                        ->from('t_survey sm')
                        ->join('users u','u.userID = sm.userID','INNER')
                        ->join('t_branch b','b.branchID = sm.branchID','INNER')
                        ->join('t_series s','s.seriesID = sm.seriesID','INNER')
                        ->join('qcStatus qcs','qcs.qcStatusID = sm.qcStatusID','INNER')
                        ->where('sm.active', 1)
                        ->where('sm.surveyID',$surveyID);

                $query = $this->db->get();
                $result = $query->row_array();

                return $result;
        }
        */

        public function get_survey_info($conditionArray)
         {
                $this->db->select('sm.*')
                        ->from('t_survey sm')
                        ->join('t_series s','s.seriesID=sm.seriesID')
                        ->where('sm.active', 1)
                        ->where('sm.active', 1);
                if(!empty($conditionArray))
                {
                        $this->db->where($conditionArray);
                }

                $query = $this->db->get();
                $result = $query->row_array();

                return $result;
        }

        public function get_survey_main(){
        	$this->db->select('clientsID, cusID, userID, seriesID, branchID, qcStatusID')
        	->from('t_survey')
        	->where(ACTIVE, 1);
        	$result = $this->db->get()->result_array();

        	return $result;
        }

        public function get_preview_answer($surveyID)
        {
                // $this->db->select('q.code,q.name, v.valueNet AS value')
                $this->db->select('q.code,q.name, v.valueNet AS value, qq.name AS answer')
                        ->from('t_survey sm')
                        ->join('t_value v','v.surveyID = sm.surveyID','INNER')
                        ->join('t_question q','q.questionID = v.questionID','LEFT')
                        ->join('t_question qq','qq.parentID = v.questionID AND qq.`value` = v.valueNet','LEFT')
                        ->where('sm.surveyID', $surveyID);
                $query = $this->db->get();
                $result = $query->result_array();

                return $result;
        }

        public function save_answer_online($cusID, $surveyID, $seriesID, $questionID, $data) 
        {
                $valueID = 0;
                if($surveyID>0)
                {
                        $data_main = array(
                                'updatedDate' => date("Y-m-d H:i:s"),
                        );

                        $this->db->where('surveyID', $surveyID);
                        $this->db->update('t_survey', $data_main);
                }
                else
                {
                        $data_main = array(
                                'cusID' => $cusID,
                                'seriesID' => $seriesID,
                                'qcStatusID' => 4,
                                'createdDate' => date("Y-m-d H:i:s"),
                                'updatedDate' => date("Y-m-d H:i:s"),
                        );

                        $this->db->insert('t_survey', $data_main);
                        $surveyID = $this->db->insert_id();
                }

                $this->db->select('*')->from('t_value')
                        ->where('cusID', $cusID)
                        ->where('seriesID', $seriesID)
                        ->where('questionID', $questionID);

                $query = $this->db->get();

                if ($query->num_rows() > 0)
                {
                        // Update value
                        $data['surveyID'] = $surveyID;
                        $data['updatedDate'] = date("Y-m-d H:i:s");

                        $this->db->where('cusID', $cusID)
                                ->where('seriesID', $seriesID)
                                ->where('questionID', $questionID);
                        $this->db->update('t_value', $data);
                        $row = $query->row_array();
                        $valueID = $row['valueID'];//$this->db->affected_rows();
                        // return $updated_status;
                }
                else
                {
                        // Insert value
                        $createdDate = date("Y-m-d H:i:s");
                        $data['surveyID'] = $surveyID;
                        $data['cusID'] = $cusID;
                        $data['createdDate'] = $createdDate;

                        $this->db->insert('t_value', $data);
                        $valueID = $this->db->insert_id();
                }

                $save_result['surveyID'] = $surveyID;
                $save_result['valueID'] = $valueID;

                return $save_result;
        }
}
