<?php

class Region_mod extends CI_Model {

        public function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        function get_data() {
                $this->db->select('regionID,code,name,nameEN')->from('t_region')->where($this->config->item('display'), 1);
                $query = $this->db->get();
                $o = array();
                if ($query->num_rows() > 0) {
                        foreach ($query->result_array() as $row) {
                                $o[$row['regionID']] = $row;
                        }
                }
                return $o;
        }
        
        function get_RegionBranch() {
        	$this->db->select('regionID, regionName, branchID, name')->from('v_regionBranch')->where($this->config->item('display'), 1);
        	$query = $this->db->get()->result_array();
        	
        	return $query;
        }
}
