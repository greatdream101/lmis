<?php

class Users_model extends CI_Model {

        var $title = '';
        var $content = '';
        var $date = '';
        private $usersTable = "users";

        function __construct() {
                // Call the Model constructor
                parent::__construct();
        }

        function delete_data() {
                $id = $_POST['id'];
                $data[$this->config->item('display')] = 0;
                $this->mdb->update($this->config->item('user_table'), $data, 'userID', $id);
                $this->mdb->update_where($this->config->item('user_group_table'), $data, array('userID' => $id));
                $this->mdb->update_where('t_rep', $data, array('userID' => $id));
        }

        function save_data() {

                if (isset($_POST["sys_validation"])) {
                        foreach ($_POST["sys_validation"] as $key => $va) {
                                $this->form_validation->set_rules($key, $key, $va);
                        }
                        if ($this->form_validation->run() === false) {
                                $continue = false;
                                echo json_encode($this->form_validation->_error_array);
                                die();
                        }
                }

                $id = $_POST['id'];
                $password = $this->mdb->generate_password($_POST['password']);

                $data['password'] = $password['user_password'];
//                $data['salt'] = $password['salt'];
                $data['remember_code'] = $password['remember_code'];
                $data['userID'] = $id;
                $data['username'] = $_POST['username'];
                $data['email'] = $_POST['email'];

                $data_rep = array();
                $data_rep['name'] = $_POST['username'];
                $data_rep['email'] = $_POST['email'];
                //update
                $this->db->select('userID')->from($this->config->item('user_table'))->where('username', $data['username'])->or_where('email', $data['username'])->where($this->config->item('display'), 1); //->or_where('email', $data['email']);
                $query = $this->db->get();
                if ($query->num_rows() >= 1) {
                        if ($id > 0) {
                                unset($data['username']);
                                unset($data['userID']);
                                $this->mdb->update($this->config->item('user_table'), $data, 'userID', $id);
                                $this->mdb->update_where('t_rep', $data_rep, array('userID' => $id));
                                $this->mdb->r_delete($this->config->item('user_group_table'), array('userID' => $id));
                                $data = array();
                                $data['userID'] = $id;
                                $data['groupID'] = $_POST['groupID'];
                                $this->mdb->insert($this->config->item('user_group_table'), $data);
                        } else {
                                echo json_encode(array('username' => $this->mcl->gl('the_username_exists')));
                                die();
                        }
                } else {
                        if ($id > 0) {
                                $this->mdb->update($this->config->item('user_table'), $data, 'userID', $id);
                                $this->mdb->update_where('t_rep', $data_rep, array('userID' => $id));
                                $this->mdb->r_delete($this->config->item('user_group_table'), array('userID' => $id));
                        } else {//insert
                                $id = $this->mdb->insert($this->config->item('user_table'), $data);

                                if ($_POST['groupID'] !== 1) {
                                        
                                        $data_rep['userID'] = $id;
                                        $this->mdb->insert('t_rep', $data_rep);
                                }
                        }
                        $data = array();
                        $data['userID'] = $id;
                        $data['groupID'] = $_POST['groupID'];
                        $this->mdb->insert($this->config->item('user_group_table'), $data);
                }
                return 1;
        }

        function get_user($id) {
                $query = $this->db->get_where($this->usersTable, array('id' => $id,
                    'active' => '1'));
                return $query->result();
        }

        function get_users() {
                $query = $this->db->get_where($this->usersTable, array('active' => '1'));
                return $query->result_array();
        }

        function get_inactive_users() {
                $query = $this->db->get_where($this->usersTable, array('active' => '0'));
                return $query->result();
        }

        function get_notification_users($select = 'email') {
                $query = $this->db->select($select);
                $query = $this->db->get_where($this->usersTable, array('notify' => '1',
                    'active' => '1'));
                return $query->result();
        }

}
