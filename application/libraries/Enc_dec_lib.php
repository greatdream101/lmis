<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Enc_dec_lib {

//        public $plaintext = 'My secret message 1234';
        public $password = '3sc3RLrpd17soso_8R7f';
        public $method = 'aes-256-cbc';

        function enc($plaintext) {
                $password = substr(hash('sha256', $this->password, true), 0, 32);
//                echo "Password:" . $password . "\n";
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);


// av3DYGLkwBsErphcyYp+imUW4QKs19hUnFyyYcXwURU=
                $encrypted = base64_encode(openssl_encrypt($plaintext, $this->method, $this->password, OPENSSL_RAW_DATA, $iv));
                return $encrypted;
        }

        function dec($encrypted) {
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

// Must be exact 32 chars (256 bit)
// My secret message 1234
                $decrypted = openssl_decrypt(base64_decode($encrypted), $this->method, $this->password, OPENSSL_RAW_DATA, $iv);
                return $decrypted;
        }

//echo 'plaintext=' . $plaintext . "\n";
//echo 'cipher=' . $method . "\n";
//echo 'encrypted to: ' . $encrypted . "\n";
//echo 'decrypted to: ' . $decrypted . "\n\n";
}
