<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Security_lib extends CI_Model{        
        
        function get_computerName() {
                $computerName = gethostname();
                if (strlen($computerName) == 0)
                        $computerName = php_uname('n');
                return $computerName . ' - ' . $this->get_client_ip();
        }

        function is_admin_module($userID) {
                $u = $this->uri->segment_array();
                
                $fo = trim($u[1]);
                $fc = trim($u[2]);
              
                $functionID = $this->mdb->get_functionID($fo, $fc);
                $groupID = $this->ion_auth->get_groupID();
                $auth = $this->auth_level->get_auth($groupID);
                $po = strpos($auth, $groupID . '_' . $functionID);
                $auth = explode(',', substr($auth, $po));
                $auth = explode('-', $auth[0]);
                $auth = $auth[1];

//                if (strlen($auth) == 10) {
                if (substr($auth, 9, 1) == 1) {
                        return true;
                } else {
                        return false;
                }
//                } else {
//                        return false;
//                }
                return true;
        }

        
        function get_client_ip() {
// Get user IP address
                if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                        $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
                }

                $ip = filter_var($ip, FILTER_VALIDATE_IP);
                $ip = ($ip === false) ? '0.0.0.0' : $ip;
                return $ip;
        }

}
