<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Array_lib {

        function assoc_array_search($needle, $haystack, $skey) {
                if (is_array($haystack)) {
                        $o = '0';
                        foreach ($haystack as $key => $value) {
                                if (isset($value[$skey])) {
                                        if ($value[$skey] == trim($needle))
                                                $o.='1';
                                        else
                                                $o.='0';
                                } else
                                        $o.='0';
                        }

                        if (strpos($o, '1') !== false) {
                                return true;
                        } else {
                                return false;
                        }
                } else {
                        return false;
                }
        }

}
