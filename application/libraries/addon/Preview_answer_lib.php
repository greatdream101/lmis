<?php

class Preview_answer_lib extends CI_Model {
    public $userGroup = '';
    function __construct() {
        parent::__construct();
    }

    function tablePreviewAnswer($data='',$footer=''){
        $o = '';
        if (!empty($data)){
            $o .='
                <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                	<div class="modal-dialog" style="width: 70% !important; margin-right: 10% !important;">
                		<div class="modal-content">
                			<div class="modal-header">
                				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                				<h4 class="modal-title">'.$this->mcl->gl('preview_answer').'
                                    <!-- <a id="print" name="print" class="btn btn-default buttons-print" title="Print">
                                        <span><i class="fa fa-print"></i></span>
                                    </a> -->
                                </h4>
                			</div>
                			<div class="modal-body">
                                <table class="table table-bordered td-modal-content">
                                    <thead>
                                        <tr>';
                                     $o .= '<th class="col-md-1" style="text-align: center;">'.$this->mcl->gl('code').'<br></th>';
                                     $o .= '<th class="col-md-8" style="text-align: center;">'.$this->mcl->gl('question').'<br></th>';
                                     $o .= '<th class="col-md-1" style="text-align: center;">'.$this->mcl->gl('value').'<br></th>';
                                     $o .= '<th class="col-md-2" style="text-align: center;">'.$this->mcl->gl('answer').'<br></th>';
                                 $o .= '</tr>
                                    </thead>
                                    <tbody>';
                                    foreach($data as $key => $val){
                                        $o .= '<tr id="criteria">';
                                        $o .= '<td class="col-md-1">'.$val['code'].'</td>';
                                        $o .= '<td class="col-md-8">'.$val['name'].'</td>';
                                        $o .= '<td class="col-md-1" style="text-align: center;">'.$val['value'].'</td>';
                                        $o .= '<td class="col-md-2" style="text-align: left;">'.$val['answer'].'</td>';
                                        $o .= '</tr>';
                                    }
                             $o .= '</tbody>
                                </table>
                            </div>';
                            if($footer='save')
                            {
                                 $o .= '<div class="modal-footer">';
                                 $o .= '<button type="button" class="btn btn-danger" data-dismiss="modal">'.$this->mcl->gl('close').'</button>';
                                 $o .= '<button type="button" class="btn btn-default preview_confirm" onclick="confirm_save();">'.$this->mcl->gl('confirm_save').'</button>';
                                 $o .= '</div>';
                            }
      		$o .= '</div>
                	</div>
                </div>
                <div style="display: none;">
                    <button type="button" class="btn btn-success waves-effect waves-light"
                    data-toggle="modal" data-target="#con-close-modal" id="click-modal"></button>
                </div>

                <!-- Modal-Effect -->
                <script src="../assets/plugins/custombox/dist/custombox.min.js"></script>
                <script src="../assets/plugins/custombox/dist/legacy.min.js"></script>

                <script >
                    $(document).ready(function () {
                        $("#click-modal").trigger("click");
                        $("#print").click(function () {
                            $(".td-modal-content").printThis({
                                loadCSS: [
                                            "/msp/assets/css/bootstrap.min.css",
                                        ],
                                loadStyle: ["textarea.form-control { height: 100px !important; }"]
                            });
                        });
                    });
                </script>';
        }

        return $o;
    }
}
