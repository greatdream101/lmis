<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
    class Report_lib extends CI_Model {
        /*
         * To change this license header, choose License Headers in Project Properties.
         * To change this template file, choose Tools | Templates
         * and open the template in the editor.
         */
        
        public function __construct() {
            // Call the Model constructor
            parent::__construct();
            $this->load->model('addon/Customer_model');
            $this->load->model('addon/Report_model');
            $this->load->model('addon/Question_mod');
            $this->load->model('addon/Region_mod');
            $this->load->model('Users_model');
        }
        /***
         *
         * @param string $clientsID
         * @param string $seriesID
         */
        function get_rawdata($clientsID, $seriesID, $dateStart, $dateEnd) {
            $arr = [];
            $arr = $this->Question_mod->get_all_questions($seriesID);
            $arrQuestion = [];
            $arrQuestionCode = [];
            foreach ($arr as $lst){
                $arrQuestion[$lst['questionID']] = $lst['name'];
                $arrQuestionCode[] = $lst['code'];
            }
            array_pop($arrQuestion);
            array_pop($arrQuestionCode);
            
            $select = 'id, insuredDedupID ,effectiveDate
                    , region, division, agentName, agencyName, agencyCD';            
            $arr = $this->Customer_model->getReportAVG($select,$dateStart, $dateEnd);
            $arrCus = [];
            foreach ($arr as $lst){
                $arrCus[$lst['id']] = $lst;
            }
            
            $o = '<table class = "table table-bordered table-striped table-hover table-condensed datatable_local highchartd" data-graph-container-before="1" data-graph-type="column">';
            $o .='<thead style="color: black;">';
            $o .='<tr>';
            $o .= '<th>No.</th>';
            $o .= '<th>insuredCD</th>';
            $o .= '<th>agentName</th>';
            $o .= '<th>agencyName</th>';
            $o .= '<th>division</th>';
            $o .= '<th>region</th>';
            $o .= '<th>effectiveDate</th>';
            foreach ($arrQuestionCode as $lst){
                $o .= '<th style="text-align:center;">'.$lst.'</th>';
            }
            $o .= '</tr>';
            $o .= '</thead>';
            
            $haystack = [];
            $dataTable = [];
            $arr = $this->Question_mod->get_value($seriesID);
            foreach ($arr['values'] as $key => $lst){                
                $id = explode('_', $key);
                $QID = $id[3];//_questionID
                $idKeyTable = $id[0].'_'.$id[2];//seriesID_cusID
                if (!in_array($idKeyTable, $haystack)) {
                    $dataTable[$idKeyTable][$QID] = $lst;
                }else {
                    $haystack = $idKeyTable;
                }
            }
            $o .= '<tbody>';
            $i=0;            
            foreach ($arrCus as $lsts){
                $i++;                
                $insuredName  = isset($lsts['insuredDedupID'])?$lsts['insuredDedupID']:'-';
                $agentName  = isset($lsts['agentName'])?$lsts['agentName']:'-';
                $agencyName  = isset($lsts['agencyName'])?$lsts['agencyName']:'-';
                $division  = isset($lsts['division'])?$lsts['division']:'-';
                $region  = isset($lsts['region'])?$lsts['region']:'-';
                $effectiveDate  = isset($lsts['effectiveDate'])?$lsts['effectiveDate']:'-';
                $o .= '<tr>';
                $o .= '<td class ="text-center">'.$i.'</td>';
                $o .= '<td>'.$insuredName.'</td>';
                $o .= '<td>'.$agentName.'</td>';
                $o .= '<td>'.$agencyName.'</td>';
                $o .= '<td>'.$division.'</td>';
                $o .= '<td>'.$region.'</td>';
                $o .= '<td>'.$effectiveDate.'</td>';
                              
                foreach ($arrQuestion as $idkey => $lst){
                    $value = isset($dataTable[$seriesID.'_'.$lsts['id']][$idkey])?$dataTable[$seriesID.'_'.$lsts['id']][$idkey]:0;
                    $o .= '<td class ="text-center">'.$value.'</td>';
                }
                $o .= '</tr>';
            }
            $o .= '</tbody>';
            $o .= '</table>';
            echo $o;
        }
        
        function get_dealer_rawdata($dateStart, $dateEnd, $userGroupID, $dealerCode) {
                        
            $select = '*';
            $arr = $this->Customer_model->getDealerReport($select,$dateStart, $dateEnd, $userGroupID, $dealerCode);
            $arrCus = [];
            foreach ($arr as $lst){
                $arrCus[$lst['id']] = $lst;
            }
            
            $o = '<table class = "table table-bordered table-striped table-hover table-condensed datatable_local highchartd" data-graph-container-before="1" data-graph-type="column">';
            $o .='<thead style="color: black;">';
            $o .='<tr>';
            $o .= '<th>No.</th>';
            $o .= '<th>FirstName</th>';
            $o .= '<th>LastName</th>';
            $o .= '<th>MobileNo</th>';
            $o .= '<th>VIN</th>';
            $o .= '<th>Code</th>';
            $o .= '<th>Dealer</th>';
            $o .= '<th>Redeem Firstname</th>';
            $o .= '<th>Redeem Lastname</th>';
            $o .= '<th>Redeem MobileNo</th>';
            $o .= '<th>Redeem VIN</th>';
            $o .= '<th>Submit Date</th>';
            $o .= '<th>QA1</th>';
            $o .= '<th>QA2</th>';
            // $o .= '<th>Status Code</th>';
            $o .= '</tr>';
            $o .= '</thead>';
            
            $o .= '<tbody>';
            $i=0;
            $arrFlagUse = [
                '0' => "Allow"
                ,'1' => "Disallow"
            ];
            
            foreach ($arrCus as $lsts){
                $i++;
                // $Name    = isset($lsts['Name'])?$lsts['Name']:'-';
                $fname          = isset($lsts['fname'])?$lsts['fname']:'-';
                $lname          = isset($lsts['lname'])?$lsts['lname']:'-';
                $Tel            = isset($lsts['Tel'])?$lsts['Tel']:'-';
                $VIN_No         = isset($lsts['VIN_No'])?$lsts['VIN_No']:'-';
                $fname2         = isset($lsts['fname2'])?$lsts['fname2']:'-';
                $lname2         = isset($lsts['lname2'])?$lsts['lname2']:'-';
                $mobile2        = isset($lsts['mobile2'])?$lsts['mobile2']:'-';
                $code           = isset($lsts['code'])?$lsts['code']:'-';
                $Dealer         = isset($lsts['dealer_code'])?$lsts['dealer_code']:'-';
                $Redeem_VIN     = isset($lsts['Redeem_VIN'])?$lsts['Redeem_VIN']:'-';
                $created_date   = isset($lsts['submit_date'])?$lsts['submit_date']:'-';
                $QA1            = isset($lsts['QA1'])?$lsts['QA1']:'-';
                $QA2            = isset($lsts['QA2'])?$lsts['QA2']:'-';
                $flagUsed       = isset($lsts['flagUsed'])?$arrFlagUse[$lsts['flagUsed']]:'-';

                $o .= '<tr>';
                $o .= '<td class ="text-center">'.$i.'</td>';
                $o .= '<td>'.$fname.'</td>';
                $o .= '<td>'.$lname.'</td>';
                $o .= '<td>'.$Tel.'</td>';
                $o .= '<td>'.$VIN_No.'</td>';
                $o .= '<td>'.$code.'</td>';
                $o .= '<td>'.$Dealer.'</td>';
                $o .= '<td>'.$fname2.'</td>';
                $o .= '<td>'.$lname2.'</td>';
                $o .= '<td>'.$mobile2.'</td>';
                $o .= '<td>'.$Redeem_VIN.'</td>';
                $o .= '<td>'.$created_date.'</td>';
                $o .= '<td>'.$QA1.'</td>';
                $o .= '<td>'.$QA2.'</td>';
                // $o .= '<td>'.$flagUsed.'</td>';

                $o .= '</tr>';
            }
            $o .= '</tbody>';
            $o .= '</table>';
            echo $o;
        }
        
        function get_score_data($clientsID, $seriesID, $regionID, $equa){
            
            $arr = $this->Region_mod->get_RegionBranch();
            $arrBranch = [];
            $arrRegion = [];
            $branchName = [];
            $branchValue = 0;
            $arrBranchValue = [];
            $bValue = 0;
            foreach ($arr as $lst){
                $arrBranch[$lst['branchID']] = $lst['name'];
                $arrRegion[$lst['regionID']] = $lst['regionName'];
            }
            
            $arr = $this->Report_model->get_seriesName($seriesID);
            $arrSeries = [];
            foreach ($arr as $lst){
                $arrSeries[$lst['seriesID']] = $lst['name'];
            }
            
            $arr = $this->Question_mod->get_all_questions($seriesID);
            $arrQuestionPie = [];
            $arrQuestion = [];
            $forCal = [];
            foreach ($arr as $lst){
                $arrQuestion[$lst['questionID']] = $lst['name'];
                $forCal[$lst['questionID']] = $lst['weight'];
                $cntWeight = array_count_values($forCal);
                if ($lst['graphTypeID'] == 3) {
                    $arrQuestionPie[] = $lst['questionID'];
                }
            }
            
            $arr = $this->Question_mod->get_exCalID($seriesID);
            $arrExCalID = [];
            foreach ($arr as $lst){
                $arrExCalID[] = $lst['parentID'].'_'.$lst['value'];
            }
            
            
            $arr = $this->Question_mod->get_section($seriesID);
            $arrSection=[];
            foreach ($arr as $lst){
                $arrSection[$lst['sectionID']] = $lst['name'];
            }
            
            //เลือก สาขาทั้งหมด จาก clients
            $where = ['clientsID'=>$clientsID, 'regionID'=>$regionID, 'seriesID'=>$seriesID];
            $groupBranch = [];
            $arr = $this->Report_model->get_mapClientsToSeries(['clientsID', 'branchID', 'seriesID', 'regionID'],array_filter($where));
            foreach ($arr as $lst){
                $groupBranch[$lst['regionID']][] = $lst['clientsID'].'_'.$lst['branchID'].'_'.$lst['seriesID'];
            }
            
            $o = '<table class = "table table-bordered table-striped table-hover table-condensed datatable_local highchartd" data-graph-container-before="1" data-graph-type="column">';
            $o .= '<thead style="color: black;">';
            $o .= '<tr>';
            $o .= '<td style="background:#f5cb6f;">'.$arrSeries[$seriesID].'</td>';
            /////////////// Header
            
            foreach ($groupBranch as $key => $lst){
                foreach ($lst as $tmp) {
                    $getBranchID = explode('_', $tmp);
                    $o .= '<td style="background:#f5cb6f;">' . $arrBranch[$getBranchID['1']] . '</td>';
                    $branchName['name'][] = $arrBranch[$getBranchID['1']];
                }
                $o .= '<td style="background:#ed8137;">' . $arrRegion[$key]. '</td>';
            }
            
            $o .= '<td style="background:#5aa3e2;"> ' . $this->mcl->gl('country') . '</td>';
            $o .= '</tr>';
            $o .= '</thead>';
            $o .= '<tbody style="color: black;">';
            /////////////// END Header
            $arr = $this->Question_mod->get_survey_main();
            $ApprovedKey = [];
            foreach ($arr as $lst){
                if ($lst['qcStatusID'] == 2) {
                    $ApprovedKey[] = $lst['clientsID'].'_'.$lst['userID'].'_'.$lst['branchID'].'_'.$lst['seriesID'];
                }
            }
            
            $arr = $this->Question_mod->get_value($seriesID);
            $varlueByKey = [];
            $keyCheck = [];
            $valueRaw = [0,11,12];
            foreach ($arr['values'] as $key => $lst){
                $keyID = explode('_', $key);
                //		   branchID  _  seriesID   _  questionID
                $strKey = $keyID[1].'_'.$keyID[2].'_'.$keyID[3];
                //		   	 clientsID   _   userID    _   branchID   _  seriesID
                $keyAprove = $clientsID.'_'.$keyID[0].'_'.$keyID[1].'_'.$keyID[2];
                $valueChk = $keyID[3].'_'.$lst;
                
                if (in_array($keyAprove, $ApprovedKey)) {
                    if(!in_array($valueChk, $arrExCalID) && !in_array($lst, $valueRaw)){
                        if (in_array($strKey, $keyCheck)) {
                            $varlueByKey[$strKey]['value'] += intval($lst);
                            $varlueByKey[$strKey]['CntSurveyer'] += 1;
                        }else {
                            $varlueByKey[$strKey]['value'] = intval($lst);
                            $varlueByKey[$strKey]['CntSurveyer'] = 1;
                            $keyCheck[] = $strKey;
                        }
                    }
                }
            }
            
            $branchName['value'][] = 0;
            $arrQuestionPieVal = [];
            foreach ($arrQuestion as $id => $lst){
                $valkey = '';
                $CntCountry = 0;
                $tatalCountry = 0;
                $o .= '<tr>';
                $o .= '<td>'.$lst.'</td>';
                foreach ($groupBranch as $key => $lst){
                    $CntRegion = 0;
                    $tatalRegion = 0;
                    foreach ($lst as $tmp) {
                        $buffer = 0;
                        $branchValue = 0;
                        $CntSurveyer = 0;
                        $valDis = 0;
                        $AvgValDis = 0;
                        $getBranchID = explode('_', $tmp);
                        //			branchID				series		   questionID
                        $valkey = $getBranchID['1'].'_'.$getBranchID['2'].'_'.$id;
                        
                        $valDis = isset($varlueByKey[$valkey]['value'])?$varlueByKey[$valkey]['value']:0;
                        $forCount =
                        $CntSurveyer = isset($varlueByKey[$valkey]['CntSurveyer'])?$varlueByKey[$valkey]['CntSurveyer']:0;
                        if ($CntSurveyer!==0) {
                            $AvgValDis = $valDis/$CntSurveyer;
                        }
                        
                        $o .= '<td>'.number_format($AvgValDis*$equa,2).'</td>';
                        
                        $buffer = $AvgValDis*intval($forCal[$id]);
                        $branchValue += $buffer;
                        $sumBranchKey = $getBranchID['1'].'_'.$id;//branchID_QuestionID
                        $branchName['value'][$sumBranchKey] = $branchValue;
                        
                        $CntRegion += $CntSurveyer;
                        $tatalRegion += $valDis;
                        
                        $CntCountry += $CntSurveyer;
                        $tatalCountry += $valDis;
                        //var_dump($id);
                        if (in_array($id, $arrQuestionPie)) {
                            if (isset($arrQuestionPieVal[$id][$valDis])&&$arrQuestionPieVal[$id][$valDis]!=0) {
                                $arrQuestionPieVal[$id][$valDis] += 1;
                            }else {
                                $arrQuestionPieVal[$id][$valDis] = 1;
                            }
                        }
                    }
                    
                    if ($CntRegion!==0) {
                        $tatalRegion = $tatalRegion/$CntRegion;
                    }
                    
                    $o .= '<td style="background:#ed8137;">' .number_format($tatalRegion*$equa,2). '</td>';
                }
                if ($CntCountry!==0) {
                    $tatalCountry = $tatalCountry/$CntCountry;
                }
                
                $o .= '<td style="background:#5aa3e2;">' .number_format($tatalCountry*$equa,2). '</td>';
                $o .= '</tr>';
            }
            $o .= '<tr>';
            $o .= '<td style="background:#5aa3e2;">รวม</td>';
            $Check = [];
            foreach ($branchName['value'] as $key => $lst){
                $valKey = explode('_', $key);
                if (in_array($valKey[0], $Check)) {
                    $arrBranchValue[$valKey[0]] += intval($lst);
                }else {
                    $arrBranchValue[$valKey[0]] = intval($lst);
                    $Check[] = $valKey[0];
                }
            }
            $fbranchValue = [];
            foreach ($groupBranch as $key => $lst){
                $sumValueRegion = 0;
                $valueRegion = 0;
                $sumValueCountry = 0;
                $valueCountry = 0;
                foreach ($lst as $id =>$tmp) {
                    $fValue = 0;
                    $fBranchID = explode('_', $tmp);
                    $fValue = (intval($arrBranchValue[$fBranchID[1]])/$cntWeight[1])*$equa;
                    //SumBranch
                    $o .= '<td style="background:#5aa3e2;">'. $fValue .'</td>';
                    
                    $fbranchValue[] =  $fValue;
                    $sumValueRegion += intval($arrBranchValue[$fBranchID[1]]);
                    $sumValueCountry += intval($arrBranchValue[$fBranchID[1]]);
                }
                $valueRegion = ($sumValueRegion/$cntWeight[1])*$equa;
                //SumBranch Region
                $o .= '<td <td style="background:#5aa3e2;">'. $valueRegion .'</td>';
            }
            $valueCountry = ($sumValueCountry/$cntWeight[1])*$equa;
            //SumBranch Country
            $o .= '<td style="background:#5aa3e2;">'. $valueCountry .'</td>';
            $o .= '</tr>';
            $o .= '</tbody>';
            $o .= '</table>';
            
            // Column Graph
            $sum_value = $fbranchValue;
            $data[] = ['name' => $arrSeries[$seriesID], 'data' => $fbranchValue];
            $branch = $branchName['name'];
            $gpColumn = ['data' => $data,
                'branch' => $branch,
                'yasix_name' => 'Score Data'];
            
            // Pie
            
            foreach ($arrQuestionPieVal as $key => $buff){
                $dataPie = [];
                unset($buff[0]);
                $yes = isset($buff[1])?$buff[1]:0;
                $no = isset($buff[2])?$buff[2]:0;
                $dataPie[] = ['name' => 'ข้อที่ตอบ ใช่', 'y' => $yes];
                $dataPie[] = ['name' => 'ข้อที่ตอบ ไม่ใช่', 'y' => $no];
                
                $gpPie[$key] = ['data' => $dataPie,
                    'name'=>'Brand',
                    'colorByPoint'=>'true'
                ];
            }
            
            $gpPieChart = array('data'=>$gpPie);
            
            return array('table'=>$o,'graph'=>array('pie'=>$gpPieChart,'column'=>$gpColumn));
        }
        
        function get_rawDataSMS($dateStart, $dateEnd) {
            $arrDatatype = [];
            $arr = $this->db->select('dataTypeID,nameEN')->from('t_dataType')->get()->result_array();
            foreach ($arr as $lsts){
                $arrDatatype[$lsts['dataTypeID']] = $lsts['nameEN'];
            }
            
            $select = 'sms_result, dataTypeID, insuredDedupID ,effectiveDate
                    , region, division, agentName, agencyName, agencyCD';
            $arr = $this->Customer_model->getReportAVG($select,$dateStart, $dateEnd);
            
            $o = '<table class = "table table-bordered table-striped table-hover table-condensed datatable_local highchartd" data-graph-container-before="1" data-graph-type="column">';
            $o .='<thead style="color: black;">';
            $o .= '<tr>';
            $column = ['sms_result','dataTypeID','insuredDedupID','agentName','agencyName','division','region','effectiveDate'];
            foreach ($column as $lst){
                $o .= '<th>'.$lst.'</th>';
            }
            $o .= '</tr>';
            $o .= '</thead>';
            $o .= '<tbody>';
            foreach ($arr as $lst){
                $o .= '<tr>';
                $o .= '<td>'.$lst['sms_result'].'</td>';
                $o .= '<td>'.$arrDatatype[$lst['dataTypeID']].'</td>';
                $o .= '<td>'.$lst['insuredDedupID'].'</td>';
                $o .= '<td>'.$lst['agentName'].'</td>';
                $o .= '<td>'.$lst['agencyName'].'</td>';                
                $o .= '<td>'.$lst['division'].'</td>';
                $o .= '<td>'.$lst['region'].'</td>';
                $o .= '<td>'.$lst['effectiveDate'].'</td>';
                $o .= '</tr>';
            }
            $o .= '</tbody>';
            $o .= '</table>';
            echo $o;
        }
        
        function get_performanceSMS($dateStart, $dateEnd, $sourceID) {
            $cntSource = sizeof($sourceID);
            $source = $this->Customer_model->get_DataSource();
            $arr = $this->Report_model->get_SMSPerformance($dateStart, $dateEnd, $sourceID);
            foreach($arr as $k => $v) foreach ($source as $vv ) if($v['sourceID'] == $vv['sourceID']) $arr[$k]['sourceName'] = $vv['sourceName'];
            
            $o = '<table class = "table table-bordered table-striped table-hover table-condensed datatable_local highchartd" data-graph-container-before="1" data-graph-type="column">';
            $o .= '<thead style="color: black;">';
            $o .= '<tr>';
            $column = ['sourceName','response','cnt'];
            foreach ($column as $lst) $o .= '<th>'.$lst.'</th>';
            $o .= '</tr>';
            $o .= '</thead>';
            $o .= '<tbody>';
            foreach ($arr as $lst){
                $o .= '<tr>';
                if($cntSource > 1) $o .= '<td>Total</td>'; else $o .= '<td>'.$lst['sourceName'].''.$this->mcl->hd('sourceID',$lst['sourceID']).'</td>';
                $o .= '<td>'.$lst['response'].'</td>';
                $o .= '<td>'.$lst['cnt'].'</td>';;
                $o .= '</tr>';
            }
            $o .= '</tbody>';
            echo $o;
        }
        
    }
    