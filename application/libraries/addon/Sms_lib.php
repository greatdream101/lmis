<?php

if (!defined('BASEPATH'))
{
	exit('No direct script access allowed');
}
        

class Sms_lib extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->model('addon/Customer_model');
	}

	function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$pieces = [];
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$pieces []= $keyspace[random_int(0, $max)];
		}
		return implode('', $pieces);
	}

	function genSurveyToken($cusID,$length=5,$seriesID=1)
	{
		$token = $this->random_str($length);
		$userID = $this->ion_auth->get_userID();
		$updatedDate = date("Y-m-d H:i:s");
		$data = array(
			'surveyToken' => $token,
			'seriesID' => $seriesID,
			'updated_id' => $userID,
			'updated_date' => $updatedDate,
		);

		$updateResult = $this->Customer_model->updateData($cusID, $data);
		return $token;
	}
}
