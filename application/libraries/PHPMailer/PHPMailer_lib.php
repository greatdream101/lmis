<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PHPMailer_lib{
    public function load(){
        require_once(APPPATH."libraries/PHPMailer/PHPMailerAutoload.php");
        $objMail = new PHPMailer;
        return $objMail;
    }
}
