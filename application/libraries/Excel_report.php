<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_report extends PHPExcel
{
	public $cell;
	public $objPHPExcel;
	public $styleRawHeader;
	public $styleRawContent;
	public $rep_name;
	
	public function __construct()
	{
		parent::__construct();
		
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		//ini_set('max_execution_time',300);
		//ini_set('memory_limit', '2048M');
		date_default_timezone_set('Asia/Bangkok');
		
		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');
			
        $this->styleRawHeader = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'argb' => 'FF000000'),
                ),
            ),
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        
        $this->styleRawContent = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'argb' => 'FF000000'),
                ),
            ),
        );
        
        // Create new PHPExcel object
        $this->objPHPExcel = new PHPExcel();
	}
	
	public function excel_raw($params){
		$rep_title = $params['rep_title'];
		$this->rep_name = $params['rep_name'];
		$data_array = $params['data_array'];
		// Set Active Sheet
		$objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);
		
		// Rename worksheet
		$objWorksheet->setTitle($rep_title);
		$row_header = $data_array[0];

        $len = count($data_array);
        $header_name = array();
		
		// Loop Header
		foreach($row_header as $key => $value){
			$header_name[] = $key;
        }

        $objWorksheet->fromArray(
		    $header_name,   // The data to set
		    NULL,           // Array values with this value will not be set
		    'A1'            // Top left coordinate of the worksheet range where we want to set these values (default is A1)
		);
		
		// Add Header style
        $objWorksheet->getStyle("A1:".$this->objPHPExcel->getActiveSheet()->getHighestDataColumn()."1")->applyFromArray($this->styleRawHeader);

        $objWorksheet->fromArray(
		    $data_array,    // The data to set
		    NULL,           // Array values with this value will not be set
		    'A2'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
		);

		// Add Content style
        $objWorksheet->getStyle("A2:".$this->objPHPExcel->getActiveSheet()->getHighestDataColumn().$this->objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray($this->styleRawContent);
		// $objWorksheet->setAutoFilter($objWorksheet->calculateWorksheetDimension());
		
		$this->pop_save_file();
    }
    
    public function pop_save_file() {
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=' . $this->rep_name);
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');		
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		
		exit;
	}
	
	public function excel_template($params)	{
		$this->rep_name = $params['rep_name'];
		$rep_template = $params['rep_template'];
        $data_array = $params['data_array'];
        
        // vd::d($data_array);
        // exit;
		
		if (!file_exists('./application/excel_template/'.$rep_template)) {
			exit("Template not found. " .$rep_template);
		}
		
		// Load Template
		$this->objPHPExcel = PHPExcel_IOFactory::load('./application/excel_template/'.$rep_template);
		
		// Get Active Sheet
		$objWorksheet = $this->objPHPExcel->getActiveSheet();
		
		// Set document properties
		$this->objPHPExcel->getProperties()->setCreator("MOCAP")
		->setLastModifiedBy("MOCAP")
		->setTitle("Office 2007 XLSX Document")
		->setSubject("Office 2007 XLSX Document")
		->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("office 2007 openxml php")
		->setCategory("Excel Raw");		
		// Rename worksheet
		//$objWorksheet->setTitle($rep_title);		
		// Read Template Setting
		//================================================
		$headWidth = $objWorksheet->getCell('C2')->getValue();
		$headHeight = $objWorksheet->getCell('D2')->getValue();		
		$startRow = $headHeight+1;
		$rowNo = $startRow;
		
		// read some config
		// $sumRow = $objWorksheet->getCell('F2')->getValue();
        $useRowNo = $objWorksheet->getCell('G2')->getValue();
        
        // read Formula
		$template_formula = [];
		$formula_row = $objWorksheet->getCell('E2')->getValue();
		for($col=0;$col<$headWidth;$col++){
            $template_formula[$col] = $objWorksheet->getCellByColumnAndRow($col,$formula_row)->getValue();
        }
        $template_formula = array_filter($template_formula, 'strlen'); // remove null index

        // check SUM row and col
        $template_sum = [];
        $sumRow = $objWorksheet->getCell('F2')->getValue();
		for($col=0;$col<$headWidth;$col++){
            $template_sum[$col] = $objWorksheet->getCellByColumnAndRow($col,$sumRow)->getValue();
        }
        $template_sum = array_filter($template_sum, 'strlen'); // remove null index
		
		// Remove Setting Rows
		$objWorksheet->removeRow(1,2);
		
		// Insert New Row (use this method to copy format ant style from previous row)
		$len = count($data_array);
		$objWorksheet->insertNewRowBefore($rowNo + 1, $len - 1);

        // Add row no.
        if($useRowNo == 'Yes')
        {
            $rowCount = 1;
            while($rowCount<$len+1)
            {
                $objWorksheet->setCellValue('A'.$rowNo,$rowCount);
                $rowNo++;
                $rowCount++;
            }

            $startCol = 'B';
        }
        else
        {
            $startCol = 'A';
        }

        // Add data to Excel
        $objWorksheet->fromArray(
            $data_array,
            NULL,
            $startCol.$startRow
        );

        // If has row formula, Add it.
        $countFormula = count($template_formula);
        if($countFormula>0)
        {
            $rowNo = $startRow;
            $rowCount = 1;
            while($rowCount<$len+1)
            {
                foreach ($template_formula as $key => $value)
                {
                    $objWorksheet->setCellValueByColumnAndRow($key, $rowNo,"=".str_replace('[row]', $rowNo, $value));
                }

                $rowNo++;
                $rowCount++;
            }
        }

        // If has SUM row, Add it.
        $countSUM = count($template_sum);
        if($countSUM>0)
        {
            $rowNo = $startRow+$len;
            // vd::d($rowNo);exit;

            foreach ($template_sum as $key => $value)
            {
                $colString = PHPExcel_Cell::stringFromColumnIndex($key);
                $objWorksheet->setCellValueByColumnAndRow($key, $rowNo,"=SUM(".$colString.$startRow.":".$colString.($rowNo-1).")");
            }
        }
		
		$this->pop_save_file();
	}

	// public function genInternalReport($dateStart='',$arrValue=array(),$arrQuestion=array(),$arrDivision=array(),$arrRegion=array(),$arrDivSMS=array(),$arrRegSMS=array(),$arrAppendix=array()){
    public function genInternalReport($arrReport){
	    //use template
	    $this->rep_name = 'internal_report';
        // $rep_template = 'internal_report.xlsx';
        
        $rep_template = $arrReport['rep_template'];
        $dateStart = $arrReport['dateStart'];
        $arrValue = $arrReport['arrValue'];
        $arrQuestion = $arrReport['arrQuestion'];
        $arrDivision = $arrReport['arrDivision'];
        $arrRegion = $arrReport['arrRegion'];
        $arrDivSMS = $arrReport['arrDivSMS'];
        $arrRegSMS = $arrReport['arrRegSMS'];
        $arrAppendix = $arrReport['arrAppendix'];
	    
	    if (!file_exists('./application/excel_template/'.$rep_template)) {
	        exit("Template not found. " .$rep_template);
	    }
	    // Load Template
	    $this->objPHPExcel = PHPExcel_IOFactory::load('./application/excel_template/'.$rep_template);
	    // Get Active Sheet
        $objWorksheet = $this->objPHPExcel->getActiveSheet();
        
        // --- Set parameters ---
        // ---------------------------------------------------------------------------------------------------------------------------------
	    $month = [1,2,3,4,5,6,7,8,9,10,11,12];
        $arrMonthEn = ['Month','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        $arrMonth = ['เดือน','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
        $arrMonthFull = ['เดือน','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        
        $selectedMonthNum = date_format(date_create($dateStart),'n');
        $selectedYearNum = date_format(date_create($dateStart),'o');
        $selectedYearNum += 543;
        // $selectedMonth = $arrMonthFull[$selectedMonthNum];
        $selectedMonth = $arrMonthEn[$selectedMonthNum];

        $this->rep_name .= '_'.$selectedMonth.'_'.$selectedYearNum.'.xlsx';

        $arr = [];
        
        // $questionID = [1,7,13,19,25,31,37];
        $questionID = array();
        array_pop($arrQuestion);

        // $sheetIndex = 0;
        $this->sheetIndex = 0;

        foreach($arrQuestion as $key => $value)
        {
            $questionID[] = $key;

            $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);
            $objWorksheet->setTitle($value);

            $this->sheetIndex++;
        }
        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);

        $sumAnswerQuestion = array();
        $countAnswerQuestion = array();
        $avgAnswerQuestion = array();

        $sumAnswerDivision = array();
        $countAnswerDivision = array();
        $avgAnswerDivision = array();

        $divisionRanking = array();

        $sumDivYTD = array();
        $countDivYTD = array();
        $avgDivYTD = array();

        $breakdownDivision = array();

        // Region
        $sumAnswerRegion = array();
        $countAnswerRegion = array();
        // $avgAnswerRegion = array();

        $breakdownRegion = array();
        // $avgBreakdownRegion = array();
        
        // var_dump($questionID);exit;

        foreach($questionID as $k => $v)
        {
            // vd::d($v);
            foreach($month as $kk => $vv)
            {
                // vd::d($vv);
                foreach($arrDivision as $kkkDivision => $vvvDivision)
                {
                    // [questionID][month][division]
                    $sumAnswerQuestion[$v][$vv] = 0;
                    $countAnswerQuestion[$v][$vv] = 0;

                    $sumAnswerDivision[$v][$vv][$vvvDivision['division']] = 0;
                    $countAnswerDivision[$v][$vv][$vvvDivision['division']] = 0;

                    $sumDivYTD[$v][$vvvDivision['division']] = 0;
                    $countDivYTD[$v][$vvvDivision['division']] = 0;
                }

                foreach($arrRegion as $kkkRegion => $vvvRegion)
                {
                    // [questionID][month][region]

                    $sumAnswerRegion[$v][$vv][$vvvRegion['region']] = 0;
                    $countAnswerRegion[$v][$vv][$vvvRegion['region']] = 0;
                    $sumBreakdownRegion[$v][$vv] = 0;
                    // $countBreakdownRegion[$v][$vv] = 0;
                }
            }
        }
        // vd::d($sumAnswerDivision); exit;
        // vd::d($countAnswerDivision); exit;
        // exit;

        // ---------------------------------------------------------------------------------------------------------------------------------
        // --- End Set parameters ---


        // --- Calculate values ---
        // ---------------------------------------------------------------------------------------------------------------------------------
        foreach($arrValue as $k => $v)
        {
            $sumAnswerQuestion[$v['questionID']][$v['sourceMonth']]+= $v['valueNet'];
            $countAnswerQuestion[$v['questionID']][$v['sourceMonth']]++;
            
            $sumAnswerDivision[$v['questionID']][$v['sourceMonth']][$v['division']] += $v['valueNet'];
            $countAnswerDivision[$v['questionID']][$v['sourceMonth']][$v['division']]++;

            $sumAnswerRegion[$v['questionID']][$v['sourceMonth']][$v['region']] += $v['valueNet'];
            $countAnswerRegion[$v['questionID']][$v['sourceMonth']][$v['region']]++;

            $sumBreakdownRegion[$v['questionID']][$v['sourceMonth']] += $v['valueNet'];
            // $countBreakdownRegion[$v['questionID']][$v['sourceMonth']]++;

            $sumDivYTD[$v['questionID']][$v['division']] += $v['valueNet'];
            $countDivYTD[$v['questionID']][$v['division']]++;
        }
        // vd::d($sumAnswerQuestion); exit;
        // vd::d($countAnswerQuestion); exit;
        // vd::d($sumAnswerDivision); exit;
        // vd::d($countAnswerDivision); exit;
        // vd::d($sumAnswerRegion); exit;
        // vd::d($countAnswerRegion); exit;
        // vd::d($countBreakdownRegion); exit;

        foreach($questionID as $k => $v)
        {
            foreach($arrDivision as $kkkDivision => $vvvDivision)
            {
                $avgAnswerDivision[$v][$vvvDivision['division']][0] = $vvvDivision['division'];

                $breakdownDivision[$vvvDivision['division']][0] = $vvvDivision['division'];

                if($countDivYTD[$v][$vvvDivision['division']]>0)
                {
                    $avgDivYTD[$v][$vvvDivision['division']][0] = $sumDivYTD[$v][$vvvDivision['division']]/$countDivYTD[$v][$vvvDivision['division']];
                }
                else
                {
                    $avgDivYTD[$v][$vvvDivision['division']][0] = '';
                }

                foreach($month as $kk => $vv)
                {
                    if($countAnswerDivision[$v][$vv][$vvvDivision['division']]>0)
                    {
                        // $avgAnswerDivision[$v][$vvvDivision['division']][$vv] = round($sumAnswerDivision[$v][$vv][$vvvDivision['division']]/$countAnswerDivision[$v][$vv][$vvvDivision['division']],2);
                        $avgAnswerDivision[$v][$vvvDivision['division']][$vv] = $sumAnswerDivision[$v][$vv][$vvvDivision['division']]/$countAnswerDivision[$v][$vv][$vvvDivision['division']];
                    }
                    else
                    {
                        $avgAnswerDivision[$v][$vvvDivision['division']][$vv] = '';
                    }

                    if($countAnswerQuestion[$v][$vv]>0)
                    {
                        // $avgAnswerQuestion[$v][$vv] = round($sumAnswerQuestion[$v][$vv]/$countAnswerQuestion[$v][$vv],2);
                        $avgAnswerQuestion[$v][$vv] = $sumAnswerQuestion[$v][$vv]/$countAnswerQuestion[$v][$vv];
                    }
                    else
                    {
                        $avgAnswerQuestion[$v][$vv] = '';
                    }

                    if($selectedMonthNum == $vv)
                    {
                        $breakdownDivision[$vvvDivision['division']][$v] = $avgAnswerDivision[$v][$vvvDivision['division']][$vv];

                    }
                }
            }

            foreach($arrRegion as $kkkRegion => $vvvRegion)
            {
                $breakdownRegion[$vvvRegion['region']][0] = $vvvRegion['region'];

                foreach($month as $kk => $vv)
                {
                    if($countAnswerRegion[$v][$vv][$vvvRegion['region']]>0)
                    {
                        $avgAnswerRegion[$v][$vvvRegion['region']][$vv] = $sumAnswerRegion[$v][$vv][$vvvRegion['region']]/$countAnswerRegion[$v][$vv][$vvvRegion['region']];
                    }
                    else
                    {
                        $avgAnswerRegion[$v][$vvvRegion['region']][$vv] = '';
                    }

                    if($selectedMonthNum == $vv)
                    {
                        $breakdownRegion[$vvvRegion['region']][$v] = $avgAnswerRegion[$v][$vvvRegion['region']][$vv];
                    }
                }
            }

            // foreach($month as $kk => $vv)
            // {
            //     if($selectedMonthNum == $vv)
            //     {
            //         $avgBreakdownRegion[0][$v] = $sumBreakdownRegion[$v][$vv]/$countAnswerQuestion[$v][$vv];
            //     }
            // }
        }

        // vd::d($arrQuestion); exit;
        // vd::d($breakdownDivision); exit;
        // vd::d($breakdownRegion); exit;
        // vd::d($sumBreakdownRegion);
        // vd::d($countAnswerQuestion);
        // vd::d($avgBreakdownRegion); exit;

        // ---------------------------------------------------------------------------------------------------------------------------------
        // --- End Calculate values ---

        $this->writeSheet1($arrQuestion,$arrDivision,$selectedMonth,$selectedYearNum,$avgAnswerQuestion,$countAnswerQuestion,$avgDivYTD,$avgAnswerDivision,$divisionRanking);
        $this->writeSheet8($arrQuestion,$arrDivision,$arrRegion,$selectedMonth,$selectedMonthNum,$selectedYearNum,$breakdownDivision,$breakdownRegion);
        $this->writeSheet9($arrDivision,$arrRegion,$selectedMonth,$selectedMonthNum,$selectedYearNum,$arrDivSMS,$arrRegSMS);
        $this->writeSheet10($arrDivision,$month,$arrDivSMS);
        $this->writeSheet11($arrDivision,$arrAppendix,$selectedMonth,$selectedYearNum);

        // ++++++++++++++++++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex(0);

        // $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        // $objWriter->setIncludeCharts(TRUE);
        // $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
        
        // exit;

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=' . $this->rep_name);
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');		
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		
        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
		$objWriter->save('php://output');
        
        // $this->pop_save_file();

        exit;
    }

    function writeSheet1($arrQuestion,$arrDivision,$selectedMonth,$selectedYearNum,$avgAnswerQuestion,$countAnswerQuestion,$avgDivYTD,$avgAnswerDivision,$divisionRanking)
    {
        $this->sheetIndex = 0;
        foreach($arrQuestion as $key => $value)
        {
            $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);

            $sheetHeader = $objWorksheet->getCellByColumnAndRow(1,1)->getValue();
            $sheetHeader .= " ".$selectedMonth." ".$selectedYearNum;
            $objWorksheet->setCellValueByColumnAndRow(1,1,$sheetHeader);

            $objWorksheet->fromArray(
                $avgAnswerQuestion[$key],    // The data to set
                NULL,           // Array values with this value will not be set
                'C6'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
            );

            $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);
            $objWorksheet->fromArray(
                $countAnswerQuestion[$key],    // The data to set
                NULL,           // Array values with this value will not be set
                'C7'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
            );

            $objWorksheet->insertNewRowBefore(21, sizeof($arrDivision) - 1);
            $countDivision = sizeof($arrDivision);
            
            $objWorksheet->fromArray(
                $avgDivYTD[$key],    // The data to set
                NULL,           // Array values with this value will not be set
                'O20'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
            );

            $avgRow = $countDivision+20;
            for($j=0;$j<12;$j++)
            {
                $colString = PHPExcel_Cell::stringFromColumnIndex($j+2);
                $objWorksheet->setCellValueByColumnAndRow($j+2, $avgRow,'='.$colString.'6');
            }

            $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);
            $objWorksheet->fromArray(
                $avgAnswerDivision[$key],    // The data to set
                NULL,           // Array values with this value will not be set
                'B20'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
            );

            // Add cell color ranking
            // ========================================================================================
            for($col=0;$col<12;$col++)
            {
                for($row=0;$row<$countDivision;$row++)
                {
                    $cellValue = $objWorksheet->getCellByColumnAndRow($col+2,$row+20)->getValue();
                    if(!is_null($cellValue))
                    {
                        $divisionRanking[$row] = $cellValue;
                    }
                    else
                    {
                        $divisionRanking[$row] = NULL;
                    }
                }
                // vd::d($divisionRanking);
                $divisionRanking = array_filter($divisionRanking);
                // vd::d($divisionRanking);

                if(sizeof($divisionRanking)>0)
                {
                    for($top3=0;$top3<3;$top3++)
                    {
                        if(sizeof($divisionRanking)>0)
                        {
                            $maxScore = array_search(max($divisionRanking), $divisionRanking);
                            // vd::d($minScore);

                            $colString = PHPExcel_Cell::stringFromColumnIndex($col+2);
                            $objWorksheet->getStyle($colString.($maxScore+20))->getFill()->applyFromArray(array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array(
                                    'rgb' => 'ABE3A0'
                                )
                            ));

                            unset($divisionRanking[$maxScore]);
                        }
                        else
                        {
                            $top3 = 3;
                        }
                    }

                    for($bottom3=0;$bottom3<3;$bottom3++)
                    {
                        if(sizeof($divisionRanking)>0)
                        {
                            $minScore = array_search(min($divisionRanking), $divisionRanking);
                            // vd::d($minScore);

                            $colString = PHPExcel_Cell::stringFromColumnIndex($col+2);
                            $objWorksheet->getStyle($colString.($minScore+20))->getFill()->applyFromArray(array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array(
                                    'rgb' => 'E3A0A0'
                                )
                            ));

                            unset($divisionRanking[$minScore]);
                        }
                        else
                        {
                            $bottom3 = 3;
                        }
                    }
                }
            }
            // ========================================================================================

            // Add chart
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            $dataSeriesLabels = array(
                new PHPExcel_Chart_DataSeriesValues('String', "'".$value."'".'!$B$6', NULL, 1),
            );
            $xAxisTickValues = array(
                new PHPExcel_Chart_DataSeriesValues('String', "'".$value."'".'!$C$5:$N$5', NULL, 12),
            );
            $dataSeriesValues = array(
                new PHPExcel_Chart_DataSeriesValues('Number', "'".$value."'".'!$C$6:$N$6', NULL, 12),
            );
            
            $series = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
                PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
                range(0, count($dataSeriesValues)-1),			// plotOrder
                //$dataSeriesLabels,								// plotLabel
                NULL,
                $xAxisTickValues,								// plotCategory
                $dataSeriesValues								// plotValues
            );
            
            $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

            // Set layout
            $layout = new PHPExcel_Chart_Layout();
            $layout->setShowVal(true);

            $plotArea = new PHPExcel_Chart_PlotArea($layout, array($series));
            $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
            // $title = new PHPExcel_Chart_Title($value.' Avg. Score');
            $secondaryYAxisLabel = new PHPExcel_Chart_Title('');

            $axis = new PHPExcel_Chart_Axis();
            $axis->setAxisOptionsProperties('nextTo', null, null, null, null, null, 0, 5, 1, 1);
            // $axis->setAxisNumberProperties($axis::FORMAT_CODE_PERCENTAGE);
            
            //	Create the chart
            $yAxisLabel = new PHPExcel_Chart_Title('คะแนนเฉลี่ย');
            $chart = new PHPExcel_Chart(
                'chart1 '.$value,		// name
                // $title,			// title
                NULL,			// title
                NULL,		    // legend
                $plotArea,		// plotArea
                true,			// plotVisibleOnly
                0,				// displayBlanksAs
                NULL,			// xAxisLabel
                $yAxisLabel,	        // yAxisLabel
                $axis,
                null,
                null,
                null,
                null            // secondaryYAxisLabel
            );
            
            $chart->setTopLeftPosition('B9');
            $chart->setBottomRightPosition('O17');

            //	Add the chart to the worksheet
            $objWorksheet->addChart($chart);
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $this->sheetIndex++;
        }
    }

    function writeSheet8($arrQuestion,$arrDivision,$arrRegion,$selectedMonth,$selectedMonthNum,$selectedYearNum,$breakdownDivision,$breakdownRegion)
    {
        $this->sheetIndex = 7;
        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);

        $sheetHeader = $objWorksheet->getCellByColumnAndRow(1,1)->getValue();
        $sheetHeader .= " ".$selectedMonth." ".$selectedYearNum;
        $objWorksheet->setCellValueByColumnAndRow(1,1,$sheetHeader);

        $divisionHeader = $objWorksheet->getCellByColumnAndRow(1,4)->getValue();
        $divisionHeader .= " ".$selectedMonth." ".$selectedYearNum;
        $objWorksheet->setCellValueByColumnAndRow(1,4,$divisionHeader);

        $regionHeader = $objWorksheet->getCellByColumnAndRow(10,4)->getValue();
        $regionHeader .= " ".$selectedMonth." ".$selectedYearNum;
        $objWorksheet->setCellValueByColumnAndRow(10,4,$regionHeader);

        $countDivision = sizeof($arrDivision);

        // Save point
        // $objWorksheet->insertNewRowBefore(7, sizeof($arrDivision) - 1);
        // Copy style
        $cellStyle = $objWorksheet->getStyle('B7');
        $objWorksheet->duplicateStyle($cellStyle, 'B'.($countDivision+6));
        $cellStyle = $objWorksheet->getStyle('C7');
        $objWorksheet->duplicateStyle($cellStyle, 'C'.($countDivision+6).':I'.($countDivision+6));
        $cellValues = $objWorksheet->rangeToArray('B7:I7');
        $objWorksheet->fromArray($cellValues, null, 'B'.($countDivision+6));

        $cellStyle = $objWorksheet->getStyle('B6');
        $objWorksheet->duplicateStyle($cellStyle, 'B6:B'.($countDivision+5));
        $cellStyle = $objWorksheet->getStyle('C6');
        $objWorksheet->duplicateStyle($cellStyle, 'C6:I'.($countDivision+5));

        $objWorksheet->fromArray(
            $breakdownDivision,    // The data to set
            NULL,           // Array values with this value will not be set
            'B6'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
        );

        // Add cell color ranking
        for($col=0;$col<7;$col++)
        {
            for($row=0;$row<$countDivision;$row++)
            {
                $cellValue = $objWorksheet->getCellByColumnAndRow($col+2,$row+6)->getValue();
                if(!is_null($cellValue))
                {
                    $divisionRanking[$row] = $cellValue;
                }
                else
                {
                    $divisionRanking[$row] = NULL;
                }
            }
            // vd::d($divisionRanking);
            $divisionRanking = array_filter($divisionRanking);
            // vd::d($divisionRanking);

            if(sizeof($divisionRanking)>0)
            {
                for($top3=0;$top3<3;$top3++)
                {
                    if(sizeof($divisionRanking)>0)
                    {
                        $maxScore = array_search(max($divisionRanking), $divisionRanking);
                        // vd::d($minScore);

                        $colString = PHPExcel_Cell::stringFromColumnIndex($col+2);
                        $objWorksheet->getStyle($colString.($maxScore+6))->getFill()->applyFromArray(array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'rgb' => 'ABE3A0'
                            )
                        ));

                        unset($divisionRanking[$maxScore]);
                    }
                    else
                    {
                        $top3 = 3;
                    }
                }

                for($bottom3=0;$bottom3<3;$bottom3++)
                {
                    if(sizeof($divisionRanking)>0)
                    {
                        $minScore = array_search(min($divisionRanking), $divisionRanking);
                        // vd::d($minScore);

                        $colString = PHPExcel_Cell::stringFromColumnIndex($col+2);
                        $objWorksheet->getStyle($colString.($minScore+6))->getFill()->applyFromArray(array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'rgb' => 'E3A0A0'
                            )
                        ));

                        unset($divisionRanking[$minScore]);
                    }
                    else
                    {
                        $bottom3 = 3;
                    }
                }
            }
        }

        $avgRow = $countDivision+6;
        $avgCol = 2;
        foreach($arrQuestion as $key => $value)
        {
            $colString = PHPExcel_Cell::stringFromColumnIndex($selectedMonthNum+1);
            $objWorksheet->setCellValueByColumnAndRow($avgCol,$avgRow,"='".$value."'".'!$'.$colString.'$6');
            $avgCol++;
        }


        // Region

        // Copy style
        $countRegion = sizeof($arrRegion);

        $cellStyle = $objWorksheet->getStyle('K7');
        $objWorksheet->duplicateStyle($cellStyle, 'K'.($countRegion+6));
        $cellStyle = $objWorksheet->getStyle('L7');
        $objWorksheet->duplicateStyle($cellStyle, 'L'.($countRegion+6).':R'.($countRegion+6));
        $cellValues = $objWorksheet->rangeToArray('K7:R7');
        $objWorksheet->fromArray($cellValues, null, 'K'.($countRegion+6));

        $cellStyle = $objWorksheet->getStyle('K6');
        $objWorksheet->duplicateStyle($cellStyle, 'K6:K'.($countRegion+5));
        $cellStyle = $objWorksheet->getStyle('L6');
        $objWorksheet->duplicateStyle($cellStyle, 'L6:R'.($countRegion+5));

        $objWorksheet->fromArray(
            $breakdownRegion,    // The data to set
            NULL,           // Array values with this value will not be set
            'K6'            // Top left coordinate of the worksheet range where	we want to set these values (default is A1)
        );

        // Add cell color ranking
        $rowStart = 6;
        $colStart = 11;
        $rankCount = 1;
        for($col=0;$col<7;$col++)
        {
            for($row=0;$row<$countRegion;$row++)
            {
                $cellValue = $objWorksheet->getCellByColumnAndRow($col+$colStart,$row+$rowStart)->getValue();
                if(!is_null($cellValue))
                {
                    $regionRanking[$row] = $cellValue;
                }
                else
                {
                    $regionRanking[$row] = NULL;
                }
            }
            // vd::d($regionRanking);
            $regionRanking = array_filter($regionRanking);
            // vd::d($regionRanking);

            if(sizeof($regionRanking)>0)
            {
                for($bottomRank=0;$bottomRank<$rankCount;$bottomRank++)
                {
                    $minScore = array_search(min($regionRanking), $regionRanking);
                    // vd::d($minScore);

                    $colString = PHPExcel_Cell::stringFromColumnIndex($col+$colStart);
                    $objWorksheet->getStyle($colString.($minScore+$rowStart))->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'E3A0A0'
                        )
                    ));

                    unset($regionRanking[$minScore]);
                }

                for($topRank=0;$topRank<$rankCount;$topRank++)
                {
                    $maxScore = array_search(max($regionRanking), $regionRanking);
                    // vd::d($minScore);

                    $colString = PHPExcel_Cell::stringFromColumnIndex($col+$colStart);
                    $objWorksheet->getStyle($colString.($maxScore+$rowStart))->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'ABE3A0'
                        )
                    ));

                    unset($regionRanking[$maxScore]);
                }
            }
        }

        $avgRow = $countRegion+6;
        $avgCol = 11;
        foreach($arrQuestion as $key => $value)
        {
            $colString = PHPExcel_Cell::stringFromColumnIndex($selectedMonthNum+1);
            $objWorksheet->setCellValueByColumnAndRow($avgCol,$avgRow,"='".$value."'".'!$'.$colString.'$6');
            $avgCol++;
        }

        $this->sheetIndex++;   
    }

    function writeSheet9($arrDivision,$arrRegion,$selectedMonth,$selectedMonthNum,$selectedYearNum,$arrDivSMS_year,$arrRegSMS)
    {
        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);

        $sheetHeader = $objWorksheet->getCellByColumnAndRow(0,1)->getValue();
        $sheetHeader .= " ".$selectedMonth." ".$selectedYearNum;
        $objWorksheet->setCellValueByColumnAndRow(0,1,$sheetHeader);

        $countDivision = sizeof($arrDivision);
        $countRegion = sizeof($arrRegion);

        $rowStart = 7;
        
        // Division
        $cellStyle = $objWorksheet->getStyle('A8');
        $objWorksheet->duplicateStyle($cellStyle, 'A'.($countDivision+7));
        $cellStyle = $objWorksheet->getStyle('B7');
        $objWorksheet->duplicateStyle($cellStyle, 'B'.($countDivision+7).':H'.($countDivision+7));
        $cellValues = $objWorksheet->rangeToArray('A8:H8');
        $objWorksheet->fromArray($cellValues, null, 'A'.($countDivision+7));

        $cellStyle = $objWorksheet->getStyle('A7');
        $objWorksheet->duplicateStyle($cellStyle, 'A7:A'.($countDivision+6));
        $cellStyle = $objWorksheet->getStyle('B7');
        $objWorksheet->duplicateStyle($cellStyle, 'B7:H'.($countDivision+6));

        $arrDivSMS = $arrDivSMS_year;


        // Insert Value
        foreach($arrDivSMS as $key => $value)
        {
            // vd::d($value); exit;
            if($value['createdMonth']!=$selectedMonthNum)
            {
                unset($arrDivSMS[$key]);
            }
            else
            {
                unset($arrDivSMS[$key]['createdYear']);
                unset($arrDivSMS[$key]['createdMonth']);
            }
        }

        $objWorksheet->fromArray($arrDivSMS, null, 'A7');

        // Insert Formular
        $objWorksheet->setCellValueByColumnAndRow(1, ($countDivision+7),'=SUM(B7:B'.($countDivision+6).')');
        $objWorksheet->setCellValueByColumnAndRow(2, ($countDivision+7),'=SUM(C7:C'.($countDivision+6).')');
        $objWorksheet->setCellValueByColumnAndRow(4, ($countDivision+7),'=SUM(E7:E'.($countDivision+6).')');
        $objWorksheet->setCellValueByColumnAndRow(6, ($countDivision+7),'=SUM(G7:G'.($countDivision+6).')');
        $objWorksheet->setCellValueByColumnAndRow(7, ($countDivision+7),'=SUM(H7:H'.($countDivision+6).')');

        // Insert percent
        for($row=0;$row<=$countDivision;$row++)
        {
            $objWorksheet->getStyle('D'.($row+$rowStart).'')->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorksheet->setCellValueByColumnAndRow(3, ($row+$rowStart),'=((C'.($row+$rowStart).'*100)/B'.($row+$rowStart).')');

            $objWorksheet->getStyle('F'.($row+$rowStart).'')->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorksheet->setCellValueByColumnAndRow(5, ($row+$rowStart),'=((E'.($row+$rowStart).'*100)/C'.($row+$rowStart).')');
        }

        // Add cell color ranking
        // Division SMS Ranking

        $col=array('0'=>3,'1'=>5);

        for($i=0;$i<sizeof($col);$i++)
        {
            // $col=3;
            for($row=0;$row<$countDivision;$row++)
            {
                $cellValue = $objWorksheet->getCellByColumnAndRow($col[$i],$row+7)->getCalculatedValue();
                if(!is_null($cellValue))
                {
                    $divisionRanking[$row] = $cellValue;
                }
                else
                {
                    $divisionRanking[$row] = NULL;
                }
            }
            $divisionRanking = array_filter($divisionRanking);

            if(sizeof($divisionRanking)>0)
            {
                for($top3=0;$top3<3;$top3++)
                {
                    if(sizeof($divisionRanking)>0)
                    {
                        $maxScore = array_search(max($divisionRanking), $divisionRanking);

                        $colString = PHPExcel_Cell::stringFromColumnIndex($col[$i]);
                        $objWorksheet->getStyle($colString.($maxScore+7))->getFill()->applyFromArray(array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'rgb' => 'ABE3A0'
                            )
                        ));

                        unset($divisionRanking[$maxScore]);
                    }
                    else
                    {
                        $top3 = 3;
                    }
                }
                
                for($bottom3=0;$bottom3<3;$bottom3++)
                {
                    if(sizeof($divisionRanking)>0)
                    {
                        $minScore = array_search(min($divisionRanking), $divisionRanking);

                        $colString = PHPExcel_Cell::stringFromColumnIndex($col[$i]);
                        $objWorksheet->getStyle($colString.($minScore+7))->getFill()->applyFromArray(array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'rgb' => 'E3A0A0'
                            )
                        ));

                        unset($divisionRanking[$minScore]);
                    }
                    else
                    {
                        $bottom3 = 3;
                    }
                }
            }
        }

        // Region
        $cellStyle = $objWorksheet->getStyle('J8');
        $objWorksheet->duplicateStyle($cellStyle, 'J'.($countRegion+7));
        $cellStyle = $objWorksheet->getStyle('K7');
        $objWorksheet->duplicateStyle($cellStyle, 'K'.($countRegion+7).':Q'.($countRegion+7));
        $cellValues = $objWorksheet->rangeToArray('J8:Q8');
        $objWorksheet->fromArray($cellValues, null, 'J'.($countRegion+7));

        $cellStyle = $objWorksheet->getStyle('J7');
        $objWorksheet->duplicateStyle($cellStyle, 'J7:J'.($countRegion+6));
        $cellStyle = $objWorksheet->getStyle('K7');
        $objWorksheet->duplicateStyle($cellStyle, 'K7:Q'.($countRegion+6));

        // Insert Value
        foreach($arrRegSMS as $key => $value)
        {
            unset($arrRegSMS[$key]['createdMonth']);
        }
        $objWorksheet->fromArray($arrRegSMS, null, 'J7');

        // Insert Formular
        $objWorksheet->setCellValueByColumnAndRow(10, ($countRegion+7),'=SUM(K7:K'.($countRegion+6).')');
        $objWorksheet->setCellValueByColumnAndRow(11, ($countRegion+7),'=SUM(L7:L'.($countRegion+6).')');
        $objWorksheet->setCellValueByColumnAndRow(13, ($countRegion+7),'=SUM(N7:N'.($countRegion+6).')');
        $objWorksheet->setCellValueByColumnAndRow(15, ($countRegion+7),'=SUM(P7:P'.($countRegion+6).')');
        $objWorksheet->setCellValueByColumnAndRow(16, ($countRegion+7),'=SUM(Q7:Q'.($countRegion+6).')');

        // Insert percent
        for($row=0;$row<=$countRegion;$row++)
        {
            $objWorksheet->getStyle('M'.($row+$rowStart).'')->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorksheet->setCellValueByColumnAndRow(12, ($row+$rowStart),'=((L'.($row+$rowStart).'*100)/K'.($row+$rowStart).')');

            $objWorksheet->getStyle('O'.($row+$rowStart).'')->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorksheet->setCellValueByColumnAndRow(14, ($row+$rowStart),'=((N'.($row+$rowStart).'*100)/L'.($row+$rowStart).')');
        }


        // Add cell color ranking
        // Region SMS Ranking

        $col=array('0'=>12,'1'=>14);
        $rankCount = 1;

        for($i=0;$i<sizeof($col);$i++)
        {
            for($row=0;$row<$countRegion;$row++)
            {
                $cellValue = $objWorksheet->getCellByColumnAndRow($col[$i],$row+$rowStart)->getCalculatedValue();
                if(!is_null($cellValue))
                {
                    $regionRanking[$row] = $cellValue;
                }
                else
                {
                    $regionRanking[$row] = NULL;
                }
            }
            // vd::d($regionRanking);
            $regionRanking = array_filter($regionRanking);
            // vd::d($regionRanking);
            
            if(sizeof($regionRanking)>0)
            {
                for($bottomRank=0;$bottomRank<$rankCount;$bottomRank++)
                {
                    $minScore = array_search(min($regionRanking), $regionRanking);
                    // vd::d($minScore);

                    $colString = PHPExcel_Cell::stringFromColumnIndex($col[$i]);
                    $objWorksheet->getStyle($colString.($minScore+$rowStart))->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'E3A0A0'
                        )
                    ));

                    unset($regionRanking[$minScore]);
                }

                for($topRank=0;$topRank<$rankCount;$topRank++)
                {
                    $maxScore = array_search(max($regionRanking), $regionRanking);
                    // vd::d($minScore);

                    $colString = PHPExcel_Cell::stringFromColumnIndex($col[$i]);
                    $objWorksheet->getStyle($colString.($maxScore+$rowStart))->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'ABE3A0'
                        )
                    ));

                    unset($regionRanking[$maxScore]);
                }
            }
        }
    }

    function writeSheet10($arrDivision,$month,$arrDivSMS)
    {
        $this->sheetIndex = 9;
        $startRow = 5;
        $startCol = 2;
        $currentRow = 5;
        $countDivision = sizeof($arrDivision);

        $arrTable = array();
        $maxTable = 7;

        // Set value;
        foreach($arrDivision as $kkkDivision => $vvvDivision)
        {
            for($i=0;$i<$maxTable;$i++)
            {
                $arrTable[$i][$vvvDivision['division']][0] = $vvvDivision['division'];
            }

            foreach($month as $kk => $vv)
            {
                for($i=0;$i<$maxTable;$i++)
                {
                    $arrTable[$i][$vvvDivision['division']][$vv]='';
                }
            }
        }

        // Get value and calculate
        foreach($arrDivSMS as $k => $v)
        {
            $arrTable[0][$v['division']][$v['createdMonth']] = $v['send'];
            $arrTable[1][$v['division']][$v['createdMonth']] = $v['success'];
            $arrTable[2][$v['division']][$v['createdMonth']] = (($v['success']*100)/$v['send']);
            $arrTable[3][$v['division']][$v['createdMonth']] = $v['answer'];
            $arrTable[4][$v['division']][$v['createdMonth']] = (($v['answer']*100)/$v['success']);
            $arrTable[5][$v['division']][$v['createdMonth']] = $v['error'];
            $arrTable[6][$v['division']][$v['createdMonth']] = $v['rollback'];
        }

        // vd::d($arrTable); exit;

        // Loop to write data
        for($tableIndex=0;$tableIndex<$maxTable;$tableIndex++)
        {
            $tbStartRow = $currentRow;
            $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);

            $objWorksheet->insertNewRowBefore($currentRow+1, sizeof($arrDivision) - 1);

            $objWorksheet->fromArray(
                $arrTable[$tableIndex],
                NULL,
                'B'.$currentRow
            );

            // right side summary
            if(($tableIndex!=2)&&($tableIndex!=4))
            {
                for($i=0;$i<$countDivision;$i++)
                {
                    $objWorksheet->setCellValueByColumnAndRow(14, $currentRow,"=SUM(C".$currentRow.":N".$currentRow.")");
                    $currentRow++;
                }
            }
            else
            {
                $currentRow = $currentRow+$countDivision;
            }

            for($j=0;$j<sizeof($month);$j++)
            {
                $colIndex = $startCol+$j;
                $colString = PHPExcel_Cell::stringFromColumnIndex($colIndex);
                $objWorksheet->setCellValueByColumnAndRow($colIndex, $currentRow,"=SUM(".$colString.$tbStartRow.":".$colString.($currentRow-1).")");
            }

            $currentRow = $currentRow+7;
        }
    }

    function writeSheet11($arrDivision,$arrAppendix,$selectedMonth,$selectedYearNum)
    {
        $this->sheetIndex = 10;
        $startRow = 5;
        $currentRow = 5;
        $startCol = 2;
        $countDivision = sizeof($arrDivision);

        $objWorksheet = $this->objPHPExcel->setActiveSheetIndex($this->sheetIndex);

        $sheetHeader = $objWorksheet->getCellByColumnAndRow(1,1)->getValue();
        $sheetHeader .= " ".$selectedMonth." ".$selectedYearNum;
        $objWorksheet->setCellValueByColumnAndRow(1,1,$sheetHeader);

        $arrDivisionAL = array();
        $arrALScore = array();

        // Rearrange array
        foreach($arrAppendix as $k => $rowArray)
        {
            // row array
            $arrDivisionAL[$rowArray['division']][$rowArray['agencyCD']] = $rowArray;

            // score to sort
            $arrALScore[$rowArray['division']][$rowArray['agencyCD']] = $rowArray['AvgQ1'];
        }

        // Sort order DESC by AvgQ1
        foreach($arrALScore as $kk => $vv)
        {
            arsort($vv);
            $arrALScore[$kk] = $vv;
        }

        foreach($arrALScore as $k1 => $v1)
        {
            $countDivisionAL = sizeof($v1);
            $divisionRowStart = $currentRow;

            if($countDivisionAL<10)
            {
                $objWorksheet->insertNewRowBefore($currentRow+1, $countDivisionAL - 1);
                foreach($arrALScore[$k1] as $k2 => $v2)
                {
                    $objWorksheet->fromArray(
                        $arrDivisionAL[$k1][$k2],
                        NULL,
                        'B'.$currentRow
                    );
                    $currentRow++;
                }
                $divisionRowEnd = $currentRow-1;
                $objWorksheet->mergeCells('B'.$divisionRowStart.':B'.$divisionRowEnd);

                $objWorksheet->removeRow($currentRow,2);
            }
            else
            {

                // Start write top rank +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                $loopCount = 1;
                foreach($arrALScore[$k1] as $k2 => $v2)
                {
                    if($loopCount<=$countDivisionAL)
                    {
                        if($loopCount==1)
                        {
                            $maxAvgQ1 = $arrDivisionAL[$k1][$k2]['AvgQ1'];
                        }
                        
                        if( ($loopCount>5)&&(($arrDivisionAL[$k1][$k2]['AvgQ1'])<$maxAvgQ1) )
                        {
                            if(($arrDivisionAL[$k1][$k2]['AvgQ1'])<$maxAvgQ1)
                            {
                                $loopCount = $countDivisionAL;
                            }
                        }
                        else
                        {
                            $objWorksheet->insertNewRowBefore($currentRow+1, 1);
                            $objWorksheet->fromArray(
                                $arrDivisionAL[$k1][$k2],
                                NULL,
                                'B'.$currentRow
                            );

                            unset($arrALScore[$k1][$k2]);
                            unset($arrDivisionAL[$k1][$k2]);
                            $currentRow++;
                        }
                    }


                    $loopCount++;
                }
                $divisionRowEnd = $currentRow-1;
                $objWorksheet->mergeCells('B'.$divisionRowStart.':B'.$divisionRowEnd);

                $objWorksheet->removeRow($currentRow,1);

                // End write top rank +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


                // Start write bottom rank ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                $currentRow++;
                $divisionRowStart = $currentRow;

                asort($arrALScore[$k1]);

                $loopCount = 1;
                $countDivisionAL = sizeof($arrALScore[$k1]);

                foreach($arrALScore[$k1] as $k2 => $v2)
                {
                    if($loopCount<=$countDivisionAL)
                    {
                        if($loopCount==1)
                        {
                            $minAvgQ1 = $arrDivisionAL[$k1][$k2]['AvgQ1'];
                        }
                        
                        if( ($loopCount>5)&&(($arrDivisionAL[$k1][$k2]['AvgQ1'])>$minAvgQ1) )
                        {
                            if(($arrDivisionAL[$k1][$k2]['AvgQ1'])>$minAvgQ1)
                            {
                                $loopCount = $countDivisionAL;
                            }
                        }
                        else
                        {
                            $objWorksheet->insertNewRowBefore($currentRow+1, 1);
                            $objWorksheet->fromArray(
                                $arrDivisionAL[$k1][$k2],
                                NULL,
                                'B'.$currentRow
                            );

                            unset($arrALScore[$k1][$k2]);
                            unset($arrDivisionAL[$k1][$k2]);
                            $currentRow++;
                        }
                    }

                    $loopCount++;
                }
                $divisionRowEnd = $currentRow-1;
                $objWorksheet->mergeCells('B'.$divisionRowStart.':B'.$divisionRowEnd);

                $objWorksheet->removeRow($currentRow,1);

                // End write bottom rank ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            }
            $currentRow++;
        }

        $currentRow = $currentRow-1;
        $maxRow = $objWorksheet->getHighestRow();

        $removeLoop = ($maxRow-$currentRow);
        for($i=0;$i<=$removeLoop;$i++)
        {
            $objWorksheet->removeRow($currentRow,1);
        }
    }
}
