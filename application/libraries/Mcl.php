<?php

if (!defined('BASEPATH')) {
        exit('No direct script access allowed');
}

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_valuidation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Table $table
 * @property CI_Session $session
 * @property CI_FTP $ftp
 */
class Mcl extends CI_Model {

        public function __construct() {

                $this->load->library('Number_lib');
        }

        public function load_lang() {
                $site_lang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang'] : 'thai'; //$this->input->cookie('site_lang');

                if ($site_lang == false) {
                        $site_lang = 'english';
                        $_SESSION['site_lang'] = $site_lang;
                }
                $this->lang->load("application", $site_lang);
        }

        public function input_page($a, $t = [], $option = '', $name = []) {
                $o = '<div id=input_page>';

                $i = 1;

                $o .= '<section id="widget-grid" class="">';

                $o .= '<div class="jarviswidget" id="wid-id-edit">';

                $o .= '<header>';
                $o .= '<ul class="nav nav-tabs pull-left in">';
                $i = 1;
                foreach ($a as $k => $v) {
                        if (!empty($name[$k])) {
                                $tabname = $name[$k];
                        } else {
                                $tabname = $i;
                        }

                        if ($i == 1)
                                $class = 'active';
                        else
                                $class = '';
                        $o .= '<li class=' . $class . '>';
                        $o .= '<a data-toggle="tab" href="#tab_' . $i . '"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ' . $this->gl($tabname) . ' </span> </a>';
                        $o .= '</li>';
                        $i++;
                }

                $o .= '</ul>';
                $o .= '</header>';

                $o .= '<div>';

                $o .= '<div class="jarviswidget-editbox">';


                $o .= '</div>';

                $o .= '<div class="widget-body">';

                $o .= '<div class="tab-content">';
                $i = 1;
                foreach ($a as $k => $v) {
                        if ($i == 1)
                                $class = 'tab-pane active';
                        else
                                $class = 'tab-pane';
                        $o .= '<div class="' . $class . '" id="tab_' . $i . '">';

                        foreach ($v as $kk => $vv) {
                                if (!is_array($vv)) {
                                        if (strpos($vv, 'full-width') !== false) {
                                                $o .= '<div class="col-lg-12">' . $vv . '</div>';
                                        } else if (strpos($vv, 'half-width') !== false) {
                                                $o .= '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        } else if (strpos($vv, 'quarter-width') !== false) {
                                                $o .= '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        } else if (strpos($vv, 'half-width') !== false) {
                                                $o .= '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        } else if (strpos($vv, 'one-third-width') !== false) {
                                                $o .= '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        } else if (strpos($vv, '1-width') !== false) {
                                                $o .= '<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        } else if (strpos($vv, '2-width') !== false) {
                                                $o .= '<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        } else {
                                                $o .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' . $vv . '</div>';
                                        }
                                }
                        }
                        $o .= '</div>';
                        $i++;
                }

                $o .= '</div>';
                $o .= '</div>';
                $o .= '</div>';
                $o .= '</div>';
                $o .= '</section>';

                if (array_key_exists('edit_data', $t) && count($t['edit_data']) > 0 && array_key_exists('tableData', $t) && strlen($t['tableData']['indexColumn']) > 0 && isset($t['edit_data'][$t['tableData']['indexColumn']])) {
                        $id = $t['edit_data'][$t['tableData']['indexColumn']];
                } else {
                        $id = 0;
                }

                // $id = isset($t['edit_data']['id']) ? $t['edit_data']['id'] : 0;
                $o .= $this->hd('id', $id);
                if (isset($t['auth_data'])) {
                        $o .= form_hidden($t['auth_data']);
                }

                //                $o.=form_close
                $o .= $this->hd('saveTypeID', isset($t['tableData']['saveTypeID']) ? $t['tableData']['saveTypeID'] : 0);
                $o .= $this->hd('uri', isset($t['tableData']['uri']) ? $t['tableData']['uri'] : '');
                $o .= '</div>';
                return $o;
        }

        public function tabs($tabs) {
                $o = '';

                $o .= '<div class="row">

                                                        <div class="col-sm-12">
                                                                <ul class="' . $this->config->item('tab_class') . '" role="tablist">';
                foreach ($tabs as $k => $v) {
                        if ($k == 0) {
                                $c = 'active';
                        } else {
                                $c = '';
                        }

                        if (is_numeric($v['label'])) {
                                $o .= '<li class="tab ' . $c . '"><a href="#' . $v['name'] . '" role="tab" data-toggle="tab" aria-expanded="false">' . $this->number_lib->number2roman($this->gl($v['label'])) . '</a></li>';
                        } else {
                                $o .= '<li class=" tab ' . $c . '"><a href="#' . $v['name'] . '" role="tab" data-toggle="tab" aria-expanded="false">' . $this->gl($v['label']) . '</a></li>';
                        }
                }

                $ani = ['fadeInDown', 'fadeInRight', 'fadeInLeft', 'fadeInUp', 'fadeIn'];

                $sani = rand(0, 4);
                $ani = $ani[$sani];
                $ani = '';
                $o .= '</ul><!-- End .ext-tabs -->
                                                                <div class="tab-content">';
                foreach ($tabs as $k => $v) {
                        if ($k == 0) {
                                $c = 'active';
                        } else {
                                $c = '';
                        }
                        $o .= '<div id="' . $v['name'] . '" class="tab-pane ' . $c . ' animated ' . $ani . '">' . $v['content'];

                        $o .= '<div class="spacer-40"></div>';
                        $o .= '</div>';
                }

                $o .= '</div>
                        </div>
                        </div>';

                //                $o.='</div>';
                //                $o.='<p id="render_time" class="hide">Page rendered in <span id=elapsed><strong>{elapsed_time}</strong></span> s.</p>';
                return $o;
        }

        function tc($name, $t, $dtn) {
                //table checkbox
                $addon_data = $t['addonData'][$name];
                $table_name = $addon_data['data'];
                $header = explode(',', $addon_data['columnData']);
                $header = array_diff($header, $this->config->item('ignore_columns'));
                $data = $addon_data['data'];
                $selected_value = [];

                $index_column = $name;

                if (!empty($t['edit_data']['subTableData'])) {
                        if (array_key_exists($dtn, $t['edit_data']['subTableData'])) {
                                $values = $t['edit_data']['subTableData'][$dtn];
                                $v1 = array();
                                foreach ($values as $k => $v) {
                                        foreach ($v as $kk => $vv) {
                                                if ($kk == $name) {
                                                        array_push($v1, $vv);
                                                }
                                        }
                                }
                                $values = $v1;
                        } else {
                                $values = array();
                        }
                } else {
                        $values = array();
                }

                $o = '';
                //begin
                $o .= '<div class="sort-table-area">';
                $o .= '<table table_name="' . $dtn . '" class="sort-table table" id="tc_' . $name . '"  >';
                $o .= '<thead>';
                $o .= '<tr>';
                $o .= '<th width=50px class="filter-false">';
                $o .= '<label>
						<input class="checkbox-master" type="checkbox" value="" name="tc_' . $name . '">
						<span></span>
					</label>';
                $o .= '</th>';

                $colspan = 1;
                foreach ($header as $k => $v) {
                        if (in_array(strtolower($v), $this->config->item('ignore_columns')) == false) {
                                if (substr(strtolower($v), strlen($v) - 3) !== '_id') {
                                        $c = '';
                                        $o .= '<th class="' . $c . '">' . $this->gl($v) . '</th>';
                                        $colspan++;
                                }
                        }
                }

                $o .= '</tr>';
                $o .= '</thead>';

                $o .= '<tfoot>
					    <tr>
					      <th colspan="' . $colspan . '" class="ts-pager form-horizontal">
					        <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
					        <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
					        <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
					        <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
					        <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
					        <select class="pagesize input-mini" title="Select page size">
					          <option selected="selected" value="10">10</option>
					          <option value="20">20</option>
					          <option value="30">30</option>
					          <option value="40">40</option>
					          <option value="1000">1000</option>
					        </select>
					        <select class="pagenum input-mini" title="Select page number"></select>
					      </th>
					    </tr>
  				</tfoot>';

                $o .= '<tbody>';
                foreach ($data as $k => $v) {
                        $o .= '<tr>';
                        $o .= '<td>';
                        $check = '';
                        if (in_array($v[$name], $values)) {
                                $check = 'checked';
                                $o .= $this->hd($dtn . '_' . $name . '_' . $v[$name] . 'i', $v[$name]);
                        }
                        $o .= '<label>
							<input type="checkbox" class="tc" value="' . $v[$name] . '" name="' . $dtn . '_' . $name . '_' . $v[$name] . '" id="' . $dtn . '_' . $name . '_' . $v[$name] . '" ' . $check . '>
							<span></span>
						</label>';

                        $o .= '</td>';

                        foreach ($v as $kk => $vv) {
                                if (in_array(strtolower($kk), $this->config->item('ignore_columns')) == false) {

                                        if (substr(strtolower($kk), strlen($kk) - 3) !== '_id') {
                                                $o .= '<td>' . $vv . '</td>';
                                        } else {
                                                //                                                $c = 'hide';
                                                //                                                $o.='<td class="' . $c . '">' . $vv . '</td>';
                                        }
                                }
                        }
                        $o .= '</tr>';
                }
                $o .= '</tbody>';
                $o .= '</table>';

                //end

                $o .= '</div>';

                return $o;
                //        }
        }

        public function hd($name, $value) {
                $data = ['type' => 'hidden', 'id' => $name, 'name' => $name, 'value' => $value];
                $o = form_input($data);
                return $o;
        }

        public function get_placeholder($name) {
                return $this->gl('please_enter_the_value_of') . ' ' . $this->gl($name);
        }

        public function get_validation_attr($name, $d, $t, $class) {
                $data = array();

                if (count($t) > 0 && is_array($t)) {
                        if (isset($t['schema_data']['schema'][$name])) {
                                $data['maxlength'] = $t['schema_data']['schema'][$name]['max_length'];
                                $data['data-limit'] = $t['schema_data']['schema'][$name]['max_length'];
                        } else {
                                $data['maxlength'] = 254;
                                $data['data-limit'] = 254;
                        }

                        if (array_key_exists('schema_data', $t) && array_key_exists($name, $t['schema_data']['schema']) && strtolower($t['schema_data']['schema'][$name]['is_null']) == 'no') {
                                $data['required'] = 'required';
                                $data['validation_rules'] = 'trim|required';
                        }

                        if (strpos($class, 'required') !== false) {
                                $data['required'] = 'required';
                                $data['validation_rules'] = 'trim|required';
                        }
                } else {
                        if (strpos($class, 'required') !== false) {
                                $data['required'] = 'required';
                                $data['validation_rules'] = 'trim|required';
                        } else {
                                $data['required'] = '';
                                $data['validation_rules'] = '';
                        }
                        $data['maxlength'] = 254;
                        $data['data-limit'] = 254;
                }
                return array_merge($d, $data);
        }

        public function get_label_attr($data, $class) {
                $o = '';
                $o .= '<span style="inline-block">';
                if (strpos($class, 'no-label') == false) {
                        $o .= '<label for="' . $data['name'] . '">';
                        $o .= $this->gl($data['name']) . ' :' . $this->get_required_attr($data, $class);
                        if (array_key_exists('show_refresh', $data) && strpos($class, 'no-show-refresh') == false)
                                $o .= '<i class="show_refresh fa fa-cog pull-right" style="color:green"></i>';
                        $o .= '</label>';
                }

                $o .= '</span>';

                return $o;
        }

        public function get_label_attr_emp($data, $class) {
                $o = '';
                if (strpos($class, 'no-label') == false) {
                        $o .= '<label for="' . $data['name'] . '">';
                        $o .= $this->gl($data['name']) . ' :' . $this->get_required_attr($data, $class);
                        if (array_key_exists('show_refresh', $data) && strpos($class, 'no-show-refresh') == false)
                                $o .= '<i class="show_refresh fa fa-cog" style="color:green"></i>';
                        $o .= '</label>';
                }

                return $o;
        }

        public function get_info_attr($name0, $data, $class) {
                $o = '';
                $o .= '<div id="helper-' . $name0 . '" name="helper-' . $name0 . '" class="helper-text-box">';

                $class = ' ' . $class;
                if (strpos($class, 'no-description') == false) {
                        $o .= $this->get_description_attr($name0, $class);
                }

                $o .= '</div>';
                return $o;
        }

        public function get_required_attr($data, $class) {
                $o = '';
//                vd::d($data);
                if (isset($data['required']) && strlen($data['required']) > 0) {
                        $o .= '<span class=required><img width=12px src="' . base_url() . 'assets/images/asterisk.png"></span>';
                } else if (strpos($class, 'required') !== false) {
                        $o .= '<span class=required><img width=12px src="' . base_url() . 'assets/images/asterisk.png"></span>';
                } else if(isset($data['rules']) && strpos($data['rules'],'required')!==false){
                        $o .= '<span class=required><img width=12px src="' . base_url() . 'assets/images/asterisk.png"></span>';
                }
                return $o;
        }

        public function get_description_attr($name0, $class) {
                $o = '';

                if (strpos($class, 'wysiwyg') !== false) {
                        if (strpos($class, 'no-description') == false) {
                                $o .= '<span>' . $this->gl('Please enter long description in this area. You can also write in HTML format') . '</span>';
                        }
                } else if (strpos($class, 'select2') !== false) {
                        if (strpos($class, 'multiple') == true) {
                                $o .= '<span>' . $this->gl('select_more_option') . '</span>';
                        } else {
                                $o .= '<span> ' . $this->gl('select_one_option') . '</span>';
                        }
                } else if (strpos($class, 'msb') !== false) {
                        $o .= '<span>' . $this->gl('select_more_option') . '</span>';
                } else if (strpos($class, 'cb') !== false) {
                        $o .= '<span>' . $this->mcl->gl('select_more_checkbox') . '</span>';
                } else if (strpos($class, 'rb') !== false) {
                        $o .= '<span>' . $this->mcl->gl('select_one_radio') . '</span>';
                } else if (strpos($class, 'dp') !== false) {
                        $o .= '<span>' . $this->mcl->gl('Please enter the date in format of yyyy-mm-dd') . '</spam>';
                } else if (strpos($class, 'tp') !== false) {
                        $o .= '<span>' . $this->mcl->gl('Please enter the date in format of hh:rr') . '</spam>';
                } else {
                        //textbox
                        if (strpos($class, 'no-description') == false) {
                                $o .= '<span>' . $this->gl('you_have ') . ' <span id="' . $name0 . '_counter"></span> ' . $this->gl(' characters left') . '</span>';
                        }
                }
                return $o;
        }

        public function cb($name, $t = array(), $attr = array()) {
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }
                $swap_key = '';
                $class = '';
                $options = array();
                $data = array();
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                $class .= $data[$k];
                                                break;
                                        case 'swap_key':
                                                $swap_key = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('addonData', $t)) {
                        if (array_key_exists($name, $t['addonData'])) {
                                $options = $t['addonData'][$name]['data'];
                        } else if (strlen($swap_key) > 0 && array_key_exists('swap_key', $t['addonData'])) {
                                $options = $t['addonData'][$swap_key]['data'];
                        }

                        $opt = [];
                        foreach ($options as $k => $v) {
                                $opt[$k] = $v['name'];
                        }
                        if (array_key_exists('edit_data', $t) && array_key_exists($name, $t['edit_data'])) {
                                $selected = [$t['edit_data'][$name]];
                        } else {
                                $selected = [];
                        }
                        $data = [
                            'id' => $name,
                            'name' => $name,
                            'options' => $opt,
                            'selected' => $selected,
                        ];
                        $data = $this->get_validation_attr($name, $data, $t, $class);
                        $o = '<div class=row><div class=col-md-12>';

                        $o .= $this->get_label_attr($data, $class);
                        $o .= $this->get_required_attr($data, $class);
                        $o .= '</div></div>';


                        //                  $o .= '<div class=row><div class=col-md-12>';


                        foreach ($data['options'] as $k => $v) {
                                if (in_array($k, $data['selected'])) {
                                        $checked = true;
                                } else {
                                        $checked = false;
                                }
                                $o .= '<div class="checkbox checkbox-primary">';
                                $e = [
                                    'name' => $name,
                                    'id' => $name,
                                    'value' => $k,
                                    'checked' => $checked,
                                        //              'class' => "form-checkbox",
                                        //              'data-link-field' => $name,
                                ];
                                $o .= form_checkbox($e);
                                $o .= '<label for=' . $name . '>';

                                //                        $o .= '<span></span>';
                                $o .= $v;
                                $o .= '</label>';

                                $o .= '</div>';
                        }
                        $o .= $this->get_info_attr($name0, $data, $class);

                        $data = ['type' => 'hidden', 'id' => $name, 'name' => $name, 'value' => isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : 0];

                        $o .= form_input($data);
                        //                  $o .= '</div></div>';
                        return $o;
                } else {
                        $o = '';
                        return $o;
                }
        }

        public function emp_cb($name, $t = array(), $attr = array()) {
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }
                $swap_key = '';
                $class = '';
                $options = array();
                $data = array();
                $count = 0;
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                $class .= $data[$k];
                                                break;
                                        case 'swap_key':
                                                $swap_key = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('addonData', $t)) {
                        if (array_key_exists($name, $t['addonData'])) {
                                $options = $t['addonData'][$name]['data'];
                        } else if (strlen($swap_key) > 0 && array_key_exists('swap_key', $t['addonData'])) {
                                $options = $t['addonData'][$swap_key]['data'];
                        }

                        $opt = [];
                        foreach ($options as $k => $v) {
                                $opt[$k] = $v['name'];
                        }
                        if (array_key_exists('edit_data', $t) && array_key_exists($name, $t['edit_data'])) {
                                // $selected = [$t['edit_data'][$name]];
                                $selected = explode(',', $t['edit_data'][$name]);
                        } else {
                                $selected = [];
                        }
                        $data = [
                            'id' => $name,
                            'name' => $name,
                            'options' => $opt,
                            'selected' => $selected,
                        ];
                        $data = $this->get_validation_attr($name, $data, $t, $class);
                        $o = '<div class=row><div class=col-md-12>';

                        $o .= $this->get_label_attr($data, $class);
                        $o .= $this->get_required_attr($data, $class);
                        $o .= '</div></div>';


                        //                  $o .= '<div class=row><div class=col-md-12>';

                        foreach ($data['options'] as $k => $v) {

                                $count++;

                                if (in_array($k, $data['selected'])) {
                                        $checked = true;
                                } else {
                                        $checked = false;
                                }
                                $o .= '<div class="checkbox checkbox-primary">';
                                $e = [
                                        'name' => $name.'_'.$count,
                                        'id' => $name.'_'.$count,
                                        'value' => $k,
                                        'checked' => $checked,
                                        'class' => "form-checkbox",
                                        //              'data-link-field' => $name,
                                ];
                                $o .= form_checkbox($e);
                                $o .= '<label for=' . $name.'_'.$count . '>';

                                //                        $o .= '<span></span>';
                                $o .= $v;
                                $o .= '</label>';

                                $o .= '</div>';
                        }

                        $o .= $this->get_info_attr($name0, $data, $class);

                        $data = ['type' => 'hidden', 'id' => $name, 'name' => $name, 'value' => isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : 0];

                        $o .= form_input($data);
                        //                  $o .= '</div></div>';
                        return $o;
                } else {
                        $o = '';
                        return $o;
                }
        }

        public function rb($name, $t = array(), $attr = array()) {

                $opt = [];
                $data = [];

                $options = [];
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $class = '';
                $swap_key = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                $class .= $data[$k];
                                                break;
                                        case 'swap_key':
                                                $swap_key = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('addonData', $t)) {
                        if (isset($t['addonData'][$name])) {
                                $options = $t['addonData'][$name]['data'];
                        } else if (isset($t['addonData'][$swap_key])) {
                                $options = $t['addonData'][$swap_key]['data'];
                        }
                        foreach ($options as $k => $v) {
                                $opt[$k] = $v['name'];
                        }
                } else {
                        foreach ($t as $k => $v) {
                                $opt[$v['value']] = $this->mcl->gl($v['name']);
                        }
                }


                $data = $this->get_validation_attr($name, $data, $t, $class);

                if (array_key_exists('edit_data', $t) && count($t['edit_data']) > 0 && array_key_exists($name, $t['edit_data'])) {
                        $selected = [$t['edit_data'][$name]];
                } else {
                        $selected = [];
                }
                $data['options'] = $opt;
                $data['selected'] = $selected;
                $data['name'] = $name;
                $o = '<div class=row><div class=col-md-12><div class="form-group">';
                $o .= $this->get_label_attr($data, $class);
                foreach ($data['options'] as $k => $v) {
                        if (in_array($k, $data['selected'])) {
                                $checked = 'checked';
                        } else {
                                $checked = false;
                        }
                        $o .= '<div class="radio ' . $class . '">';
                        $e = [
                            'name' => $name,
                            'id' => $name,
                            'value' => $k,
                            'checked' => $checked,
                        ];
                        //                        var_dump($checked);
                        $e = $this->get_validation_attr($name, $e, $t, $class);
                        $o .= form_radio($e);
                        $o .= '<label>';

                        $o .= $v;

                        $o .= '</label>';

                        $o .= '</div>';
                }
                $class .= ' rb';
                $o .= $this->get_info_attr($name0, $data, $class);

                $data = ['type' => 'hidden', 'id' => $name, 'name' => $name, 'value' => isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : ''];
                $o .= form_input($data);
                //                $o .= '</div>';
                $o .= '</div></div></div>';
                return $o;
        }

        public function sb($name, $t = array(), $attr = array()) {

                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $class = 'select2 p-a-3 select-border ';
                $swap_key = '';
                $rules = '';
                $data = array();

                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class .= $data[$k];
                                                break;
                                        case 'swap_key':
                                                $swap_key = $data[$k];
                                                break;
                                        case 'rules':
                                        case 'rule':
                                                $rules = $data[$k];
                                                break;
                                }
                        }
                }

                $data['id'] = $name0;
                $data['name'] = $name0;
                $data['class'] = $class;
                if (is_array($t) && count($t) > 0) {
                        if (array_key_exists($name, $t['addonData'])) {
                                $options = $t['addonData'][$name]['data'];
                                $selected = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else if (strlen($swap_key) > 0 && array_key_exists($swap_key, $t['addonData'])) {
                                $options = $t['addonData'][$swap_key]['data'];
                                $selected = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else {
                                $options = [];
                                $selected = '';
                        }
                } else {
                        $options = [];
                        $selected = '';
                }
                $opt = [];
                $data_subtext = [];
                foreach ($options as $k => $v) {
                        $fcode = array_key_exists('code', $v) ? $v['code'] : '';
                        $fname = array_key_exists('name', $v) ? $v['name'] : '';
                        if (strlen($fname) == 0)
                                $fname = array_key_exists('username', $v) ? $v['username'] : '';
                        $detail = array_key_exists('detail', $v) ? $v['detail'] : '';
                        //                       / print($fname);
                        if (strlen($fcode) > 0 && strlen($fname) > 0) {
                                $opt[$k] = $fcode . '-' . substr($fname, 0, $this->config->item('option_length'));
                        } else {
                                $opt[$k] = substr($fname, 0, $this->config->item('option_length'));
                        }
                        if (strlen($detail) > 0) {
                                $data_subtext[$k] = '<div class="data-subtext">' . $detail . '</div>';
                        }
                }

                $data['options'] = $opt;
                $data['selected'] = $selected;
                $data['table_name'] = isset($t['addonData'][$name0]['table_name']) ? $t['addonData'][$name0]['table_name'] : '';
                $data['validation_rules'] = isset($data['validation_rules']) ? $data['validation_rules'] : '';
                $data['validation_rules'] = strlen($rules) > 0 ? $rules : '';
                $data['show_refresh'] = true;
                $data['function_id'] = isset($t['addonData'][$name0]['functionID']) ? $t['addonData'][$name0]['functionID'] : '';

                $data = $this->get_validation_attr($name, $data, $t, $class);

                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }

                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr($data, $class);

                $o .= form_dropdown($data);

                $o .= $this->get_info_attr($name0, $data, $class);
                $o .= $this->hd($name0, $selected);
                $o .= '</div></div>';

                if (count($data_subtext) > 0) {
                        //                  $o .= '<div class="row"><div class=col-lg-12>';
                        $o .= '<span id=' . $name0 . ' class="data-subtext">';
                        foreach ($data_subtext as $k => $v) {
                                $o .= '<span id=' . $k . ' class="hide">' . html_entity_decode($v) . '</span>';
                        }
                        $o .= '</span>';
                        //                  $o .= '</div></div>';
                }

                $o .= '</div>';
                return $o;
        }

        public function sb_emp($name, $t = array(), $attr = array()) {

                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $class = 'select2 p-a-3 select-border ';
                $swap_key = '';
                $data = array();

                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class .= $data[$k];
                                                break;
                                        case 'swap_key':
                                                $swap_key = $data[$k];
                                                break;
                                }
                        }
                }

                $data['id'] = $name0;
                $data['name'] = $name0;
                $data['class'] = $class;
                if (is_array($t) && count($t) > 0) {
                        if (array_key_exists($name, $t['addonData'])) {
                                $options = $t['addonData'][$name]['data'];
                                $selected = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else if (strlen($swap_key) > 0 && array_key_exists($swap_key, $t['addonData'])) {
                                $options = $t['addonData'][$swap_key]['data'];
                                $selected = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else {
                                $options = [];
                                $selected = '';
                        }
                } else {
                        $options = [];
                        $selected = '';
                }
                $opt = [];
                $data_subtext = [];
                foreach ($options as $k => $v) {
                        $fcode = array_key_exists('code', $v) ? $v['code'] : '';
                        $fname = array_key_exists('name', $v) ? $v['name'] : '';
                        if (strlen($fname) == 0)
                                $fname = array_key_exists('username', $v) ? $v['username'] : '';
                        $detail = array_key_exists('detail', $v) ? $v['detail'] : '';
                        //                       / print($fname);
                        if (strlen($fcode) > 0 && strlen($fname) > 0) {
                                $opt[$k] = $fcode . '-' . substr($fname, 0, $this->config->item('option_length'));
                        } else {
                                $opt[$k] = substr($fname, 0, $this->config->item('option_length'));
                        }
                        if (strlen($detail) > 0) {
                                $data_subtext[$k] = '<div class="data-subtext">' . $detail . '</div>';
                        }
                }

                $data['options'] = $opt;
                $data['selected'] = $selected;
                $data['table_name'] = isset($t['addonData'][$name0]['table_name']) ? $t['addonData'][$name0]['table_name'] : '';
                $data['validation_rules'] = isset($data['validation_rules']) ? $data['validation_rules'] : '';
                $data['show_refresh'] = true;
                $data['function_id'] = isset($t['addonData'][$name0]['functionID']) ? $t['addonData'][$name0]['functionID'] : '';

                $data = $this->get_validation_attr($name, $data, $t, $class);

                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }

                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr_emp($data, $class);

                $o .= form_dropdown($data);

                $o .= $this->get_info_attr($name0, $data, $class);
                $o .= $this->hd($name0, $selected);
                $o .= '</div></div>';

                if (count($data_subtext) > 0) {
                        //                  $o .= '<div class="row"><div class=col-lg-12>';
                        $o .= '<span id=' . $name0 . ' class="data-subtext">';
                        foreach ($data_subtext as $k => $v) {
                                $o .= '<span id=' . $k . ' class="hide">' . html_entity_decode($v) . '</span>';
                        }
                        $o .= '</span>';
                        //                  $o .= '</div></div>';
                }

                $o .= '</div>';
                return $o;
        }

        public function msb($name, $t = array(), $attr = array()) {

                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $class = 'multiselect form-control ';
                $swap_key = '';
                $data = array();

                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class .= $data[$k];
                                                break;
                                        case 'swap_key':
                                                $swap_key = $data[$k];
                                                break;
                                }
                        }
                }

                $data['id'] = $name0;
                $data['name'] = $name0;
                $data['class'] = $class;
                if (is_array($t) && count($t) > 0) {
                        if (array_key_exists($name, $t['addonData'])) {
                                $options = $t['addonData'][$name]['data'];
                                $selected = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else if (strlen($swap_key) > 0 && array_key_exists($swap_key, $t['addonData'])) {
                                $options = $t['addonData'][$swap_key]['data'];
                                $selected = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else {
                                $options = [];
                                $selected = '';
                        }
                } else {
                        $options = [];
                        $selected = '';
                }
                $opt = [];
                $data_subtext = [];
                foreach ($options as $k => $v) {
                        $fcode = array_key_exists('code', $v) ? $v['code'] : '';
                        $fname = array_key_exists('name', $v) ? $v['name'] : '';
                        if (strlen($fname) == 0)
                                $fname = array_key_exists('username', $v) ? $v['username'] : '';
                        $detail = array_key_exists('detail', $v) ? $v['detail'] : '';
                        //                       / print($fname);
                        if (strlen($fcode) > 0 && strlen($fname) > 0) {
                                $opt[$k] = $fcode . '-' . substr($fname, 0, $this->config->item('option_length'));
                        } else {
                                $opt[$k] = substr($fname, 0, $this->config->item('option_length'));
                        }
                        if (strlen($detail) > 0) {
                                $data_subtext[$k] = '<div class="data-subtext">' . $detail . '</div>';
                        }
                }
                $data['options'] = $opt;
                $data['selected'] = $selected;
                $data['table_name'] = isset($t['tableData']['table_name']) ? $t['tableData']['table_name'] : '';
                $data['validation_rules'] = isset($data['validation_rules']) ? $data['validation_rules'] : '';
                $data['multiple'] = 'multiple';


                $data = $this->get_validation_attr($name, $data, $t, $class);

                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }

                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr($data, $class);

                $o .= form_dropdown($data);

                $o .= $this->get_info_attr($name0, $data, $class);
                $o .= $this->hd($name0, $selected);
                $o .= '</div></div>';

                if (count($data_subtext) > 0) {
                        $o .= '<div class="row"><div class=col-lg-12>';
                        $o .= '<span id=' . $name0 . ' class="data-subtext">';
                        foreach ($data_subtext as $k => $v) {
                                $o .= '<span id=' . $k . ' class="hide">' . html_entity_decode($v) . '</span>';
                        }
                        $o .= '</span>';
                        $o .= '</div></div>';
                }

                $o .= '</div>';
                return $o;
        }

        public function tb($name, $t = array(), $attr = array()) {

                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                if (isset($t['edit_data'][$name])) {
                        if (!is_array($t['edit_data'][$name])) {
                                $vvv = $t['edit_data'][$name];
                        } else {
                                $vvv = implode(',', array_keys($t['edit_data'][$name]));
                        }
                } else {
                        $vvv = '';
                }

                $data = [
                    'name' => $name0,
                    'id' => $name0,
                    'value' => $vvv,
                    'class' => 'form-control',
                    'placeholder' => $this->get_placeholder($name),
                ];
                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }
                ///////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }
                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr($data, $class);
                $o .= form_input($data);
                $o .= $this->get_info_attr($name0, $data, $class);
//            $o .= $this->hd($name0, $vvv);
                $o .= '</div></div>';
                $o .= '</div>';

                return $o;
        }

        public function tb_emp($name, $t = array(), $attr = array()) {


                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                if (isset($t['edit_data'][$name])) {
                        if (!is_array($t['edit_data'][$name])) {
                                $vvv = $t['edit_data'][$name];
                        } else {
                                $vvv = implode(',', array_keys($t['edit_data'][$name]));
                        }
                } else {
                        $vvv = '';
                }

                $class = explode(' ', $attr['class']);

                foreach ($class as $k => $v) {

                        if ($v === 'no-placeholder') {
                                $data = [
                                    'name' => $name0,
                                    'id' => $name0,
                                    'value' => $vvv,
                                    'class' => 'form-control',
                                    'placeholder' => '',
                                ];
                        } else {
                                $data = [
                                    'name' => $name0,
                                    'id' => $name0,
                                    'value' => $vvv,
                                    'class' => 'form-control',
                                    'placeholder' => $this->get_placeholder($name),
                                ];
                        }
                }



                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }
                ///////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }
                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr_emp($data, $class);
                $o .= form_input($data);
                $o .= $this->get_info_attr($name0, $data, $class);
                //            $o .= $this->hd($name0, $vvv);
                $o .= '</div></div>';
                $o .= '</div>';

                return $o;
        }

        public function pb($name, $t = array(), $attr = array()) {


                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                if (isset($t['edit_data'][$name])) {
                        if (!is_array($t['edit_data'][$name])) {
                                $vvv = $t['edit_data'][$name];
                        } else {
                                $vvv = implode(',', array_keys($t['edit_data'][$name]));
                        }
                } else {
                        $vvv = '';
                }

                $data = [
                    'name' => $name0,
                    'id' => $name0,
                    'value' => $vvv,
                    'class' => 'form-control',
                    'type' => 'password',
                    'placeholder' => $this->get_placeholder($name),
                ];
                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }
                ///////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }
                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr($data, $class);
                $o .= form_input($data);
                $o .= $this->get_info_attr($name0, $data, $class);
//            $o .= $this->hd($name0, $vvv);
                $o .= '</div></div>';
                $o .= '</div>';

                return $o;
        }

        public function label($name, $t = array(), $attr = array()) {
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }
                if (isset($t['edit_data'][$name])) {
                        if (!is_array($t['edit_data'][$name])) {
                                $vvv = $t['edit_data'][$name];
                        } else {
                                $vvv = implode(',', array_keys($t['edit_data'][$name]));
                        }
                } else {
                        $vvv = '';
                }
                $data = [
                    'name' => $name0,
                    'id' => $name0,
                    'value' => $vvv,
                    'class' => 'form-control'
                ];
                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }
                $attributes = array(
                    'class' => 'form-control'
                );
                $o = '<div class=row><div class=col-md-12>';
                $o .= '<div class="form-group">';
                $o .= $this->get_label_attr($data, $class);
                $o .= '<br>' . $vvv . '';
                //$o .= form_label($vvv, $name0, $attributes);
                $o .= '</div>';
                $o .= '</div></div>';
                return $o;
        }

        public function dp($name, $t = array(), $attr = array()) {

                $selected_value = '';
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $data = array();
                $class = 'p-a-20 m-t-button';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('edit_data', $t) && array_key_exists($name, $t['edit_data'])) {
                        $selected_value = $t['edit_data'][$name];
                }


                $o = '';

                $data['name'] = $name;
                $data['class'] = ' form-control';
                $data['id'] = $name;
                if ($selected_value != '') {
                        $data['value'] = $selected_value;
                }
                $data['type'] = 'date';
                $class .= ' dp';
                ////////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                $o .= '<div class="form-group">';
                $o .= $this->get_label_attr($data, $class);
//                $o .= '<div class="input-group date" id="' . $name . '">';
                $o .= form_input($data);
                /* $o .= ' <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                  </span>'; */
//                $o .= '</div>';
                $o .= $this->get_info_attr($name0, $data, $class);
                //$o .= $this->hd($name0, $selected_value);
                $o .= '</div>';

                return $o;
        }

        public function dp_emp($name, $t = array(), $attr = array()) {

                $selected_value = '';
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $data = array();
                $class = 'p-a-20 m-t-button';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('edit_data', $t) && array_key_exists($name, $t['edit_data'])) {
                        $selected_value = $t['edit_data'][$name];
                }


                $o = '';

                $data['name'] = $name;
                $data['class'] = ' form-control';
                $data['id'] = $name;
                if ($selected_value != '') {
                        $data['value'] = $selected_value;
                }
                $data['type'] = 'date';
                $class .= ' dp';
                ////////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                $o .= '<div class="form-group">';
                $o .= $this->get_label_attr_emp($data, $class);
                //                $o .= '<div class="input-group date" id="' . $name . '">';
                $o .= form_input($data);
                /* $o .= ' <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                  </span>'; */
                //                $o .= '</div>';
                $o .= $this->get_info_attr($name0, $data, $class);
                //$o .= $this->hd($name0, $selected_value);
                $o .= '</div>';

                return $o;
        }

        public function dtp($name, $t = array(), $attr = array()) {

                $selected_value = '';
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $data = array();
                $class = 'p-a-20 m-t-button';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }                                
                                switch ($k) {
                                        case 'class':
                                                // if (trim($data[$k]) == 'form-control') {
                                                //         $data[$k] .= ' full-width';
                                                // }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('edit_data', $t) && array_key_exists($name, $t['edit_data'])) {
                        $selected_value = $t['edit_data'][$name];
                }


                $o = '';

                $data['name'] = $name;
                $data['class'] .= ' form-control';
                $data['id'] = $name;
                if ($selected_value != '') {
                        $data['value'] = $selected_value;
                }
                $data['type'] = 'datetimepicker';
                $class .= ' dp';
                ////////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                $o .= '<div class="form-group">';
                $o .= $this->get_label_attr($data, $class);
                $o .= '<div class="input-group date" id="' . $name . '">';
                $o .= form_input($data);
                $o .= ' <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                        </span>';
                $o .= '</div>';
                $o .= $this->get_info_attr($name0, $data, $class);
                //$o .= $this->hd($name0, $selected_value);
                $o .= '</div>';

                return $o;
        }

        public function tp($name, $t = array(), $attr = array()) {

                $selected_value = '';
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $data = array();
                $class = 'p-a-20 m-t-button';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }

                if (array_key_exists('edit_data', $t) && array_key_exists($name, $t['edit_data'])) {
                        $selected_value = $t['edit_data'][$name];
                }
                $o = '';

                $data['name'] = $name;
                $data['class'] = ' form-control';
                $data['id'] = $name;

                $o = '<div class=form-group>';
                $o .= '<section>';

                $o .= $this->get_label_attr($data, $class);
//      	$o .= '<div class="input-group timepicker" id="' . $name . '">';
//      	$o .= form_input($data);
//      	$o .= '<span class="input-group-addon">
//                        <span class="glyphicon glyphicon-time"></span>
//                    </span>
//      		</div>';
                $o .= ' <div class="input-group bootstrap-timepicker timepicker">
            			<input id="' . $name . '" type="text" class="timepicker form-control input-small" value = "' . $selected_value . '">
            			<span class="input-group-addon">
							<i class="glyphicon glyphicon-time"></i>
						</span>
        			</div>';

                if (strlen($selected_value) > 0) {
                        $o .= $this->hd($name0, $selected_value);
                }

                $class .= ' tp';
                $data = [];
                $data = $this->get_validation_attr($name, $data, $t, $class);

                $o .= $this->get_info_attr($name0, $data, $class);
                $o .= '</section>';
                $o .= '</div>';


                return $o;
        }

        public function fi($name, $t = [], $attr = array()) {

                if (strpos($name, '-') !== false) {
                        $name1 = explode('-', $name);
                        $name = $name1[1];
                        $table_name = $name1[0];
                        $name0 = str_replace('-', '_', $name);
                } else {
                        $name0 = $name;
                        $table_name = isset($t['tableData']['table_name']) ? $t['tableData']['table_name'] : '';
                }

                $data = [
                    'name' => $name0,
                    'id' => $name0,
                    'value' => isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '',
                    'maxlength' => '255',
                    'size' => '50',
                    'class' => 'form-control',
                    'placeholder' => 'type',
                ];

                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                } $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }

                $o = '<div class=row><div class=col-md-12>';
                $o .= '<div class="form-group">';
                if (strpos($class, 'no-label') == false) {
                        $o .= form_label($this->gl($data['name']), $data['name']);
                }

                if (strpos($class, 'image') !== false && strpos($class, 'pdf') == false && strpos($class, 'office') == false && strpos($class, 'zip') == false) {
                        $extension = 'jpg,jpeg,png,gif';
                        $o .= '<div class="fileinput fileinput-new" data-provides="fileinput">';
                        $o .= '<div id=' . $name . '-preview class="fileinput-preview thumbnail margin-bottom-10" data-trigger="fileinput" style="width: 150px; height: 200px">';
                        $selected_value = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';


//                        $upload_dir = $this->config->item('upload_dir');
//                        $upload_dir = isset($upload_dir[$t['tableData']['table_name']]) ? $upload_dir[$t['tableData']['table_name']] : '/upload';
                        $upload_dir = '/upload';
                        if (strlen($selected_value) > 0) {
                                if ($this->is_image('.' . $upload_dir . '/' . $selected_value)) {

                                        $o .= '<img src=' . $upload_dir . '/' . $selected_value . '>';
                                }
                        }

                        if(!empty($attr['img'])){
                            $o .= '<img src=' . $attr['img'] . '>';

                        }

                        $o .= '</div>';
                        $o .= '<div class=row>';
                        $o .= '<div class="col-md-12">';
                        $o .= '<span class="btn btn-default btn-file">';
                        $o .= '<span class="fileinput-new">' . $this->mcl->gl('select_image') . '</span>';
                        $o .= '<span class="fileinput-exists">' . $this->mcl->gl('change') . '</span>';
                        $o .= "<input class='image_input' type='file' name='" . $name . "[]' id='" . $name . "' onchange=fileSelected('" . $name . "') multiple='multiple'>";
                        $o .= "<output id='filesInfo'></output>";
                        $o .= '</span>';
                        $o .= ' <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">' . $this->mcl->gl('remove') . '</a>';
                        $o .= '</div>';
                        $o .= '</div>';

                        $o .= '</div>';
                        if (strpos($class, 'is_show_submit') !== false) {
                                $o .= '<input type="submit" value="Upload" class="btn btn-default" />';
                        }
                        $o .= '<div id="' . $name . '_fileinfo" class="hide">';
                        $o .= '<div id="' . $name . '_filename"></div>';
                        $o .= '<div id="' . $name . '_filesize"></div>';
                        $o .= '<div id="' . $name . '_filetype"></div>';
                        $o .= '<div id="' . $name . '_filedim"></div>';
                        $o .= '</div>';

                        $o .= '<img id="' . $name . '_preview" class="hide"/>';
                        $ve = [];
                        $o .= '<p class="help-block helper-text-box text-sm">';
                        $o .= $this->mcl->gl('allowed_extension') . ': ';
                        if (strpos($class, 'office')) {
                                $o .= $this->config->item('extension_office') . ',';
                                $e = explode(',', $this->config->item('extension_office'));
                                $ve = array_merge($ve, $e);
                        }
                        if (strpos($class, 'pdf')) {
                                $o .= $this->config->item('extension_pdf') . ',';
                                $e = explode(',', $this->config->item('extension_pdf'));
                                $ve = array_merge($ve, $e);
                        }
                        if (strpos($class, 'zip')) {
                                $o .= $this->config->item('extension_zip') . ',';
                                $e = explode(',', $this->config->item('extension_zip'));
                                $ve = array_merge($ve, $e);
                        }
                        if (strpos($class, 'image')) {
                                $o .= $this->config->item('extension_image') . ',';
                                $e = explode(',', $this->config->item('extension_image'));
                                $ve = array_merge($ve, $e);
                        }
                        $o .= $this->mcl->hd('valid_extension_' . $name, implode(',', $ve));
                        $o .= '</p>';
                } else {
                        if (isset($t['edit_data'][$name]) && array_key_exists('document_original_file', $t['edit_data'])) {
                                $o .= '<span class="align-right"><a href=downloads/downloads?file=' . $t['edit_data'][$name] . '&table_name=' . $table_name . '>' . $t['edit_data']['document_original_file'] . '</a></span>';
                        } else {
                                $o .= '<div class="fileinput fileinput-new input-group" data-provides="fileinput">
  <div class="form-control" data-trigger="fileinput"> <span class="fileinput-filename"></span></div>
  <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new"><i class="fa fa-file"></i></span><span class="fileinput-exists"></span>';
                                if (strlen($table_name) > 0)
                                        $o .= "<input class='' type='file' name='" . $table_name . '_' . $name0 . "' id='" . $table_name . '_' . $name0 . "'";
                                else
                                        $o .= "<input class='' type='file' name='" . $name0 . "' id='" . $name0 . "'";
                                if (is_array($t) && array_key_exists('schema_data', $t) && array_key_exists($name, $t['schema_data']['schema']) && strtolower($t['schema_data']['schema'][$name]['is_null']) == 'no') {
                                        $o .= ' required=required validation_rules=trim|required ';
                                } else if (strpos($class, 'required') !== false) {
                                        $o .= ' required=required validation_rules=trim|required ';
                                }

                                $o .= ' table_name="' . $table_name . '" onchange=fileSelected("' . $table_name . '_' . $name0 . '")>';
                                $o .= '</span>
  <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i></a>';
                                $o .= '</div>';

                                if (strpos($class, 'no-description') == false) {
                                        $o .= '<div class="helper-text-box">
<p> ';
                                }

                                if (is_array($t) && array_key_exists('schema_data', $t) && array_key_exists($name, $t['schema_data']['schema']) && strtolower($t['schema_data']['schema'][$name]['is_null']) == 'no') {
                                        $o .= '<span class=required><img width=12px src="' . base_url() . 'assets/images/asterisk.png"></span>';
                                } else if (strpos($class, 'required') !== false) {
                                        $o .= '<span class=required><img width=12px src="' . base_url() . 'assets/images/asterisk.png"></span>';
                                }

                                $ve = [];
                                $o .= '<p class="help-block helper-text-box">';
                                $o .= $this->mcl->gl('allowed_extension') . ': ';
                                if (strpos($class, 'office')) {
                                        $o .= $this->config->item('extension_office') . ',';
                                        $e = explode(',', $this->config->item('extension_office'));
                                        $ve = array_merge($ve, $e);
                                }
                                if (strpos($class, 'pdf')) {
                                        $o .= $this->config->item('extension_pdf') . ',';
                                        $e = explode(',', $this->config->item('extension_pdf'));
                                        $ve = array_merge($ve, $e);
                                }
                                if (strpos($class, 'zip')) {
                                        $o .= $this->config->item('extension_zip') . ',';
                                        $e = explode(',', $this->config->item('extension_zip'));
                                        $ve = array_merge($ve, $e);
                                }
                                if (strpos($class, 'image')) {
                                        $o .= $this->config->item('extension_image') . ',';
                                        $e = explode(',', $this->config->item('extension_image'));
                                        $ve = array_merge($ve, $e);
                                }

                                $o .= '</p>
</div>';
                                $o .= $this->mcl->hd('valid_extension_' . $name, implode(',', $ve));
                                $dir = $this->get_upload_dir($table_name);

                                if (isset($t['edit_data'][$name]) && strlen($t['edit_data'][$name]) > 0) {
                                        $dir .= '/';
                                        if ($this->is_image('.' . $dir . $t['edit_data'][$name])) {
                                                $o .= '<img class=thumbnail src=' . $dir . $t['edit_data'][$name] . '>';
                                        }
                                }
                        }
                }
                $o .= '</div></div></div>';

                return $o;
        }

        public function tan($name, $t = [], $attr = array()) {
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }
                $data = [
                    'name' => $name0,
                    'id' => $name0,
                    'value' => isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '',
                    'class' => 'form-control',
                    // 'placeholder' => $this->get_placeholder($name),
                    'rows' => 10,
                ];

                if (is_array($t) && count($t) > 0) {
                        if (array_key_exists($name, $t['addonData'])) {
                                $selected_value = isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '';
                        } else {
                                $selected_value = '';
                        }
                } else {
                        $selected_value = '';
                }

                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                        case 'rows':
                                                $data[$k] = $v;
                                                break;
                                }
                        }
                }

                ///////////////////////////////////////
                $data = $this->get_validation_attr($name, $data, $t, $class);
                if ($name == 'parentID') {
                        $data['class'] .= ' is_parent';
                }
                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr($data, $class);
                $o .= form_textarea($data);
                $o .= $this->get_info_attr($name0, $data, $class);

                $o .= '</div></div>';
                $o .= '</div>';

                return $o;
        }

        public function ta($name, $t = [], $attr = array()) {
                if (strpos($name, '-') !== false) {
                        $name0 = str_replace('-', '_', $name);
                        $name = explode('-', $name);
                        $name = $name[1];
                } else {
                        $name0 = $name;
                }

                $data = [
                    'name' => $name0,
                    'id' => $name0,
                    'value' => isset($t['edit_data'][$name]) ? $t['edit_data'][$name] : '',
                    'class' => 'form-control wysiwyg',
                    'placeholder' => $this->get_placeholder($name),
                    'rows' => 10,
                ];

                $class = '';
                if (count($attr) > 0) {
                        foreach ($attr as $k => $v) {
                                if (array_key_exists($k, $data) && strlen($data[$k]) > 0) {
                                        $data[$k] .= ' ' . $v;
                                } else {
                                        $data[$k] = $v;
                                }
                                switch ($k) {
                                        case 'class':
                                                if (trim($data[$k]) == 'form-control') {
                                                        $data[$k] .= ' full-width';
                                                }
                                                $class = $data[$k];
                                                break;
                                }
                        }
                }

                $data = $this->get_validation_attr($name, $data, $t, $class);

                $o = '<div class=row><div class=col-md-12>';
                $o .= '<div class="form-group">';
                $o .= $this->get_label_attr($data, $class);
                $o .= form_textarea($data);
                $class .= ' wysiwyg';
                $o .= $this->get_info_attr($name0, $data, $class);

                $o .= '</div>';
                $o .= '</div></div>';
                return $o;
        }

        public function gtd($a, $t, $attr = array()) {
                //table detail




                if (is_array($a) && array_key_exists('table_name', $a)) {
                        $table_name = $a['table_name'];

                        $tt = [];
                        $tt['schema_data'] = $this->schema_model->get_schema($table_name);
                        $indexColumn = $tt['schema_data']['indexColumn'];
                        $hidden = isset($a['hidden']) ? $a['hidden'] : [];
                        if (is_array($t) && array_key_exists('addonData', $t)) {
                                $tt['addonData'] = $t['addonData'];
                        } else {
                                $tt['addonData'] = [];
                        }

                        $controls = $a['controls'];
                        $columnData = [];
                        $data = [];
                        $data_hidden = [];
                        foreach ($controls as $k => $v) {

                                array_push($columnData, $k);
                                switch ($v) {
                                        case 'msb':
                                                if (array_key_exists('swap_controls', $a)) {
                                                        $swap_key = $a['swap_controls'][$k];
                                                } else {
                                                        $swap_key = '';
                                                }
                                                $attr['swap_key'] = $swap_key;
                                                $class = 'table-detail no-label no-description row0 msb';
                                                array_push($data, $this->sb($table_name . '-' . $k, $tt, array('swap_key' => $swap_key, 'class' => $class)));

                                                break;
                                        case 'sb':
                                                if (array_key_exists('swap_controls', $a)) {
                                                        $swap_key = $a['swap_controls'][$k];
                                                } else {
                                                        $swap_key = '';
                                                }
                                                $attr['swap_key'] = $swap_key;
                                                $class = 'form-control table-detail no-label no-description row0';
                                                array_push($data, $this->sb($table_name . '-' . $k, $tt, array('swap_key' => $swap_key, 'class' => $class)));

                                                break;
                                        case 'cb':
                                                if (array_key_exists('swap_controls', $a)) {
                                                        $swap_key = $a['swap_controls'][$k];
                                                } else {
                                                        $swap_key = '';
                                                }
                                                $attr['swap_key'] = $swap_key;
                                                $class = 'form-control table-detail no-label no-description row0';
                                                array_push($data, $this->cb($table_name . '-' . $k, $tt, array('swap_key' => $swap_key, 'class' => $class)));
                                                break;
                                        case 'rb':
                                                if (array_key_exists('swap_controls', $a)) {
                                                        $swap_key = $a['swap_controls'][$k];
                                                } else {
                                                        $swap_key = '';
                                                }
                                                $attr['swap_key'] = $swap_key;
                                                $class = 'form-control table-detail no-label no-description row0';
                                                array_push($data, $this->rb($table_name . '-' . $k, $tt, array('swap_key' => $swap_key, 'class' => $class)));
                                                break;
                                        case 'tb':
                                                $class = 'table-detail no-label no-description row0';
                                                array_push($data, $this->tb($table_name . '-' . $k, $tt, array('class' => $class)));
                                                break;
                                        case 'tb-click2call':
                                                $class = 'table-detail no-label no-description row0 click2call';
                                                array_push($data, $this->tb($table_name . '-' . $k, $tt, array('class' => $class)));
                                                break;

                                        case 'ta':
                                                $class = 'table-detail no-label no-description no-wysiwyg row0';
                                                array_push($data, $this->ta($table_name . '-' . $k, $tt, array('class' => $class)));
                                                break;
                                        case 'tan':
                                                $class = 'table-detail no-label no-description no-wysiwyg row0';
                                                array_push($data, $this->tan($table_name . '-' . $k, $tt, array('class' => $class)));
                                                break;
                                        case 'fi':
                                                $class = 'table-detail no-label no-description row0 office zip pdf';
                                                array_push($data, $this->fi($table_name . '-' . $k, $tt, array('class' => $class)));

                                                break;
                                        case 'dp':
                                                $class = 'table-detail no-label no-description row0';
                                                array_push($data, $this->dp($table_name . '-' . $k, $tt, array('class' => $class)));
                                                break;
                                }
                        }



                        $_headers = implode(',', $columnData);
                        if (strpos($class, 'always_disabled') !== false) {
                                $o = '<div class="table-wrapper"><header></header>';
                        } else {
                                $o = '<div class="table-wrapper"><header><a class="btn btn-default btn-sm btn-secondary row_add_detail" table-target="' . $table_name . '" column_name="' . $_headers . '"><i class="fa fa-plus"></i>&nbsp;Add</a></header>';
                        }

                        $o .= '<table id="' . $table_name . '" class="table table-striped table-bordered table-condensed" ';
                        if (is_array($hidden) && count($hidden) > 0) {
                                $suf = '';

                                foreach ($hidden as $k => $v) {
                                        $suf .= '_' . $v;
                                }

                                if (strlen($suf) > 0) {
                                        $o .= 'suffix=' . $suf;
                                }
                        }
                        $o .= '>';
                        $o .= '<thead>';
                        $o .= '<th scope="col" data-rt-column="#" class="align-center" style="width: 10%">#';
                        $o .= '</th>';
                        if (count($columnData) > 0)
                                $width = 90 / count($columnData);
                        else {
                                $width = 90;
                        }
                        $j = 0;
                        foreach ($columnData as $k => $v) {
                                $v = trim($v);
                                if (count($columnData) - 1 == $j) {
                                        $w = 90 - (count($columnData) - 1) * $width;
                                } else {
                                        $w = $width;
                                }
                                $w = (int) $w;
                                $j++;
                                $o .= '<th scope="col" data-rt-column="' . $this->gl($v) . '" class="align-center" style="width:' . $w . '%">' . $this->gl($v) . '</th>';
                        }
                        $o .= '</thead>';
                        $o .= '<tbody>';
                        $o .= '<tr id=row0 class="hide">';
                        $o .= '<td class="align-center" style="padding-left:2%;">';

                        if (strpos($class, 'always_disabled') == false) {
                                $o .= '<div class=row><div class=col-md-12><a class="row_detail_delete btn btn-danger btn-sm" column_name="' . $_headers . '"><i class="fa fa-trash-o"></i></a></div><div class=col-md-12>';
                                $o .= '<a class="row_detail_undelete btn btn-warning btn-sm hide" column_name="' . $_headers . '"><i class="fa fa-undo"></i></a>';
                                $o .= '</div><div class=col-md-12><a class="row_detail_up btn btn-default btn-secondary btn-sm"><i class="fa fa-arrow-up"></i></a>';
                                $o .= '</div><div class=col-md-12><a class="row_detail_down btn btn-default btn-secondary btn-sm"><i class="fa fa-arrow-down"></i></a>';
                                $o .= '</div></div>';
                        }
                        $o .= '</td>';

                        foreach ($data as $k => $v) {
                                $o .= '<td>' . $v . '</td>';
                        }
                        foreach ($data_hidden as $k => $v) {
                                $o .= $v;
                        }
                        $o .= '</tr>';

                        if (array_key_exists('subTableData', $t['edit_data']) && count($t['edit_data']['subTableData']) > 0) {
                                foreach ($t['edit_data']['subTableData'][$table_name] as $k => $v) {
                                        $ttt = [];
                                        $ttt['tableData'] = $t['tableData'];
                                        $ttt['schema_data'] = $tt['schema_data'];
                                        $ttt['addonData'] = $tt['addonData'];
                                        $ttt['edit_data'] = $v;
                                        // print $indexColumn;
                                        if (count($ttt['edit_data']) > 0 && array_key_exists($indexColumn, $ttt['edit_data'])) {
                                                $o .= '<tr id=' . $ttt['edit_data'][$indexColumn] . ' class="">';
                                                $o .= '<td class="align-center">';

                                                if (strpos($class, 'always_disabled') == false) {
                                                        $o .= '<div class=row><div class=col-md-12><a class="row_detail_delete btn btn-sm btn-danger" column_name="' . $_headers . '"><i class="fa fa-trash-o"></i></a></div><div class=col-md-12>';
                                                        $o .= '<a class="row_detail_undelete btn btn-sm btn-warning hide" column_name="' . $_headers . '"><i class="fa fa-undo"></i></a>';
                                                        $o .= '</div><div class=col-md-12><a class="row_detail_up btn btn-default btn-secondary btn-sm"><i class="fa fa-arrow-up"></i></a>';
                                                        $o .= '</div><div class=col-md-12><a class="row_detail_down btn btn-default btn-secondary btn-sm"><i class="fa fa-arrow-down"></i></a>';
                                                        $o .= form_hidden($table_name . '_sortOrder_' . $ttt['edit_data'][$indexColumn], $ttt['edit_data'][$indexColumn]);

                                                        $o .= '</div>';
                                                        $o .= form_hidden($table_name . '_' . $indexColumn . '_' . $ttt['edit_data'][$indexColumn], $ttt['edit_data'][$indexColumn]);
                                                        $o .= '</div>';
                                                }
                                                $o .= '</td>';

                                                foreach ($v as $kk => $vv) {
                                                        if (array_key_exists($kk, $controls)) {
                                                                $control = $controls[$kk];
                                                                switch ($control) {
                                                                        case 'msb':
                                                                                if (array_key_exists('swap_controls', $a)) {
                                                                                        $swap_key = $a['swap_controls'][$kk];
                                                                                } else {
                                                                                        $swap_key = '';
                                                                                }
                                                                                $class = 'table-detail no-label no-description msb';
                                                                                $o .= '<td>' . $this->sb($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('swap_key' => $swap_key, 'class' => $class)) . '</td>';
                                                                                break;
                                                                        case 'sb':
                                                                                if (array_key_exists('swap_controls', $a)) {
                                                                                        $swap_key = $a['swap_controls'][$kk];
                                                                                } else {
                                                                                        $swap_key = '';
                                                                                }
                                                                                $class = 'table-detail no-label no-description';
                                                                                $o .= '<td>' . $this->sb($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('swap_key' => $swap_key, 'class' => $class)) . '</td>';
                                                                                break;
                                                                        case 'tb':
                                                                                $class = 'table-detail no-label no-description';
                                                                                $o .= '<td>' . $this->tb($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => $class)) . '</td>';
                                                                                break;
                                                                        case 'tb-click2call':
                                                                                $class = 'table-detail no-label no-description click2call';
                                                                                $bt_class = 'bt_click_to_call';
                                                                                // $o .= '<td>' . $this->tb($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => $class)) . '</td>';
                                                                                $o .= '<td>';
                                                                                $o .= $this->bt('bt_' . $kk . '_' . $ttt['edit_data'][$indexColumn], $this->gl('click_to_call'), array('class' => $bt_class));
                                                                                $o .= $this->tb($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => $class));
                                                                                // $o .= $this->hd('hid_'.$kk.'_'.$ttt['edit_data'][$indexColumn],$ttt['edit_data'][$kk]);
                                                                                $o .= '</td>';
                                                                                break;
                                                                        case 'ta':
                                                                                $class = 'table-detail no-label no-wysiwyg no-description';
                                                                                $o .= '<td>' . $this->ta($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => $class)) . '</td>';
                                                                                break;
                                                                        case 'tan':
                                                                                $class = 'table-detail no-label no-wysiwyg no-description';
                                                                                $o .= '<td>' . $this->tan($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => $class)) . '</td>';
                                                                                break;
                                                                        case 'fi':
                                                                                $class = 'table-detail no-label office zip pdf no-description';
                                                                                $o .= '<td>';
                                                                                $o .= $this->fi($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => $class));
                                                                                $o .= '</td>';
                                                                                break;
                                                                        case 'dp':
                                                                                $class = 'table-detail no-label no-description';
                                                                                $o .= '<td>' . $this->dp($table_name . '-' . $kk . '-' . $ttt['edit_data'][$indexColumn], $ttt, array('class' => 'table-detail no-label no-description')) . '</td>';
                                                                                break;
                                                                }
                                                        }
                                                }
                                                $o .= '</tr>';
                                        }
                                }
                        }
                        $o .= '</tbody>';
                        $o .= '</table>';
                        $o .= '</div>';

                        return $o;
                } else {
                        return '';
                }
        }

        public function get_upload_dir($table) {
                $upload_dir = $this->config->item('upload_dir');

                if (array_key_exists($table, $upload_dir)) {
                        $upload_dir = $upload_dir[$table];
                } else {
                        $upload_dir = '/upload';
                }
                return $upload_dir;
                //                return array('upload_dir' => $upload_dir);
        }

        function sub_hdr($label, $attr = array()) {
                $class = '';
                if (array_key_exists('class', $attr))
                        $class = $attr['class'];
                $o = '';
                $o .= '<div class="sub_header ' . $class . '" align="left"><div style="font-size:20px">' . $this->mcl->gl($label) . ' <span class="sub_header_change"></span></div></div>';

                return $o;
        }

        public function hdr($label, $class = '') {
                $o = '';
                $o .= '<div class="sub_header ' . $class . '" align="center"><div style="font-size:20px">' . $this->mcl->gl($label) . ' <span class="sub_header_change"></span></div></div>';
                $o .= $this->mcl->hr();
                return $o;
        }

        public function hr($class = '') {
                $o = '<div class="spacer-10 ' . $class . '"></div><div class="hr-totop">
<span></span>
</div>';
                $o .= '<div class=spacer-20></div>';
                return $o;
        }

        public function gl($label) {
                if (strlen($label) > 0) {
                        if (chr($label[0]) < 127) {
                                $label = strtolower(str_replace(" ", "_", $label));
                                $lang = $this->lang->line($label);
                                $lang = str_replace('_', '', $lang);
                                $lang = trim($lang);
                                if (strlen($lang) == 0) {
                                        $lang = $label;
                                }

                                $o = $lang; //str_replace('_',' ',$lang);
                        } else {
                                $o = $label;
                        }
                        return $o;
                } else {
                        return '';
                }
        }

        public function bt($name, $value = '', $array = array()) {
                $class = '';
                $tab = '';
                if (count($array) > 0) {
                        foreach ($array as $k => $v) {
                                switch ($k) {
                                        case 'class':
                                                $class = $v;
                                                break;
                                        case 'tab':
                                                $tab = $v;
                                                break;
                                }
                        }
                }
                $name0 = str_replace('btn_', '', $name);
                $name0 = explode('-', $name0);
                $name0 = $name0[0];

                switch ($name0) {
                        case 'new':
                        case 'add_output':
                        case 'add_document':
                                $icon = '<i class="fa fa-plus-square"></i>';
                                break;
                        case 'edit':
                                $icon = '<i class="fa fa-pencil"></i>';
                                break;
                        case 'excel':
                                $icon = '<i class="fa fa-file-excel-o"></i>';
                                break;
                        case 'excel_all':
                                $icon = '<i class="fa fa-file-excel-o"></i><i class="fa fa-file-excel-o"></i>';
                                break;
                        case 'pdf':
                                $icon = '<i class="fa fa-file-pdf-o"></i>';
                                break;
                        case 'pdf_all':
                                $icon = '<i class="fa fa-file-pdf-o"></i><i class="fa fa-file-pdf-o"></i>';
                                break;
                        case 'save':
                        case 'save_all':
                        case 'save_ending':
                                $icon = '<i class="fa fa-save"></i>';
                                break;
                        case 'save_refresh':
                                $icon = '<i class="glyphicon glyphicon-floppy-saved"></i>';
                                break;
                        case 'cancel':
                                $label = 'back to list';
                                $icon = '<i class="fa fa-backward"></i>';
                                break;
                        case 'delete':
                                $icon = '<i class="fa fa-trash-o"></i>';
                                break;
                        case 'message_tb':
                                $icon = '<i class="fa fa-comment"></i>';
                                break;
                        case 'message_ta':
                                $icon = '<i class="fa fa-comments"></i>';
                                break;
                        case 'message_sb':
                                $icon = '<i class="fa fa-chevron-circle-down"></i>';
                                break;
                        case 'message_fi':
                                $icon = '<i class="fa fa-file"></i>';
                                break;
                        case 'message_delete':
                                $icon = '<i class="fa fa-trash-o"></i>';
                                break;
                        case 'next':
                                $icon = '<i class="fa fa-arrow-circle-right"></i>';
                                break;
                        case 'back':
                                $icon = '<i class="fa fa-arrow-circle-left"></i>';
                                break;
                        case 'finish':
                                $icon = '<i class="fa fa-flag"></i>';
                                break;
                        case 'close':
                        case 'close_output':
                        case 'close_upload':
                        case 'close_leave':
                                $icon = '<i class="fa fa-times"></i>';
                                break;
                        case 'submit':
                        case 'submit_document':
                        case 'submit_output':
                        case 'submit_leave':
                                $icon = '<i class="fa fa-share-square"></i>';
                                break;
                        default:
                                $icon = '<i class="fa fa-arrow-circle-right"></i>';
                                break;
                }
                if ($name0 != 'next') {
                        $data = [
                            'name' => 'btn_' . $name,
                            'id' => 'btn_' . $name,
                            'content' => $icon . ' <span class="hidden-xs">' . $this->mcl->gl($value) . '</span>',
                            'class' => 'btn btn-default ' . $class,
                            'title' => $this->mcl->gl($value),
                            'onClick' => "log_events('$name')",
                            'attached_tab' => $tab
                        ];
                } else {
                        $data = [
                            'name' => 'btn_' . $name,
                            'id' => 'btn_' . $name,
                            'content' => $icon . ' <span class="hidden-xs">' . $this->mcl->gl($value) . '</span>',
                            'class' => 'btn btn-default ' . $class,
                            'title' => $this->mcl->gl($value),
                            'onClick' => "log_events('$name')",
                            'attached_tab' => $tab,
                            'type' => 'submit'
                        ];
                }

                $o = '';

                if (count($array) > 0) {
                        foreach ($array as $k => $v) {
                                switch ($k) {
                                        case 'bid':
                                                $data[$k] = $v;
                                                break;
                                }
                        }
                }

                $o .= form_button($data);

                return $o;
        }

        public function btEmp($name, $value = '', $array = array()) {
                $class = '';
                $tab = '';
                if (count($array) > 0) {
                        foreach ($array as $k => $v) {
                                switch ($k) {
                                        case 'class':
                                                $class = $v;
                                                break;
                                        case 'tab':
                                                $tab = $v;
                                                break;
                                }
                        }
                }
                $name0 = str_replace('btn_', '', $name);
                $name0 = explode('-', $name0);
                $name0 = $name0[0];

                switch ($name0) {
                        case 'new':
                        case 'add_output':
                        case 'add_document':
                                $icon = '<i class="fa fa-plus-square"></i>';
                                break;
                        case 'edit':
                                $icon = '<i class="fa fa-pencil"></i>';
                                break;
                        case 'excel':
                                $icon = '<i class="fa fa-file-excel-o"></i>';
                                break;
                        case 'excel_all':
                                $icon = '<i class="fa fa-file-excel-o"></i><i class="fa fa-file-excel-o"></i>';
                                break;
                        case 'pdf':
                                $icon = '<i class="fa fa-file-pdf-o"></i>';
                                break;
                        case 'pdf_all':
                                $icon = '<i class="fa fa-file-pdf-o"></i><i class="fa fa-file-pdf-o"></i>';
                                break;
                        case 'save':
                        case 'save_all':
                        case 'save_ending':
                                $icon = '<i class="fa fa-save"></i>';
                                break;
                        case 'save_refresh':
                                $icon = '<i class="glyphicon glyphicon-floppy-saved"></i>';
                                break;
                        case 'cancel':
                                $label = 'back to list';
                                $icon = '<i class="fa fa-backward"></i>';
                                break;
                        case 'delete':
                                $icon = '<i class="fa fa-trash-o"></i>';
                                break;
                        case 'message_tb':
                                $icon = '<i class="fa fa-comment"></i>';
                                break;
                        case 'message_ta':
                                $icon = '<i class="fa fa-comments"></i>';
                                break;
                        case 'message_sb':
                                $icon = '<i class="fa fa-chevron-circle-down"></i>';
                                break;
                        case 'message_fi':
                                $icon = '<i class="fa fa-file"></i>';
                                break;
                        case 'message_delete':
                                $icon = '<i class="fa fa-trash-o"></i>';
                                break;
                        case 'next':
                                $icon = '<i class="fa fa-arrow-circle-right"></i>';
                                break;
                        case 'back':
                                $icon = '<i class="fa fa-arrow-circle-left"></i>';
                                break;
                        case 'finish':
                                $icon = '<i class="fa fa-flag"></i>';
                                break;
                        case 'close':
                        case 'close_output':
                        case 'close_upload':
                        case 'close_leave':
                                $icon = '<i class="fa fa-times"></i>';
                                break;
                        case 'submit':
                        case 'submit_document':
                        case 'submit_output':
                        case 'submit_leave':
                                $icon = '<i class="fa fa-share-square"></i>';
                                break;
                        default:
                                $icon = '<i class="fa fa-arrow-circle-right"></i>';
                                break;
                }

                $data = [
                    'name' => 'btn_' . $name,
                    'id' => 'btn_' . $name,
                    'content' => $icon . ' <span class="hidden-xs">' . $this->mcl->gl($value) . '</span>',
                    'class' => 'btn btn-default ' . $class,
                    'title' => $this->mcl->gl($value),
                    //'onClick' => "log_events('$name')",
                    'attached_tab' => $tab
                ];

                $o = '';

                if (count($array) > 0) {
                        foreach ($array as $k => $v) {
                                switch ($k) {
                                        case 'bid':
                                                $data[$k] = $v;
                                                break;
                                }
                        }
                }

                $o .= form_button($data);

                return $o;
        }

        public function get_list_view($uri) {
                if ($this->ion_auth->is_logged_in()) {
                        $tableData = $this->mdb->get_tableData($uri);

                        //vd::d($uri);die();
                        if (isset($tableData['tableData']['table_name']) && strlen($tableData['tableData']['table_name']) > 0) {
                                $table_name = $tableData['tableData']['table_name'];
                                $view_name = $tableData['tableData']['view_name'];
                                $indexColumn = $tableData['tableData']['indexColumn'];
                                $whereData = $tableData['tableData']['whereData'];
                                $saveTypeID = $tableData['tableData']['saveTypeID'];
                                $orderBy = $tableData['tableData']['orderBy'];
                                if (strlen($tableData['tableData']['columnData']) > 0) {
                                        if (strpos($tableData['tableData']['columnData'], ',') !== false) {
                                                $columnData = explode(',', $tableData['tableData']['columnData']);
                                        } else {
                                                $columnData = array($tableData['tableData']['columnData']);
                                        }
                                } else {
                                        $columnData = is_array($tableData['schema_data']['columns']) ? $tableData['schema_data']['columns'] : array();
                                }
                                $auth_data = $tableData['auth_data'];

                                $columns = array_diff($columnData, $this->config->item('ignore_columns'));
                                $columnData = implode(',', $columns);
                                $o = '';
                                $o .= '<section id="widget-grid" class="">';
                                $o .= '<div class="jarviswidget" id="wid-id-' . $table_name . '" data-widget-deletebutton="false">';

                                $o .= '<header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>';
                                $o .= '<h2>' . $this->mcl->gl($uri) . '</h2>';
                                $o .= '</header>';
                                $o .= '<div>';

                                $o .= '<div class="jarviswidget-editbox">';
                                $o .= '<input class="form-control" type="text">';
                                $o .= '<span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>';
                                $o .= '</div>';
                                $o .= '<div class="widget-body p-a-10">';


                                if (strlen($table_name) > 0) {
                                        $o .= '<table id="' . $table_name . '_main_list_view" class="table table-bordered table-striped table-hover table-condensed datatable" cellspacing="0" width="100%" table_name="' . $table_name . '" view_name="' . $view_name . '" indexColumn="' . $indexColumn . '" column_name="' . $columnData . '" whereData="' . $whereData . '" action_data="list" orderBy="' . $orderBy . '">';
                                        $o .= '<thead>';
                                        $o .= '<tr>';

                                        $cd = explode(',', $columnData);

                                        foreach ($cd as $k => $v) {
                                                if (strpos($v, '_id') == false || $v == $indexColumn) {
                                                        $o .= '<th>' . $this->mcl->gl($v) . '</th>';
                                                }
                                        }

                                        $o .= '</tr>';
                                        $o .= '<tr class="second">';

                                        $i = 0;

                                        foreach ($cd as $k => $v) {
                                                if (strpos($v, '_id') == false || $v == $indexColumn) {
                                                        $o .= '<td>';
                                                        $o .= '<label class="input">';
                                                        if (null !== ($this->session->userdata("sSearch_" . $i))) {
                                                                $vv = $this->session->userdata("sSearch_" . $i);
                                                                $o .= '<input type="text" name="search_' . $v . '" value="' . $vv . '" class="search_init">';
                                                        } else {
                                                                $o .= '<input type="text" name="search_' . $v . '" value="" class="search_init">';
                                                        }

                                                        $o .= '</label>';
                                                        $o .= '</td>';
                                                        $i++;
                                                }
                                        }

                                        $o .= '</tr></thead>';

                                        $o .= '</table>';

                                        $o .= form_hidden(['table_name' => $table_name, 'saveTypeID' => $saveTypeID, 'indexColumn' => $indexColumn, 'uri' => uri_string()]);
                                        $o .= form_hidden($auth_data);
                                } else {
                                        return $this->generate_404();
                                }
                                $o .= '</div>';
                                $o .= '</div>';
                                $o .= '</div>';
                                $o .= '</section>';
                                return $o;
                        } else {
                                return '';
                        }
                } else {
                        redirect('auth/login');
                }
        }

        public function div($id, $data = '', $class = '') {
                if (strlen($class) > 0) {
                        $o = '<div id="' . $id . '" name="' . $id . '" class="output-area col-lg-12 ' . $class . '">';
                } else {
                        $o = '<div id="' . $id . '" name="' . $id . '" class="output-area col-lg-12">';
                }
                $o .= $data;
                $o .= '</div>';
                return $o;
        }

        /**
         * 
         * @param array $data
         * @param array $url
         * @param string $class
         * @return string
         */
        function table($data = [], $url = [], $class = '') {
                //$data = array(), $url = array(), $class = ''

                $o = '';
                if (count($data) != 0) {
                        $index_column = array_key_exists('index_column', $url) ? $url['index_column'] : '';
                        $columns = array_keys($data[0]);
                        $o = '';
                        $o .= '<table class = "table table-bordered table-striped table-hover table-condensed ' . $class . '">';
                        $o .= '<thead>';

                        $o .= '<tr>';
                        if (strpos($class, 'runNumber') !== false) {
                                $o .= ' <th>' . $this->gl('no') . ' </ th>';
                        }
                        if (count($columns) > 0) {
                                foreach ($columns as $k => $v) {
                                        $o .= ' <th>' . $this->gl($v) . ' </ th>';
                                }
                        }
                        $o .= '</tr>';

                        if (strpos($class, 'no-filter') == 0) {
                                $o .= '<tr class="second" role="row">';
                                if (strpos($class, 'runNumber') !== false) {
                                        $o .= '<td><input class="form-control search_init" name="search_no" value="" type="text"></td>';
                                }
                                if (count($columns) > 0) {
                                        foreach ($columns as $k => $v) {
                                                $o .= '<td><input class="form-control search_init" name="search_' . $v . '" value="" type="text"></td>';
                                        }
                                }
                                $o .= '</tr>';
                        }

                        $o .= '</thead>';
                        $o .= '<tbody>';
                        $rn = 0;
                        foreach ($data as $k => $v) {$rn++;
                                $o .= '<tr>';
                                if (strpos($class, 'runNumber') !== false) {
                                        $o .= ' <td>' . $rn . ' </ td>';
                                }
                                $i = 0;
                                foreach ($columns as $kk => $vv) {
                                        if ($i == 0 & !empty($url)) {
//row_action row_action_edit btn btn-xs btn-default
                                                $o .= '<td>';
                                                for ($li = 0; $li < count($url['url']); $li ++) {
                                                        if (isset($url['attr_id'][$li]) and ! empty($url['attr_id'][$li])) {
                                                                $tkid = $url['attr_id'][$li];
                                                        } else {
                                                                $tkid = "";
                                                        }

                                                        if (strpos($url['url'][$li], 'http') !== false) {
                                                                if (strlen($index_column) > 0) {
                                                                        $o .= '<a id="' . $v[trim($vv)] . '" class="row_action row_action_edit btn btn-xs btn-default ' . $tkid . '" href="' . $url['url'][$li] . '/?' . $index_column . '=' . $v[trim($vv)] . '" target="_blank"><i class="fa fa-edit"></i></a>';
                                                                } else {
                                                                        $o .= '<a id="' . $v[trim($vv)] . '" class="row_action row_action_edit btn btn-xs btn-default ' . $tkid . '" href="' . $url['url'][$li] . '/' . $v[trim($vv)] . '"><i class="fa fa-edit"></i></a>';
                                                                }
                                                        } else {
                                                                $o .= '<a id="' . $v[trim($vv)] . '" class="row_action row_action_edit btn btn-xs btn-default ' . $tkid . '" href="#' . $url['url'][$li] . '/' . $v[trim($vv)] . '"><i class="fa fa-edit"></i></a>';
                                                        }
                                                }
                                                $o .= '</td>';
                                        } else {
                                                $o .= '<td>' . $v[trim($vv)] . ' </ td>';
                                        }
                                        $i++;
                                }
                                $o .= '</tr>';
                        }

                        $o .= '</tbody>';
                        $o .= '</table>';
                }
                return $o;
        }

        function widget($name, $content) {
                $o = '';
                $o .= '<section id="widget-grid" class="">';
                $o .= '<div class="jarviswidget" id="wid-id-' . $name . '" data-widget-deletebutton="false">';

                $o .= '<header>';
                $o .= '<span class="widget-icon"> <i class="fa fa-table"></i> </span>';
                $o .= '<h2>' . $this->mcl->gl($name) . '</h2>';
                $o .= '</header>';
                $o .= '<div>';

                $o .= '<div class="jarviswidget-editbox">';
                $o .= '<input class="form-control" type="text">';
                $o .= '<span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>';
                $o .= '</div>';
                $o .= '<div class="widget-body">';
                $o .= $content;
                $o .= '</div>';
                $o .= '</div>';
                $o .= '</div>';
                $o .= '</section>';
                return $o;
        }

        function accordion($id, $items) {
                $o = '';
                $o .= '<div id="' . $id . '" class="panel-group panel-group-joined">';
                foreach ($items as $k => $v) {
                        $o .= '<div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#' . $id . '" href="#' . $k . '" aria-expanded="false" class="collapsed">';
                        $o .= $k;
                        $o .= '</a>
                                            </h4>
                                        </div>
                                        <div id="' . $k . '" class="panel-collapse collapse">
                                            <div class="panel-body">';
                        $o .= $v;
                        $o .= '</div>
                                        </div>
                                    </div>';
                }
                $o .= '</div>';
                return $o;
        }

        function import($name = '', $class = '') {
//            $o = '';
//            $o .= '<div class=row><div class=col-md-12>';
//            $o .= '<div class="form-group">';
//            $o .= '<div class="dz-default dz-message">';
//            $o .= '    <br>';
//            $o .= '    <span class="dz-text">Select files to upload</span>';
//            $o .= '    <div class="" style="">';
//            $o .= '        <input type="file" id="files" name="' . $name . '" class="form-control">';
//            $o .= '    </div>';
//            $o .= '    <span class="dz-text">Please upload file .xls or .xlsx</span>';
//            $o .= '</div>     ';
//            $o .= '</div>';
//            $o .= '</div></div>';


                $data = [
                    'name' => $name,
                    'id' => $name,
                    'value' => '',
                    'class' => 'form-control',
                    'placeholder' => $this->get_placeholder($name),
                ];

                $o = '';
                $o .= '<div class="form-group">';
                if (strpos($class, 'col-') !== false) {
                        $o .= '<div class="row"><div class="' . $class . '">';
                } else {
                        $o .= '<div class="row"><div class="col-lg-12">';
                }
                $o .= $this->get_label_attr($data, $class);
                $o .= '<div class="dz-default dz-message">';
                $o .= '    <div class="" style="">';
                $o .= '        <input type="file" id="files" name="' . $name . '" class="form-control">';
                $o .= '    </div>';

                $o .= '</div>     ';


                $o .= '<div id="helper-' . $name . '" name="helper-' . $name . '" class="helper-text-box">';
                $o .= $this->get_required_attr($data, $class);
                $class = ' ' . $class;
                if (strpos($class, 'no-description') == false) {
                        $o .= '    <span class="dz-text">Please upload file .xls or .xlsx</span>';
                }
                $o .= '</div>';



                $o .= '</div></div>';
                $o .= '</div>';

                return $o;
        }

        function question_wizard($name, $data = array(), $cusid = '', $project_id = '') {
                $o = '';
                $o .= '<div id="' . $name . '" class="swMain">';
                //var_dump($data);
                foreach ($data as $v) {
                        // Start Head stepNumber
                        $stepNumber = count($v['questions']);
                        $cus_value = $this->question_mod->get_question_val($project_id, $cusid, $stepNumber);

                        //var_dump($cus_value);

                        $o .= '<ul>';
                        $p = 0;
                        $g = 0;
                        foreach ($cus_value['value_id'] as $vid) {
                                $g++;
                                $o .= '<li><a href="#step-' . $g . '" '
                                        . 'id="' . $v['questions'][$p]['code'] . '" '
                                        . 'question_id="' . $v['questions'][$p]['question_id'] . '"'
                                        . 'value_id="' . $vid . '">'
                                        . '<label class="stepNumber">' . $g . '</label></a></li>';
                                $p++;
                        }
                        $o .= '</ul>';
                        // End Head stepNumber
                        $j = 0;
                        $tb_case5 = array();
                        foreach ($v['questions'] as $kk => $vv) {
                                $j++;
                                $o .= '<div id="step-' . $j . '">';
                                $o .= '<h2 class="StepTitle">' . $vv['name'] . '</h2>';
                                $sortOrder1 = is_null($vv['sortOrder']) ? $vv['question_id'] : $vv['sortOrder'];
                                $sortOrder = str_pad($sortOrder1, 5, '0', STR_PAD_LEFT);
                                $o .= '<table id=' . $vv['question_id'] . ' class="table" order=' . $sortOrder . '>';
                                foreach ($vv['children'] as $kkk => $vvv) {
                                        $tbdata = array();
                                        $tbdata['addonData'][] = array();
                                        if (isset($cus_value['question'][$vv['question_id']])) {
                                                $tb_case5 = explode(',', $cus_value['question'][$vv['question_id']]);
                                                $tbdata['edit_data'][$vvv['code']] = $cus_value['question'][$vv['question_id']];
                                        }
                                        // Check customer values.
                                        if (isset($cus_value['question'][$vv['question_id']])) {
                                                if ($cus_value['question'][$vv['question_id']] == $vvv['value_id']) {
                                                        $checked = "checked";
                                                        $selected = "selected";
                                                } else {
                                                        $checked = "";
                                                        $selected = "";
                                                }
                                        } else {
                                                $checked = "";
                                                $selected = "";
                                        }

                                        $o .= '<tr id=' . $vvv['question_id'] . ' parentID="' . $vvv['parentID'] . '" order=' . $sortOrder . '>';
                                        $o .= '<td colspan=2 class="col-lg-10">';
                                        switch ($vvv['control_id']) {
                                                case 1: //detail
                                                        $o .= $this->mcl->tan($vvv['code'], $tbdata, array('class' => 'no-description no-label disabled'));
                                                        break;
                                                case 2: //detail
                                                        $name = $vvv['code'];
                                                        $val = $vvv['value_id'];
                                                        $label = $vvv['name'];

                                                        $o .= '<div class="checkbox">
                                              <input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="radio">
                                              <label for="' . $name . '"> ' . $label . ' </label>
                                              </div>';
                                                        break;
                                                case 3: //value_net
                                                        $gg = 2;
                                                        $name = $vvv['code'];
                                                        $val = $vvv['value_id'];
                                                        $label = $vvv['name'];
                                                        $o .= '<div class="radio">
                                              <input id="' . $name . '" name="' . $name . '" value="' . $val . '" type="radio" ' . $checked . '>
                                              <label for="' . $name . '"> ' . $label . ' </label>
                                              </div>';
                                                        break;
                                                case 4: //detail
                                                        $val = explode('|', $vvv['value_id']);
                                                        $label = explode('|', $vvv['option']);
                                                        if (count($val) == count($label)) {
                                                                $o .= '<select name="select1" id="select1" class="select2">';
                                                                foreach ($label as $k1 => $v1) {
                                                                        $o .= '<option value=' . $val[$k1] . '>' . $label[$k1] . '</option>';
                                                                }
                                                                $o .= '</select>';
                                                        }
                                                        break;
                                                case 5: //detail
                                                        $o .= $this->mcl->tb($vvv['name'], '', array('class' => 'no-description no-label disabled'));
                                                        break;
                                                case 6://ten star //value_net
                                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                                        for ($i = 1; $i <= 10; $i++) {
                                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                                        }
                                                        $o .= '</select>';
                                                        break;
                                                case 7://eleven star
                                                        $o .= '<select class=rating name=' . $vvv['name'] . '>';
                                                        for ($i = 0; $i <= 10; $i++) {
                                                                $o .= '<option value=' . $i . '>' . $i . '</option>';
                                                        }
                                                        $o .= '</select>';
                                                        break;
                                        }

                                        $o .= '</td></tr>';
                                }
                                $o .= '</table>';
                                $o .= '</div>';
                        }
                }
                $o .= '</div>';

                return $o;
        }

        public function is_image($path) {
                if (file_exists($path)) {
                        $a = getimagesize($path);
                        $image_type = $a[2];

                        if (in_array($image_type, [IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_GIF])) {
                                return true;
                        }
                        return false;
                } else {
                        return false;
                }
        }

        function pg($chart_id, $title, $x, $x_short = '', $chart_type, $series) {
                $o = '<div id="' . $chart_id . '" class="highchart" series-column="' . $series . '" x-axis="' . $x . '" x-axis-short="' . $x_short . '" title="' . $title . '" chart_type="' . $chart_type . '">';
                $o .= '</div>';
                return $o;
        }

}
