<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Import_lib extends CI_Model {

    function get_success_table() {
        
    }
    
    function get_result_import($cnt_success,$cnt_error) {
        $total = $cnt_success + $cnt_error;
        $output = '';
        $output .= "
					<div class='col-lg-4'>
					        <div class='panel'>
					            <div class='panel-body text-center bg-primary' style='padding: 75px 20px;'>
					                <i class='fa fa-smile-o fa-5x'></i>
					            </div>
					            <div class='pad-all text-center'>
					                <p style='font-size: 20px; color:#000'>Total</p>
					                <p style='padding: 22px 0px; font-size: 45px; color:#000'>" . $total . "</p>					                
					            </div>
					        </div>
					</div>";
        $output .= "
						<div class='col-lg-4'>
					        <div class='panel'>
					            <div class='panel-body text-center bg-success' style='padding: 75px 20px;'>
					                <i class='fa fa-check fa-5x'></i>
					            </div>
					            <div class='pad-all text-center'>
					                <p style='font-size: 20px; color:#000'>Success</p>
					                <p style='padding: 22px 0px; font-size: 45px; color:#000'>" . $cnt_success . "</p>
					            </div>
					        </div>
					    </div>";
        $output .= "
						<div class='col-lg-4'>
					        <div class='panel'>
					            <div class='panel-body text-center bg-danger' style='padding: 75px 20px;'>
					                <i class='fa fa-ban fa-5x'></i>
					            </div>
					            <div class='pad-all text-center'>
					                <p style='font-size: 20px; color:#000'>Error</p>
					                <p style='padding: 22px 0px; font-size: 45px; color:#000'>" . $cnt_error . "</p>					                
					            </div>
					        </div>
					    </div> <hr>";
        
        return $output;
    }
    
    function get_error_table($source_id){
        $o = '';
        $this->db->list_fields('error_log_customer');
        $this->db->select('message,sales_group,mgmt_no,units_location,cus_name,tel,mobile,technician_tel,work_start,start_time,end_time,technician_name,remark')
                ->from('error_log_customer')
                ->where('source_id',$source_id);
        $query = $this->db->get();
        $o .= '<div id="table-error">';
        $o .= '<table name="datatable" id="datatable" class="datatable_local table table-striped table-bordered dataTable no-footer" style="overflow: scroll;">';
        $o .= '<thead>';
            $o .= '<tr>';
            $o .= '<th>#</th>';
                foreach ($query->list_fields() as $field){
                   $o .= '<th>'.strtoupper($field).'</th>';
                }
            $o .= '</tr>';
        $o .= '</thead>';
        $i = 0;
        foreach($query->result_array() as $val){ $i++;
           $o .= '<tr>'; 
           $o .= '<td>'.$i.'</td>';
           $o .= '<td>'.$val['message'].'</td>'; 
           $o .= '<td>'.$val['sales_group'].'</td>'; 
           $o .= '<td>'.$val['mgmt_no'].'</td>'; 
           $o .= '<td>'.$val['units_location'].'</td>'; 
           $o .= '<td>'.$val['cus_name'].'</td>'; 
           $o .= '<td>'.$val['tel'].'</td>'; 
           $o .= '<td>'.$val['mobile'].'</td>'; 
           $o .= '<td>'.$val['technician_tel'].'</td>'; 
           $o .= '<td>'.$val['work_start'].'</td>'; 
           $o .= '<td>'.$val['start_time'].'</td>'; 
           $o .= '<td>'.$val['end_time'].'</td>'; 
           $o .= '<td>'.$val['technician_name'].'</td>'; 
           $o .= '<td>'.$val['remark'].'</td>';
           $o .= '</tr>'; 
        }
        $o .= '</tbody>';        
        $o .= '</table>';
        $o .= '</div>';
        
        return $o;
    }
}
