<?php

defined('BASEPATH') or die('Acceso Restringido');

class Tree_Menu_Inner extends CI_Model {

        var $_CI;
        var $_forest;
        var $_settings;
        var $_parent;
        var $output;
        var $nodes;
        var $arr = array();

        function __construct($setting) {

                $this->load->library('tree/tree_node');

                //$this->load->database();

                $this->_forest = array();
//                $this->_settings = array();
                $this->_settings = $setting;

                $this->config->load('tree_menu', TRUE, TRUE);
//                $config = $this->config->item('tree_menu');
                $this->initialize($setting);
        }

        function initialize($config = array()) {

                if (empty($config)) {
                        $config['pk'] = $_GET['indexColumn'];
                        $config['fk'] = $_GET['parent_column'];
                        $code = substr($_GET['indexColumn'], 0, strlen($_GET['indexColumn']) - 3) . '_code';
                        $config['code_column'] = $code;
//                        $config['text_column_eng'] = $_GET['text_column_eng'];
                        $config['text_column'] = $_GET['text_column'];
                        $config['table'] = $_GET['view_name'];
                        $config['menu_list_class'] = 'menu_esp';
                        if (isset($_GET['orderBy_column']))
                                $config['orderBy_column'] = $_GET['orderBy_column'];
                        if (isset($_GET['is_checkbox_treegrid'])) {
                                $config['is_checkbox_treegrid'] = $_GET['is_checkbox_treegrid'];
                        } else {
                                $config['is_checkbox_treegrid'] = 'F';
                        }
                        if (isset($_GET['where_column'])) {
                                $config['where_column'] = $_GET['where_column'];
                                $config['where_value'] = $_GET['where_value'];
                        }
                        $config['level'] = 'level';
                }

                $this->_settings = $config;
        }

        function _get_id(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['pk'], $vars))
                        return $vars[$this->_settings['pk']];

                return 0;
        }

        function _get_text(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['text_column'], $vars)) {
                        $text = $vars[$this->_settings['text_column']];
                        return htmlspecialchars($text);
                }
                return '';
        }

//        function _get_text_eng(&$tree) {
//                $stdClassObject = $tree->root();
//
//                $vars = get_object_vars($stdClassObject);
//
//                if (array_key_exists($this->_settings['text_column_eng'], $vars)) {
//                        $text = $vars[$this->_settings['text_column_eng']];
//                        return htmlspecialchars($text);
//                }
//                return '';
//        }

        function _get_name(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['column_name'], $vars)) {
                        $text = $vars[$this->_settings['column_name']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_code(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['column_code'], $vars)) {
                        $text = $vars[$this->_settings['column_code']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_parent(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['parent'], $vars)) {
                        $text = $vars[$this->_settings['parent']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_level(&$tree) {
                $stdClassObject = $tree->root();

                $vars = get_object_vars($stdClassObject);

                if (array_key_exists($this->_settings['level'], $vars)) {
                        $text = $vars[$this->_settings['level']];
                        return htmlspecialchars($text);
                }
                return '';
        }

        function _get_roots() {

                $this->db->from($this->_settings['table']);
                $this->db->where($this->_settings['fk'] . ' IS NULL');
                $this->db->where_or($this->_settings['fk'] . '-1');
                $this->db->where('display', '1');
                $this->db->orderBy($this->_settings['text_column'], "asc");
//            $this->db->group_by($this->_settings['pk']);
                $query = $this->db->get();
                return $query->result();
        }

        function _get_children($id) {
                foreach ($this->nodes as $node) {
                        if ($node->parent == $id) {
                                $forest[] = $node;
                        }
                }
                return $forest;
        }

        function _has_children($id) {
                $found = false;
                foreach ($this->nodes as $node) {
                        if ($node->parent == $id) {
                                $found = true;
                                break;
                        }
                }

                return $found;
        }

        function _build_tree(&$tree) {
                $id = $this->_get_id($tree);

                if ($this->_has_children($id)) {
                        $children = $this->_get_children($id);

                        foreach ($children as $child) {
                                $child_tree = Tree::factory($child);

                                $this->_build_tree($child_tree);

                                $tree->insert($child_tree);
                        }
                }
        }

        function _print_sublist(&$tree) {
                $id = $this->_get_id($tree);
                if (isset($this->_settings['is_checkbox_treegrid']) && $this->_settings['is_checkbox_treegrid'] == 'T') {
                        $text = "<input type=checkbox name=" . $this->_settings['pk'] . "-" . $id . " id=" . $this->_settings['pk'] . "-" . $id . " style=margin-top:-6px>&nbsp;" . $this->_get_text($tree);
                } else {
                        $text = $this->_get_text($tree);
                }
                $level = $this->_get_level($tree);

                //$link = strtolower($this->_get_text_eng($tree));
                //$link = str_replace(' ', '_', $link);
                $link = anchor($id, $text);

                if (!$tree->has_children()) {
                        //print "<li>\n<div class=\'file\'>". $link. "</div>\n</li>\n";
                        $this->output .= "<li><span class=file level=" . $level . " the_id=" . $id . " is_leaf=true>" . $link . "</span>";
                } else {

                        $this->_parent = strtolower($text);
                        //print "<li>\n<span class=\'folder\'>".$text."</span>\n";
                        $this->output .= "<li><span class=folder level=" . $level . " the_id=" . $id . " is_leaf=false>" . $link . "</span>";

                        //print "<ul>\n";
                        $this->output .= "<ul>";
                        $children = & $tree->children();
                        foreach ($children as $child) {
                                $this->_print_sublist($child);
                        }
                        //print "</ul>\n";
                        $this->output .= "</ul>";
                }
        }



        function &build() {
                $this->nodes = $this->_get_all();

                foreach ($this->nodes as $node) {

                        if ($node->parent == 0 || $node->parent == -1) {
                                $tree = Tree::factory($node);
                                $this->_build_tree($tree);
                                $this->_forest[] = $tree;
                        }
                }
          
                return $this->_forest;
        }

        function &build_custom() {
                $this->nodes = $this->_get_all();
                foreach ($this->nodes as $node) {

                        if ($node->parent == 0 || $node->parent == -1) {
                                $tree = Tree::factory($node);
                                $this->_build_tree($tree);
                                $this->_forest[] = $tree;
                        }
                }
                return $this->_forest;
        }

        function print_menu() {

                $this->output = "";
                $this->output .= '<ul id="exampletreeview" class="filetree">';
                foreach ($this->_forest as $tree) {                
                        $this->_print_sublist($tree);
                }
                $this->output .='</ul>';
                return $this->output;
        }

        function _print_tree_sublist(&$tree) {
                $id = $this->_get_id($tree);
                $level = $this->_get_level($tree);
                $name = $this->_get_name($tree);
                $code = $this->_get_code($tree);
                $parent = $this->_get_parent($tree);
                if (!$tree->has_children()) {
//                        $a = array($this->_settings['pk']=>$id, $this->_settings['column_name'])=>$name, 'level'=>$level, 'is_leaf'=>'F');
                        $a = array($this->_settings['pk'] => $id, $this->_settings['column_code'] => $code, $this->_settings['column_name'] => $name, 'level' => $level, 'is_leaf' => 'T', $this->_settings['parent'] => $parent);
                        array_push($this->arr, $a);
                } else {

                        $a = array($this->_settings['pk'] => $id, $this->_settings['column_code'] => $code, $this->_settings['column_name'] => $name, 'level' => $level, 'is_leaf' => 'F', $this->_settings['parent'] => $parent);
                        array_push($this->arr, $a);
                        $children = & $tree->children();
                        foreach ($children as $child) {
                                $this->_print_tree_sublist($child);
                        }
                }
        }

        function print_tree() {
                $this->arr = array();

                foreach ($this->_forest as $tree) {
                        $this->_print_tree_sublist($tree);
                }

                return $this->arr;
        }

        function _get_all() {

//                $this->db->select($this->_settings['pk'], $this->_settings['code_column'], $this->_settings['fk'], $this->_settings['text_column'], $this->_settings['text_column_eng'], $this->_settings['level']);
                $this->db->select('*')->from($this->_settings['table'])->where('display', '1');

                if (isset($this->_settings['where_column']) && !is_null($this->_settings['where_column'])) {
//                        $this->db->where($this->_settings['where_column'], $this->_settings['where_value']);
                        if (strpos($this->_settings['where_column'], ',') > 0) {
                                $w = explode(",", $this->_settings['where_column']);
                                $v = explode(",", $this->_settings['where_value']);
                                for ($i = 0; $i < count($w); $i++) {
                                        $this->db->where(trim($w[$i]), trim($v[$i]));
                                }
                        } else {
                                $this->db->where($this->_settings['where_column'], $this->_settings['where_value']);
                        }
                }
                if (isset($this->_settings['orderBy_column']) && !is_null($this->_settings['orderBy_column']))
                        $this->db->orderBy($this->_settings['orderBy_column'], "asc");
                else
                        $this->db->orderBy($this->_settings['pk'], "asc");

//                $this->db->orderBy($this->_settings['text_column'], "asc");

                $query = $this->db->get();

                return $query->result();
        }

}

?>