<?php

if (!defined('BASEPATH'))
      exit('No direct script access allowed');

class Date_lib {

      //////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////

      function days_diff($date_1, $date_2, $differenceFormat = '%a') {
            $datetime1 = date_create($date_1);
            $datetime2 = date_create($date_2);

            $interval = date_diff($datetime1, $datetime2);

            return $interval->format($differenceFormat);
      }

      function weeks_in_month($year, $month) {
//                var_dump($year);
            $weeks = array();
            // Start of month
            $start = mktime(0, 0, 0, $month, 1, $year);
            // End of month
            $end = mktime(0, 0, 0, $month, date('t', $start), $year);
            // Start week
            $start_week = date('W', $start);
            // End week
            $end_week = date('W', $end);

            for ($i = $start; $i <= $end; $i += 6 * 24 * 60 * 60) {
                  $week = date('W', $i);
                  if ($week == 52 || $week == 53) {
                        $y = $year - 1;
                  } else {
                        $y = $year;
                  }
                  $weeks[$week] = $this->getStartAndEndDate($week, $y);
            }
//               
//                if ($end_week < $start_week) { // Month wraps
//                        return ((52 + $end_week) - $start_week) + 1;
//                }
            return $weeks;
//                return array('start_week' => $start_week, 'end_week' => $end_week, 'start_date');
      }

      function nbweeks_of_month($month, $year) {
            $nb_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $first_day = date('w', mktime(0, 0, 0, $month, 1, $year));

            if ($first_day > 1 && $first_day < 6) {
                  // month started on Tuesday-Friday, no chance of having 5 weeks
                  return 4;
            } else if ($nb_days == 31)
                  return 5;
            else if ($nb_days == 30)
                  return ($first_day == 0 || $first_day == 1) ? 5 : 4;
            else if ($nb_days == 29)
                  return $first_day == 1 ? 5 : 4;
      }

      function getStartAndEndDate($week, $year) {
            $dto = new DateTime();
            $dto->setISODate($year, $week);
            $ret['start'] = $dto->format('Y-m-d');
            $dto->modify('+6 days');
            $ret['end'] = $dto->format('Y-m-d');
            return $ret;
      }

      function days_in_month($month, $year) {
// calculate number of days in a month 
            return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
      }

      function get_current_datetime() {
            date_default_timezone_set('Asia/Bangkok');
            return date('Y-m-d H:i:s');
      }

      function convert_to_thai_date($date, $f = 1) {
            switch ($f) {
                  case 2:
                        if ($date !== null && strpos($date, '-') !== false) {
                              $a = explode('-', $date);
                              $y = (int) $a[0] + 543;
                              $m = $a[1];
                              $d = $a[2];
                              return $d . '/' . $m . '/' . $y;
                        } else {
                              return '';
                        }
                  default:// Full month array 
                        $f_m = array("01" => "มกราคม",
                                                                    "02" => "กุมภาพันธ์",
                                                                    "03" => "มีนาคม",
                                                                    "04" => "เมษายน",
                                                                    "05" => "พฤษภาคม",
                                                                    "06" => "มิถุนายน",
                                                                    "07" => "กรกฎาคม",
                                                                    "08" => "สิงหาคม",
                                                                    "09" => "กันยายน",
                                                                    "10" => "ตุลาคม",
                                                                    "11" => "พฤศจิกายน",
                                                                    "12" => "ธันวาคม"
                        );
// Quick month array 
                        $q_m = array("01" => "ม.ค.",
                                                                    "02" => "ก.พ.",
                                                                    "03" => "มี.ค.",
                                                                    "04" => "เม.ย.",
                                                                    "05" => "พ.ค.",
                                                                    "06" => "มิ.ย.",
                                                                    "07" => "ก.ค.",
                                                                    "08" => "ส.ค.",
                                                                    "09" => "ก.ย.",
                                                                    "10" => "ต.ค.",
                                                                    "11" => "พ.ย.",
                                                                    "12" => "ธ.ค."
                        );
                        if (strpos($date, ' ') !== false) {
                              $e = explode(' ', $date);
                              $date = $e[0];
                              $time = $e[1];
                        } else {
                              $time = '';
                        }
                        if (strlen($date) > 0 && $date !== '0000-00-00') {
                              if ((int) $f == 1) {
                                    $b = explode('-', $date);
                                    $y = (int) $b[0] + 543;
                                    $a = str_pad($b[2], 2, '0', STR_PAD_LEFT) . ' ' .
                                            $q_m[str_pad($b[1], 2, '0', STR_PAD_LEFT)] . ' ' . $y;
                                    if (strlen($time) > 0)
                                          $a .= ' - ' . $time . ' น.';
                              }
                              if ((int) $f == 22) {
                                    $a = (int) substr($date, 8) . ' ' .$f_m[substr($date, 5, -3)] . ' ' . ((int) substr($date, 0, -6) + 543);
                                            if ($time!=='') {
                                                $a .= ' - ' . $time;
                                            }else {
                                                $a .= '';
                                            }
                                    
                              }
                              if ((int) $f == 23) {
                                  $a = $f_m[substr($date, 5, -3)] . ' ' . ((int) substr($date, 0, -6) + 543);
                                  if ($time!=='') {
                                      $a .= ' - ' . $time;
                                  }else {
                                      $a .= '';
                                  }
                                  
                              }

                              if ((int) $f == '3') {
                                    $a = 'วันที่ ' . (int) substr($date, 8) . ' เดือน ' .
                                            $f_m[substr($date, 5, -3)] . ' พ.ศ. ' . ((int) substr($date, 0, -6) + 543);
                                    $a .= ' เวลา ' . $time;
                              }
                        } else {
                              $a = '';
                        }

                        return $a;
            }
      }

      function get_date_duration($start_date, $end_date) {
            $datetime1 = new DateTime($start_date);
            $datetime2 = new DateTime($end_date);
            $interval = $datetime1->diff($datetime2);
//                echo $interval->format('%R%a days');
            $years = $interval->format('%y');
            $months = $interval->format('%m');
            $days = $interval->format('%d');
            return $years . ' ปี ' . $months . ' เดือน ' . $days . ' วัน';
      }

      function date_convert_to_db($date, $formate = "Y-m-d") {
            $cv_date = strtotime($date);
            return date($formate, $cv_date);
      }

}
