<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tree_smart {

        function process_sub_nav($nav_item) {
                $sub_item_htm = "";
                if (isset($nav_item["sub"]) && $nav_item["sub"]) {
                        $sub_nav_item = $nav_item["sub"];
                        $sub_item_htm = $this->process_sub_nav($sub_nav_item);
                } else {
                        $sub_item_htm .= '<ul>';
                        foreach ($nav_item as $key => $sub_item) {

                                if (isset($sub_item['url']) && strlen($sub_item['url']) == 0) {
                                        $url = '#';
                                } else {
                                        if ($sub_item["url"] !== '#')
                                                $url = $sub_item["url"];
                                        else
                                                $url = '#';
//                                        if (isset($sub_item['url'])) {
//                                                $url = "/" . $sub_item["url"];
//                                        } else {
//                                                $url = '#';
//                                        }
                                }

                                $url_target = isset($sub_item["url_target"]) ? 'target="' . $sub_item["url_target"] . '"' : "";
                                $icon = isset($sub_item["icon"]) ? '<i class="fa fa-lg fa-fw ' . $sub_item["icon"] . '"></i>' : "";
                                $code = isset($sub_item["code"]) ? $sub_item["code"] : "";
                                $nav_title = isset($sub_item["name"]) ? $sub_item["name"] : "";
                             
                                $label_htm = isset($sub_item["label_htm"]) ? $sub_item["label_htm"] : "";
                                if (isset($sub_item["sub"])) {
                                        $sub_item_htm .=
                                                '<li ' . (isset($sub_item["active"]) ? 'class = "active"' : '') . '>
											<a href="' . $url . '" ' . $url_target . '>' . $icon . ' ' . $code.'-'.$nav_title . $label_htm . '</a>' . (isset($sub_item["sub"]) ? $this->process_sub_nav($sub_item["sub"]) : '') . '
										</li>';
                                } else {
                                        $sub_item_htm .= '<li ' . (isset($sub_item["active"]) ? 'class = "active"' : '') . '>
											<a href="' . $url . '" ' . $url_target . '>' . $icon . ' ' . $code.'-'.$nav_title . $label_htm . '</a>' . (isset($sub_item["sub"]) ? $this->process_sub_nav($sub_item["sub"]) : '') . '
										</li>';
                                }
                        }
                        $sub_item_htm .= '</ul>';
                }
                return $sub_item_htm;
        }

}
