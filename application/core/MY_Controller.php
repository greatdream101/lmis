<?php

if (!defined('BASEPATH'))
      exit('No direct script access allowed');

/**
 * Name:  MY_Controller
 * 
 * Author:  Ben Edmunds
 * Created:  7.21.2009 
 * 
 * Description:  Class to extend the CodeIgniter Controller Class.  All controllers should extend this class.
 * 
 */
class MY_Controller extends CI_Controller {

      protected $data = Array();
      protected $controller_name;
      protected $action_name;
      protected $previous_controller_name;
      protected $previous_action_name;
      protected $save_previous_url = false;
      protected $page_title;
      protected $configs = array();
      protected $t = array();
      var $i = 0;

      public function __construct() {
            parent::__construct();

            $this->mdb->set_translation();
            $this->mcl->load_lang();
//                $this->load->library(array('session', 'ion_auth'));
            //save the previous controller and action name from session
            $this->previous_controller_name = $this->session->flashdata('previous_controller_name');
            $this->previous_action_name = $this->session->flashdata('previous_action_name');

            //set the current controller and action name

            if (!empty($_COOKIE['uri'])) {
                  if (strpos($_COOKIE['uri'], '/') !== false) {
                        $a = explode('/', $_COOKIE['uri']);
                        $this->controller_name = $a[0];
                  } else {
                        $this->controller_name = $_COOKIE['uri']; //$this->router->fetch_directory() . $this->router->fetch_class();
                  }
            }

            $this->action_name = $this->router->fetch_method();

            if ($this->ion_auth->is_logged_in()) {
                  $this->data['web'] = $this->get_web();
                  $this->data['title'] = $this->data['web']['title'];
                  $this->data['web']['function_name'] = $this->data['title'];
                  $this->data['content'] = '';
                  $this->data['css'] = '';
                  $e = $this->uri->segment_array();
//                  vd::d($_SERVER);die();
                  
                  if (count($e) > 1) {
                        if ($e[2] == 'edit') {
                              $id = $e[3];
                        } else {
                              $id = 0;
                        }
                  } else {
                        $id = 0;
                  }
                  
                  if ($this->controller_name !== 'Auth') {
                        $this->t = $this->mdb->get_tableData($this->controller_name);
                  }
            } else {
                  $this->data['content'] = '';
                  $this->data['title'] = '';
                  $this->data['web'] = array('function_tree' => array(), 'function_name' => '');
            }
      }

      function get_web() {
            return $this->mdb->get_website_data();
      }

      public function index() {
            print $this->mcl->get_list_view($_POST['uri']);
      }

      function edit($id) {

            $url = explode('/', uri_string());
            $url = $url[0];
            $e = explode('_', $url);
            $e0 = $e[0];
            $e1 = '';

            for ($i = 1; $i < count($e); $i++) {
                  $e1 .= $e[$i] . '_';
            }

            $e1 = substr($e1, 0, strlen($e1) - 1);

            $this->t = $this->mdb->get_tableData($url, 'edit', $id);
            $o = $this->load->view($e0 . '/' . $e1, array('t' => $this->t), true);
            print $o;
      }

      function undelete_data() {
            return $this->mdb->undelete_data();
      }

      protected function render($template = 'minton') {
            //save the controller and action names in session
            if ($this->ion_auth->is_logged_in()) {
                  if ($this->save_previous_url) {
                        $this->session->set_flashdata('previous_controller_name', $this->previous_controller_name);
                        $this->session->set_flashdata('previous_action_name', $this->previous_action_name);
                  } else {
                        $this->session->set_flashdata('previous_controller_name', $this->controller_name);
                        $this->session->set_flashdata('previous_action_name', $this->action_name);
                  }

                  $view_path = $this->controller_name . '/' . $this->action_name . '.php'; //set the path off the view
                  if (file_exists(APPPATH . 'views/' . $view_path)) {
                        $this->data['content'] .= $this->load->view($view_path, $this->data, true);  //load the view
                  }
                  $this->data['content'] .= $this->mcl->hd('base_url', base_url());
//vd::d($this->data);
                  $this->load->view("layouts/$template.tpl.php", $this->data);  //load the template
            } else {
                  redirect('auth/logout');
            }
      }

      protected function add_title() {
            $this->load->model('page_model');

            //the default page title will be whats set in the controller
            //but if there is an entry in the db override the controller's title with the title from the db
            $page_title = $this->page_model->get_title($this->controller_name, $this->action_name);
            if ($page_title) {
                  $this->data['title'] = $page_title;
            }
      }

      protected function save_url() {
            $this->save_previous_url = true;
      }

      function save_data() {
            $uri = $this->uri->segment_array();
            $uri = $uri[1];

            $tableData = $this->mdb->get_tableData($uri);
            print $this->mdb->save_data($tableData['schema_data'], $tableData['tableData']);
      }

      function delete_data() {
            $tableData = $this->mdb->get_tableData($_POST['uri']);
//                var_dump($tableData);
            $_POST['table_name'] = $tableData['tableData']['table_name'];
            $_POST['indexColumn'] = $tableData['tableData']['indexColumn'];
            print $this->mdb->delete_data();
      }

      function load_view($path, $a = array()) {
            $o = $this->load->view($path, $a, true);

            if (isset($a['t']['auth_data'])) {
                  $o .= form_hidden($a['t']['auth_data']);
            }
            print $o;
      }

      function add($id) {
            $this->edit($id);
      }
}
