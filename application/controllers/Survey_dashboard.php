<?php

class Survey_dashboard extends MY_Controller {
        public $clients = 0;
        public function __construct() {
                parent::__construct();
                $this->load->model('addon/question_mod');
                $this->load->model('addon/dashboard_mod');
                $this->clients = $this->dashboard_mod->get_clientsByUser();
        }

        public function index() {
                $today = date('Y-m-d');
                $uptodate = $this->date_lib->convert_to_thai_date($today);
                $status = $this->dashboard_mod->count_status($this->clients); 
                $this->load->view('survey/dashboard', array(
                        't' => $this->t,
                        'toDay'=>$uptodate,
                        'ctStatus'=>$status
                ));
        }

        function get_dashboard() {
                if (isset($_POST['series_id'])) {
                        $series_id = $_POST['series_id'];
                        $a = $this->dashboard_mod->get_data($series_id);
                        print $this->load->view('survey/sd', array('a' => $a), true);
                }
        }

        function get_surveyData(){
                $data = $this->dashboard_mod->get_surveyData($this->clients);
                $table = $this->tb_surveyer();
                $output = array('data'=>$data,'table'=>$table);
                print json_encode($output);
        }

        function tb_surveyer(){
                $regionID = $_POST['regionID'];
                $branchID = $_POST['branchID'];
                $seriesID = $_POST['seriesID'];
                $survey_list = $this->dashboard_mod->get_survey_list($this->clients);
                if(!empty($survey_list))
                {
                        $url = array( 'url' => array('survey_conduct/pull_questions'),
                                'attr_id'=>array('surveyID'),
                                'index_column'=>'id',
                        );
                        $table = $this->mcl->table($survey_list, $url , '');
                }
                else
                {
                     $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
                }
                return $table;
        }

}
