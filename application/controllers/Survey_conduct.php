<?php

class Survey_conduct extends MY_Controller {

         public function __construct() {
                parent::__construct();
                $this->load->model('addon/question_mod');
                $this->load->library('addon/Scripts_lib');
        }

        public function index() {
             $survey_list = $this->question_mod->get_survey_list($this->session->userdata('userID'));
             if(!empty($survey_list))
             {
                  $url = array( 'url' => array('survey_conduct/pull_questions'),
                       'attr_id'=>array('surveyID'),
                       'index_column'=>'id',
                  );
                  $table = $this->mcl->table($survey_list, $url , 'datatable_local');
             }
             else
             {
                  $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
             }

             $data['question_info'] = "";
             // $this->t['edit_data']['branchID'] = 0;
             $this->t['edit_data']['seriesID'] = 0;

             $o = $this->load->view('survey/surveyList', array('t' => $this->t,'table'=>$table,'data'=>$data), true);
             print $o;
        }

        /*
        function get_questions() {
                if( (isset($_POST['branchID']))&&(isset($_POST['seriesID'])) )
                {
                        $branchID = $_POST['branchID'];
                        $seriesID = $_POST['seriesID'];
                        $a = $this->question_mod->get_data($seriesID);

                        $userID = $this->session->userdata('userID');
                        $answer_value = $this->question_mod->get_answer_value_jump($userID, $branchID, $seriesID);

                        print $this->load->view('survey/cs', array(
                             'a' => $a,
                             // 't'=>$this->t,
                             't_value' => $answer_value,
                             'branchID' => $branchID,
                             'seriesID' => $seriesID,
                             'surveyerID' => $userID,
                        ), true);
                }
        }
        */
        function pull_questions($surveyID)
        {
               $survey_info = $this->question_mod->get_survey_info($surveyID);

               // $branchID = $survey_info['branchID'];
               $seriesID = $survey_info['seriesID'];
               $a = $this->question_mod->get_data($seriesID);

               $userID = $this->session->userdata('userID');
               $answer_value = $this->question_mod->get_answer_value_jump($surveyID);

               $data['question_info'] = $this->load->view('survey/cs', array(
                    'a' => $a,
                    't_value' => $answer_value,
                    // 'branchID' => $branchID,
                    'seriesID' => $seriesID,
                    'surveyerID' => $userID,
               ), true);

               $data['scriptsGreeting'] = $this->scripts_lib->getScriptsGreeting($surveyID);
               // print $data['scriptsGreeting'];exit;
               $data['scriptsEnding'] = $this->scripts_lib->getScriptsEnding($surveyID);
               // print $data['scriptsEnding'];exit;

               $o = $this->load->view('survey/conductSurvey', array('t' => $this->t,'data'=>$data), true);
               print $o;
        }

        function frm_answer() {
                $ans = $this->mdb->get_addon_data('outbound_frm_answer');
                $this->t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                $this->t['addon_data']['jump'] = $ans['question_id'];
                $this->t['addon_data']['jump']['data'][99999] = array('question_id' => '99999', 'name' => $this->mcl->gl('end_survey'));
//                vd::d($ans['question_id']);
                print $this->load->view('survey/frm_answer', array('t' => $this->t), true);
        }

        function frm_question() {
                $series_id = $_POST['series_id'];
                $project_id = $_POST['project_id'];
                $question_type_id = $_POST['question_type_id'];
                $phase_id = $_POST['phase_id'];
                $t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                $t['addon_data']['section_id']['data'] = $this->question_mod->get_section($series_id, $project_id, $question_type_id, $phase_id);

                print $this->load->view('survey/frm_question', array('t' => $t), true);
        }

        function add_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_question();
                }
        }

        function add_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_answer();
                }
        }

        function delete_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_answer();
                }
        }

        function delete_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_question();
                }
        }

        function save_position() {
                if (isset($_POST['q'])) {
                        $this->question_mod->save_position();
                }
        }

        function get_series()
        {
                if (isset($_POST['branchID']))
                {
                     $this->load->model('addon/series_mod');
                     $o = $this->series_mod->get_branch_series($_POST['branchID']);
                     print json_encode($o);
                }
        }

        // Create by Ja
        function get_last_answer($userID, $branchID, $seriesID)
        {
             $last_answer = $this->question_mod->get_last_answer($userID, $branchID, $seriesID);
             // var_dump($last_answer);
             return $last_answer;
        }

        function get_question_info($questionID)
        {
              $question_info = $this->question_mod->get_question_info($questionID);
              // var_dump($question_info);
             return $question_info;
        }

        function get_next_question($parentID, $value)
        {
              $question_info = $this->question_mod->get_next_question($parentID, $value);
              // var_dump($question_info);
             return $question_info;
        }

        function get_current_question()
        {
             if( (isset($_POST['branchID']))&&(isset($_POST['seriesID'])) )
             {
                    $userID = $this->session->userdata('userID');
                    $branchID = $_POST['branchID'];
                    $seriesID = $_POST['seriesID'];

                    $last_answer = $this->get_last_answer($userID, $branchID, $seriesID);
                    if($last_answer > 0)
                    {
                         $question_info = $this->get_question_info($last_answer['questionID']);
                         if($question_info > 0)
                         {
                              $next_question = $this->get_next_question($question_info['questionID'], $last_answer['valueNet']);
                              if($next_question['jumpTo'] > 0)
                              {
                                   // return Current Question
                                   // print $next_question['jumpTo'];
                                   $return_result['currentQuestion'] = $next_question['jumpTo'];
                                   $return_result['hidJumpFrom'] = $last_answer['questionID'];
                                   // echo json_encode($return_result);
                              }
                              else
                              {
                                   // print $question_info['questionID'];
                                   // Edit by Ja - 2018/06/20
                                   // $return_result['currentQuestion'] = -1;
                                   // $return_result['hidJumpFrom'] = -1;
                                   $return_result['currentQuestion'] = $question_info['questionID'];
                                   $return_result['hidJumpFrom'] = $last_answer['questionID'];
                                   // echo json_encode($return_result);
                              }
                         }
                    }
                    else
                    {
                         // print $last_answer;
                         $return_result['currentQuestion'] = 0;
                         $return_result['hidJumpFrom'] = 0;
                    }

                    echo json_encode($return_result);
             }
             else {
                  // code...
             }
        }

        function save_answer()
        {
               if( (isset($_POST['branchID']))&&(isset($_POST['seriesID'])) )
               {
                    $userID = $this->session->userdata('userID');
                    $surveyID = $_POST['surveyID'];
                    $branchID = $_POST['branchID'];
                    $seriesID = $_POST['seriesID'];

                    $questionID = $_POST['questionID'];
                    $valueNet = $_POST['valueNet'];

                    $data = array(
                         'branchID' => $branchID,
                         'seriesID' => $seriesID,
                         'questionID' => $questionID,
                         'valueNet' => $valueNet,
                    );

                    $a = $this->question_mod->save_answer($userID, $surveyID, $branchID, $seriesID, $questionID, $data);
                    // echo $a;
                    // return($a);

                    echo json_encode($a);
               }
        }

        function update_qc_status()
        {
             $userID = $this->session->userdata('userID');
             $surveyID = $_POST['surveyID'];
             $is_update = 0;
             if(isset($_POST['qcStatusID']))
             {
                  $update_data = array(
                       'qcStatusID' => $_POST['qcStatusID'],
                       'updatedID' => $userID,
                       'updatedDate' => date("Y-m-d H:i:s"),
                  );

                  $is_update = $this->question_mod->update_qc_status($surveyID, $update_data);
             }

             print $is_update;
        }

        function get_answer_value($userID, $seriesID)
        {
             $answer_value = $this->question_mod->get_answer_value($userID, $seriesID);
             // var_dump($answer_value);
             return $answer_value;
        }

        function upload_attachments() {
                if (!empty($_FILES)){
                $computer_name = $this->security_lib->get_computerName();
                $valueid = $_POST["valueid"];
                $controlName = $_POST["controlName"];
                $dataDocument['documentID'] = '';
                foreach ($_FILES as $key => $val){
                    if (!empty($val['name'])){
                        $documentID = '';
                        if ($key === 'avatar')          {$documentID = 1; $dataDocument['documentID'] .= ",".$documentID; }
                        if ($key === 'copyOfIDCard')    {$documentID = 2; $dataDocument['documentID'] .= ",".$documentID;}
                        if ($key === 'copyOfHouse')     {$documentID = 3; $dataDocument['documentID'] .= ",".$documentID;}
                        if ($key === 'copyOftranscript'){$documentID = 4; $dataDocument['documentID'] .= ",".$documentID;}
                        if ($key === 'copyOfMilitary')  {$documentID = 5; $dataDocument['documentID'] .= ",".$documentID;}
                        if ($key === 'copyOfMarriage')  {$documentID = 6; $dataDocument['documentID'] .= ",".$documentID;}
                        if ($key === 'copyOfChangeName'){$documentID = 7; $dataDocument['documentID'] .= ",".$documentID;}
                        if ($key === 'copyOfBookBank')  {$documentID = 8; $dataDocument['documentID'] .= ",".$documentID;}
                        $originalName = $val['name'];
                        $extensionFile = pathinfo($originalName, PATHINFO_EXTENSION);

                        //////////////////////////     Gen new name      //////////////////////////

                        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $charactersLength = strlen($characters);

                        $randomString = '';
                        for ($i = 0; $i < 10; $i++) {
                            $randomString .= $characters[rand(0, $charactersLength - 1)];
                        }

                        $code = strtoupper($randomString);
                        $newName = $code.'.'.$extensionFile;

                        $upload = $_SERVER["DOCUMENT_ROOT"].'/msp/upload';
                        // vd::d($_POST);
                        // exit();
                        if (!file_exists($upload."/".$valueid)) {
                            mkdir($upload."/".$valueid, DIR_READ_WRITE_MODE, true);
                        }

                        $filePath = "/upload/".$valueid;

                        $fn = $val['tmp_name'];
                        $size = getimagesize($fn);
                        $ratio = $size[0]/$size[1]; // width/height
                        if( $ratio > 1) {
                            $width = 500;
                            $height = 500/$ratio;
                        }
                        else {
                            $width = 500*$ratio;
                            $height = 500;
                        }

                        $src = imagecreatefromstring(file_get_contents($fn));
                        $dst = imagecreatetruecolor($width,$height);
                        imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
                        imagedestroy($src);
                        imagejpeg($dst,$fn); // adjust format as needed
                        imagedestroy($dst);

                            if(move_uploaded_file($val["tmp_name"], "./".$filePath."/".$newName)){
                                $dataFile["valueID"] = $valueid;
                                $dataFile["controlName"] = $controlName;
                                $dataFile["documentID"] = $documentID;
                                $dataFile["originalName"] = $originalName;
                                $dataFile["code"] = $code;
                                $dataFile["extensionFile"] = $extensionFile;
                                $dataFile["filePath"] = $filePath;
                                //$dataFile["url"] = "http://".$_SERVER["SERVER_NAME"]."/hrm".$filePath;
                                $dataFile["active"] = 1;
                                $dataFile['createdID'] = !empty($user_id) ? $user_id : NULL;
                                $dataFile['computerName'] = $computer_name;
                                // if($valueid > 0 && $documentID != ''){
                                //      $old_file = $this->mdb->get_data('t_attachments',array('personalID'=>$valueid;,'documentID'=>$documentID),
                                //      'concat(code,".",extensionFile) as fileName,filePath');
                                //      if(!empty($old_file)){
                                //          if(file_exists($upload."/".$$valueid.'/'.$old_file[0]['fileName'])){
                                //              unlink($upload."/".$$valueid.'/'.$old_file[0]['fileName']);
                                //          }
                                //      }
                                //      $this->db->where('personalID',$$valueid);
                                //      $this->db->where('documentID',$documentID);
                                //      $this->db->delete('t_attachments');
                                //  }
                                $this->question_mod->delete_attachments($valueid,$controlName);
                                $image_id = $this->question_mod->save_attachments($dataFile);
                                //redirect('pages/index#survey_conduct');
                            }

                    }
                }
                // $docID = substr($dataDocument['documentID'],1);
                // $strDocumentID = $this->Employment_model->check_documentID($docID,$personal_id);
                // $dataDocument['documentID'] = $strDocumentID;
                // $image_id = $this->Employment_model->update_personal($dataDocument,$personal_id);
            }
        }

        function preview_answer()
        {
             $this->load->library('addon/Preview_answer_lib');
             $data = $this->question_mod->get_preview_answer($_POST["surveyID"]);

             $tablePreviewAnswer = $this->preview_answer_lib->tablePreviewAnswer($data,$footer='save');

             print $tablePreviewAnswer;
        }
}
