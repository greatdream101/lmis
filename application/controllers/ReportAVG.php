<?php
class ReportAVG extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('addon/Customer_model');
		$this->load->model('addon/Question_mod');
		$this->load->library('PHPExcel/Classes/PHPExcel');
		$this->load->library('Export_excel');
		$this->load->library('date_lib');
	}
	
	function index() {
		$data = array();
		$url = array();
		$table = $this->mcl->table($data, $url, 'datatable_local');
		$this->load_view('report/avgscoremouth', array('table' => $table,'t' => $this->t));
	}
	
	function exportAvgScoreAll(){
	    $seriesID = 1;
	    $dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:'';
	    $tempdateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:date('Y-m-d');
	    $dateEnd = !empty($tempdateEnd)?$tempdateEnd:date('Y-m-d');
	    
	    $date = $this->date_lib->convert_to_thai_date($dateEnd,23);
	    
	    $arr = [];
	    $arr = $this->Question_mod->get_all_questions($seriesID);
	    $arrQuestion = [];
	    foreach ($arr as $lst){
	        $arrQuestion[$lst['questionID']] = $lst['code'];
	    }
	    
	    $haystack = [];
	    $dataTable = [];
	    $arr = $this->Question_mod->get_value($seriesID);
	    foreach ($arr['values'] as $key => $lst){
	        $id = explode('_', $key);
	        $QID = $id[3];//_questionID
	        $idKeyTable = $id[0].'_'.$id[2];//seriesID_cusID
	        if (!in_array($idKeyTable, $haystack)) {
	            $dataTable[$idKeyTable][$QID] = $lst;
	        }else {
	            $haystack = $idKeyTable;
	        }
	    }
	    
	    $select = 'id, sms_status, effectiveDate,effectiveMonth, createdDate, flagID
                    , seriesID, region, division, divisionName, insuredName, agentName, agentCD, agencyName, agencyCD';
	    $arr = $this->Customer_model->getReportAVG($select,$dateStart, $dateEnd);
	    
	    $arrExcel = [];
	    $dataAgency = [];
	    $hayAgency = [];
	    foreach ($arr as $lst){
	        $keyVal = $lst['seriesID'].'_'.$lst['id'];
	        foreach ($arrQuestion as $idkey => $lsts){
	            $value = isset($dataTable[$keyVal][$idkey])?$dataTable[$keyVal][$idkey]:'';
	            $lst[$lsts] = $value;
	        }
	        $arrExcel[] = $lst;
	        if (!in_array($lst['agencyCD'], $hayAgency)) {
	            $dataAgency[] = [
	                'region' => $lst['region']
	                ,'division' => $lst['division']
	                ,'agencyCD' => $lst['agencyCD']
	                ,'agencyName' => $lst['agencyName']
	            ];
	            $hayAgency[] = $lst['agencyCD'];
	        }
	    }
	    
	    var_dump('Start=> '.date('Y-M-d H:i'));
	    if (!empty($arrExcel)) {
           foreach ($dataAgency as $agency){
               $this->export_excel->excel_AvgALL($arrExcel, $agency, $date);
           }
        }
        var_dump('End=> '.date('Y-M-d H:i'));
        exit();
	    //check data by excel
        /* $params = array('rep_title' => 'raw', 'rep_name' => 'rawdata.xlsx', 'rep_template' => 'rawdata.xlsx','data_array'=>$arrExcel);
	    $this->export_excel->excel_raw($params); */
	}
   
}