<?php

class ReportInternal extends MY_Controller {

    public function __construct() {
        parent::__construct();
        // $this->load->library('addon/Report_lib');

        // $this->load->model('addon/Customer_model');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

        $this->load->model('addon/Question_mod');
        $this->load->model('addon/Report_model');
		$this->load->library('PHPExcel/Classes/PHPExcel');
        // $this->load->library('Export_excel');
        $this->load->library('Excel_report');
		$this->load->library('date_lib');
        
    }

    public function index() {
        $this->t['edit_data']['dateStart'] = date('Y-m-d');
        // $this->t['edit_data']['dateEnd'] = date('Y-m-d');
        $rep_template = 'internal_report.xlsx';

        $this->load->view('report/report_internal', array(
            't' => $this->t,
            'rep_template' => $rep_template
        ));
    }

    public function genInternalReport() {    
        $seriesID = 1;
	    $dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:'';
        // $dateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:'';
        $rep_template = isset($_POST['rep_template'])?$_POST['rep_template']:'';

        $arr = [];
        $arr = $this->Question_mod->get_all_questions($seriesID);
	    $arrQuestion = [];
	    foreach ($arr as $lst){
            // $arrQuestion[$lst['questionID']] = $lst['code'];
            $arrQuestion[$lst['questionID']] = str_replace('_','-',$lst['code']);
        }

        // vd::d($arrQuestion);exit;

        $arrValue = $this->Report_model->getInternalReport($dateStart);
        $arrDivSMS = $this->Report_model->getDivSummarySMS($dateStart);
        $arrRegSMS = $this->Report_model->getRegSummarySMS($dateStart);
        $arrDivision = $this->Report_model->getAllDivision();
        $arrRegion = $this->Report_model->getAllRegion();

        $arrAppendix = $this->Report_model->getScoreByAL($dateStart);

        // var_dump($arrValue);exit();
        // var_dump($arrDivSMS);exit();
        // var_dump($arrRegSMS);exit();
        // var_dump($arrDivision);exit();
        // var_dump($arrRegion);exit();
        // vd::d($arrAppendix);exit();

        $arrReport = array(
            'rep_template' => $rep_template,
            'dateStart' => $dateStart,
            'arrValue' => $arrValue,
            'arrQuestion' => $arrQuestion,
            'arrDivision' => $arrDivision,
            'arrRegion' => $arrRegion,
            'arrDivSMS' => $arrDivSMS,
            'arrRegSMS' => $arrRegSMS,
            'arrAppendix' => $arrAppendix
        );

        // $this->excel_report->genInternalReport($dateStart,$arrValue,$arrQuestion,$arrDivision,$arrRegion,$arrDivSMS,$arrRegSMS,$arrAppendix);
        $this->excel_report->genInternalReport($arrReport);
    }
}
