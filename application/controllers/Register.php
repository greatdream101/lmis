<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {

        function __construct() {
                parent::__construct();
                $this->load->model('addon/Customer_model');
                $this->load->model('addon/Question_mod');
                $this->load->library('addon/Scripts_lib');
        }

        //redirect if needed, otherwise display the user list
        function index() {
            // $this->load->view('welcome_message');
            // $data['scriptsTerminate'] = $this->scripts_lib->getScriptsNotFound();
            print $this->load->view('register/activatecode', array('t' => $this->t), true);
        }

        function vinnumber() {
            // $this->load->view('welcome_message');
            // $data['scriptsTerminate'] = $this->scripts_lib->getScriptsNotFound();
            print $this->load->view('register/activevinnumber', array('t' => $this->t), true);
        }

        function register() {

                // $data    = $this->mdb->get_data('t_customer', '', 'VIN1');

                $this->t['addonData'] = $this->mdb->get_addonData('Register');

                print $this->load->view('register/register',  array('t'=>$this->t), true);
        }

        function home(){

                $this->register();
        }

        function save_data() {

                $itemID  = '';
                $newCode = "";

                while(empty($newCode)){

                    $code    = 'MZ';
                    $code   .= $this->getToken(6);

                    $checkCode = $this->db->SELECT('code')->FROM('t_customer_register')->WHERE('code', $code)->get()->result_array();

                    if(empty($checkCode)){
                        $newCode = $code;
                    }else{
                        $newCode = "";
                    }
                }

                // vd::d($newCode);

                $items = $this->db->SELECT('*')->FROM('items')->get()->result_array();

                foreach($items as $key => $val){
                    $itemID .= isset($_POST['itemID_'.$val['itemID']])?$_POST['itemID_'.$val['itemID']].',':'';
                }

                $data = array(
                        'fname' => isset($_POST['fname'])?$_POST['fname']:''
                        ,'lname' => isset($_POST['lname'])?$_POST['lname']:''
                        ,'mobile' => isset($_POST['mobile'])?$_POST['mobile']:''
                        ,'VIN1' => isset($_POST['VIN1'])?$_POST['VIN1']:''
                        ,'itemID' => isset($itemID)?$itemID:''
                        ,'agree_permission' => isset($_POST['agree_permission'])?$_POST['agree_permission']:''
                        ,'code' => $newCode
                        ,'register_date' => date('Y-m-d')
                );

                $checkVIN = $this->db->SELECT('*')->FROM('t_customer_register')->WHERE('VIN1', $data['VIN1'])->get()->result_array();

                if(!empty($checkVIN) && isset($checkVIN)){
                    $result = 0;
                }else{
                    $this->db->INSERT('t_customer_register', $data);
                    $result = "<div align=center><h2><u>".$this->mcl->gl('title_code')."</u></h2></div>";
                    $result .= "<div align=center><h3 style='background: #e2e2e5;width: 80%;'>".$newCode."</h3></div>";
                    $result .= "<div align=center>".$this->mcl->gl('note_code')."</div>";
                }

                print $result;
        }

        public function activatecode(){

            $o    = "";
            $code = isset($_POST['code'])?$_POST['code']:'';
            $vin  = isset($_POST['vin'])?$_POST['vin']:'';
            $this->t['addonData'] = $this->mdb->get_addonData('Register');

                        $this->db->SELECT('*')->FROM('t_customer_register');
                        $this->db->WHERE('code', $code);
                        $this->db->OR_WHERE('VIN1', $vin);
            $checkCode = $this->db->get()->result_array();

                if(!empty($checkCode) && isset($checkCode)){
                    #have data

                    if($checkCode[0]['flagUsed'] == 1){
                        #Activated already
                        $o .= "<form id='customer-info'>";
                            // $o .= "<div class='group_result'>";
                            //     $o .= "<span class='glyphicon glyphicon-info-sign'></span>";
                            // $o .= "</div>";
                            $o .= "<div class='group_result'>";
                                $o .= "<span class='glyphicon glyphicon-remove-sign'></span>";
                            $o .= "</div>";

                            $o .= "<div class='group_result'>";
                                $o .= "<font>".$this->mcl->gl('code_has_activated')."</font>";
                            $o .= "</div>";

                            $o .= "<div>";
                                $o .= $this->mcl->div('button_group', $this->mcl->bt('back', 'back'));
                            $o .= "</div>";
                        $o .= "</form>";

                        print $this->mcl->div('no_data', $o, '');

                    }else{
                        #Update
                        $this->t['edit_data']['fname']  = $checkCode[0]['fname'];
                        $this->t['edit_data']['lname']  = $checkCode[0]['lname'];
                        $this->t['edit_data']['mobile'] = $checkCode[0]['mobile'];
                        $this->t['edit_data']['VIN1']   = $checkCode[0]['VIN1'];
                        $this->t['edit_data']['itemID'] = $checkCode[0]['itemID'];

                        $o .= "<form id='customer-info'>";
                        $o .= $this->mcl->tb('fname',  $this->t, array('class'=>'no-descriptions', 'disabled'=>'disabled'));
                        $o .= $this->mcl->tb('lname',  $this->t, array('class'=>'no-descriptions', 'disabled'=>'disabled'));
                        $o .= $this->mcl->tb('mobile', $this->t, array('class'=>'no-descriptions', 'disabled'=>'disabled', 'type'=>'number'));
                        $o .= $this->mcl->tb('VIN1',   $this->t, array('class'=>'no-descriptions', 'disabled'=>'disabled'));
                        $o .= $this->mcl->emp_cb('itemID', $this->t, array('class'=>'no-descriptions custom-control custom-checkbox'));
                        $o .= $this->mcl->div('button_group', $this->mcl->bt('active', 'button_active'));
                        $o .= "</form>";

                        print $this->mcl->div('div_result_checkcode', $o, '');
                    }

                }else{
                    #No data
                    $o .= "<form id='customer-info'>";
                        $o .= "<div class='group_result'>";
                            $o .= "<span class='glyphicon glyphicon-remove-sign'></span>";
                        $o .= "</div>";

                        $o .= "<div class='group_result'>";
                            $o .= "<font>Wrong code. Please enter code again.</font>";
                        $o .= "</div>";

                        $o .= "<div>";
                            $o .= $this->mcl->div('button_group', $this->mcl->bt('back', 'back'));
                        $o .= "</div>";
                    $o .= "</form>";

                    print $this->mcl->div('no_data', $o, '');
                }

        }

        public function active(){

            $staffid = $this->ion_auth->get_userID();
            $code    = isset($_POST['code'])?$_POST['code']:'';
            $vin     = isset($_POST['VIN1'])?$_POST['VIN1']:'';

            if(!empty($code) || !empty($vin)){
                    $this->db->SET('flagUsed', 1);
                    $this->db->SET('staff_id', $staffid);
                    $this->db->SET('usedDate', date('Y-m-d H:i:s'));
                    $this->db->WHERE('code', $code);
                    $this->db->OR_WHERE('VIN1', $vin);
                $result = $this->db->UPDATE('t_customer_register');
            }else{
                    $result = false;
            }

            print $result;
        }

        function crypto_rand_secure($min, $max){
                $range = $max - $min;
                if ($range < 1) return $min; // not so random...
                $log = ceil(log($range, 2));
                $bytes = (int) ($log / 8) + 1; // length in bytes
                $bits = (int) $log + 1; // length in bits
                $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
                do {
                    $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                    $rnd = $rnd & $filter; // discard irrelevant bits
                } while ($rnd > $range);
                return $min + $rnd;
            }

        function getToken($length){
                $token = "";
                $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                // $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
                $codeAlphabet.= "0123456789";
                $max = strlen($codeAlphabet); // edited

                for ($i=0; $i < $length; $i++) {
                    $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
                }

                return $token;
            }

}
