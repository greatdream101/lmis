<?php

class Report_survey_summary extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('addon/Report_lib');
    }

    public function index() {
        $this->t['edit_data']['dateStart'] = date('Y-m-d');
        $this->t['edit_data']['dateEnd'] = date('Y-m-d');
        
        $this->load->view('report/survey_summary', array(
            't' => $this->t
        ));
    }

}
