<?php

class Sms_performance extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('addon/Report_lib');
    }

    public function index() {
        $this->t['edit_data']['dateStart'] = date('Y-m-d');
        $this->t['edit_data']['dateEnd'] = date('Y-m-d');
        
        $this->load->view('sms/report_performance', array(
            't' => $this->t
        ));
    }

    public function get_SearchSMS() {
        //vd::d($_POST); exit();
        $sourceID = explode(",", $_POST['sourceID']);
        $dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:'';
        $dateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:'';

		$dataSearch = [
                'dateStart' => $dateStart
                ,'dateEnd' => $dateEnd
                ,'sourceID' => $sourceID
        ];
        
		$a = $this->report_lib->get_performanceSMS($dateStart, $dateEnd, $sourceID);
        print ($a);
    }
}
