<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sendmail extends CI_Controller {

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         * 		http://example.com/index.php/welcome
         * 	- or -
         * 		http://example.com/index.php/welcome/index
         * 	- or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index() {
                $a = $this->get_data();
                vd::d($a);
        }

        function get_data() {
                $a = array();
                $this->load->model('sendmail_mod');
                $a = $this->sendmail_mod->get_data();
                return $a;
        }

        function send_email() {
                
        }

}
