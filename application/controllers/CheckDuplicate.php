<?php

class CheckDuplicate extends MY_Controller
{
    
    public $dir = "./insured_file/";
    public $dir_bk = "./insured_file_bk/";
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('addon/CheckDuplicate_model');
        $this->load->library('excel_lib');
    }
    
    public function index()
    {
        ini_set('memory_limit', '-1');
        $userID = 1;
        $computerName = $this->security_lib->get_computerName();
        
        // Scan folder
        $filesName = scandir($this->dir, 1);
        $s_high = '';
        $s_normal = '';
        $file_count = 0;
        foreach ($filesName as $file) {
            if (strpos($file, EXCEL_TEMPLATE) !== false) {
                $file_count++;
            }
        }
        
        if ($file_count > 0) {
            foreach ($filesName as $file) {
                if ($file !== '..' && $file !== '.') {
                    $data = [];
                    $arr = explode('.', $file);
                    if (count($arr) == 2) {
                        // Check have file name RawData_ ?
                        if (strpos($file, EXCEL_TEMPLATE) !== false && ($arr[1] == 'xls' || $arr[1] == 'xlsx')) {
                            $fileName = $arr[0];
                            $fileExtendsion = $arr[1];
                            $data = $this->readFile($fileName, $fileExtendsion);
                            
                            // Save Data source
                            $dataSource['sourceName'] = $file;
                            $dataSource['extendsionName'] = $fileExtendsion;
                            $dataSource['createdDate'] = date('Y-m-d H:i:s');
                            $dataSource['createdID'] = $userID;
                            $dataSource['computerName'] = $computerName;
                            $dataSource['dataTypeID'] = 1;
                            
                            $sourceID = $this->CheckDuplicate_model->saveDataSource($dataSource);
                            
                            if ($sourceID) {
                                if ($sourceID != 0) {
                                    // Send to Clean data
                                    $this->dataCleaning($data, $sourceID);
                                    $s_high = $sourceID;
                                }
                            }
                        }
                        if ($sourceID) {
                            if ($sourceID != 0) {
                                // Sent to Check Dupplicate data
                                $this->dataDeDup($s_high);
                                //send email of success
                            } else {
                                die('Duplicate dataSource');
                            }
                        }
                        // Sleep flow
                        if ($sourceID) {
                            if ($sourceID != 0) {
                                rename($this->dir . $file, $this->dir_bk . $file);
                            } else {
                                unlink($file);
                            }
                        }
                    } else {
                        //send mail of wrong name
                    }
                }
            }
        } else {
            //send email of no files or number files is not two
            if ($file_count == 0) {
                die('no files');
            } else {
                die('not two');
            }
        }
    }
    
    public function dataDeDup($s_high)
    {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes
        $a = $this->CheckDuplicate_model->dataDeDup($s_high);
        $high = $a['high'];
        $id = $this->CheckDuplicate_model->insert($high, $s_high);
        
        return $id;
    }
    
    public function readFile($filename = '', $extendsion = '')
    {
        
        $data = [];
        if ($filename != '' && $extendsion != '') {
            
            // inputFileType
            if ($extendsion == 'xlsx') {
                $inputFileType = 'Excel2007';
            }
            if ($extendsion == 'xls') {
                $inputFileType = 'Excel5';
            }
            
            // inputFileName
            $inputFileName = $this->dir . $filename . '.' . $extendsion;
            
            // Library Reader
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
            
            // Return Data
            $data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            
            return $data;
        }
        
        return $data;
    }
    
    public function moveFile()
    {
        if (rename($this->dir . '/' . $file, $this->dir_bk . '/' . $file)) {
            $status = 1; // Success
        } else {
            $status = 0; // Error
        }
        return $status;
    }
    
    public function dataCleaning($data, $sourceID)
    {
        
        // Header data
        $header = $data[1];
        
        //Check header template match or not?
        $master_head_high = array('VIN', 'TITLE', 'FIRST_NAME', 'SURNAME', 'Final PH number', 'CODE', 'discount', 'contentSMSID');
        
        $master_head_normal = array('VIN', 'TITLE', 'FIRST_NAME', 'SURNAME', 'Final PH number', 'CODE', 'discount', 'contentSMSID');
        $intersect_high = array_intersect($master_head_high, $header);
        $intersect_normal = array_intersect($master_head_normal, $header);
        
        $headKey = [];
        if (count($master_head_high) != count($intersect_high) && count($master_head_normal) !== count($intersect_normal)) {
            $pass = false;
        } else {
            $pass = true;
            // Mapping cell to database
            $headKey = array(
                'A' => 'insuredDedupID',
                'B' => 'mobilePhone',
                'C' => 'agentCD',
                'D' => 'agentName',
                'E' => 'mobilePhone',
                'F' => 'code',
                'G' => 'discount',
                'H' => 'contentSMSID'
            );
        }
        
        $data_success = [];
        $data_error = [];
        $txt_error = '';
        
        // แบ่งก้อน array เพื่อ insert เป็นก้อนๆ ก้อนละ 100
        unset($data[1]);
        $chunkData = array_chunk($data, 100);
        
        if ($pass) {
            $userID = $this->ion_auth->get_userID();
            $computerName = $this->security_lib->get_computerName();
            
            foreach ($chunkData as $values) {
                $saveData = [];
                foreach ($values as $val) {
                    $data_success = [];
                    $data_error = [];
                    $txt_error = '';
                    
                    foreach ($headKey as $key => $hd) {
                        if (isset($val[$key]) || !empty($val[$key])) {
                            $data_success[$hd] = $val[$key];
                        } else {
                            $data_success[$hd] = '';
                            $txt_error .= '|' . $hd;
                        }
                    }
                    
                    $data_success['sourceID'] = $sourceID;
                    $data_success['createdDate'] = date('Y-m-d H:i:s');
                    $data_success['createdID'] = $userID;
                    $data_success['computerName'] = $computerName;
                    
                    // Error Flag
                    if ($txt_error != '') {
                        $data_success['flagID'] = 4; // cleaning error
                    } else {
                        $data_success['flagID'] = 1; // completed
                    }
                    
                    // Add array for insert_batch
                    array_push($saveData, $data_success);
                }
                
                // Insert Data
                $this->CheckDuplicate_model->saveCleanData($saveData);
            }
        }
    }
    
}
