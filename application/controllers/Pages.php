<?php

class Pages extends MY_Controller {

        public function __construct() {
                parent::__construct();
        }

        public function index() {

                $this->data['title'] = $this->data['web']['function_name'];
                $this->data['content'] = $this->mcl->get_list_view($this->controller_name);
                $this->render();
        }

        function settings() {
                $this->data['title'] = $this->data['web']['function_name'];
//                $this->data['content'] = $this->load->view('pages/settings', array(), true);
                $this->render();
        }

        function profile() {
                $this->data['title'] = $this->data['title'] = $this->data['web']['function_name'];
//                $this->data['content'] = $this->load->view('pages/profile', array(), true);
                $this->render();
        }

        function home() {
                $this->data['title'] = $this->data['title'] = $this->data['web']['function_name'];
//                $this->data['content'] = $this->load->view('pages/home', array(), true);
                $this->render();
        }

        function about() {
                $this->load->model('pages_model');
                $this->data['title'] = $this->data['title'] = $this->data['web']['function_name'];
                $this->data['department'] = $this->pages_model->get_department();
                $this->data['division'] = $this->pages_model->get_division($this->data['department']['department_id']);
//                $this->data['content'] = $this->load->view('pages/about', array(), true);
                $this->render();
        }

        function contact() {
                $this->data['title'] = $this->data['title'] = $this->data['web']['function_name'];
//                $this->data['content'] = $this->load->view('pages/contact', array(), true);
                $this->render();
        }

}
