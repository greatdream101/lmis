<?php

class Survey_assign extends MY_Controller {
         public function __construct() {
                parent::__construct();

                $this->load->model('addon/assignment_mod');
        }

        public function index() {
                $clients_list = $this->assignment_mod->get_clients_list();
                // vd::d($clients_list);exit;

                if(!empty($clients_list))
                {
                     $url = array( 'url' => array('survey_assign/pick_series'),
                          'attr_id'=>array('clientsID'),
                          'index_column'=>'id',
                     );
                     $table = $this->mcl->table($clients_list, $url , 'datatable_local');
                }
                else
                {
                     $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
                }
                // vd::d($table);exit;
                $o = $this->load->view('survey/assignclients', array('t' => $this->t, 'table'=>$table), true);
                print $o;
        }

        function pick_series($clientsID) {
             $series_list = $this->assignment_mod->get_series_list($clientsID);
             if(!empty($series_list))
             {
                  $url = array( 'url' => array('survey_assign/pick_branch/'.$clientsID),
                       'attr_id'=>array('seriesID'),
                       'index_column'=>'id',
                  );
                  $table = $this->mcl->table($series_list, $url , 'datatable_local');
             }
             else
             {
                  $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
             }
             $o = $this->load->view('survey/assignseries', array('t' => $this->t, 'table'=>$table), true);
             print $o;
        }

        function pick_branch($clientsID, $seriesID) {
             $this->t['clientsID'] = $clientsID;
             $this->t['seriesID'] = $seriesID;
             $o = $this->load->view('survey/assignment', array('t' => $this->t), true);
             print $o; exit;
        }

        function get_sb_branch()
        {
             if (isset($_POST['seriesID']))
             {
                  $o = $this->assignment_mod->get_branch_series($_POST['seriesID']);
                  print json_encode($o);
             }
        }

        function get_surveyor_list()
        {

               $data = $this->assignment_mod->get_surveyor_list($_POST['seriesID'],$_POST['branchID']);
               // var_dump($surveyor_list);exit;
               // var_dump($data);
               // $this->t['addonData']['userID'] = $data;

               $dtn = 'surveyor_list';
               $name = 'userID';

               $o = '';
               $o .= '<form name="frm_assignment" id="frm_assignment" class="request-form" onsubmit="return false;">';
               $o .= '<div class="sort-table-area">';
               $o .= '<table table_name="' . $dtn . '" class="sort-table table" id="tc_' . $name . '"  >';
               $o .= '<thead>';
               $o .= '<tr>';
               $o .= '<th width=50px class="filter-false">';
               $o .= '<label>
                              <input class="checkbox-master" type="checkbox" value="" name="tc_' . $name . '">
                              <span></span>
                         </label>';
               $o .= '</th>';

               $colspan = 1;
               foreach ($data['0'] as $k => $v)
               {
                    $c = '';
                    $o .= '<th class="' . $c . '">' . $this->mcl->gl($k) . '</th>';
                    $colspan++;
               }

               $o .= '</tr>';
               $o .= '</thead>';

               $o .= '<tfoot>
                              <tr>
                                 <th colspan="' . $colspan . '" class="ts-pager form-horizontal">
                                   <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                   <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                   <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                   <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                   <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
                                   <select class="pagesize input-mini" title="Select page size">
                                     <option selected="selected" value="10">10</option>
                                     <option value="20">20</option>
                                     <option value="30">30</option>
                                     <option value="40">40</option>
                                     <option value="1000">1000</option>
                                   </select>
                                   <select class="pagenum input-mini" title="Select page number"></select>
                                 </th>
                              </tr>
                         </tfoot>';

               $o .= '<tbody>';
               foreach ($data as $k => $v)
               {
                    $o .= '<tr>';
                    $o .= '<td>';
                    $check = '';

                    $i = 0;
                    // $o .= '<label>
                    //                <input type="checkbox" class="tc" value="' . $v[$name] . '" name="' . $dtn . '_' . $name . '_' . $v[$name] . '" id="' . $dtn . '_' . $name . '_' . $v[$name] . '" ' . $check . '>
                    //                <span></span>
                    //           </label>';
                    $o .= '<label>
                                   <input type="checkbox" class="tc" value="' . $v[$name] . '" name="' . 'select_id[]' . '" id="' . $name . '_' . $v[$name] . '" ' . $check . '>
                                   <span></span>
                              </label>';

                    $o .= '</td>';

                    foreach ($v as $kk => $vv)
                    {
                         if (in_array(strtolower($kk), $this->config->item('ignore_columns')) == false)
                         {

                              if (substr(strtolower($kk), strlen($kk) - 3) !== '_id')
                              {
                                   $o .= '<td>' . $vv . '</td>';
                              }
                              else
                              {
                                   $c = 'hide';
                                   $o.='<td class="' . $c . '">' . $vv . '</td>';
                              }
                         }
                    }
                    $o .= '</tr>';
               }
               $o .= '</tbody>';
               $o .= '</table>';
               $o .= '</form>';
               //end

               $o .= '</div>';

             print $o;
        }

        function assign_survey()
        {
             $userID = $this->session->userdata('userID');

             if( (isset($_POST['clientsID']))&&(isset($_POST['seriesID']))&&(isset($_POST['branchID'])) )
             {
                  // vd::d($_POST['branchID']);
                  // var_dump($_POST['select_id']);
                  $surveyorID = $_POST['select_id'];
                  $data = array();
                  $i = 0;
                  foreach ($surveyorID as $key => $value)
                  {
                       // print $value."<br/>";
                       $inner_array = array(
                         'clientsID' => $_POST['clientsID'],
                         'seriesID' => $_POST['seriesID'],
                         'branchID' => $_POST['branchID'],
                         'userID' => $value,
                         'createdID' => $userID,
                         'createdDate' => date("Y-m-d H:i:s"),
                         'qcStatusID'=>'5',
                       );

                       $data[$i] = $inner_array;
                       $i++;
                  }
                  // var_dump($data);
                  // exit;
             }

             $result = $this->assignment_mod->assign_survey($data);
             print $result;
        }
}
