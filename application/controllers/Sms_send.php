<?php
date_default_timezone_set("Asia/Bangkok");
class Sms_send extends MY_Controller
{

    function __construct() {
        parent::__construct();
        
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 36000); //36000 seconds = 10 H
        
        $this->load->model('addon/sms_model');
        $this->load->model('addon/customer_model');
        $this->load->library('addon/Sms_lib');
        $this->load->library('Enc_dec_lib');
    }

    public function index() {
        //vd::d('Send SMS Completed.');
        $strDate = date ( 'Y-m-d' );
        $officeHours = $this->sms_model->get_holiday($strDate);
        
        if (empty($officeHours)){
            if ( date( "D" ) == "Sun" || date( "D" ) == "Sat" ){
                $officeHours = 1;
            }
        }

        if (empty($officeHours)){
            
            $officeHours = $this->sms_model->get_officeHours();
            $strTime = date ( 'H:i:s' );
            //$strTime = "17:00:00";
            $startTime = $officeHours['startTime'];
            $endTime = $officeHours['endTime'];
            
            $startTimeDiff = (strtotime($strTime) - strtotime($startTime));
            $endTimeDiff = (strtotime($endTime) - strtotime($strTime));
            
            if ($startTimeDiff >= 0 && $endTimeDiff >= 0){
                
                $tempSMS = $this->sms_model->get_tempSMS();
                
                if(!empty($tempSMS)) {
                    $i = 0;
                    $o = '';
                    foreach ( $tempSMS as $key => $val){
                        $strTime = date ( 'H:i:s' );
                        $startTime = $officeHours['startTime'];
                        $endTime = $officeHours['endTime'];
                        
                        $startTimeDiff = (strtotime($strTime) - strtotime($startTime));
                        $endTimeDiff = (strtotime($endTime) - strtotime($strTime));
                        
                        if ($startTimeDiff >= 0 && $endTimeDiff >= 0){
                            ++$i;
                            //echo $i."</br>";
                            $select = array('id', 'insuredName', 'mobilePhone', 'sourceID', 'sourceName', 'dataTypeID', 'nameEN', 'contentSMS', 'createdID', 'discount');
                            $conditionArray = array('id' => $val['cusID']);
                            $cusData = $this->customer_model->get_CustomerSMS($select, $conditionArray);

                            
                            if(!empty($cusData)) {
                                foreach ($cusData as $k => $v){
                                    
                                    $token = $this->sms_lib->genSurveyToken($val['cusID']);
                                    $surveyURL = $this->sms_model->get_surveyURL();

                                    if(!empty($v['discount'])){
                                        $discount = number_format($v['discount'], 0, '', ',');
                                    }else{
                                        $discount = '0';
                                    }

                                    $cusData[$k]['contentSMS'] = str_replace('{name}', $v['insuredName'], $cusData[$k]['contentSMS']);
                                    $cusData[$k]['contentSMS'] = str_replace('{discount}', $discount, $cusData[$k]['contentSMS']);
                                    $cusData[$k]['contentSMS'] = str_replace('{link}', $surveyURL['surveyURL'].$cusData[$k]['sourceID'].'/'.$token, $cusData[$k]['contentSMS']);
                                    
                                    if (!empty($cusData[$k]['mobilePhone'])) {
                                        
                                        if( (int)$cusData[$k]['mobilePhone'] > 0 ){
                                            $mobilePhone = $cusData[$k]['mobilePhone'];
                                        }else{
                                            $mobilePhone = $this->enc_dec_lib->dec($cusData[$k]['mobilePhone']);
                                        }
                                        
                                        //$url = 'http://172.22.1.53/io/sms_sender_api/sms_sender';
                                        $url = 'http://172.22.1.54/io/sms_sender_api/sms_sender';
                                        //$url = 'http://61.91.124.15/io/sms_sender_api/sms_sender';
                                        
                                        //$aut_key = 'VEVTVCBTTVM6MTUzMTEwOTIzMDgxNg=='; // <= Test MOCAP
                                        //$aut_key = 'QUlBQ3VzdG9tZXI6MTUzMjA4NTAxOTk2MQ=='; // <= AIA itwist
                                        $aut_key = 'TUFaREE6MTU2NjU0MzE3NzMwNA=='; // <= AIA Dtac
                                        $message = $cusData[$k]['contentSMS'];
                                        $refNo = 'mzgift/'.$cusData[$k]['sourceID'].'/'.$token;
                                        //$message = 'ทดสอบ SMS ' . date('d/m/Y H:i:s');
                                        
                                        $str = '{
                                            "aut_key":"'.$aut_key.'",
                                            "refNo":"'.$refNo.'",
                                            "phone":"'.$mobilePhone.'",
                                            "message":"'.$message.'",
                                            "function_uri":"mzgift/survey"
                                        }';

                                        //vd::d($message);
                                        
                                        //$o .= '<a href="'.base_url('sq/'.$cusData[$k]['sourceID'].'/'.$token).'" target="_blank">'.$message.'</a><br>';
                                        
                                        $curl = curl_init();
                                        curl_setopt($curl, CURLOPT_URL, $url);
                                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
                                        $response = curl_exec($curl);
                                        $code = curl_getinfo($curl);
                                        $err = curl_error($curl);
                                        curl_close($curl);
                                        
                                        // //$result = json_decode($response);
                                        
                                        // //vd::d($response);
                                        // //vd::d($result);
                                        // //$result = 1;
                                        
                                        // vd::d($response);
                                        
                                        if (!empty($response)){
                                             $e = explode( ',', $response);
                                             $result = array();
                                             foreach ($e as $keya => $val){
                                                 $val = str_replace('"', "", $val);
                                                 $val = str_replace("{", "", $val);
                                                 $val = str_replace("}", "", $val);
                                                 $ee = explode(":", $val);
                                                 $result[$keya] = trim($ee[1]);
                                             }
                                         }
                                        
                                         if (!empty($result)){
                                             $responseData = array();
                                             $responseData['cusID'] = $cusData[$k]['id'];
                                             $responseData['contact'] = $cusData[$k]['mobilePhone'];
                                             $responseData['message'] = $message;
                                             $responseData['function_uri'] = "app-demo/survey";
                                             $responseData['status'] = $result[0];
                                             $responseData['response'] = $result[1];
                                             $responseData['sourceID'] = $cusData[$k]['sourceID'];
                                             $responseData['created_id'] = $cusData[$k]['createdID'];
                                            
                                             $this->sms_model->save_response($cusData[$k]['id'], $responseData);
                                         }
                                    }
                                }
                            }
                        }
                    }

                    $output = '
                                <h3>Link Survey</h3>
                                <div class="col-lg-12">'.$o.'</div>
                                <script>$("#footer-main").hide();</script>
                            ';
                    print $output;
                    
                }
            }
        }
    }
}
