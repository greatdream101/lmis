<?php

class Survey_question extends MY_Controller {

         public function __construct() {
                parent::__construct();
                $this->load->model('addon/question_mod');
//                $this->configs = array('detail'=>'ta');
        }

        public function index() {
                $o = $this->load->view('survey/question_answer', array('t' => $this->t), true);
                print $o;
        }

        function get_questions() {
                if (isset($_POST['seriesID'])) {
                        $series_id = $_POST['seriesID'];
                        $a = $this->question_mod->get_data($series_id);
                        print $this->load->view('survey/qq', array('a' => $a), true);
                }
        }

        function frm_answer() {
                $ans = $this->mdb->get_addonData('setup_frm_answer');
                $this->t['edit_data'] = $this->question_mod->get_question(isset($_POST['questionID']) ? $_POST['questionID'] : 0);
                $this->t['addonData']['jumpFrom'] = $ans['questionID'];
                $this->t['addonData']['jumpFrom']['data'][99999] = array('questionID' => '99999', 'name' => $this->mcl->gl('end_survey'));
                $this->t['addonData']['jumpTo'] = $ans['questionID'];
                $this->t['addonData']['jumpTo']['data'][99999] = array('questionID' => '99999', 'name' => $this->mcl->gl('end_survey'));
                $this->t['addonData']['exCalID'] = $ans['exCalID'];
                print $this->load->view('survey/frm_answer', array('t' => $this->t), true);
        }

        function frm_duplicateAnswer() {
                $ans = $this->mdb->get_addonData('setup_frm_answer');
                $seriesID = $_POST['seriesID'];
                $questionID = $_POST['parentID'];
                $this->t['addonData']['duplicateTo']['data'] =  $this->question_mod->get_section($seriesID);
                print $this->load->view('survey/frm_duplicateAnswer', array('t' => $this->t), true);
        }

        function frm_question() {
                $question = $this->mdb->get_addonData('setup_frm_question');
                $series_id = $_POST['seriesID'];
                $t['edit_data'] = $this->question_mod->get_question(isset($_POST['questionID']) ? $_POST['questionID'] : 0);
                $t['addonData']['sectionID']['data'] = $this->question_mod->get_section($series_id);
                $t['addonData']['graphTypeID'] = $question['graphTypeID'];
                print $this->load->view('survey/frm_question', array('t' => $t), true);
        }

        function add_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_question();
                }
        }

        function add_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_answer();
                }
        }

        function duplicate_answer() {
                //vd::d($_POST);
                if (isset($_POST['uri'])) {
                        $this->question_mod->duplicate_answer();
                }
        }

        function delete_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_answer();
                }
        }

        function delete_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_question();
                }
        }

        function save_position() {
                if (isset($_POST['q'])) {
                        $this->question_mod->save_position();
                }
        }

}
