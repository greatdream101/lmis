<?php

class Admin_privilege extends MY_Controller {

        public function __construct() {
                parent::__construct();
                $this->configs = array('detail' => 'ta');
        }

        function index(){
              $o = $this->load->view('admin/privilege', array('t' => $this->t), true);
              print $o;
        }

        function get_group_function() {
                $groupID = $_POST['groupID'];
                $output = $this->mdb->get_group_function($groupID);
                print($output);
        }

        function save_data(){
                return $this->mdb->save_group_privilege();
        }
}
