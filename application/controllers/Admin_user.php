<?php

class Admin_user extends MY_Controller {

      public function __construct() {
            parent::__construct();
            $this->configs = array('detail' => 'ta');
      }

      function save_data() {
            $this->load->model('admin_mod');
            print $this->admin_mod->save_user_password();
      }

      function delete_data() {
            $this->load->model('admin_mod');
            print $this->admin_mod->delete_data();
      }

      public function edit($id) {
            $url = 'admin_user';
            $this->t = $this->mdb->get_tableData($url, 'edit', $id);
            $this->load->model('admin_mod');
            $this->t['editData']['groupID'] = $this->admin_mod->get_groupID($id);
            
            $o = $this->load->view('admin/user', array('t' => $this->t), true);
            print $o;
      }

}
