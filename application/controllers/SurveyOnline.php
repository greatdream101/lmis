<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SurveyOnline extends MY_Controller {

        function __construct() {
                parent::__construct();
                $this->load->model('addon/Customer_model');
                $this->load->model('addon/Question_mod');
                $this->load->library('addon/Scripts_lib');
        }

        //redirect if needed, otherwise display the user list
        function index() {
                // $this->load->view('welcome_message');
                $data['scriptsTerminate'] = $this->scripts_lib->getScriptsNotFound();
                print $this->load->view('survey/surveyTerminate', array('t' => $this->t, 'data' => $data), true);
                exit;
        }

        function sms($sourceID = 0, $token = '') {
                // vd::d($token);
                $conditionArray = array(
                    'sourceID' => $sourceID,
                    'surveyToken' => $token,
                );

                // Get Customer Data
                $cusInfo = $this->checkCustomerList($conditionArray);
                // vd::d($cusInfo);
                // +++++++++++++++++++++++++++++++++++++
                $seriesID = $cusInfo['seriesID'];
                $survey_info = $this->Question_mod->get_survey_info(array('cusID' => $cusInfo['cusID']));
                $surveyID = $survey_info['surveyID'];

                $question_answer = $this->Question_mod->get_data($seriesID);
                $answer_value = $this->Question_mod->get_answer_value_jump($surveyID);

                $data['question_info'] = $this->load->view('survey/smsSurvey', array(
                    'a' => $question_answer,
                    't_value' => $answer_value,
                    'seriesID' => $seriesID,
                        ), true);

                $data['scriptsGreeting'] = $this->scripts_lib->getScriptsGreeting($seriesID);
                // print $data['scriptsGreeting'];exit;
                $data['scriptsEnding'] = $this->scripts_lib->getScriptsEnding($seriesID);
                // print $data['scriptsEnding'];exit;

                $this->t['cusID'] = $cusInfo['cusID'];
                if (($survey_info['qcStatusID'] > 0) && ($survey_info['qcStatusID'] < 4)) {
                        $this->t['surveyStatus'] = 'pending';
                } else {
                        $this->t['surveyStatus'] = 'completed';
                }
                $o = $this->load->view('survey/surveyOnline', array('t' => $this->t, 'data' => $data), true);
                print $o;
        }

        function aq($sourceID = 0, $token = '') {
                // vd::d($token);
                $conditionArray = array(
                    'sourceID' => $sourceID,
                    'surveyToken' => $token,
                );

                // Get Customer Data
                $cusInfo = $this->checkCustomerList($conditionArray);
                // vd::d($cusInfo);
                // +++++++++++++++++++++++++++++++++++++
                $seriesID = $cusInfo['seriesID'];
                $survey_info = $this->Question_mod->get_survey_info(array('cusID' => $cusInfo['cusID']));
                $surveyID = $survey_info['surveyID'];

                $question_answer = $this->Question_mod->get_data($seriesID);
                $answer_value = $this->Question_mod->get_answer_value_jump($surveyID);

                $data['question_info'] = $this->load->view('survey/smsSurveyAQ', array(
                    'a' => $question_answer,
                    't_value' => $answer_value,
                    'seriesID' => $seriesID,
                        ), true);

                $data['scriptsGreeting'] = $this->scripts_lib->getScriptsGreeting($seriesID);
                // print $data['scriptsGreeting'];exit;
                $data['scriptsEnding'] = $this->scripts_lib->getScriptsEnding($seriesID);
                // print $data['scriptsEnding'];exit;

                $this->t['cusID'] = $cusInfo['cusID'];
                if (($survey_info['qcStatusID'] > 0) && ($survey_info['qcStatusID'] < 4)) {
                        $this->t['surveyStatus'] = 'pending';
                } else {
                        $this->t['surveyStatus'] = 'completed';
                }
                $o = $this->load->view('survey/surveyOnlineAQ', array('t' => $this->t, 'data' => $data), true);
                print $o;
        }

        function sq($sourceID = 0, $token = '') {

                $conditionArray = array(
                        'sourceID' => $sourceID,
                        'surveyToken' => $token,
                );

                $cusInfo = $this->Customer_model->getCustomerData($conditionArray);

                if(empty($cusInfo['QA1']) && !isset($cusInfo['QA1'])){
                        $getQuestionData = array('sectionID' => 1);
                }else{
                        $getQuestionData = array('sectionID' => 2);
                }

                $question = $this->Customer_model->getQuestionData($getQuestionData);

                print $this->load->view('survey/surveyTerminate',  array("cusInfo"=>$cusInfo, "question"=>$question), true);
        }

        function save_answer() {
                if ((isset($_POST['cusID'])) && (isset($_POST['answerID']))) {

                        $cusID      = $_POST['cusID'];
                        $answerID   = $_POST['answerID'];
                        $sectionID  = $_POST['sectionID'];

                        $data = array(
                                $sectionID    => $answerID,
                                'survey_date' => date('Y-m-d')
                        );

                        $result = $this->Customer_model->updateData($cusID, $data);

                        print $result;
                }
        }

        function update_qc_status() {
                $surveyID = $_POST['surveyID'];
                $is_update = 0;
                if (isset($_POST['qcStatusID'])) {
                        $update_data = array(
                            'qcStatusID' => $_POST['qcStatusID'],
                            'updatedDate' => date("Y-m-d H:i:s"),
                        );

                        $is_update = $this->Question_mod->update_qc_status($surveyID, $update_data);
                        // $this->send_email($surveyID);
                }

                print $is_update;
        }

        function get_answer_value($userID, $seriesID) {
                $answer_value = $this->Question_mod->get_answer_value($userID, $seriesID);
                // var_dump($answer_value);
                return $answer_value;
        }

        function send_email($surveyID) {
                
                $this->load->model('sendmail_mod');
                $customer = $this->sendmail_mod->get_customer($surveyID);
                
                if (count($customer) > 0) {
                        $this->sendmail_mod->send_mail($customer);
                }
        }

}
