<?php

class Sms_status extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes
        
        /*$this->load->model('addon/flag_model');
        $this->load->model('addon/sms_model');*/
        
        $this->load->model('addon/customer_model');
    }
    
    public function index() {
        $this->load->view("sms/status", array("t" => $this->t));
    }
    
    public function get_ref_no() {
        if (!empty($_POST['sourceID'])) {
            $sourceID = $_POST['sourceID'];
            $refNo = $this->customer_model->getRefNo($sourceID);
            $i = 0;
            foreach ($refNo as $key => $val){
                $i++;
                $refNo = 'aia/'.$val["sourceID"].'/'.$val["surveyToken"];
                $url = 'http://172.22.1.54/io/sms_get_status/sms_get_status';
                
                $str = '{"refNo":"'.$refNo.'"}';
                 $curl = curl_init();
                 curl_setopt($curl, CURLOPT_URL, $url);
                 curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                 curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
                 $response = curl_exec($curl);
                 $code = curl_getinfo($curl);
                 $err = curl_error($curl);
                 curl_close($curl);
                
                $opts = array( 'http'=>array( 'method'=>"GET") );
                $context = stream_context_create($opts);
                $fp = @file_get_contents($url.'?refNo='.$refNo, false, $context);
                
                if( !empty($fp) ){
                    date_default_timezone_set("Asia/Bangkok");
                    $arr = json_decode($fp);
                    $data = array("sms_status" => $arr->status, "sms_result" => $arr->response, "updated_id" => $this->ion_auth->get_userID(), "updated_date" => date ( "Y-m-d H:i:s" ));
                    $id = $this->customer_model->updateData($val["cusID"], $data);
                    if($id > 0){
                        echo "Update cusID : ".$arr->status." SMS Status : ".$arr->status." SMS Result : ".$arr->response;
                        echo "<br>";
                    }
                    
                }else{
                    echo "cusID : ".$val["cusID"]." No SMS Log";
                    echo "<br>";
                }
            }
        }
    }
}
