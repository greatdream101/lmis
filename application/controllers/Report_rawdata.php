<?php

class Report_rawdata extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('addon/Report_lib');
        $this->load->model('addon/Customer_model');
        $this->load->model('addon/Question_mod');
        $this->load->library('PHPExcel/Classes/PHPExcel');
        $this->load->library('Export_excel');
        $this->load->library('date_lib');
    }
    
    function index() {
        $data = array();
        $url = array();
        $table = $this->mcl->table($data, $url, 'datatable_local');
        $this->load_view('report/rawdata', array('table' => $table,'t' => $this->t));
    }
    
    function get_data() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 36000);
        $clientsID = isset($_POST['clientsID'])?$_POST['clientsID']:'';
        $seriesID = isset($_POST['seriesID'])?$_POST['seriesID']:'';
        $dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:'';
        $tempdateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:date('Y-m-d');
        $dateEnd = !empty($tempdateEnd)?$tempdateEnd:date('Y-m-d');        
        $flag = isset($_POST['flag'])?$_POST['flag']:'';
        
        if ($flag == 'export') {
            $arr = [];
            $arr = $this->Question_mod->get_all_questions($seriesID);
            $arrQuestion = [];
            foreach ($arr as $lst){
                $arrQuestion[$lst['questionID']] = $lst['code'];
            }
            
            $haystack = [];
            $dataTable = [];
            $arr = $this->Question_mod->get_value($seriesID);
            foreach ($arr['values'] as $key => $lst){
                $id = explode('_', $key);
                $QID = $id[3];//_questionID
                $idKeyTable = $id[0].'_'.$id[2];//seriesID_cusID
                if (!in_array($idKeyTable, $haystack)) {
                    $dataTable[$idKeyTable][$QID] = $lst;
                }else {
                    $haystack = $idKeyTable;
                }
            }
            
            $select = 'id, insuredDedupID, sms_status, sms_result, effectiveDate, effectiveMonth, createdDate, flagID
                    , seriesID, region, division, divisionName, insuredName, agentName, agentCD, agencyName, agencyCD';
            $arr = $this->Customer_model->getReportAVG($select, $dateStart, $dateEnd);
            
            $arrExcel = [];
            foreach ($arr as $lst){
                $keyVal = $lst['seriesID'].'_'.$lst['id'];
                foreach ($arrQuestion as $idkey => $lsts){
                    $value = isset($dataTable[$keyVal][$idkey])?$dataTable[$keyVal][$idkey]:'';
                    $lst[$lsts] = $value;
                }
                $arrExcel[] = $lst;
            }
            //var_dump($arrExcel);exit();
            //check data by excel
            $params = array('rep_title' => 'raw', 'rep_name' => 'rawdata.xlsx', 'rep_template' => 'rawdata.xlsx','data_array'=>$arrExcel);
            $this->export_excel->excel_rawArray($params);
        }else {
            $a = $this->report_lib->get_rawdata($clientsID, $seriesID, $dateStart, $dateEnd);
            echo $a;
        }      
        
    }
    
}
