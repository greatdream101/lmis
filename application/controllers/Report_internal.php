<?php

class Report_internal extends MY_Controller {

    public function __construct() {
        parent::__construct();

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

        $this->load->model('addon/Question_mod');
        $this->load->model('addon/Report_model');
		$this->load->library('PHPExcel/Classes/PHPExcel');
        // $this->load->library('Excel_report');
        $this->load->library('Excel_report_internal');
		$this->load->library('date_lib');
        
    }

    public function index() {
        $this->t['edit_data']['dateStart'] = date('Y-m-d');

        $arr = $this->Report_model->get_msbDataType();
        
        $this->t['addonData']['dataTypeID']['data'] = $arr;
        $rep_template = 'internal_report_v2.xlsx';

        $this->load->view('report/report_internal', array(
            't' => $this->t,
            'rep_template' => $rep_template
        ));
    }

    public function genInternalReport() {    
        $seriesID = 1;
	    $dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:'';
        // $dateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:'';
        $dataTypeID = isset($_POST['dataTypeID'])?$_POST['dataTypeID']:'1,2';

        $rep_template = isset($_POST['rep_template'])?$_POST['rep_template']:'';

        $arr = [];
        $arr = $this->Question_mod->get_all_questions($seriesID);
	    $arrQuestion = [];
	    foreach ($arr as $lst){
            // $arrQuestion[$lst['questionID']] = $lst['code'];
            $arrQuestion[$lst['questionID']] = str_replace('_','-',$lst['code']);
        }

        // vd::d($arrQuestion); exit;

        $arrValue = $this->Report_model->getInternalReport($dateStart,$dataTypeID);
        $arrDivSMS = $this->Report_model->getDivSummarySMS($dateStart,$dataTypeID);
        $arrRegSMS = $this->Report_model->getRegSummarySMS($dateStart,$dataTypeID);
        $arrDivision = $this->Report_model->getAllDivision();
        $arrRegion = $this->Report_model->getAllRegion();

        $arrAppendix = $this->Report_model->getScoreByAL($dateStart);

        // var_dump($arrValue);exit();
        // var_dump($arrDivSMS);exit();
        // var_dump($arrRegSMS);exit();
        // var_dump($arrDivision);exit();
        // var_dump($arrRegion);exit();
        // vd::d($arrAppendix);exit();

        $arrReport = array(
            'rep_template' => $rep_template,
            'dateStart' => $dateStart,
            'arrValue' => $arrValue,
            'arrQuestion' => $arrQuestion,
            'arrDivision' => $arrDivision,
            'arrRegion' => $arrRegion,
            'arrDivSMS' => $arrDivSMS,
            'arrRegSMS' => $arrRegSMS,
            'arrAppendix' => $arrAppendix
        );

        // $this->excel_report_internal->genInternalReport($dateStart,$arrValue,$arrQuestion,$arrDivision,$arrRegion,$arrDivSMS,$arrRegSMS,$arrAppendix);
        $this->excel_report_internal->genInternalReport($arrReport);
    }
}
