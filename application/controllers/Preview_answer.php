<?php
class Preview_answer extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->library('addon/Preview_answer_lib');
    }
    
    public function preview_answer($data='') {
        /*$data = array (     "0"=>array ( 
                                "code"=>"QC5",
                                "name"=>"C5 ความสามารถในการนำเสนอรถยนต์",
                                "value"=>"8",
                             ),
                            "1"=>array (
                                "code"=>"QA2",
                                "name"=>"A2 คุณจะแนะนำโชว์รูมแก่เพื่อน หรือคนรู้จักของคุณหรือไม่คะ/ครับ?",
                                "value"=>"ใช่",
                                ),
                            "2"=>array (
                                "code"=>"QB1",
                                "name"=>"B1 คุณจะให้คะแนนความพอใจต่อสิ่งอำนวยความสะดวกของโชว์รูม โดยรวมเท่าไหร่คะ/ครับ (คำอธิบาย:สภาพโดยรวม,ความสบาย,ความสะอาด และอื่นๆ)",
                                "value"=>"10",
                                ),
                        );*/
        $tablePreviewAnswer = $this->preview_answer_lib->tablePreviewAnswer($data);
        
        /////////////////////////
        // Output
        print $tablePreviewAnswer;
        //return $tablePreviewAnswer;
    }

}
