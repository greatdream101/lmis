<?php

class Survey_qc extends MY_Controller {

        public function __construct() {
                parent::__construct();
                $this->load->model('addon/question_mod');
                $this->load->library('addon/QcSurvey_lib');
//                $this->configs = array('detail'=>'ta');
        }

        public function index() {
             $survey_list = $this->question_mod->get_survey_list();
             if(!empty($survey_list))
             {
                  // $url = array( 'url' => array('Survey_qc/get_questions'),
                  //      'attr_id'=>array('surveyID'),
                  //      'index_column'=>'id',
                  // );
                  $url = array( 'url' => array('survey_qc/qc_questions'),
                       'attr_id'=>array('surveyID'),
                       'index_column'=>'id',
                  );
                  $table = $this->mcl->table($survey_list, $url , 'datatable_local');
             }
             else
             {
                  $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
             }
             // $addon_data = $this->mdb->get_addonData('employmentIntranet');
             // $this->t['addonData'] = $addon_data;
             // vd::d($this->t);exit;
             $o = $this->load->view('survey/dashboardQC', array('t' => $this->t,'table'=>$table), true);
             print $o;
        }

        function get_series()
        {
                if (isset($_POST['branchID']))
                {
                     $this->load->model('addon/series_mod');
                     $o = $this->series_mod->get_branch_series($_POST['branchID']);
                     print json_encode($o);
                }
        }

        function qc_questions($surveyID)
        {
               $this->t['surveyID'] = $surveyID;
               print $this->load->view('survey/qc', array('t' => $this->t), true);
        }

        function get_questions()
        {
               $surveyID = $_POST['surveyID'];
               $survey_info = $this->question_mod->get_survey_info($surveyID);

               $branchID = $survey_info['branchID'];
               $seriesID = $survey_info['seriesID'];
               $surveyerID = $survey_info['userID'];

               $a = $this->question_mod->get_data($seriesID);

               // $answer_value = $this->question_mod->get_answer_value_jump($surveyerID, $branchID, $seriesID);
               $answer_value = $this->question_mod->get_answer_value_jump($surveyID);

               // $surveyerID = 21; // Fix Test user
               $qcHistory = $this->qcsurvey_lib->get_qcHistory($surveyerID,$seriesID,$branchID);

               print('<div class = "section-header">Branch : '.$survey_info['branch']." | Surveyor : ".$survey_info['fname']." ".$survey_info['lname'].'</div>');
               print('<div class = "section-header">'.$survey_info['series'].'</div>');
               print $this->load->view('survey/cs', array(
                   'a' => $a,
                   't_value' => $answer_value,
                   'isQCPage' => true,
                   'qcHistory' => $qcHistory,
                   'branchID' => $branchID,
                   'seriesID' => $seriesID,
                   'surveyerID' => $surveyerID,
                   't' => $this->t,
               ), true);
        }

        function frm_answer() {
                $ans = $this->mdb->get_addon_data('outbound_frm_answer');
                $this->t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                $this->t['addon_data']['jump'] = $ans['question_id'];
                $this->t['addon_data']['jump']['data'][99999] = array('question_id' => '99999', 'name' => $this->mcl->gl('end_survey'));
//                vd::d($ans['question_id']);
                print $this->load->view('survey/frm_answer', array('t' => $this->t), true);
        }

        function frm_question() {
                $seriesID = $_POST['seriesID'];
                $project_id = $_POST['project_id'];
                $question_type_id = $_POST['question_type_id'];
                $phase_id = $_POST['phase_id'];
                $t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                $t['addon_data']['section_id']['data'] = $this->question_mod->get_section($seriesID, $project_id, $question_type_id, $phase_id);

                print $this->load->view('survey/frm_question', array('t' => $t), true);
        }

        function frm_qcDialog() {
                //$seriesID = $_POST['seriesID'];
                //$project_id = $_POST['project_id'];
                //$question_type_id = $_POST['question_type_id'];
                //$phase_id = $_POST['phase_id'];
                //$t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                //$t['addon_data']['section_id']['data'] = $this->question_mod->get_section($seriesID, $project_id, $question_type_id, $phase_id);
                print $this->load->view('survey/frm_qcDialog', array(), true);
        }

        function add_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_question();
                }
        }

        function add_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_answer();
                }
        }

        function delete_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_answer();
                }
        }

        function delete_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_question();
                }
        }

        function save_position() {
                if (isset($_POST['q'])) {
                        $this->question_mod->save_position();
                }
        }

        function save_qc_status(){
                if (isset($_POST['user'])&&isset($_POST['series'])&&isset($_POST['branch'])&&isset($_POST['question'])&&isset($_POST['status'])) {
                       $this->question_mod->save_qc_status();
                }
        }

        function save_main_qc()
        {
                if (isset($_POST['surveyID'])&&isset($_POST['qcStatusID']))
                {
                     $userID = $this->session->userdata('userID');
                     $update_data = array(
                          'qcStatusID' => $_POST['qcStatusID'],
                          'updatedID' => $userID,
                          'updatedDate' => date("Y-m-d H:i:s"),
                     );
                     $result = $this->question_mod->update_qc_status($_POST['surveyID'],$update_data);
                     print $result;
                }
        }
}
