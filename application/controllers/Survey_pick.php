<?php

class Survey_pick extends MY_Controller {

         public function __construct() {
                parent::__construct();
                $this->load->model('addon/question_mod');
//                $this->configs = array('detail'=>'ta');
        }

        public function index() {
                $o = $this->load->view('survey/question_answer', array('t' => $this->t), true);
                print $o;
        }

        function get_questions() {
                if (isset($_POST['series_id'])) {
                        $series_id = $_POST['series_id'];
                        $project_id = $_POST['project_id'];
                        $question_type_id = $_POST['question_type_id'];
                        $phase_id = $_POST['phase_id'];


                        $a = $this->question_mod->get_data($project_id, $series_id, $question_type_id, $phase_id);

                        print $this->load->view('survey/qq', array('a' => $a), true);
                }
        }

        function frm_answer() {
                $ans = $this->mdb->get_addon_data('outbound_frm_answer');
                $this->t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                $this->t['addon_data']['jump'] = $ans['question_id'];
                $this->t['addon_data']['jump']['data'][99999] = array('question_id' => '99999', 'name' => $this->mcl->gl('end_survey'));
//                vd::d($ans['question_id']);
                print $this->load->view('survey/frm_answer', array('t' => $this->t), true);
        }

        function frm_question() {
                $series_id = $_POST['series_id'];
                $project_id = $_POST['project_id'];
                $question_type_id = $_POST['question_type_id'];
                $phase_id = $_POST['phase_id'];
                $t['edit_data'] = $this->question_mod->get_question(isset($_POST['question_id']) ? $_POST['question_id'] : 0);
                $t['addon_data']['section_id']['data'] = $this->question_mod->get_section($series_id, $project_id, $question_type_id, $phase_id);

                print $this->load->view('survey/frm_question', array('t' => $t), true);
        }

        function add_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_question();
                }
        }

        function add_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->add_answer();
                }
        }

        function delete_answer() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_answer();
                }
        }

        function delete_question() {
                if (isset($_POST['uri'])) {
                        $this->question_mod->delete_question();
                }
        }

        function save_position() {
                if (isset($_POST['q'])) {
                        $this->question_mod->save_position();
                }
        }

}
