<?php

    class Registerreport extends MY_Controller{

        public function __construct(){
            parent::__construct();

            $this->load->model('Registerreport_model');
        }

        public function index(){
            #index
        }

        public function registerreport(){

            $select = "DATE(created_date) AS Date, DATE_FORMAT(created_date,'%H:%i:%s') Registration_time, itemID,
                        CONCAT(fname,' ',lname) AS Name_Surname, mobile AS Mobile_number, VIN1 AS VIN, code AS Discount_code";
            $where = array();
            $table = 't_customer_register';

            $arrCustomerRegister = $this->Registerreport_model->get_report($select, $where, $table);

            $itemList = $this->db->SELECT('*')->FROM('items')->get()->result_array();

            // vd::d($itemList);
            // exit;
            foreach($arrCustomerRegister as $key => $value){

                foreach($itemList as $ikey => $ival){

                    $selectItem = explode(',', $arrCustomerRegister[$key]['itemID']);

                    if(in_array($ival['itemID'], $selectItem)){
                        $arrCustomerRegister[$key]["service_".$ival['itemID']] = 'Y';
                    }else{
                        $arrCustomerRegister[$key]["service_".$ival['itemID']] = 'N';
                    }

                }

                unset($arrCustomerRegister[$key]['itemID']);

            }

            if(!empty($arrCustomerRegister)){
                // $url = array( 'url' => array('ManPowerRequest/requestForm'),
                //     'attr_id'=>array('manPowerRequestID')
                // );
                    $table = $this->mcl->table($arrCustomerRegister, array() , 'datatable_local');
                }
                else{
                    $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
                }

            $this->load->view('report/registerreport', array('table'=>$table, 't' => $this->t));

        }

        public function dealerreport(){

            $select  = "DATE(register_date) AS Date, 'Website' AS Claim_channel, CONCAT(fname,' ',lname) AS Job_receipt_number, mobile AS Dealer_code, VIN1 AS VIN, code AS Discount_code, username AS dealer_name, ro_number AS ROnumber";
            $table   = 'v_dealerreport';
            $userid  = $this->ion_auth->get_userID();
            $groupid = $this->ion_auth->get_groupID();

            if(in_array($groupid, array('2'))){
                $where = array(
                    'staff_id' => $userid
                );
            }else{
                $where = array();
            }

            $arrCustomerRegister = $this->Registerreport_model->get_report($select, $where, $table);

            if(!empty($arrCustomerRegister)){
                // $url = array( 'url' => array('ManPowerRequest/requestForm'),
                //     'attr_id'=>array('manPowerRequestID')
                // );
                    $table = $this->mcl->table($arrCustomerRegister, array() , 'datatable_local');
                }
                else{
                    $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
                }

            $this->load->view('report/dealerreport', array('table'=>$table, 't' => $this->t));
        }

    }