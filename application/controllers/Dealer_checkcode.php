<?php
    Class Dealer_checkcode extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model("addon/Dealer_checkcode_model");
        }

        public function index(){
            $this->load->view('dealer/dealer_checkcode');
        }

        public function save_data(){
            $result = $this->Dealer_checkcode_model->save_data($_POST);
            print $result;
        }

        public function update_data(){
            $result = $this->Dealer_checkcode_model->update_data($_POST);
            print $result;
        }

        function checkcode(){
            $selectStr = "*";
            $where     = array("code"=>$_POST['code']);
            $result    = $this->Dealer_checkcode_model->dealer_checkcode($selectStr, $where);

            if(!empty($result) && isset($result)){
                print json_encode($result);
            }else{
                $result = 0;
                print $result;
            }
        }
    }

?>