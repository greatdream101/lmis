<?php

class Customer extends MY_Controller {

        function __construct() {
                parent::__construct();
                $this->load->model('addon/Customer_model');
                $this->load->model('addon/Question_mod');
                $this->load->library('addon/Scripts_lib');
        }

        function index() {

            $column   = "cusID, fname, lname, mobile, VIN1, code, username AS dealer_name, ro_number";
            $userid   = $this->ion_auth->get_userID();
            $groupid  = $this->ion_auth->get_groupID();

            if(in_array($groupid, array('2'))){
                $where = array(
                    'staff_id' => $userid
                );
            }else{
                $where = array();
            }

            $this->db->SELECT($column)->FROM('v_dealerreport');
            $this->db->WHERE($where);
            $arrCustomerList = $this->db->orderby('cusID', 'asc')->get()->result_array();

				foreach ($arrCustomerList as $tmp => $lst){//custom colum

                    //check ro number for disable input
                    if($lst['ro_number'] != ""){
                        $arrCustomerList[$tmp]['ro_number']      = "<div>";
                        // $arrCustomerList[$tmp]['ro_number'] 	 .= "<input type=text name=".$lst['cusID']." id=".$lst['cusID']." disabled='disabled' value='".$lst['ro_number']."' class='ronumber inline'>";
                        // $arrCustomerList[$tmp]['ro_number']      .= "<button id=".$lst['cusID']." class='btn-ronumber inline glyphicon glyphicon-edit'></button>";
                        $arrCustomerList[$tmp]['ro_number'] 	 .= "<a href='javascript:void(0)' id=".$lst['cusID']." class='edit_ronumber'>".$lst['ro_number']."</a>";
                        $arrCustomerList[$tmp]['ro_number']      .= "</div>";
                    }else{
                        $arrCustomerList[$tmp]['ro_number'] 	 = "<input type=text name=".$lst['cusID']." id=".$lst['cusID']." value='".$lst['ro_number']."' class='ronumber'>";
                    }

				}

				if(!empty($arrCustomerList)){
					// $url = array( 'url' => array('Customer/customerdetail'),
					// 	'attr_id'=>array('cusID')
					// );
						$table = $this->mcl->table($arrCustomerList, array() , 'datatable_local');
					}
					else{
						$table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
					}
					$this->load->view('customer/customerlist', array('table'=>$table, 't' => $this->t));
        }

        public function update_ro_number(){

            $cusID      = isset($_POST['cusID'])?$_POST['cusID']:'';
            $ro_number   = isset($_POST['ro_number'])?$_POST['ro_number']:'';

            $this->db->SET('ro_number', $ro_number);
            $this->db->WHERE('cusID', $cusID);
            $result = $this->db->UPDATE('t_customer_register');

            echo $result;
        }

}
