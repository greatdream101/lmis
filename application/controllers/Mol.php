<?
    class Mol extends MY_Controller{
        public function __construct(){
            parent::__construct();
        }

        public function index(){

            $this->db->SELECT('*')->FROM('t_management_coverpage')->WHERE('active', 1);
            $coverpage = $this->db->get()->result_array();

            if(!empty($coverpage)){
                $this->load->view('nlic/coverpage', array('coverpage'=>$coverpage));
            }else{
                $this->home();
            }
        }

        public function home(){
            $where    = array('active'=>1);
            $menu     = $this->mdb->get("t_management_menu", $where, "*", "", "");
            $eservice = $this->mdb->get("t_management_eservice", $where, "*", "", "");
            $document = $this->mdb->get("t_management_document", $where, "*", "", "");

            $activity = array(
                                'menu'=>$menu
                                ,'eservice'=>$eservice
                                ,'document'=>$document
                            );

            $this->load->view('nlic/nlic', $activity);
            $this->stat_counter();
        }

        public function dashboard(){
            $this->load->view('nlic/dashboard');
        }

        public function data(){

            $data = array();

            $array_1 = array('name'=>'data1', 'data'=>array(49.9, 71.5, 106.4));
            $array_2 = array('name'=>'data2', 'data'=>array(9, 131, 62));
            $array_3 = array('name'=>'data3', 'data'=>array(3, 7, 16.8));

            array_push($data, $array_1);
            array_push($data, $array_2);
            array_push($data, $array_3);
            
            print json_encode($data);
        }

        public function stat_counter(){
            $this->load->view('nlic/stat_counter');
        }

    }
?>