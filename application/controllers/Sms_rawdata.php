<?php

class Sms_rawdata extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('addon/Report_lib');
    }

    public function index() {
        $this->t['edit_data']['dateStart'] = date('Y-m-d');
        $this->t['edit_data']['dateEnd'] = date('Y-m-d');

        $this->load->view('sms/report_rawdata', array(
            't' => $this->t
        ));
    }

    public function get_SearchSMS() {

        $dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:'';
        $dateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:'';
        
        $a = $this->report_lib->get_rawDataSMS($dateStart,$dateEnd);
        print $a;
    }
}
