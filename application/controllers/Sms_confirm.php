<?php

class Sms_confirm extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3200); //600 seconds = 10 minutes
        
        $this->load->model('addon/flag_model');
        $this->load->model('addon/customer_model');
        $this->load->model('addon/sms_model');
    }
    
    function mysort1 ($x, $y) {
        return ($x['sourceID'] > $y['sourceID']);
    }
    
    public function index() {
        
        $dataSource = $this->sms_model->get_dataSource();
        
        if (!empty($_POST['sourceID'])){
            
            $where = array( 'sourceID' => $_POST['sourceID']);
            $dataSource = $this->sms_model->get_dataSource($where);
            $arr = explode ( " ", $dataSource[0]['createdDate'] );
            $where = array( 'createdDate' => $arr[0] );
            $dataSource = $this->sms_model->get_dataSource($where);
            
            $dataType = $this->flag_model->get_dataType();
            
            foreach ($dataSource as $key => $row) {
                $volume[$key]  = $row['sourceID'];
            }
            
            array_multisort($volume, SORT_ASC, $dataSource);
            
            foreach ($dataType as $k => $v){
                $i = 0;
                //if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                    $conditionArray = array("dataTypeID" => $dataSource[$k]['dataTypeID'], "sourceID" => $dataSource[$k]['sourceID'] );
                    $sumFlag = $this->flag_model->get_sumFlag($conditionArray);
                    $dataType[$k]["totle"] = $sumFlag["flagSum"];
                /*}else{
                    $dataType[$k]["totle"] = 0;
                }*/
                
                $flag[$v["nameEN"]] = $this->flag_model->get_flagData();
                
                foreach ($flag[$v["nameEN"]] as $kk => $vv){
                    //if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                        $conditionArray = array("flagID" => $vv["flagID"], "dataTypeID" => $dataSource[$k]['dataTypeID'], "sourceID" => $dataSource[$k]['sourceID']);
                        $sumFlag = $this->flag_model->get_sumFlag($conditionArray);
                        $flag[$v["nameEN"]][$kk]["flagSum"] = $sumFlag["flagSum"];
                    /*}else{
                        $flag[$v["nameEN"]][$kk]["flagSum"] = 0;
                    }*/
                    $i = $kk;
                }
                
                $i++;
                $flag[$v["nameEN"]][$i]["nameEN"] = "In Que";
                //if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                    $select = "count(id) AS sumSMS";
                    $table = "v_cusForDashboardSMS";
                    $conditionArray = array("dataTypeID" => $dataSource[$k]['dataTypeID'], "sourceID" => $dataSource[$k]['sourceID']);
                    $str = "id IN ( SELECT cusID FROM tempSMS)";
                    $sumFlag = $this->flag_model->get_sumStatusSMS($select, $table, $conditionArray, $str);
                    $flag[$v["nameEN"]][$i]["flagSum"] = $sumFlag["sumSMS"];
                /*}else{
                    $flag[$v["nameEN"]][$i]["flagSum"] = 0;
                }*/
                
                
                $i++;
                $flag[$v["nameEN"]][$i]["nameEN"] = "SMS Completed";
                //if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                    $data = 1;
                    $select = "count(cusID) AS sumSMS";
                    $table = "t_customer";
                    $conditionArray = array("sms_status" => "1");
                    $str = "cusID IN ( SELECT id FROM v_cusForDashboardSMS WHERE dataTypeID = ".$dataSource[$k]['dataTypeID']." AND sourceID = ".$dataSource[$k]['sourceID']." )";
                    $sumFlag = $this->flag_model->get_sumStatusSMS($select, $table, $conditionArray, $str);
                    $flag[$v["nameEN"]][$i]["flagSum"] = $sumFlag["sumSMS"];
                /*}else{
                    $data = 0;
                    $flag[$v["nameEN"]][$i]["flagSum"] = 0;
                }*/
                
            }
            
            $o = $this->load->view("sms/confirm", array(
                "t" => $this->t,
                "flag" => $flag,
                "dataType" => $dataType,
                "data" => $data
            ),true);
            
            print $o;
            
        }else{
            $dataType = $this->flag_model->get_dataType();
            
            foreach ($dataSource as $key => $row) {
                $volume[$key]  = $row['sourceID'];
            }
            
            array_multisort($volume, SORT_ASC, $dataSource);
            
            foreach ($dataType as $k => $v){
                $i = 0;                
                if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                    $conditionArray = array("dataTypeID" => $dataSource[$k]['dataTypeID'], "sourceID" => $dataSource[$k]['sourceID'] );
                    $sumFlag = $this->flag_model->get_sumFlag($conditionArray);
                    $dataType[$k]["totle"] = $sumFlag["flagSum"];
                }else{
                    $dataType[$k]["totle"] = 0;
                }
                
                $flag[$v["nameEN"]] = $this->flag_model->get_flagData();
                
                foreach ($flag[$v["nameEN"]] as $kk => $vv){
                    if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                        $conditionArray = array("flagID" => $vv["flagID"], "dataTypeID" => $dataSource[$k]['dataTypeID'], "sourceID" => $dataSource[$k]['sourceID']);
                        $sumFlag = $this->flag_model->get_sumFlag($conditionArray);
                        $flag[$v["nameEN"]][$kk]["flagSum"] = $sumFlag["flagSum"];
                    }else{
                        $flag[$v["nameEN"]][$kk]["flagSum"] = 0;
                    }
                    $i = $kk;
                }
                
                $i++;
                $flag[$v["nameEN"]][$i]["nameEN"] = "In Que";
                if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                    $select = "count(id) AS sumSMS";
                    $table = "v_cusForDashboardSMS";
                    $conditionArray = array("dataTypeID" => $dataSource[$k]['dataTypeID'], "sourceID" => $dataSource[$k]['sourceID']);
                    $str = "id IN ( SELECT cusID FROM tempSMS)";
                    $sumFlag = $this->flag_model->get_sumStatusSMS($select, $table, $conditionArray, $str);
                    $flag[$v["nameEN"]][$i]["flagSum"] = $sumFlag["sumSMS"];
                }else{
                    $flag[$v["nameEN"]][$i]["flagSum"] = 0;
                }
                
                
                $i++;
                $flag[$v["nameEN"]][$i]["nameEN"] = "SMS Completed";
                if( date_format( date_create($dataSource[$k]['createdDate']), 'W') == date( 'W') ){
                    $data = 1;
                    $select = "count(cusID) AS sumSMS";
                    $table = "t_customer";
                    $conditionArray = array("sms_status" => "1");
                    $str = "cusID IN ( SELECT id FROM v_cusForDashboardSMS WHERE dataTypeID = ".$dataSource[$k]['dataTypeID']." AND sourceID = ".$dataSource[$k]['sourceID']." )";
                    $sumFlag = $this->flag_model->get_sumStatusSMS($select, $table, $conditionArray, $str);
                    $flag[$v["nameEN"]][$i]["flagSum"] = $sumFlag["sumSMS"];
                }else{
                    $data = 0;
                    $flag[$v["nameEN"]][$i]["flagSum"] = 0;
                }
                
            }

            $this->load->view("sms/confirm", array(
                "t" => $this->t,
                "flag" => $flag,
                "dataType" => $dataType,
                "data" => $data
            ));
        }
    }
    
    function search() {
        $dataSource = $this->sms_model->get_dataSource();
        //$dataSource['createdDate'] = "2018-07-23 07:39:09";
        
        if( date_format( date_create($dataSource[$_POST["dataType"]]['createdDate']), 'W') == date( 'W') ){
            
            if ($_POST["dataType"] == "0"){
                $dataType = 1;
                $key = 0;
            }
            
            $select = array('id', 'insuredName', /*'mobilePhone',*/ 'sourceName', 'nameEN', 'contentSMS');
            $conditionArray = array('flagID' => '1', 'dataTypeID' => $dataType);
            $str = ' sourceID = '.$dataSource[$key]['sourceID'].' AND id NOT IN ( SELECT cusID FROM tempSMS)';
            $cusData = $this->customer_model->get_CustomerSMS($select, $conditionArray, $str);
            
            if(!empty($cusData)) {
                
                foreach ($cusData as $k => $v){
                    array_unshift ( $cusData[$k], 'ck_'.$v['id'] );
                    $cusData[$k]['contentSMS'] = str_replace('{name}', $v['insuredName'], $cusData[$k]['contentSMS']);
                    //$cusData[$k]['contentSMS'] = str_replace('{link}', 'https://www.google.co.th/', $cusData[$k]['contentSMS']);
                }
                
                $tableCusData = $this->mcl->table($cusData);
                
                $tableCusData = str_replace('<th>0', '<th><input type="checkbox" name="checkAll" id="checkAll">', $tableCusData);
                foreach ($cusData as $k => $v){
                    $tableCusData = str_replace('<td>ck_'.$v['id'].' ', '<td><input type="checkbox" class="checkbox" name="chk" value="'.$v['id'].'">', $tableCusData);
                }
                
            }else {
                $tableCusData = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
            }
        }
        
        print $tableCusData;

    }
    
    function countCusID(){
        $o = '';
        $countCus = $_POST['countCus'];
        $hours = ($countCus * 3);
        $time = "sec";
        if($hours > 60){
            $hours = $hours / 60 ;
            $time = "min";
            if($hours > 60){
                $hours = $hours / 60 ;
                $time = "hours";
            }
        }
        
        $o .='
            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            	<div class="modal-dialog" style="width: 70% !important; margin-right: 10% !important;">
            		<div class="modal-content">
            			<div class="modal-header">
            				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            				<h4 class="modal-title">'.$this->mcl->gl('sms_info').'</h4>
            			</div>
            			<div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                            	   <h3 class="text-success counter" style="text-align: center;"> '.$this->mcl->gl('sum_sms').' </h3>
                                    <div class="widget-simple-chart text-right card-box" style="text-align: center; background-color: #dedede;">
                                        <h3 class="text-success counter"> '.$countCus.' </h3>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                            	   <h3 class="text-success counter" style="text-align: center;"> '.$this->mcl->gl('sum_hours').' </h3>
                                    <div class="widget-simple-chart text-right card-box" style="text-align: center; background-color: #dedede;">
                                        <h3 class="text-success counter"> '.round( $hours ,2 ).' '.$this->mcl->gl($time).' </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">'.$this->mcl->gl('close').'</button>
                            <button type="button" class="btn btn-default preview_confirm" id="confirm_save">'.$this->mcl->gl('confirm_save').'</button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal" id="click-modal"></button>
            </div>
            
            <!-- Modal-Effect -->
            <script src="../assets/plugins/custombox/dist/custombox.min.js"></script>
            <script src="../assets/plugins/custombox/dist/legacy.min.js"></script>
            
            <script>
                $(document).ready(function () {
                    $("#con-close-modal").modal("show");
                    $("#confirm_save").off("click").on("click", function (e) {
                        e.preventDefault();
                        //var data = $("#approveSMS").serialize();
                        var url = get_base_url() + "sms_confirm/saveData";
                        $.ajax({
                            type: "POST",
                            url: url,
                            cache: false,
                            async: false,
                            data: { textData: $("#approveSMS").serialize() },
                            beforeSend: function () {
                            },
                            success: function (data) {
                                location.reload();
                            }
                        });
                	});
                });
            </script>';
        
        print $o;
    }
    
    function saveData() {
        $arr1 = explode("&", $_POST['textData']);
        $chk = array();
        foreach ($arr1 as $k => $v){
            $arr2 = explode("=", $v);
            if ($arr2[0] == "chk"){
                $chk[] = $arr2[1];
            }
            
        }
        if( !empty($chk) ) {
            foreach ($chk as $k => $v){
                $data['cusID'] = $v;
                $data['createdID'] = $this->ion_auth->get_userID();
                $this->customer_model->save_tempSMS($data);
            }
        }
        print 1;
    }
}
