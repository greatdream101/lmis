<?php

class Survey_scripts extends MY_Controller {

         public function __construct() {
                parent::__construct();
                $this->load->model('addon/Series_mod');
                $this->load->library('addon/Scripts_lib');
        }

        public function index() {
             $seriesList = $this->Series_mod->get_series_list();
             // vd::d($seriesList);exit;
             if(!empty($seriesList))
             {
                  $url = array( 'url' => array('survey_scripts/pick_series'),
                       'attr_id'=>array('seriesID'),
                       'index_column'=>'id',
                  );
                  $table = $this->mcl->table($seriesList, $url , 'datatable_local');
             }
             else
             {
                  $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
             }
             $this->t['widget_header'] = 'Survey_list';
             $o = $this->load->view('survey/scripts', array('t' => $this->t,'table'=>$table), true);
             print $o;
        }

        function pick_series($seriesID)
        {
             $this->load->model('addon/Scripts_mod');
             $scriptsList = $this->Scripts_mod->get_scripts_list($seriesID);

             if(!empty($scriptsList))
             {
                  $url = array();
                  $table = $this->scripts_lib->genScriptsTable($scriptsList, $url, 'datatable_local runNumber no-filter full-width','Edit','scriptsID');
             }
             else
             {
                  $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
             }

             // print $table;
             $this->t['widget_header'] = 'Scripts_list';
             $o = $this->load->view('survey/scripts', array('t' => $this->t,'table'=>$table), true);
             print $o;
        }

        function pull_scripts($surveyID)
        {
             $this->load->model('addon/Scripts_mod');
             $scripts_list = $this->Scripts_mod->get_scripts_list($surveyID);

             if(!empty($scripts_list))
             {
                  $url = array();
                  $table = $this->ticket_lib->genScriptsTable($scripts_list, $url, 'datatable_local no-filter full-width','detail');
                  // $table = $this->mcl->table($scripts_list, $url , 'datatable_local');
             }
             else
             {
                  $table = "<table><tr><td>".$this->mcl->gl('zero_record')."</td></tr></table>";
             }

             print $table;
        }

        function getDAta(){
            $arr = array('scriptsText'=>'scriptsText','scriptsTextEn'=>'scriptsTextEn');
            print json_encode($arr);
        }
}
